<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFixedtoureTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fixed_tours', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('hotel_id');
            $table->foreign('hotel_id')->references('id')->on('hotels')->onDelete('cascade');
            $table->unsignedBigInteger('category_id');
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
            $table->dateTime('from');
            $table->dateTime('to');
            $table->double('total_cost');
//            $table->string('main_image');
            $table->boolean('active');
            $table->string('days_nights_counts');
            $table->timestamps();
            $table->softDeletes();
        });


        Schema::create('fixed_tours_translations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('fixed_tour_id')->unsigned();
            $table->foreign('fixed_tour_id')->references('id')->on('fixed_tours')->onDelete('cascade');

            $table->string('name')->nullable();
            $table->text('description')->nullable();
            $table->string('locale')->index();

            $table->unique(['fixed_tour_id', 'locale']);

            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fixed_tours');
    }
}
