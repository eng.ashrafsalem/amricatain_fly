<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFrontContactUsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('front_contact_us', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('map_lang');
            $table->unsignedBigInteger('map_lat');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('front_contact_us_translations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('front_contact_us_id')->unsigned();
            $table->foreign('front_contact_us_id')->references('id')->on('front_contact_us')->onDelete('cascade');

            $table->string('title')->nullable();
            $table->string('value')->nullable();
            $table->string('locale')->index();

            $table->unique(['front_contact_us_id', 'locale']);

            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('front_contact_us');
    }
}
