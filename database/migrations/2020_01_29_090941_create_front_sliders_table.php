<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFrontSlidersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('front_sliders', function (Blueprint $table) {
            $table->bigIncrements('id');
//            $table->string('image_url');
//            $table->string('title');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('front_sliders_translations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('front_slider_id')->unsigned();
            $table->foreign('front_slider_id')->references('id')->on('front_sliders')->onDelete('cascade');

            $table->string('title')->nullable();
            $table->string('locale')->index();

            $table->unique(['front_slider_id', 'locale']);

            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('front_sliders');
    }
}
