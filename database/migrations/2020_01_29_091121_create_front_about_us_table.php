<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFrontAboutUsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('front_about_us', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
        });
        Schema::create('front_about_us_translations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('aboutus_id')->unsigned();
            $table->foreign('aboutus_id')->references('id')->on('front_about_us')->onDelete('cascade');

            $table->string('contents')->nullable();
            $table->string('locale')->index();

            $table->unique(['aboutus_id', 'locale']);

            $table->timestamps();
            $table->softDeletes();

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('front_about_us');
    }
}
