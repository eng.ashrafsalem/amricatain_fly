<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFrontNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('front_news', function (Blueprint $table) {
            $table->bigIncrements('id');
//            $table->string('image_url');
            $table->dateTime('news_date');
            $table->timestamps();
            $table->softDeletes();
        });
        Schema::create('front_news_translations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('front_news_id')->unsigned();
            $table->foreign('front_news_id')->references('id')->on('front_news')->onDelete('cascade');

            $table->string('title')->nullable();
            $table->text('description')->nullable();
            $table->string('locale')->index();

            $table->unique(['front_news_id', 'locale']);

            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('front_news');
    }
}
