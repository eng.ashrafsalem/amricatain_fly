<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFrontServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('front_services', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->softDeletes();
        });
        Schema::create('front_services_translations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('front_service_id')->unsigned();
            $table->foreign('front_service_id')->references('id')->on('front_services')->onDelete('cascade');

            $table->string('contents')->nullable();
            $table->string('locale')->index();

            $table->unique(['front_service_id', 'locale']);

            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('front_services');
    }
}
