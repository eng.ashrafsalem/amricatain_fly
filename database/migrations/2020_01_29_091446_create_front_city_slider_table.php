<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFrontCitySliderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('front_city_slider', function (Blueprint $table) {
            $table->bigIncrements('id');
//            $table->string('image_url');
            $table->timestamps();
            $table->softDeletes();
        });
        Schema::create('front_city_translations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('city_slider_id')->unsigned();
            $table->foreign('city_slider_id')->references('id')->on('front_city_slider')->onDelete('cascade');

            $table->string('title')->nullable();
            $table->string('locale')->index();

            $table->unique(['city_slider_id', 'locale']);

            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('front_city_slider');
    }
}
