<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFixedtourePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fixed_tour_payments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('fixed_tour_booking_id');
            $table->foreign('fixed_tour_booking_id')->references('id')->on('fixed_tour_bookings')->onDelete('cascade');
            $table->double('amount');
            $table->dateTime('payment_date');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fixed_tour_payments');
    }
}
