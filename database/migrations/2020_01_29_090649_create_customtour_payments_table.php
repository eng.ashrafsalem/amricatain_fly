<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomtourPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('custom_tour_payments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('custom_tour_booking_id');
            $table->foreign('custom_tour_booking_id')->references('id')->on('custom_tour_bookings')->onDelete('cascade');
            $table->double('amount');
            $table->dateTime('payment_date');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('custom_tour_payments');
    }
}
