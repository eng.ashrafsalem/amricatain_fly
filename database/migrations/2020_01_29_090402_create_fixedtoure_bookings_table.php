<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFixedtoureBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fixed_tour_bookings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('customer_id');
            $table->foreign('customer_id')->references('id')->on('customers')->onDelete('cascade');
            $table->unsignedBigInteger('fixed_tour_id');
            $table->foreign('fixed_tour_id')->references('id')->on('fixed_tours')->onDelete('cascade');
            $table->dateTime('from');
            $table->dateTime('to');
            $table->double('total_cost');
            $table->smallInteger('adult_count');
            $table->smallInteger('child_count');
            $table->smallInteger('baby_count');
            $table->enum('status', ['pending', 'confirmed', 'canceled'])->default('pending');
            $table->unsignedBigInteger('currency_exchange_id')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fixed_tour_bookings');
    }
}
