<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFrontNavbarTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('front_navbar', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->softDeletes();
        });
        Schema::create('front_navbar_translations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('front_navbar_id')->unsigned();
            $table->foreign('front_navbar_id')->references('id')->on('front_navbar')->onDelete('cascade');

            $table->string('contents')->nullable();
            $table->string('locale')->index();

            $table->unique(['front_navbar_id', 'locale']);

            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('front_navbar');
    }
}
