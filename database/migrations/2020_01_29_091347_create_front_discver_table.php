<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFrontDiscverTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('front_discover', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->softDeletes();
        });
        Schema::create('front_discover_translations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('discover_id')->unsigned();
            $table->foreign('discover_id')->references('id')->on('front_discover')->onDelete('cascade');

            $table->string('title')->nullable();
            $table->string('description')->nullable();
            $table->string('locale')->index();

            $table->unique(['discover_id', 'locale']);

            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('front_discover');
    }
}
