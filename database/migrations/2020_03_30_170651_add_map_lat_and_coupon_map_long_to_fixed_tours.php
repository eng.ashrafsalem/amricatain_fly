<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMapLatAndCouponMapLongToFixedTours extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fixed_tours', function (Blueprint $table) {
            $table->double('map_lat', 8, 4)->nullable();
            $table->double('map_long', 8, 4)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fixed_tours', function (Blueprint $table) {
            //
        });
    }
}
