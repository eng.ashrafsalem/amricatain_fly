$(window).scroll(function() {
    var scrollval = $(this).scrollTop();
    $("#head").css('transform', 'translate(0px,-' + scrollval / 3 + '%)');
    $("#girl").css('transform', 'translate(0px,' + scrollval / 15 + '%)');
    // $("#pyramid").css('transform', 'translate(0px,-'+scrollval/3+'%)');
});



$(document).ready(function() {



    var owl = $("#owl-demo");

    owl.owlCarousel({
        autoplay: true,
        loop: true,
        items: 4,
        ltr: true,
        nav: true,
        dots: false,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
                margin: 30,
                stagePadding: 6,

            },
            481: {
                items: 2,
                margin: 30,
                stagePadding: 6,
            },
            768: {
                items: 2,
                margin: 20,
            },
            992: {
                items: 3,
                margin: 20,
                width: 240,
            }
        }
        // itemsMobile disabled - inherit from itemsTablet option
    });

    var owl2 = $("#owl-demo1");

    owl2.owlCarousel({
        autoplay: true,
        loop: true,
        items: 5,
        ltr: true,
        center: true,
        nav: true,
        dots: false,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
                margin: 20
                // stagePadding: 10,

            },
            481: {
                items: 1,
                margin: 10
                // stagePadding: 10,

            },
            768: {
                items: 2,
                margin: 10
                // stagePadding: 10,

            },
            992: {
                items: 4,
                margin: 10
                // width: 240,
            }

        }
        // itemsMobile disabled - inherit from itemsTablet option
    });
    var owl3 = $("#owl-demo2");

    owl3.owlCarousel({
        autoplay: true,
        loop: true,
        items: 5,
        ltr: true,
        center: true,
        nav: true,
        dots: false,
        autoplayTimeout: 4000,
        navSpeed: 2000,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
                margin: 20
                // stagePadding: 10,

            },
            481: {
                items: 1,
                margin: 10
                // stagePadding: 10,

            },
            768: {
                items: 1,
                margin: 10
                // stagePadding: 10,

            },
            992: {
                items: 1,
                margin: 10
                // width: 240,
            }
        }
        // itemsMobile disabled - inherit from itemsTablet option
    });
    var owl4 = $("#owl-demo3");

    owl4.owlCarousel({
        autoplay: true,
        loop: true,
        items: 5,
        ltr: true,
        center: true,
        nav: false,
        dots: true,
        autoplayTimeout: 4000,
        navSpeed: 2000,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
                margin: 20
                // stagePadding: 10,

            },
            481: {
                items: 1,
                margin: 10
                // stagePadding: 10,

            },
            768: {
                items: 1,
                margin: 10
                // stagePadding: 10,

            },
            992: {
                items: 1,
                margin: 10
                // width: 240,
            }
        }
        // itemsMobile disabled - inherit from itemsTablet option
    });
    var owl5 = $("#owl-demo5");

    owl5.owlCarousel({
        autoplay: true,
        loop: true,
        items: 5,
        ltr: true,
        center: true,
        nav: false,
        dots: false,
        autoplayTimeout: 4000,
        navSpeed: 15000,
        animateOut: 'slideOutUp',
        animateIn: 'slideInUp',
        touchDrag: false,
        mouseDrag: false,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
                margin: 20
                // stagePadding: 10,

            },
            481: {
                items: 1,
                margin: 10
                // stagePadding: 10,

            },
            768: {
                items: 1,
                margin: 10
                // stagePadding: 10,

            },
            992: {
                items: 1,
                margin: 10
                // width: 240,
            }
        }
        // itemsMobile disabled - inherit from itemsTablet option
    });

    var owl6 = $("#owl-demo6");

    owl6.owlCarousel({
        autoplay: true,
        loop: true,
        items: 5,
        ltr: true,
        center: true,
        nav: false,
        dots: true,
        autoplayTimeout: 4000,
        navSpeed: 2000,
        touchDrag: true,
        mouseDrag: true,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
                margin: 20
                // stagePadding: 10,

            },
            481: {
                items: 1,
                margin: 10
                // stagePadding: 10,

            },
            768: {
                items: 1,
                margin: 10
                // stagePadding: 10,

            },
            992: {
                items: 1,
                margin: 10
                // width: 240,
            }
        }
        // itemsMobile disabled - inherit from itemsTablet option
    });

    flag = 1;
    $(".next").click(function() {
        if (flag == 0) {
            $(".side1").css("z-index", "5");
            $(".side2").css("z-index", "4");
            $(".side3").css("z-index", "5");
            $(".side1").css("transform", "translateX(0px) scale(1.2)");
            $(".side2").css("transform", "translateX(-220px) scale(1)");
            $(".side3").css("transform", "translateX(220px)");
            $(".side1").css("opacity", "1");
            $(".side3").css("opacity", "0.8");
            flag = 1;
        } else if (flag == 1) {
            $(".side3").css("z-index", "4");
            $(".side2").css("z-index", "5");
            $(".side1").css("z-index", "5");
            $(".side3").css("transform", "translateX(-220px) ");
            $(".side1").css("transform", "translateX(220px) scale(1)");
            $(".side2").css("transform", "translateX(0px) scale(1.2)");
            $(".side2").css("opacity", "1");
            $(".side1").css("opacity", "0.8");

            flag = 2;
        } else if (flag == 2) {
            $(".side2").css("z-index", "5");
            $(".side3").css("z-index", "5");
            $(".side1").css("z-index", "4");
            $(".side2").css("transform", "translateX(220px)");
            $(".side3").css("transform", "translateX(0px) scale(1.2)");
            $(".side3").css("opacity", "1");
            $(".side2").css("opacity", "0.8");
            $(".side1").css("transform", "translateX(-220px) scale(1)");
            flag = 0;
        }
    });
    $(".prev").click(function() {
        if (flag == 0) {
            $(".side1").css("z-index", "5");
            $(".side2").css("z-index", "4");
            $(".side3").css("z-index", "5");
            $(".side1").css("transform", "translateX(0px) scale(1.2)");
            $(".side2").css("transform", "translateX(-220px) scale(1)");
            $(".side3").css("transform", "translateX(220px) ");
            $(".side1").css("opacity", "1");
            $(".side2").css("opacity", "0.8");
            flag = 1;
        } else if (flag == 1) {
            $(".side3").css("z-index", "4");
            $(".side2").css("z-index", "5");
            $(".side1").css("z-index", "5");
            $(".side3").css("transform", "translateX(0px) scale(1.2)");
            $(".side1").css("transform", "translateX(-220px) scale(1)");
            $(".side2").css("transform", "translateX(220px)");
            $(".side3").css("opacity", "1");
            $(".side1").css("opacity", "0.8");

            flag = 2;
        } else if (flag == 2) {
            $(".side2").css("z-index", "5");
            $(".side3").css("z-index", "5");
            $(".side1").css("z-index", "4");
            $(".side2").css("transform", "translateX(0px) scale(1.2)");
            $(".side3").css("transform", "translateX(-220px) scale(1)");
            $(".side3").css("opacity", "0.8");
            $(".side2").css("opacity", "1");
            $(".side1").css("transform", "translateX(220px)");
            flag = 0;
        }
    });
    $(".timer").countTo();

});
