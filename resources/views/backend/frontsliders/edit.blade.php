@extends('backend.layouts.app')

@section('title', __('labels.backend.frontsliders.management') . ' | ' . __('labels.backend.frontsliders.edit'))

@push('after-styles')

    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/trix/1.2.1/trix.css">
@endpush

@push('after-scripts')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/trix/1.2.1/trix.js">
    </script>
@endpush


@section('content')
    {{ html()->form('POST', $data->url->update)->class('form-horizontal')->acceptsFiles()->open() }}

    @method('PUT')

    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        @lang('labels.backend.frontsliders.management')
                        <small class="text-muted">@lang('labels.backend.frontsliders.edit')</small>
                    </h4>
                </div><!--col-->
            </div><!--row-->

            <hr>

            <div class="row mt-4 mb-4">
                <div class="col">

                    <div class="form-group row">
                        {{ html()->label(__('labels.backend.frontsliders.title'))->class('col-md-2 form-control-label')->for('title') }}
                        <div class="col-md-10">

                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="arabic-tab" data-toggle="tab" href="#arabic" role="tab" aria-controls="arabic" aria-selected="true">
                                        {{ html()->label(__('labels.backend.frontsliders.ar_title')) }}
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="english-tab" data-toggle="tab" href="#english" role="tab" aria-controls="english" aria-selected="false">
                                        {{ html()->label(__('labels.backend.frontsliders.en_title')) }}
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="frensh-tab" data-toggle="tab" href="#frensh" role="tab" aria-controls="frnsh" aria-selected="false">
                                        {{ html()->label(__('labels.backend.frontsliders.fr_title')) }}
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="italy-tab" data-toggle="tab" href="#italian" role="tab" aria-controls="italian" aria-selected="false">
                                        {{ html()->label(__('labels.backend.frontsliders.it_title')) }}
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="spanish-tab" data-toggle="tab" href="#spanish" role="tab" aria-controls="italian" aria-selected="false">
                                        {{ html()->label(__('labels.backend.frontsliders.es_title')) }}
                                    </a>
                                </li>
                            </ul>
                            <div class="tab-content" id="myTabContent">
                                <div class="tab-pane fade  show active" id="arabic" role="tabpanel" aria-labelledby="arabic-tab">
                                    <input id="ar[title]" type="hidden" name="ar[title]" value="{{ $data->getTranslation('ar')["title"] }}">
                                    <trix-editor input="ar[title]"></trix-editor>
                                </div>
                                <div class="tab-pane fade" id="english" role="tabpanel" aria-labelledby="english-tab">
                                    <input id="en[title]" type="hidden" name="en[title]" value="{{ $data->getTranslation('en')["title"] }}">
                                    <trix-editor input="en[title]"></trix-editor>
                                </div>
                                <div class="tab-pane fade" id="frensh" role="tabpanel" aria-labelledby="frensh-tab">
                                    <input id="fr[title]" type="hidden" name="fr[title]" value="{{ $data->getTranslation('fr')["title"] }}">
                                    <trix-editor input="fr[title]"></trix-editor>
                                </div>
                                <div class="tab-pane fade" id="italian" role="tabpanel" aria-labelledby="italian-tab">
                                    <input id="it[title]" type="hidden" name="it[title]" value="{{ $data->getTranslation('it')["title"] }}">
                                    <trix-editor input="it[title]"></trix-editor>
                                </div>
                                <div class="tab-pane fade" id="spanish" role="tabpanel" aria-labelledby="spanish-tab">
                                    <input id="es[title]" type="hidden" name="es[title]" value="{{ $data->getTranslation('es')["title"] }}">
                                    <trix-editor input="es[title]"></trix-editor>
                                </div>
                            </div>

                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                        {{ html()->label(__('labels.backend.frontsliders.main_image'))->class('col-md-2 form-control-label')->for('image_url') }}

                        <div class="col-md-10">
                            {{ html()->file('image_url')
                                ->class('form-control') }}
                        </div><!--col-->
                        @if(!empty(mainImage($data->id, \App\Models\FrontSliders\FrontSlider::class)))
                            <img src="{{ asset(str_replace("public/", "", mainImage($data->id,  \App\Models\FrontSliders\FrontSlider::class))) }}" class="img" alt="">
                        @endif
                    </div><!--form-group-->


                </div><!--col-->
            </div><!--row-->
        </div><!--card-body-->

        <div class="card-footer clearfix">
            <div class="row">
                <div class="col">
                    {{ form_cancel(route('admin.frontsliders.index'), __('buttons.general.cancel')) }}
                </div><!--col-->

                <div class="col text-right">
                    {{ form_submit(__('buttons.general.crud.edit')) }}
                </div><!--col-->
            </div><!--row-->
        </div><!--card-footer-->
    </div><!--card-->
    {{ html()->form()->close() }}
@endsection
