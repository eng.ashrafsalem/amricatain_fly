@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('labels.backend.users.management'))

@section('content')
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        {{ __('labels.backend.users.management') }}
                    </h4>
                </div><!--col-->

                <div class="col-sm-7">
                    <div class="btn-toolbar float-right" role="toolbar" aria-label="@lang('labels.general.toolbar_btn_groups')">
                        <a href="{{ route('admin.users.create') }}" class="btn btn-success ml-1" data-toggle="tooltip"
                           title="@lang('labels.general.create_new')"><i class="fas fa-plus-circle"></i></a>
                    </div><!--btn-toolbar-->
                </div><!--col-->
            </div><!--row-->

            <div class="row mt-4">
                <div class="col">
                    <div class="table-responsive">
                        <table class="table" id="result-table">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>@lang('labels.backend.users.image_url')</th>
                                <th>@lang('labels.backend.users.first_name')</th>
                                <th>@lang('labels.backend.users.last_name')</th>
                                <th>@lang('labels.backend.users.email')</th>
                                <th>@lang('labels.backend.users.phone')</th>
                                <th>@lang('labels.backend.users.address')</th>
                                <th>@lang('labels.general.actions')</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div><!--col-->
            </div><!--row-->
        </div><!--card-body-->
    </div><!--card-->
@endsection

@push('after-scripts')
    <script>
        $dataTable('{{ route('admin.users.index') }}', [
            {data: 'DT_RowIndex', name: 'id'},
            {data: 'image.image_url', name: 'image.image_url', render: function (data) {
                    if(data)

                        return '<img width="40px" src="'+ data + '" class="user-profile-image">'
                    return '{{__('labels.backend.none')}}'
                }},
            {data: 'first_name', name: 'first_name'},
            {data: 'last_name', name: 'last_name'},
            {data: 'email', name: 'email'},
            {data: 'phone', name: 'phone'},
            {data: 'address', name: 'address'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ]);
    </script>
@endpush
