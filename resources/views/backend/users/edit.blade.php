@extends('backend.layouts.app')

@section('title', __('labels.backend.users.management') . ' | ' . __('labels.backend.users.edit'))


@section('content')
    {{ html()->form('POST', $data->url->update)->class('form-horizontal')->acceptsFiles()->open() }}

    @method('PUT')

    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        @lang('labels.backend.users.management')
                        <small class="text-muted">@lang('labels.backend.users.edit')</small>
                    </h4>
                </div><!--col-->
            </div><!--row-->

            <hr>

            <div class="row mt-4 mb-4">
                <div class="col">

                    <div class="form-group row">
                        {{ html()->label(__('labels.backend.users.image_url'))->class('col-md-2 form-control-label')->for('image_url') }}

                        <div class="col-md-10">
                            {{ html()->file('image_url')
                                ->class('form-control') }}
                        </div><!--col-->
                        @if(isset($data->image->image_url))
                            <br>

                            <img style="max-width: 20%; margin: 10px;" src="{{  $data->image->image_url }}" >
                        @endif
                    </div><!--form-group-->

                    <div class="form-group row">
                        {{ html()->label(__('labels.backend.users.first_name'))->class('col-md-2 form-control-label')->for('first_name') }}

                        <div class="col-md-10">
                            {{ html()->text('first_name')
                                ->class('form-control')
                                ->placeholder(__('labels.backend.users.first_name'))
                                ->attribute('maxlength', 191)
                                ->autofocus()
                                ->value(old('name', $data->first_name)) }}
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                        {{ html()->label(__('labels.backend.users.last_name'))->class('col-md-2 form-control-label')->for('last_name') }}

                        <div class="col-md-10">
                            {{ html()->text('last_name')
                                ->class('form-control')
                                ->placeholder(__('labels.backend.users.last_name'))
                                ->attribute('maxlength', 191)
                                ->autofocus()
                                ->value(old('name', $data->last_name)) }}
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                        {{ html()->label(__('labels.backend.users.email'))->class('col-md-2 form-control-label')->for('email') }}

                        <div class="col-md-10">
                            {{ html()->email('email')
                                ->class('form-control')
                                ->placeholder(__('labels.backend.users.email'))
                                ->attribute('maxlength', 191)
                                ->autofocus()
                                ->value(old('email', $data->email)) }}
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                        {{ html()->label(__('labels.backend.users.password'))->class('col-md-2 form-control-label')->for('password') }}

                        <div class="col-md-10">
                            {{ html()->password('password')
                                ->class('form-control')
                                ->placeholder(__('labels.backend.users.password'))
                                ->attribute('maxlength', 191)
                                 }}
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                        {{ html()->label(__('labels.backend.users.phone'))->class('col-md-2 form-control-label')->for('phone') }}

                        <div class="col-md-10">
                            {{ html()->text('phone')
                                ->class('form-control')
                                ->placeholder(__('labels.backend.users.phone'))
                                ->attribute('maxlength', 191)
                                ->value(old('phone', $data->phone)) }}
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                        {{ html()->label(__('labels.backend.users.address'))->class('col-md-2 form-control-label')->for('address') }}

                        <div class="col-md-10">
                            {{ html()->textarea('address')
                                ->class('form-control')
                                ->placeholder(__('labels.backend.users.address'))
                                ->attributes(['row' => 5, 'col' => 5])
                                ->value(old('address', $data->address)) }}
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                        {{ html()->label(__('labels.backend.users.gender'))->class('col-md-2 form-control-label')->for('gender') }}

                        <div class="col-md-10">
                            <select required class="form-control" name="gender">
                                {{ html()->option(__('labels.backend.users.gender'))
                                        ->class('form-control')
                                        ->value('')
                                        ->attribute('disabled',true)
                                        ->selected()
                                         }}
                                {{ html()->option(__('labels.backend.users.male'))
                                      ->class('form-control')
                                      ->value('male')
                                      ->attribute($data->gender == 'male'? 'selected': '') }}
                                {{ html()->option(__('labels.backend.users.female'))
                                      ->class('form-control')
                                      ->value('female')
                                      ->attribute($data->gender == 'female'? 'selected' : '') }}
                            </select>
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                        {{ html()->label(__('labels.backend.users.age'))->class('col-md-2 form-control-label')->for('age') }}

                        <div class="col-md-10">
                            {{ html()->text('age')
                                ->class('form-control')
                                ->placeholder(__('labels.backend.users.age'))
                                ->attribute('maxlength', 191)
                                ->value(old('age', $data->age)) }}
                        </div><!--col-->
                    </div><!--form-group-->

{{--                    <div class="form-group row">--}}
{{--                        {{ html()->label(__('labels.backend.users.isAdmin'))->class('col-md-2 form-control-label')->for('isAdmin') }}--}}

{{--                        <div class="col-md-10">--}}
{{--                            <select required class="form-control" name="isAdmin">--}}
{{--                                {{ html()->option(__('labels.backend.users.isAdmin'))--}}
{{--                                        ->class('form-control')--}}
{{--                                        ->value('')--}}
{{--                                        ->attribute('disabled',true)--}}
{{--                                        ->selected()--}}
{{--                                         }}--}}
{{--                                {{ html()->option(__('labels.backend.users.Admin'))--}}
{{--                                      ->class('form-control')--}}
{{--                                      ->value(1)--}}
{{--                                      ->attribute($data->isAdmin == '1'? 'selected' : '')--}}
{{--                                       }}--}}
{{--                                {{ html()->option(__('labels.backend.users.User'))--}}
{{--                                      ->class('form-control')--}}
{{--                                      ->value(0)--}}
{{--                                      ->attribute($data->isAdmin == '0'? 'selected' : '')--}}
{{--                                       }}--}}
{{--                            </select>--}}
{{--                        </div><!--col-->--}}
{{--                    </div><!--form-group-->--}}

                </div><!--col-->
            </div><!--row-->
        </div><!--card-body-->

        <div class="card-footer clearfix">
            <div class="row">
                <div class="col">
                    {{ form_cancel(route('admin.users.index'), __('buttons.general.cancel')) }}
                </div><!--col-->

                <div class="col text-right">
                    {{ form_submit(__('buttons.general.crud.edit')) }}
                </div><!--col-->
            </div><!--row-->
        </div><!--card-footer-->
    </div><!--card-->
    {{ html()->form()->close() }}
@endsection
