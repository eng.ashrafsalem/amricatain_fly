@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('labels.backend.customtourbookings.management'))

@section('content')
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        {{ __('labels.backend.customtourbookings.management') }}
                    </h4>
                </div><!--col-->

            </div><!--row-->

            <div class="row mt-4">
                <div class="col">
                    <div class="table-responsive">
                        <table class="table" id="result-table">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>@lang('labels.backend.customtourbookings.customer')</th>
                                <th>@lang('labels.backend.customtourbookings.email')</th>
                                <th>@lang('labels.backend.customtourbookings.phone')</th>
                                <th>@lang('labels.backend.customtourbookings.hotel')</th>
                                <th>@lang('labels.backend.customtourbookings.from')</th>
                                <th>@lang('labels.backend.customtourbookings.to')</th>
                                <th>@lang('labels.backend.customtourbookings.total_cost')</th>
                                <th>@lang('labels.backend.customtourbookings.status')</th>
                                <th>@lang('labels.general.actions')</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div><!--col-->
            </div><!--row-->
        </div><!--card-body-->
    </div><!--card-->
@endsection

@push('after-scripts')
    <script>
        $dataTable('{{ route('admin.customtourbookings.index') }}', [
            {data: 'DT_RowIndex', name: 'id'},
            {data: 'customer.name', name: 'customer.name'},
            {data: 'customer.email', name: 'customer.email'},
            {data: 'customer.phone', name: 'customer.phone'},
            {data: 'hotel.name', name: 'hotel.name'},
            {data: 'from', name: 'from'},
            {data: 'to', name: 'to'},
            {data: 'total_cost', name: 'total_cost'},
            {data: 'status', name: 'status'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ]);
    </script>
@endpush
