@extends('backend.layouts.app')

@section('title', __('labels.backend.fixedtours.management') . ' | ' . __('labels.backend.fixedtours.create'))

@push('after-styles')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
@endpush

@push('after-scripts')
    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBIATE_TQXlrdKhFo2QxQdIulT2kvm51AI&libraries=places" type="text/javascript"></script>
    <script>
        flatpickr('.date', {
            // enableTime:true
            // enableTime: true,
            dateFormat: "Y-m-d",

        })
 $(document).ready(function() {
      map=new google.maps.Map(document.getElementById('map-canvas'),{
          center:{
            lat:31.256045,
            lng:29.98436,
          },
        zoom:15
    });

    var marker = new google.maps.Marker({
    position: {
    lat: 31.256045,
    lng: 29.98436,
    },
    map: map,
    draggable:true,
});
var searchBox = new google.maps.places.SearchBox(document.getElementById('searchmap'));
google.maps.event.addListener(searchBox,'places_changed',function(){
var places = searchBox.getPlaces();
var bounds = new google.maps.LatLngBounds();
var i , place;
for(i=0;place=places[i];i++){
 bounds.extend(place.geometry.location);
 marker.setPosition(place.geometry.location);
}
map.fitBounds(bounds);
map.setZoom(17);
});
google.maps.event.addListener(marker,'position_changed',function(){
 var lat = marker.getPosition().lat();
 var lng = marker.getPosition().lng();
 $('#lat').val(lat);
 $('#lng').val(lng);
});
        });
    </script>
@endpush

@section('content')
    {{ html()->form('POST', route('admin.fixedtours.store'))->class('form-horizontal')->acceptsFiles()->open() }}
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        @lang('labels.backend.fixedtours.management')
                        <small class="text-muted">@lang('labels.backend.fixedtours.create')</small>
                    </h4>
                </div><!--col-->
            </div><!--row-->

            <hr>

            <div class="row mt-4 mb-4">
                <div class="col">

                    <div class="form-group row">
                        {{ html()->label(__('labels.backend.fixedtours.name'))->class('col-md-2 form-control-label')->for('name') }}
                        <div class="col-md-10">

                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="arabic-tab" data-toggle="tab" href="#arabic" role="tab" aria-controls="arabic" aria-selected="true">
                                        {{ html()->label(__('labels.backend.fixedtours.ar_name')) }}
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="english-tab" data-toggle="tab" href="#english" role="tab" aria-controls="english" aria-selected="false">
                                        {{ html()->label(__('labels.backend.fixedtours.en_name')) }}
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="frensh-tab" data-toggle="tab" href="#frensh" role="tab" aria-controls="frensh" aria-selected="false">
                                        {{ html()->label(__('labels.backend.fixedtours.fr_name')) }}
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="italy-tab" data-toggle="tab" href="#italian" role="tab" aria-controls="italian" aria-selected="false">
                                        {{ html()->label(__('labels.backend.fixedtours.it_name')) }}
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="spanish-tab" data-toggle="tab" href="#spanish" role="tab" aria-controls="italian" aria-selected="false">
                                        {{ html()->label(__('labels.backend.fixedtours.es_name')) }}
                                    </a>
                                </li>
                            </ul>
                            <div class="tab-content" id="myTabContent">
                                <div class="tab-pane fade  show active" id="arabic" role="tabpanel" aria-labelledby="arabic-tab">
                                    {{ html()->text('ar[name]')
                                    ->class('form-control')
                                    ->placeholder(__('labels.backend.fixedtours.ar_name'))
                                    ->attribute('maxlength', 191)
                                    ->attribute('required')
                                    ->autofocus() }}
                                </div>
                                <div class="tab-pane fade" id="english" role="tabpanel" aria-labelledby="english-tab">
                                    {{ html()->text('en[name]')
                                    ->class('form-control')
                                    ->placeholder(__('labels.backend.fixedtours.en_name'))
                                    ->attribute('maxlength', 191)
                                    ->attribute('required')
                                    ->autofocus() }}
                                </div>
                                <div class="tab-pane fade" id="frensh" role="tabpanel" aria-labelledby="frensh-tab">
                                    {{ html()->text('fr[name]')
                                    ->class('form-control')
                                    ->placeholder(__('labels.backend.fixedtours.fr_name'))
                                    ->attribute('maxlength', 191)
                                    ->attribute('required')
                                    ->autofocus() }}
                                </div>
                                <div class="tab-pane fade" id="italian" role="tabpanel" aria-labelledby="italian-tab">
                                    {{ html()->text('it[name]')
                                    ->class('form-control')
                                    ->placeholder(__('labels.backend.fixedtours.it_name'))
                                    ->attribute('maxlength', 191)
                                    ->attribute('required')
                                    ->autofocus() }}
                                </div>
                                <div class="tab-pane fade" id="spanish" role="tabpanel" aria-labelledby="spanish-tab">
                                    {{ html()->text('es[name]')
                                    ->class('form-control')
                                    ->placeholder(__('labels.backend.fixedtours.es_name'))
                                    ->attribute('maxlength', 191)
                                    ->attribute('required')
                                    ->autofocus() }}
                                </div>
                            </div>

                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                        {{ html()->label(__('labels.backend.fixedtours.description'))->class('col-md-2 form-control-label')->for('description') }}
                        <div class="col-md-10">

                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="arabic-tab" data-toggle="tab" href="#arabic_description" role="tab" aria-controls="arabic" aria-selected="true">
                                        {{ html()->label(__('labels.backend.fixedtours.ar_description')) }}
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="english-tab" data-toggle="tab" href="#english_description" role="tab" aria-controls="english" aria-selected="false">
                                        {{ html()->label(__('labels.backend.fixedtours.en_description')) }}
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="frensh-tab" data-toggle="tab" href="#frensh_description" role="tab" aria-controls="frensh" aria-selected="false">
                                        {{ html()->label(__('labels.backend.fixedtours.fr_description')) }}
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="italy-tab" data-toggle="tab" href="#italian_description" role="tab" aria-controls="italian" aria-selected="false">
                                        {{ html()->label(__('labels.backend.fixedtours.it_description')) }}
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="spanish-tab" data-toggle="tab" href="#spanish_description" role="tab" aria-controls="italian" aria-selected="false">
                                        {{ html()->label(__('labels.backend.fixedtours.es_description')) }}
                                    </a>
                                </li>
                            </ul>
                            <div class="tab-content" id="myTabContent">
                                <div class="tab-pane fade  show active" id="arabic_description" role="tabpanel" aria-labelledby="arabic-tab">
                                    {{ html()->textarea('ar[description]')
                                    ->class('form-control')
                                    ->placeholder(__('labels.backend.fixedtours.ar_description'))
                                    ->attribute('required')
                                    ->autofocus() }}
                                </div>
                                <div class="tab-pane fade" id="english_description" role="tabpanel" aria-labelledby="english-tab">
                                    {{ html()->textarea('en[description]')
                                    ->class('form-control')
                                    ->placeholder(__('labels.backend.fixedtours.en_description'))
                                    ->attribute('required')
                                    ->autofocus() }}
                                </div>
                                <div class="tab-pane fade" id="frensh_description" role="tabpanel" aria-labelledby="frensh-tab">
                                    {{ html()->textarea('fr[description]')
                                    ->class('form-control')
                                    ->placeholder(__('labels.backend.fixedtours.fr_description'))
                                    ->attribute('required')
                                    ->autofocus() }}
                                </div>
                                <div class="tab-pane fade" id="italian_description" role="tabpanel" aria-labelledby="italian-tab">
                                    {{ html()->textarea('it[description]')
                                    ->class('form-control')
                                    ->placeholder(__('labels.backend.fixedtours.it_description'))
                                    ->attribute('required')
                                    ->autofocus() }}
                                </div>
                                <div class="tab-pane fade" id="spanish_description" role="tabpanel" aria-labelledby="spanish-tab">
                                    {{ html()->textarea('es[description]')
                                    ->class('form-control')
                                    ->placeholder(__('labels.backend.fixedtours.es_description'))
                                    ->attribute('required')
                                    ->autofocus() }}
                                </div>
                            </div>

                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                        {{ html()->label(__('labels.backend.fixedtours.category'))->class('col-md-2 form-control-label')->for('category') }}

                        <div class="col-md-10">
                            <select class="form-control" id="category_id" name="category_id" required>
                                <option value="">إختر</option>
                                @foreach($categories as $value)
                                    <option value="{{ $value->id }}">{{ $value->name }}</option>
                                @endforeach
                            </select>
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                        {{ html()->label(__('labels.backend.hotels.cities'))->class('col-md-2 form-control-label')->for('cities') }}

                        <div class="col-md-10">
                            <select class="form-control" id="city_id" name="city_id" required>
                                <option value="">إختر</option>
                                @foreach($cities as $value)
                                    <option value="{{ $value->id }}">{{ $value->name }}</option>
                                @endforeach
                            </select>
                        </div><!--col-->


                    </div><!--col-->


                    <div class="form-group row">
                        {{ html()->label(__('labels.backend.fixedtours.hotel'))->class('col-md-2 form-control-label')->for('hotel') }}

                        <div class="col-md-10">
                            <select class="form-control" id="hotel_id" name="hotel_id" required>
                                <option value="">إختر</option>
                                @foreach($hotels as $value)
                                    <option value="{{ $value->id }}">{{ $value->name }}</option>
                                @endforeach
                            </select>
                        </div><!--col-->


                    </div><!--col-->

                    <div class="form-group row">
                        {{ html()->label(__('labels.backend.fixedtours.from'))->class('col-md-2 form-control-label')->for('from') }}

                        <div class="col-md-10">
                            {{ html()->text('from')
                                ->class(['form-control', 'date'])
                                ->placeholder(__('labels.backend.fixedtours.from'))
                                ->attribute('maxlength', 191)
                                ->attribute('required')
                                ->autofocus() }}
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                        {{ html()->label(__('labels.backend.fixedtours.to'))->class('col-md-2 form-control-label')->for('to') }}

                        <div class="col-md-10">
                            {{ html()->text('to')
                                ->class(['form-control', 'date'])
                                ->placeholder(__('labels.backend.fixedtours.to'))
                                ->attribute('maxlength', 191)
                                ->attribute('required')
                                ->autofocus() }}
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                        {{ html()->label(__('labels.backend.fixedtours.totalCost'))->class('col-md-2 form-control-label')->for('totalCost') }}

                        <div class="col-md-8">
                            {{ html()->text('total_cost')
                                ->class('form-control')
                                ->placeholder(__('labels.backend.fixedtours.totalCost'))
                                ->attribute('maxlength', 191)
                                ->attribute('required')
                                ->autofocus() }}
                        </div><!--col-->

                        {{ html()->label(__('labels.backend.fixedtours.pound'))->class('col-md-2 form-control-label')->for('pound') }}
                    </div><!--form-group-->
                    <div class="form-group row">
                        {{ html()->label(__('الخصم'))->class('col-md-2 form-control-label')->for('discount') }}

                        <div class="col-md-8">
                            {{ html()->text('discount')
                                ->class('form-control')
                                ->placeholder(__('الخصم'))
                                ->attribute('maxlength', 191)

                                ->autofocus() }}
                        </div><!--col-->


                    </div><!--form-group-->

                    <div class="form-group row">
                        {{ html()->label(__('labels.backend.fixedtours.active'))->class('col-md-2 form-control-label')->for('active') }}

                        <div class="col-md-10">
                            <select  required class="form-control" name="active">
                                {{ html()->option(__('labels.backend.fixedtours.choose-active'))
                                        ->class('form-control')
                                        ->value('')
                                        ->attribute('disabled',true)
                                        ->selected()
                                         }}
                                {{ html()->option(__('labels.backend.fixedtours.active'))
                                      ->class('form-control')
                                      ->value(1)
                                       }}
                                {{ html()->option(__('labels.backend.fixedtours.in-active'))
                                      ->class('form-control')
                                      ->value(0)
                                       }}
                            </select>
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                        {{ html()->label(__('labels.backend.fixedtours.days_nights_counts'))->class('col-md-2 form-control-label')->for('days_nights_counts') }}

                        <div class="col-md-10">
                            {{ html()->text('days_nights_counts')
                                ->class('form-control')
                                ->placeholder(__('labels.backend.fixedtours.days_nights_counts'))
                                ->attribute('maxlength', 191)
                                ->attribute('required')
                                ->autofocus() }}
                        </div><!--col-->


                    </div><!--col-->

                    <div class="form-group row">
                        {{ html()->label(__('labels.backend.fixedtours.main_image'))->class('col-md-2 form-control-label')->for('main_image') }}

                        <div class="col-md-10">
                            {{ html()->file('m_image')
                                ->class('form-control') }}
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                        {{ html()->label(__('labels.backend.fixedtours.slider_images'))->class('col-md-2 form-control-label')->for('slider_image') }}

                        <div class="col-md-10">
                            {{ html()->file('s_image[]')
                                ->class('form-control')
                                 ->attribute('multiple', 'multiple')}}
                        </div><!--col-->
                    </div><!--form-group-->

                </div><!--col-->
            </div><!--row-->

            <div class="head-conatct text-center wow flipInX">
                    <h3> حدد موقع الرحلة على الخريطة</h3>
                </div>
                <div class="form-contacts home-form">
                <div class="row">
                <div class="col-sm-12 text-right wow lightSpeedIn">
                  <div class="custom-control custom-radio">
                      <input class="form-control" type="text"  placeholder="أبحث عن موقع الرحلة" id="searchmap">

                  </div>
                  </div>
                <div id="map-canvas" style="height:500px; width:100%;margin-bottom:30px;">

                 </div>
            <input type="hidden" name="map_lat"  id="lat" value="31.256045"/>
            <input type="hidden" name="map_long" id="lng" value="29.98436"/>
                 </div>
                 </div>


        </div><!--card-body-->

        <div class="card-footer clearfix">
            <div class="row">
                <div class="col">
                    {{ form_cancel(route('admin.fixedtours.index'), __('buttons.general.cancel')) }}
                </div><!--col-->

                <div class="col text-right">
                    {{ form_submit(__('buttons.general.crud.create')) }}
                </div><!--col-->
            </div><!--row-->
        </div><!--card-footer-->
    </div><!--card-->
    {{ html()->form()->close() }}
@endsection
