@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('labels.backend.hotelbookings.management'))

@section('content')
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        {{ __('labels.backend.hotelbookings.management') }}
                    </h4>
                </div><!--col-->

                <div class="col-sm-7">
{{--                    <div class="btn-toolbar float-right" role="toolbar" aria-label="@lang('labels.general.toolbar_btn_groups')">--}}
{{--                        <a href="{{ route('admin.hotelbookings.create') }}" class="btn btn-success ml-1" data-toggle="tooltip"--}}
{{--                           title="@lang('labels.general.create_new')"><i class="fas fa-plus-circle"></i></a>--}}
{{--                    </div><!--btn-toolbar-->--}}
                </div><!--col-->
            </div><!--row-->

            <div class="row mt-4">
                <div class="col">
                    <div class="table-responsive">
                        <table class="table" id="result-table">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>@lang('labels.backend.hotelbookings.customer')</th>
                                <th>@lang('labels.backend.hotelbookings.email')</th>
                                <th>@lang('labels.backend.hotelbookings.phone')</th>
                                <th>@lang('labels.backend.hotelbookings.hotel_details')</th>
                                <th>@lang('labels.backend.hotelbookings.from')</th>
                                <th>@lang('labels.backend.hotelbookings.to')</th>
                                <th>@lang('labels.backend.hotelbookings.status')</th>
                                <th>@lang('labels.general.actions')</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div><!--col-->
            </div><!--row-->
        </div><!--card-body-->
    </div><!--card-->
@endsection

@push('after-scripts')
    <script>
        $dataTable('{{ route('admin.hotelbookings.index') }}', [
            {data: 'DT_RowIndex', name: 'id'},
            {data: 'customer.name', name: 'customer.name'},
            {data: 'customer.email', name: 'customer.email'},
            {data: 'customer.phone', name: 'customer.phone'},
            {data: 'hotel.name', name: 'hotel.name'},
            {data: 'from', name: 'from'},
            {data: 'to', name: 'to'},
            {data: 'status', name: 'status'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ]);
    </script>
@endpush
