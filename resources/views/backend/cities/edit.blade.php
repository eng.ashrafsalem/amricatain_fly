@extends('backend.layouts.app')

@section('title', __('labels.backend.cities.management') . ' | ' . __('labels.backend.cities.edit'))


@section('content')
    {{ html()->form('POST', $data->url->update)->class('form-horizontal')->open() }}

    @method('PUT')

    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        @lang('labels.backend.cities.management')
                        <small class="text-muted">@lang('labels.backend.cities.edit')</small>
                    </h4>
                </div><!--col-->
            </div><!--row-->

            <hr>

            <div class="row mt-4 mb-4">
                <div class="col">

                    <div class="form-group row">
                        {{ html()->label(__('labels.backend.cities.country'))->class('col-md-2 form-control-label')->for('name') }}

                        <div class="col-md-10">
                            <select class="form-control" id="country_id" name="country_id" required>
{{--                                <option value="">إختر</option>--}}
                                @foreach($countries as $value)
                                    <option value="{{ $value->id }}" {{ $value->id== $data->country_id ? 'selected' : '' }}>{{ $value->name }}</option>
                                @endforeach
                            </select>
                        </div><!--col-->


                    </div><!--col-->

                    <div class="form-group row">
                        {{ html()->label(__('labels.backend.cities.name'))->class('col-md-2 form-control-label')->for('name') }}
                        <div class="col-md-10">

                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="arabic-tab" data-toggle="tab" href="#arabic" role="tab" aria-controls="arabic" aria-selected="true">
                                        {{ html()->label(__('labels.backend.cities.ar_name')) }}
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="english-tab" data-toggle="tab" href="#english" role="tab" aria-controls="english" aria-selected="false">
                                        {{ html()->label(__('labels.backend.cities.en_name')) }}
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="frensh-tab" data-toggle="tab" href="#frensh" role="tab" aria-controls="frnsh" aria-selected="false">
                                        {{ html()->label(__('labels.backend.cities.fr_name')) }}
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="italy-tab" data-toggle="tab" href="#italian" role="tab" aria-controls="italian" aria-selected="false">
                                        {{ html()->label(__('labels.backend.cities.it_name')) }}
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="spanish-tab" data-toggle="tab" href="#spanish" role="tab" aria-controls="italian" aria-selected="false">
                                        {{ html()->label(__('labels.backend.cities.es_name')) }}
                                    </a>
                                </li>
                            </ul>
                            <div class="tab-content" id="myTabContent">
                                <div class="tab-pane fade  show active" id="arabic" role="tabpanel" aria-labelledby="arabic-tab">
                                    {{ html()->text('ar[name]')
                                    ->class('form-control')
                                    ->placeholder(__('labels.backend.cities.ar_name'))
                                    ->attribute('maxlength', 191)
                                    ->value($data->getTranslation('ar')["name"])
                                    ->autofocus() }}
                                </div>
                                <div class="tab-pane fade" id="english" role="tabpanel" aria-labelledby="english-tab">
                                    {{ html()->text('en[name]')
                                    ->class('form-control')
                                    ->placeholder(__('labels.backend.cities.en_name'))
                                    ->attribute('maxlength', 191)
                                    ->value($data->getTranslation('en')["name"])
                                    ->autofocus() }}
                                </div>
                                <div class="tab-pane fade" id="frensh" role="tabpanel" aria-labelledby="frensh-tab">
                                    {{ html()->text('fr[name]')
                                    ->class('form-control')
                                    ->placeholder(__('labels.backend.cities.fr_name'))
                                    ->attribute('maxlength', 191)
                                    ->value($data->getTranslation('fr')["name"])
                                    ->autofocus() }}
                                </div>
                                <div class="tab-pane fade" id="italian" role="tabpanel" aria-labelledby="italian-tab">
                                    {{ html()->text('it[name]')
                                    ->class('form-control')
                                    ->placeholder(__('labels.backend.cities.it_name'))
                                    ->attribute('maxlength', 191)
                                    ->value($data->getTranslation('it')["name"])
                                    ->autofocus() }}
                                </div>
                                <div class="tab-pane fade" id="spanish" role="tabpanel" aria-labelledby="spanish-tab">
                                    {{ html()->text('es[name]')
                                    ->class('form-control')
                                    ->placeholder(__('labels.backend.cities.es_name'))
                                    ->attribute('maxlength', 191)
                                    ->value($data->getTranslation('es')["name"])
                                    ->autofocus() }}
                                </div>
                            </div>

                        </div><!--col-->
                    </div><!--form-group-->

                </div><!--col-->
            </div><!--row-->
        </div><!--card-body-->

        <div class="card-footer clearfix">
            <div class="row">
                <div class="col">
                    {{ form_cancel(route('admin.cities.index'), __('buttons.general.cancel')) }}
                </div><!--col-->

                <div class="col text-right">
                    {{ form_submit(__('buttons.general.crud.edit')) }}
                </div><!--col-->
            </div><!--row-->
        </div><!--card-footer-->
    </div><!--card-->
    {{ html()->form()->close() }}
@endsection
