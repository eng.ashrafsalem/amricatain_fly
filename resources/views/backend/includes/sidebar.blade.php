<div class="sidebar">
    <nav class="sidebar-nav">
        <ul class="nav">
            <li class="nav-title">
                @lang('menus.backend.sidebar.general')
            </li>

            <li class="nav-item">
                <a class="nav-link {{
                    active_class(Route::is('admin/dashboard'))
                }}" href="{{ route('admin.dashboard') }}">
                    <i class="nav-icon fas fa-tachometer-alt"></i>
                    @lang('menus.backend.sidebar.dashboard')
                </a>
            </li>

            {{--@if ($logged_in_user->isAdmin())--}}

            <li class="nav-title">
                @lang('menus.backend.sidebar.system')
            </li>

            @role('store_admin')
                <li class="nav-item">
                    <a class="nav-link {{
                                    active_class(Route::is('admin/edit-store-data'))
                                }}" href="{{ route('admin.edit') }}">
                        <i class="nav-icon fas fa-clock"></i>
                        تعديل البيانات
                    </a>
                </li>
            @endrole

            @role('administrator')
            <li class="nav-item nav-dropdown {{
                    active_class(Route::is('admin/auth*'), 'open')
                }}">
                <a class="nav-link nav-dropdown-toggle {{
                        active_class(Route::is('admin/auth*'))
                    }}" href="#">
                    <i class="nav-icon far fa-user"></i>
                    @lang('menus.backend.access.title')

                    @if ($pending_approval > 0)
                        <span class="badge badge-danger">{{ $pending_approval }}</span>
                    @endif
                </a>

                <ul class="nav-dropdown-items">
            <li class="nav-item">
                <a class="nav-link {{
                            active_class(Route::is('admin/customers*'))
                        }}" href="{{ route('admin.customers.index') }}">
                    @lang('labels.backend.customers.management')
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link {{
                            active_class(Route::is('admin/users*'))
                        }}" href="{{ route('admin.users.index') }}">
                    @lang('labels.backend.users.management')
                </a>
            </li>
                </ul>
            </li>
            @endrole

            <li class="nav-item">
                <a class="nav-link {{
                            active_class(Route::is('admin/countries*'))
                        }}" href="{{ route('admin.countries.index') }}">
                    @lang('labels.backend.countries.management')
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link {{
                            active_class(Route::is('admin/cities*'))
                        }}" href="{{ route('admin.cities.index') }}">
                    @lang('labels.backend.cities.management')
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link {{
                            active_class(Route::is('admin/services*'))
                        }}" href="{{ route('admin.services.index') }}">
                    @lang('labels.backend.services.management')
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link {{
                            active_class(Route::is('admin/categories*'))
                        }}" href="{{ route('admin.categories.index') }}">
                    @lang('labels.backend.categories.management')
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link {{
                            active_class(Route::is('admin/currencyexchanges*'))
                        }}" href="{{ route('admin.currencyexchanges.index') }}">
                    @lang('labels.backend.currencyexchanges.management')
                </a>
            </li>

{{--            <li class="nav-item">--}}
{{--                <a class="nav-link {{--}}
{{--                            active_class(Route::is('admin/customers*'))--}}
{{--                        }}" href="{{ route('admin.customers.index') }}">--}}
{{--                    @lang('labels.backend.customers.management')--}}
{{--                </a>--}}
{{--            </li>--}}

{{--            <li class="nav-item">--}}
{{--                <a class="nav-link {{--}}
{{--                            active_class(Route::is('admin/users*'))--}}
{{--                        }}" href="{{ route('admin.users.index') }}">--}}
{{--                    @lang('labels.backend.users.management')--}}
{{--                </a>--}}
{{--            </li>--}}

            <li class="nav-item">
                <a class="nav-link {{
                            active_class(Route::is('admin/hotels*'))
                        }}" href="{{ route('admin.hotels.index') }}">
                    @lang('labels.backend.hotels.management')
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link {{
                            active_class(Route::is('admin/fixedtours*'))
                        }}" href="{{ route('admin.fixedtours.index') }}">
                    @lang('labels.backend.fixedtours.management')
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link {{
                            active_class(Route::is('admin/frontsliders'))
                        }}" href="{{ route('admin.frontsliders.index') }}">
                    @lang('labels.backend.frontsliders.management')

                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link {{
                            active_class(Route::is('admin/aboutuses'))
                        }}" href="{{ route('admin.aboutuses.index') }}">
                    @lang('labels.backend.aboutuses.management')

                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link {{
                            active_class(Route::is('admin/customeropinions'))
                        }}" href="{{ route('admin.customeropinions.index') }}">
                    @lang('labels.backend.customeropinions.management')

                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link {{
                            active_class(Route::is('admin/discovers'))
                        }}" href="{{ route('admin.discovers.index') }}">
                    @lang('labels.backend.discovers.management')

                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link {{
                            active_class(Route::is('admin/citysliders'))
                        }}" href="{{ route('admin.citysliders.index') }}">
                    @lang('labels.backend.citysliders.management')

                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link {{
                            active_class(Route::is('admin/frontnews'))
                        }}" href="{{ route('admin.frontnews.index') }}">
                    @lang('labels.backend.frontnews.management')

                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link {{
                            active_class(Route::is('admin/settings'))
                        }}" href="{{ route('admin.settings.index') }}">
                    @lang('labels.backend.settings.management')

                </a>
            </li>

{{--            <li class="nav-item">--}}
{{--                <a class="nav-link {{--}}
{{--                            active_class(Route::is('admin/messages'))--}}
{{--                        }}" href="{{ route('admin.messages.index') }}">--}}
{{--                    @lang('labels.backend.messages.management')--}}

{{--                </a>--}}
{{--            </li>--}}

            <li class="nav-item">
                <a class="nav-link {{
                            active_class(Route::is('admin/fixedtourbookings'))
                        }}" href="{{ route('admin.fixedtourbookings.index') }}">
                    @lang('labels.backend.fixedtourbookings.management')

                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link {{
                            active_class(Route::is('admin/hotelbookings'))
                        }}" href="{{ route('admin.hotelbookings.index') }}">
                    @lang('labels.backend.hotelbookings.management')

                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link {{
                            active_class(Route::is('admin/customtourbookings'))
                        }}" href="{{ route('admin.customtourbookings.index') }}">
                    @lang('labels.backend.customtourbookings.management')

                </a>
            </li>


            <li class="nav-item">
                <a class="nav-link {{
                            active_class(Route::is('admin/ContactUss'))
                        }}" href="{{ route('admin.ContactUss.index') }}">
                    @lang('labels.backend.ContactUs.management')

                </a>
            </li>

{{--            @if(auth()->user()->hasPermissionTo('sections.management'))--}}
{{--                <li class="nav-item">--}}
{{--                    <a class="nav-link {{--}}
{{--                                    active_class(Route::is('admin/sections'))--}}
{{--                                }}" href="{{ route('admin.sections.index') }}">--}}
{{--                        <i class="nav-icon fas fa-clock"></i>--}}
{{--                        @lang('labels.backend.sections.management')--}}
{{--                    </a>--}}
{{--                </li>--}}
{{--            @endif--}}

{{--            @endif--}}
        </ul>
    </nav>

    <button class="sidebar-minimizer brand-minimizer" type="button"></button>
</div><!--sidebar-->
