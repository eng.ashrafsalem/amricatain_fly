@extends('backend.layouts.app')

@section('title', __('labels.backend.hotels.management') . ' | ' . __('labels.backend.hotels.create'))


@push('after-styles')

    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/css/select2.min.css" rel="stylesheet" />
@endpush

@push('after-scripts')
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/js/select2.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBIATE_TQXlrdKhFo2QxQdIulT2kvm51AI&libraries=places" type="text/javascript"></script>
    <script>
        // In your Javascript (external .js resource or <script> tag)
        $(document).ready(function() {
            map=new google.maps.Map(document.getElementById('map-canvas'),{
          center:{
            lat:31.256045,
            lng:29.98436,
          },
        zoom:15
    });

    var marker = new google.maps.Marker({
    position: {
    lat: 31.256045,
    lng: 29.98436,
    },
    map: map,
    draggable:true,
});
var searchBox = new google.maps.places.SearchBox(document.getElementById('searchmap'));
google.maps.event.addListener(searchBox,'places_changed',function(){
var places = searchBox.getPlaces();
var bounds = new google.maps.LatLngBounds();
var i , place;
for(i=0;place=places[i];i++){
 bounds.extend(place.geometry.location);
 marker.setPosition(place.geometry.location);
}
map.fitBounds(bounds);
map.setZoom(17);
});
google.maps.event.addListener(marker,'position_changed',function(){
 var lat = marker.getPosition().lat();
 var lng = marker.getPosition().lng();
 $('#lat').val(lat);
 $('#lng').val(lng);
});
            $('.tags-selector').select2();
        });

    </script>

@endpush

@section('content')
    {{ html()->form('POST', route('admin.hotels.store'))->class('form-horizontal')->acceptsFiles()->open() }}
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        @lang('labels.backend.hotels.management')
                        <small class="text-muted">@lang('labels.backend.hotels.create')</small>
                    </h4>
                </div><!--col-->
            </div><!--row-->

            <hr>

            <div class="row mt-4 mb-4">
                <div class="col">
                    <div class="form-group row">
                        {{ html()->label(__('labels.backend.hotels.name'))->class('col-md-2 form-control-label')->for('name') }}
                        <div class="col-md-10">

                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="arabic-tab" data-toggle="tab" href="#arabic" role="tab" aria-controls="arabic" aria-selected="true">
                                        {{ html()->label(__('labels.backend.hotels.ar_name')) }}
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="english-tab" data-toggle="tab" href="#english" role="tab" aria-controls="english" aria-selected="false">
                                        {{ html()->label(__('labels.backend.hotels.en_name')) }}
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="frensh-tab" data-toggle="tab" href="#frensh" role="tab" aria-controls="frensh" aria-selected="false">
                                        {{ html()->label(__('labels.backend.hotels.fr_name')) }}
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="italy-tab" data-toggle="tab" href="#italian" role="tab" aria-controls="italian" aria-selected="false">
                                        {{ html()->label(__('labels.backend.hotels.it_name')) }}
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="spanish-tab" data-toggle="tab" href="#spanish" role="tab" aria-controls="italian" aria-selected="false">
                                        {{ html()->label(__('labels.backend.hotels.es_name')) }}
                                    </a>
                                </li>
                            </ul>
                            <div class="tab-content" id="myTabContent">
                                <div class="tab-pane fade  show active" id="arabic" role="tabpanel" aria-labelledby="arabic-tab">
                                    {{ html()->text('ar[name]')
                                    ->class('form-control')
                                    ->placeholder(__('labels.backend.hotels.ar_name'))
                                    ->attribute('maxlength', 191)
                                    ->attribute('required')
                                    ->autofocus() }}
                                </div>
                                <div class="tab-pane fade" id="english" role="tabpanel" aria-labelledby="english-tab">
                                    {{ html()->text('en[name]')
                                    ->class('form-control')
                                    ->placeholder(__('labels.backend.hotels.en_name'))
                                    ->attribute('maxlength', 191)
                                    ->attribute('required')
                                    ->autofocus() }}
                                </div>
                                <div class="tab-pane fade" id="frensh" role="tabpanel" aria-labelledby="frensh-tab">
                                    {{ html()->text('fr[name]')
                                    ->class('form-control')
                                    ->placeholder(__('labels.backend.hotels.fr_name'))
                                    ->attribute('maxlength', 191)
                                    ->attribute('required')
                                    ->autofocus() }}
                                </div>
                                <div class="tab-pane fade" id="italian" role="tabpanel" aria-labelledby="italian-tab">
                                    {{ html()->text('it[name]')
                                    ->class('form-control')
                                    ->placeholder(__('labels.backend.hotels.it_name'))
                                    ->attribute('maxlength', 191)
                                    ->attribute('required')
                                    ->autofocus() }}
                                </div>
                                <div class="tab-pane fade" id="spanish" role="tabpanel" aria-labelledby="spanish-tab">
                                    {{ html()->text('es[name]')
                                    ->class('form-control')
                                    ->placeholder(__('labels.backend.hotels.es_name'))
                                    ->attribute('maxlength', 191)
                                    ->attribute('required')
                                    ->autofocus() }}
                                </div>
                            </div>

                        </div><!--col-->
                    </div><!--form-group-->


                    <div class="form-group row">
                        {{ html()->label(__('labels.backend.hotels.cities'))->class('col-md-2 form-control-label')->for('cities') }}

                        <div class="col-md-10">
                            <select class="form-control" id="city_id" name="city_id" required>
                                <option value="">إختر</option>
                                @foreach($cities as $value)
                                    <option value="{{ $value->id }}">{{ $value->name }}</option>
                                @endforeach
                            </select>
                        </div><!--col-->


                    </div><!--col-->

                    <!--div class="form-group row">
                        {{ html()->label(__('labels.backend.hotels.map_long'))->class('col-md-2 form-control-label')->for('map_long') }}

                        <div class="col-md-10">
                            {{ html()->text('map_long')
                                ->class('form-control')
                                ->placeholder(__('labels.backend.hotels.map_long'))
                                ->attribute('maxlength', 191)
                                ->autofocus() }}
                        </div>
                    </div>

                    <div class="form-group row">
                        {{ html()->label(__('labels.backend.hotels.map_lat'))->class('col-md-2 form-control-label')->for('map_lat') }}

                        <div class="col-md-10">
                            {{ html()->text('map_lat')
                                ->class('form-control')
                                ->placeholder(__('labels.backend.hotels.map_lat'))
                                ->attribute('maxlength', 191)
                                ->autofocus() }}
                        </div>
                    </div--><!--form-group-->

                    <div class="form-group row">
                        {{ html()->label(__('labels.backend.hotels.level'))->class('col-md-2 form-control-label')->for('level') }}

                        <div class="col-md-8">
                            <select required class="form-control" name="level">
                                {{ html()->option(__('labels.backend.hotels.level'))
                                        ->class('form-control')
                                        ->value('')
                                        ->attribute('disabled',true)
                                        ->selected()
                                         }}
                                {{ html()->option(__('labels.backend.hotels.1star'))
                                      ->class('form-control')
                                      ->value('1')
                                       }}
                                {{ html()->option(__('labels.backend.hotels.2star'))
                                      ->class('form-control')
                                      ->value('2')
                                       }}
                                {{ html()->option(__('labels.backend.hotels.3star'))
                                      ->class('form-control')
                                      ->value('3')
                                       }}
                                {{ html()->option(__('labels.backend.hotels.4star'))
                                      ->class('form-control')
                                      ->value('4')
                                       }}
                                {{ html()->option(__('labels.backend.hotels.5star'))
                                      ->class('form-control')
                                      ->value('5')
                                       }}
                            </select>
                        </div><!--col-->

                        {{ html()->label(__('labels.backend.hotels.star'))->class('col-md-2 form-control-label')->for('star') }}
                    </div><!--form-group-->

                    <div class="form-group row">
                        {{ html()->label(__('labels.backend.hotels.shown'))->class('col-md-2 form-control-label')->for('shown') }}

                        <div class="col-md-10">
                            <select  required class="form-control" name="shown">
                                {{ html()->option(__('labels.backend.hotels.choose-shown'))
                                        ->class('form-control')
                                        ->value('')
                                        ->attribute('disabled',true)
                                        ->selected()
                                         }}
                                {{ html()->option(__('labels.backend.hotels.active'))
                                      ->class('form-control')
                                      ->value(1)
                                       }}
                                {{ html()->option(__('labels.backend.hotels.in-active'))
                                      ->class('form-control')
                                      ->value(0)
                                       }}
                            </select>
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                        {{ html()->label(__('labels.backend.hotels.services'))->class('col-md-2 form-control-label')->for('services') }}

                        <div class="col-md-10">
                            <select class="form-control tags-selector" id="services" name="services[]" required multiple>
                                <option value="" disabled>إختر</option>
                                @foreach($services as $value)
                                    <option value="{{ $value->id }}">{{ $value->name }}</option>
                                @endforeach
                            </select>
                        </div><!--col-->


                    </div><!--col-->

                    <div class="form-group row">
                        {{ html()->label(__('labels.backend.hotels.main_image'))->class('col-md-2 form-control-label')->for('main_image') }}

                        <div class="col-md-10">
                            {{ html()->file('m_image')
                                ->class('form-control') }}
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                        {{ html()->label(__('labels.backend.hotels.slider_images'))->class('col-md-2 form-control-label')->for('slider_image') }}

                        <div class="col-md-10">
                            {{ html()->file('s_image[]')
                                ->class('form-control')
                                 ->attribute('multiple', 'multiple')}}
                        </div><!--col-->
                    </div><!--form-group-->

                </div><!--col-->
            </div><!--row-->

            <div class="head-conatct text-center wow flipInX">
                    <h3> حدد موقع الفندق على الخريطة</h3>
                </div>
                <div class="form-contacts home-form">
                <div class="row">
                <div class="col-sm-12 text-right wow lightSpeedIn">
                  <div class="custom-control custom-radio">
                      <input class="form-control" type="text"  placeholder="أبحث عن موقع الفندق" id="searchmap">

                  </div>
                  </div>
                <div id="map-canvas" style="height:500px; width:100%;margin-bottom:30px;">

                 </div>
            <input type="hidden" name="map_lat"  id="lat" value="31.256045"/>
            <input type="hidden" name="map_long" id="lng" value="29.98436"/>
                 </div>
                 </div>




        </div><!--card-body-->

        <div class="card-footer clearfix">
            <div class="row">
                <div class="col">
                    {{ form_cancel(route('admin.hotels.index'), __('buttons.general.cancel')) }}
                </div><!--col-->

                <div class="col text-right">
                    {{ form_submit(__('buttons.general.crud.create')) }}
                </div><!--col-->
            </div><!--row-->
        </div><!--card-footer-->
    </div><!--card-->
    {{ html()->form()->close() }}
@endsection
