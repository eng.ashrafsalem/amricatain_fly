@extends('backend.layouts.app')

@section('title', __('labels.backend.discovers.management') . ' | ' . __('labels.backend.discovers.create'))


@section('content')
    {{ html()->form('POST', route('admin.discovers.store'))->class('form-horizontal')->open() }}
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        @lang('labels.backend.discovers.management')
                        <small class="text-muted">@lang('labels.backend.discovers.create')</small>
                    </h4>
                </div><!--col-->
            </div><!--row-->

            <hr>

            <div class="row mt-4 mb-4">
                <div class="col">

                       
                <div class="form-group row">
                    {{ html()->label(__('labels.backend.discovers.title'))->class('col-md-2 form-control-label')->for('title') }}
                    <div class="col-md-10">

                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="arabic-tab" data-toggle="tab" href="#arabic" role="tab" aria-controls="arabic" aria-selected="true">
                                    {{ html()->label(__('labels.backend.discovers.ar_title')) }}
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="english-tab" data-toggle="tab" href="#english" role="tab" aria-controls="english" aria-selected="false">
                                    {{ html()->label(__('labels.backend.discovers.en_title')) }}
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="frensh-tab" data-toggle="tab" href="#frensh" role="tab" aria-controls="frensh" aria-selected="false">
                                    {{ html()->label(__('labels.backend.discovers.fr_title')) }}
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="italy-tab" data-toggle="tab" href="#italian" role="tab" aria-controls="italian" aria-selected="false">
                                    {{ html()->label(__('labels.backend.discovers.it_title')) }}
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="spanish-tab" data-toggle="tab" href="#spanish" role="tab" aria-controls="italian" aria-selected="false">
                                    {{ html()->label(__('labels.backend.discovers.es_title')) }}
                                </a>
                            </li>
                        </ul>
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade  show active" id="arabic" role="tabpanel" aria-labelledby="arabic-tab">
                                {{ html()->text('ar[title]')
                                ->class('form-control')
                                ->placeholder(__('labels.backend.discovers.ar_title'))
                                ->attribute('maxlength', 191)
                                ->attribute('required')
                                ->autofocus() }}
                            </div>
                            <div class="tab-pane fade" id="english" role="tabpanel" aria-labelledby="english-tab">
                                {{ html()->text('en[title]')
                                ->class('form-control')
                                ->placeholder(__('labels.backend.discovers.en_title'))
                                ->attribute('maxlength', 191)
                                ->attribute('required')
                                ->autofocus() }}
                            </div>
                            <div class="tab-pane fade" id="frensh" role="tabpanel" aria-labelledby="frensh-tab">
                                {{ html()->text('fr[title]')
                                ->class('form-control')
                                ->placeholder(__('labels.backend.discovers.fr_title'))
                                ->attribute('maxlength', 191)
                                ->attribute('required')
                                ->autofocus() }}
                            </div>
                            <div class="tab-pane fade" id="italian" role="tabpanel" aria-labelledby="italian-tab">
                                {{ html()->text('it[title]')
                                ->class('form-control')
                                ->placeholder(__('labels.backend.discovers.it_title'))
                                ->attribute('maxlength', 191)
                                ->attribute('required')
                                ->autofocus() }}
                            </div>
                            <div class="tab-pane fade" id="spanish" role="tabpanel" aria-labelledby="spanish-tab">
                                {{ html()->text('es[title]')
                                ->class('form-control')
                                ->placeholder(__('labels.backend.discovers.es_title'))
                                ->attribute('maxlength', 191)
                                ->attribute('required')
                                ->autofocus() }}
                            </div>
                        </div>

                    </div><!--col-->
                </div><!--form-group-->

                <div class="form-group row">
                    {{ html()->label(__('labels.backend.frontnews.description'))->class('col-md-2 form-control-label')->for('description') }}
                    <div class="col-md-10">

                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="arabic-tab" data-toggle="tab" href="#arabic_description" role="tab" aria-controls="arabic" aria-selected="true">
                                    {{ html()->label(__('labels.backend.discovers.ar_description')) }}
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="english-tab" data-toggle="tab" href="#english_description" role="tab" aria-controls="english" aria-selected="false">
                                    {{ html()->label(__('labels.backend.discovers.en_description')) }}
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="frensh-tab" data-toggle="tab" href="#frensh_description" role="tab" aria-controls="frensh" aria-selected="false">
                                    {{ html()->label(__('labels.backend.discovers.fr_description')) }}
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="italy-tab" data-toggle="tab" href="#italian_description" role="tab" aria-controls="italian" aria-selected="false">
                                    {{ html()->label(__('labels.backend.discovers.it_description')) }}
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="spanish-tab" data-toggle="tab" href="#spanish_description" role="tab" aria-controls="italian" aria-selected="false">
                                    {{ html()->label(__('labels.backend.discovers.es_description')) }}
                                </a>
                            </li>
                        </ul>
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade  show active" id="arabic_description" role="tabpanel" aria-labelledby="arabic-tab">
                                {{ html()->textarea('ar[description]')
                                ->class('form-control')
                                ->placeholder(__('labels.backend.discovers.ar_description'))
                                ->attribute('required')
                                ->autofocus() }}
                            </div>
                            <div class="tab-pane fade" id="english_description" role="tabpanel" aria-labelledby="english-tab">
                                {{ html()->textarea('en[description]')
                                ->class('form-control')
                                ->placeholder(__('labels.backend.discovers.en_description'))
                                ->attribute('required')
                                ->autofocus() }}
                            </div>
                            <div class="tab-pane fade" id="frensh_description" role="tabpanel" aria-labelledby="frensh-tab">
                                {{ html()->textarea('fr[description]')
                                ->class('form-control')
                                ->placeholder(__('labels.backend.discovers.fr_description'))
                                ->attribute('required')
                                ->autofocus() }}
                            </div>
                            <div class="tab-pane fade" id="italian_description" role="tabpanel" aria-labelledby="italian-tab">
                                {{ html()->textarea('it[description]')
                                ->class('form-control')
                                ->placeholder(__('labels.backend.discovers.it_description'))
                                ->attribute('required')
                                ->autofocus() }}
                            </div>
                            <div class="tab-pane fade" id="spanish_description" role="tabpanel" aria-labelledby="spanish-tab">
                                {{ html()->textarea('es[description]')
                                ->class('form-control')
                                ->placeholder(__('labels.backend.discovers.es_description'))
                                ->attribute('required')
                                ->autofocus() }}
                            </div>
                        </div>

                    </div><!--col-->
                </div><!--form-group-->


                </div><!--col-->
            </div><!--row-->
        </div><!--card-body-->

        <div class="card-footer clearfix">
            <div class="row">
                <div class="col">
                    {{ form_cancel(route('admin.discovers.index'), __('buttons.general.cancel')) }}
                </div><!--col-->

                <div class="col text-right">
                    {{ form_submit(__('buttons.general.crud.create')) }}
                </div><!--col-->
            </div><!--row-->
        </div><!--card-footer-->
    </div><!--card-->
    {{ html()->form()->close() }}
@endsection
@extends('backend.layouts.app')

@section('title', __('labels.backend.frontnews.management') . ' | ' . __('labels.backend.frontnews.create'))

@push('after-styles')

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
@endpush

@push('after-scripts')

     <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
    <script>
        flatpickr('.date', {
            // enableTime:true
        })

    </script>
@endpush

@section('content')
    {{ html()->form('POST', route('admin.frontnews.store'))->class('form-horizontal')->acceptsFiles()->open() }}
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        @lang('labels.backend.frontnews.management')
                        <small class="text-muted">@lang('labels.backend.frontnews.create')</small>
                    </h4>
                </div><!--col-->
            </div><!--row-->

            <hr>

            <div class="row mt-4 mb-4">
                <div class="col">

                <div class="form-group row">
                    {{ html()->label(__('labels.backend.frontnews.news_date'))->class('col-md-2 form-control-label')->for('news_date') }}

                    <div class="col-md-10">
                        {{ html()->text('news_date')
                            ->class(['form-control', 'date'])
                            ->placeholder(__('labels.backend.frontnews.news_date'))
                            ->attribute('maxlength', 191)
                            ->attribute('required')
                            ->autofocus() }}
                    </div><!--col-->
                </div><!--form-group-->

                <div class="form-group row">
                    {{ html()->label(__('labels.backend.frontnews.main_image'))->class('col-md-2 form-control-label')->for('image_url') }}

                    <div class="col-md-10">
                        {{ html()->file('image_url')
                            ->class('form-control') }}
                    </div><!--col-->
                </div><!--form-group-->

                
                
                <div class="form-group row">
                    {{ html()->label(__('labels.backend.frontnews.title'))->class('col-md-2 form-control-label')->for('title') }}
                    <div class="col-md-10">

                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="arabic-tab" data-toggle="tab" href="#arabic" role="tab" aria-controls="arabic" aria-selected="true">
                                    {{ html()->label(__('labels.backend.frontnews.ar_title')) }}
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="english-tab" data-toggle="tab" href="#english" role="tab" aria-controls="english" aria-selected="false">
                                    {{ html()->label(__('labels.backend.frontnews.en_title')) }}
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="frensh-tab" data-toggle="tab" href="#frensh" role="tab" aria-controls="frensh" aria-selected="false">
                                    {{ html()->label(__('labels.backend.frontnews.fr_title')) }}
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="italy-tab" data-toggle="tab" href="#italian" role="tab" aria-controls="italian" aria-selected="false">
                                    {{ html()->label(__('labels.backend.frontnews.it_title')) }}
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="spanish-tab" data-toggle="tab" href="#spanish" role="tab" aria-controls="italian" aria-selected="false">
                                    {{ html()->label(__('labels.backend.frontnews.es_title')) }}
                                </a>
                            </li>
                        </ul>
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade  show active" id="arabic" role="tabpanel" aria-labelledby="arabic-tab">
                                {{ html()->text('ar[title]')
                                ->class('form-control')
                                ->placeholder(__('labels.backend.frontnews.ar_title'))
                                ->attribute('maxlength', 191)
                                ->attribute('required')
                                ->autofocus() }}
                            </div>
                            <div class="tab-pane fade" id="english" role="tabpanel" aria-labelledby="english-tab">
                                {{ html()->text('en[title]')
                                ->class('form-control')
                                ->placeholder(__('labels.backend.frontnews.en_title'))
                                ->attribute('maxlength', 191)
                                ->attribute('required')
                                ->autofocus() }}
                            </div>
                            <div class="tab-pane fade" id="frensh" role="tabpanel" aria-labelledby="frensh-tab">
                                {{ html()->text('fr[title]')
                                ->class('form-control')
                                ->placeholder(__('labels.backend.frontnews.fr_title'))
                                ->attribute('maxlength', 191)
                                ->attribute('required')
                                ->autofocus() }}
                            </div>
                            <div class="tab-pane fade" id="italian" role="tabpanel" aria-labelledby="italian-tab">
                                {{ html()->text('it[title]')
                                ->class('form-control')
                                ->placeholder(__('labels.backend.frontnews.it_title'))
                                ->attribute('maxlength', 191)
                                ->attribute('required')
                                ->autofocus() }}
                            </div>
                            <div class="tab-pane fade" id="spanish" role="tabpanel" aria-labelledby="spanish-tab">
                                {{ html()->text('es[title]')
                                ->class('form-control')
                                ->placeholder(__('labels.backend.frontnews.es_title'))
                                ->attribute('maxlength', 191)
                                ->attribute('required')
                                ->autofocus() }}
                            </div>
                        </div>

                    </div><!--col-->
                </div><!--form-group-->

                <div class="form-group row">
                    {{ html()->label(__('labels.backend.frontnews.description'))->class('col-md-2 form-control-label')->for('description') }}
                    <div class="col-md-10">

                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="arabic-tab" data-toggle="tab" href="#arabic_description" role="tab" aria-controls="arabic" aria-selected="true">
                                    {{ html()->label(__('labels.backend.frontnews.ar_description')) }}
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="english-tab" data-toggle="tab" href="#english_description" role="tab" aria-controls="english" aria-selected="false">
                                    {{ html()->label(__('labels.backend.frontnews.en_description')) }}
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="frensh-tab" data-toggle="tab" href="#frensh_description" role="tab" aria-controls="frensh" aria-selected="false">
                                    {{ html()->label(__('labels.backend.frontnews.fr_description')) }}
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="italy-tab" data-toggle="tab" href="#italian_description" role="tab" aria-controls="italian" aria-selected="false">
                                    {{ html()->label(__('labels.backend.frontnews.it_description')) }}
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="spanish-tab" data-toggle="tab" href="#spanish_description" role="tab" aria-controls="italian" aria-selected="false">
                                    {{ html()->label(__('labels.backend.frontnews.es_description')) }}
                                </a>
                            </li>
                        </ul>
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade  show active" id="arabic_description" role="tabpanel" aria-labelledby="arabic-tab">
                                {{ html()->textarea('ar[description]')
                                ->class('form-control')
                                ->placeholder(__('labels.backend.frontnews.ar_description'))
                                ->attribute('required')
                                ->autofocus() }}
                            </div>
                            <div class="tab-pane fade" id="english_description" role="tabpanel" aria-labelledby="english-tab">
                                {{ html()->textarea('en[description]')
                                ->class('form-control')
                                ->placeholder(__('labels.backend.frontnews.en_description'))
                                ->attribute('required')
                                ->autofocus() }}
                            </div>
                            <div class="tab-pane fade" id="frensh_description" role="tabpanel" aria-labelledby="frensh-tab">
                                {{ html()->textarea('fr[description]')
                                ->class('form-control')
                                ->placeholder(__('labels.backend.frontnews.fr_description'))
                                ->attribute('required')
                                ->autofocus() }}
                            </div>
                            <div class="tab-pane fade" id="italian_description" role="tabpanel" aria-labelledby="italian-tab">
                                {{ html()->textarea('it[description]')
                                ->class('form-control')
                                ->placeholder(__('labels.backend.frontnews.it_description'))
                                ->attribute('required')
                                ->autofocus() }}
                            </div>
                            <div class="tab-pane fade" id="spanish_description" role="tabpanel" aria-labelledby="spanish-tab">
                                {{ html()->textarea('es[description]')
                                ->class('form-control')
                                ->placeholder(__('labels.backend.frontnews.es_description'))
                                ->attribute('required')
                                ->autofocus() }}
                            </div>
                        </div>

                    </div><!--col-->
                </div><!--form-group-->

                

                </div><!--col-->
            </div><!--row-->
        </div><!--card-body-->

        <div class="card-footer clearfix">
            <div class="row">
                <div class="col">
                    {{ form_cancel(route('admin.frontnews.index'), __('buttons.general.cancel')) }}
                </div><!--col-->

                <div class="col text-right">
                    {{ form_submit(__('buttons.general.crud.create')) }}
                </div><!--col-->
            </div><!--row-->
        </div><!--card-footer-->
    </div><!--card-->
    {{ html()->form()->close() }}
@endsection
