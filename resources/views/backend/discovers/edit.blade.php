@extends('backend.layouts.app')

@section('title', __('labels.backend.discovers.management') . ' | ' . __('labels.backend.discovers.edit'))


@section('content')
    {{ html()->form('POST', $data->url->update)->class('form-horizontal')->open() }}

    @method('PUT')

    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        @lang('labels.backend.discovers.management')
                        <small class="text-muted">@lang('labels.backend.discovers.edit')</small>
                    </h4>
                </div><!--col-->
            </div><!--row-->

            <hr>

            <div class="row mt-4 mb-4">
                <div class="col">

                <div class="form-group row">
                    {{ html()->label(__('labels.backend.discovers.title'))->class('col-md-2 form-control-label')->for('title') }}
                    <div class="col-md-10">

                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="arabic-tab" data-toggle="tab" href="#arabic" role="tab" aria-controls="arabic" aria-selected="true">
                                    {{ html()->label(__('labels.backend.discovers.ar_title')) }}
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="english-tab" data-toggle="tab" href="#english" role="tab" aria-controls="english" aria-selected="false">
                                    {{ html()->label(__('labels.backend.discovers.en_title')) }}
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="frensh-tab" data-toggle="tab" href="#frensh" role="tab" aria-controls="frensh" aria-selected="false">
                                    {{ html()->label(__('labels.backend.discovers.fr_title')) }}
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="italy-tab" data-toggle="tab" href="#italian" role="tab" aria-controls="italian" aria-selected="false">
                                    {{ html()->label(__('labels.backend.discovers.it_title')) }}
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="spanish-tab" data-toggle="tab" href="#spanish" role="tab" aria-controls="italian" aria-selected="false">
                                    {{ html()->label(__('labels.backend.discovers.es_title')) }}
                                </a>
                            </li>
                        </ul>
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade  show active" id="arabic" role="tabpanel" aria-labelledby="arabic-tab">
                                {{ html()->text('ar[title]')
                                ->class('form-control')
                                ->placeholder(__('labels.backend.discovers.ar_title'))
                                ->attribute('maxlength', 191)
                                ->value($data->getTranslation('ar')['title'])
                                ->autofocus() }}
                            </div>
                            <div class="tab-pane fade" id="english" role="tabpanel" aria-labelledby="english-tab">
                                {{ html()->text('en[title]')
                                ->class('form-control')
                                ->placeholder(__('labels.backend.discovers.en_title'))
                                ->attribute('maxlength', 191)
                                ->value($data->getTranslation('en')['title'])
                                ->autofocus() }}
                            </div>
                            <div class="tab-pane fade" id="frensh" role="tabpanel" aria-labelledby="frensh-tab">
                                {{ html()->text('fr[title]')
                                ->class('form-control')
                                ->placeholder(__('labels.backend.discovers.fr_title'))
                                ->attribute('maxlength', 191)
                                ->value($data->getTranslation('fr')['title'])
                                ->autofocus() }}
                            </div>
                            <div class="tab-pane fade" id="italian" role="tabpanel" aria-labelledby="italian-tab">
                                {{ html()->text('it[title]')
                                ->class('form-control')
                                ->placeholder(__('labels.backend.discovers.it_title'))
                                ->attribute('maxlength', 191)
                                ->value($data->getTranslation('it')['title'])
                                ->autofocus() }}
                            </div>
                            <div class="tab-pane fade" id="spanish" role="tabpanel" aria-labelledby="spanish-tab">
                                {{ html()->text('es[title]')
                                ->class('form-control')
                                ->placeholder(__('labels.backend.discovers.es_title'))
                                ->attribute('maxlength', 191)
                                ->value($data->getTranslation('es')['title'])
                                ->autofocus() }}
                            </div>
                        </div>

                    </div><!--col-->
                </div><!--form-group-->

                <div class="form-group row">
                    {{ html()->label(__('labels.backend.frontnews.description'))->class('col-md-2 form-control-label')->for('description') }}
                    <div class="col-md-10">

                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="arabic-tab" data-toggle="tab" href="#arabic_description" role="tab" aria-controls="arabic" aria-selected="true">
                                    {{ html()->label(__('labels.backend.discovers.ar_description')) }}
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="english-tab" data-toggle="tab" href="#english_description" role="tab" aria-controls="english" aria-selected="false">
                                    {{ html()->label(__('labels.backend.discovers.en_description')) }}
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="frensh-tab" data-toggle="tab" href="#frensh_description" role="tab" aria-controls="frensh" aria-selected="false">
                                    {{ html()->label(__('labels.backend.discovers.fr_description')) }}
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="italy-tab" data-toggle="tab" href="#italian_description" role="tab" aria-controls="italian" aria-selected="false">
                                    {{ html()->label(__('labels.backend.discovers.it_description')) }}
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="spanish-tab" data-toggle="tab" href="#spanish_description" role="tab" aria-controls="italian" aria-selected="false">
                                    {{ html()->label(__('labels.backend.discovers.es_description')) }}
                                </a>
                            </li>
                        </ul>
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade  show active" id="arabic_description" role="tabpanel" aria-labelledby="arabic-tab">
                                {{ html()->textarea('ar[description]')
                                ->class('form-control')
                                ->placeholder(__('labels.backend.discovers.ar_description'))
                                ->value($data->getTranslation('ar')['description'])
                                ->autofocus() }}
                            </div>
                            <div class="tab-pane fade" id="english_description" role="tabpanel" aria-labelledby="english-tab">
                                {{ html()->textarea('en[description]')
                                ->class('form-control')
                                ->placeholder(__('labels.backend.discovers.en_description'))
                                ->value($data->getTranslation('en')['description'])
                                ->autofocus() }}
                            </div>
                            <div class="tab-pane fade" id="frensh_description" role="tabpanel" aria-labelledby="frensh-tab">
                                {{ html()->textarea('fr[description]')
                                ->class('form-control')
                                ->placeholder(__('labels.backend.discovers.fr_description'))
                                ->value($data->getTranslation('fr')['description'])
                                ->autofocus() }}
                            </div>
                            <div class="tab-pane fade" id="italian_description" role="tabpanel" aria-labelledby="italian-tab">
                                {{ html()->textarea('it[description]')
                                ->class('form-control')
                                ->placeholder(__('labels.backend.discovers.it_description'))
                                ->value($data->getTranslation('it')['description'])
                                ->autofocus() }}
                            </div>
                            <div class="tab-pane fade" id="spanish_description" role="tabpanel" aria-labelledby="spanish-tab">
                                {{ html()->textarea('es[description]')
                                ->class('form-control')
                                ->placeholder(__('labels.backend.discovers.es_description'))
                                ->value($data->getTranslation('es')['description'])
                                ->autofocus() }}
                            </div>
                        </div>

                    </div><!--col-->
                </div><!--form-group-->

                </div><!--col-->
            </div><!--row-->
        </div><!--card-body-->

        <div class="card-footer clearfix">
            <div class="row">
                <div class="col">
                    {{ form_cancel(route('admin.discovers.index'), __('buttons.general.cancel')) }}
                </div><!--col-->

                <div class="col text-right">
                    {{ form_submit(__('buttons.general.crud.edit')) }}
                </div><!--col-->
            </div><!--row-->
        </div><!--card-footer-->
    </div><!--card-->
    {{ html()->form()->close() }}
@endsection
