@extends('backend.layouts.app')

@section('title', __('labels.backend.customers.management') . ' | ' . __('labels.backend.customers.create'))


@section('content')
    {{ html()->form('POST', route('admin.customers.store'))->class('form-horizontal')->acceptsFiles()->open() }}
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        @lang('labels.backend.customers.management')
                        <small class="text-muted">@lang('labels.backend.customers.create')</small>
                    </h4>
                </div><!--col-->
            </div><!--row-->

            <hr>

            <div class="row mt-4 mb-4">
                <div class="col">

                    <div class="form-group row">
                        {{ html()->label(__('labels.backend.customers.image_url'))->class('col-md-2 form-control-label')->for('image_url') }}

                        <div class="col-md-10">
                            {{ html()->file('image_url')
                                ->class('form-control') }}
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                        {{ html()->label(__('labels.backend.customers.name'))->class('col-md-2 form-control-label')->for('name') }}

                        <div class="col-md-10">
                            {{ html()->text('name')
                                ->class('form-control')
                                ->placeholder(__('labels.backend.customers.name'))
                                ->attribute('maxlength', 191)
                                ->required()
                                ->autofocus() }}
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                        {{ html()->label(__('labels.backend.customers.email'))->class('col-md-2 form-control-label')->for('email') }}

                        <div class="col-md-10">
                            {{ html()->email('email')
                                ->class('form-control')
                                ->placeholder(__('labels.backend.customers.email'))
                                ->attribute('maxlength', 191)
                                ->required()
                                ->autofocus() }}
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                        {{ html()->label(__('labels.backend.customers.password'))->class('col-md-2 form-control-label')->for('password') }}

                        <div class="col-md-10">
                            {{ html()->password('password')
                                ->class('form-control')
                                ->placeholder(__('labels.backend.customers.password'))
                                ->required()
                                ->attribute('maxlength', 191)
                                 }}
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                        {{ html()->label(__('labels.backend.customers.phone'))->class('col-md-2 form-control-label')->for('phone') }}

                        <div class="col-md-10">
                            {{ html()->text('phone')
                                ->class('form-control')
                                ->placeholder(__('labels.backend.customers.phone'))
                                ->attribute('maxlength', 191)
                                ->autofocus() }}
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                        {{ html()->label(__('labels.backend.customers.address'))->class('col-md-2 form-control-label')->for('address') }}

                        <div class="col-md-10">
                            {{ html()->textarea('address')
                                ->class('form-control')
                                ->placeholder(__('labels.backend.customers.address'))
                                ->attributes(['row' => 5, 'col' => 5])
                                ->autofocus() }}
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                        {{ html()->label(__('labels.backend.customers.gender'))->class('col-md-2 form-control-label')->for('gender') }}

                        <div class="col-md-10">
                            <select required class="form-control" name="gender">
                                {{ html()->option(__('labels.backend.customers.gender'))
                                        ->class('form-control')
                                        ->value('')
                                        ->attribute('disabled',true)
                                        ->selected()
                                         }}
                                {{ html()->option(__('labels.backend.customers.male'))
                                      ->class('form-control')
                                      ->value('male')
                                       }}
                                {{ html()->option(__('labels.backend.customers.female'))
                                      ->class('form-control')
                                      ->value('female')
                                       }}
                            </select>
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                        {{ html()->label(__('labels.backend.customers.age'))->class('col-md-2 form-control-label')->for('age') }}

                        <div class="col-md-10">
                            {{ html()->text('age')
                                ->class('form-control')
                                ->placeholder(__('labels.backend.customers.age'))
                                ->attribute('maxlength', 191)
                                ->autofocus() }}
                        </div><!--col-->
                    </div><!--form-group-->

{{--                    <div class="form-group row">--}}
{{--                        {{ html()->label(__('labels.backend.customers.isAdmin'))->class('col-md-2 form-control-label')->for('isAdmin') }}--}}

{{--                        <div class="col-md-10">--}}
{{--                            <select required class="form-control" name="isAdmin">--}}
{{--                                {{ html()->option(__('labels.backend.customers.isAdmin'))--}}
{{--                                        ->class('form-control')--}}
{{--                                        ->value('')--}}
{{--                                        ->attribute('disabled',true)--}}
{{--                                        ->selected()--}}
{{--                                         }}--}}
{{--                                {{ html()->option(__('labels.backend.customers.Admin'))--}}
{{--                                      ->class('form-control')--}}
{{--                                      ->value(1)--}}
{{--                                       }}--}}
{{--                                {{ html()->option(__('labels.backend.customers.User'))--}}
{{--                                      ->class('form-control')--}}
{{--                                      ->value(0)--}}
{{--                                       }}--}}
{{--                            </select>--}}
{{--                        </div><!--col-->--}}
{{--                    </div><!--form-group-->--}}

                </div><!--col-->
            </div><!--row-->
        </div><!--card-body-->

        <div class="card-footer clearfix">
            <div class="row">
                <div class="col">
                    {{ form_cancel(route('admin.customers.index'), __('buttons.general.cancel')) }}
                </div><!--col-->

                <div class="col text-right">
                    {{ form_submit(__('buttons.general.crud.create')) }}
                </div><!--col-->
            </div><!--row-->
        </div><!--card-footer-->
    </div><!--card-->
    {{ html()->form()->close() }}
@endsection
