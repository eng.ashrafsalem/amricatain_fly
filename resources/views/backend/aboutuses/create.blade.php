@extends('backend.layouts.app')

@section('title', __('labels.backend.aboutuses.management') . ' | ' . __('labels.backend.aboutuses.create'))

@push('after-styles')

    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/trix/1.2.1/trix.css">
@endpush

@push('after-scripts')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/trix/1.2.1/trix.js">
    </script>
@endpush

@section('content')
    {{ html()->form('POST', route('admin.aboutuses.store'))->class('form-horizontal')->acceptsFiles()->open() }}
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        @lang('labels.backend.aboutuses.management')
                        <small class="text-muted">@lang('labels.backend.aboutuses.create')</small>
                    </h4>
                </div><!--col-->
            </div><!--row-->

            <hr>

            <div class="row mt-4 mb-4">
                <div class="col">

                    <div class="form-group row">
                        {{ html()->label(__('labels.backend.aboutuses.contents'))->class('col-md-2 form-control-label')->for('contents') }}
                        <div class="col-md-10">

                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="arabic-tab" data-toggle="tab" href="#arabic" role="tab" aria-controls="arabic" aria-selected="true">
                                        {{ html()->label(__('labels.backend.aboutuses.ar_contents')) }}
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="english-tab" data-toggle="tab" href="#english" role="tab" aria-controls="english" aria-selected="false">
                                        {{ html()->label(__('labels.backend.aboutuses.en_contents')) }}
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="frensh-tab" data-toggle="tab" href="#frensh" role="tab" aria-controls="frnsh" aria-selected="false">
                                        {{ html()->label(__('labels.backend.aboutuses.fr_contents')) }}
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="italy-tab" data-toggle="tab" href="#italian" role="tab" aria-controls="italian" aria-selected="false">
                                        {{ html()->label(__('labels.backend.aboutuses.it_contents')) }}
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="spanish-tab" data-toggle="tab" href="#spanish" role="tab" aria-controls="italian" aria-selected="false">
                                        {{ html()->label(__('labels.backend.aboutuses.es_contents')) }}
                                    </a>
                                </li>
                            </ul>
                            <div class="tab-content" id="myTabContent">
                                <div class="tab-pane fade  show active" id="arabic" role="tabpanel" aria-labelledby="arabic-tab">
                                    <input id="ar[contents]" type="hidden" name="ar[contents]">
                                    <trix-editor input="ar[contents]"></trix-editor>
                                </div>
                                <div class="tab-pane fade" id="english" role="tabpanel" aria-labelledby="english-tab">
                                    <input id="en[contents]" type="hidden" name="en[contents]">
                                    <trix-editor input="en[contents]"></trix-editor>
                                </div>
                                <div class="tab-pane fade" id="frensh" role="tabpanel" aria-labelledby="frensh-tab">
                                    <input id="fr[contents]" type="hidden" name="fr[contents]">
                                    <trix-editor input="fr[contents]"></trix-editor>
                                </div>
                                <div class="tab-pane fade" id="italian" role="tabpanel" aria-labelledby="italian-tab">
                                    <input id="it[contents]" type="hidden" name="it[contents]">
                                    <trix-editor input="it[contents]"></trix-editor>
                                </div>
                                <div class="tab-pane fade" id="spanish" role="tabpanel" aria-labelledby="spanish-tab">
                                    <input id="es[contents]" type="hidden" name="es[contents]">
                                    <trix-editor input="es[contents]"></trix-editor>
                                </div>
                            </div>

                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                    {{ html()->label(__('النوع'))->class('col-md-2 form-control-label')->for('image_url') }}
                        
                        <div class="col-md-10">
                            <select class="form-control" id="type_id" name="type_id" required>
                                <option value="">إختر</option>
                                    <option value="1">نبذة عنا</option>
                                    <option value="2">رؤيتنا</option>
                                    <option value="3">هدفنا</option>
                            </select>
                        </div><!--col-->


                    </div><!--col-->

                    <div class="form-group row">
                        {{ html()->label(__('labels.backend.frontsliders.main_image'))->class('col-md-2 form-control-label')->for('image_url') }}

                        <div class="col-md-10">
                            {{ html()->file('image_url')
                                ->class('form-control') }}
                        </div><!--col-->
                    </div><!--form-group-->


                </div><!--col-->
            </div><!--row-->
        </div><!--card-body-->

        <div class="card-footer clearfix">
            <div class="row">
                <div class="col">
                    {{ form_cancel(route('admin.aboutuses.index'), __('buttons.general.cancel')) }}
                </div><!--col-->

                <div class="col text-right">
                    {{ form_submit(__('buttons.general.crud.create')) }}
                </div><!--col-->
            </div><!--row-->
        </div><!--card-footer-->
    </div><!--card-->
    {{ html()->form()->close() }}
@endsection
