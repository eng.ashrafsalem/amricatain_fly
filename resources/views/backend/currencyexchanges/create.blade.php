@extends('backend.layouts.app')

@section('title', __('labels.backend.currencyexchanges.management') . ' | ' . __('labels.backend.currencyexchanges.create'))


@section('content')
    {{ html()->form('POST', route('admin.currencyexchanges.store'))->class('form-horizontal')->open() }}
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        @lang('labels.backend.currencyexchanges.management')
                        <small class="text-muted">@lang('labels.backend.currencyexchanges.create')</small>
                    </h4>
                </div><!--col-->
            </div><!--row-->

            <hr>

            <div class="row mt-4 mb-4">
                <div class="col">

                    <div class="form-group row">
                        {{ html()->label(__('labels.backend.currencyexchanges.name'))->class('col-md-2 form-control-label')->for('name') }}

                        <div class="col-md-10">
                            {{ html()->text('name')
                                ->class('form-control')
                                ->placeholder(__('labels.backend.currencyexchanges.name'))
                                ->attribute('maxlength', 191)
                                ->autofocus() }}
                        </div><!--col-->

                    </div><!--form-group-->
                    <div class="form-group row">
                        {{ html()->label(__('labels.backend.currencyexchanges.exchange_rate'))->class('col-md-2 form-control-label')->for('exchange_rate') }}

                        <div class="col-md-9">
                            {{ html()->text('exchange_rate')
                                ->class('form-control')
                                ->placeholder(__('labels.backend.currencyexchanges.exchange_rate'))
                                ->attribute('maxlength', 191) }}
                        </div><!--col-->
                        {{ html()->label(__('جنيه'))->class('col-md-1 form-control-label')->for('exchange_rate') }}
                    </div>

                </div><!--col-->
            </div><!--row-->
        </div><!--card-body-->

        <div class="card-footer clearfix">
            <div class="row">
                <div class="col">
                    {{ form_cancel(route('admin.currencyexchanges.index'), __('buttons.general.cancel')) }}
                </div><!--col-->

                <div class="col text-right">
                    {{ form_submit(__('buttons.general.crud.create')) }}
                </div><!--col-->
            </div><!--row-->
        </div><!--card-footer-->
    </div><!--card-->
    {{ html()->form()->close() }}
@endsection
