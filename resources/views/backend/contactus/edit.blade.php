@extends('backend.layouts.app')

@section('title', __('labels.backend.ContactUs.management') . ' | ' . __('labels.backend.ContactUs.edit'))


@section('content')
    {{ html()->form('POST', $data->url->update)->class('form-horizontal')->open() }}

    @method('PUT')

    <div class="card">
        <div class="card-body">


            <hr>



        </div><!--card-body-->

        <div class="card-footer clearfix">
        </div><!--card-footer-->
    </div><!--card-->
@endsection
