@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('labels.backend.ContactUs.management'))

@section('content')
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        {{ __('labels.backend.ContactUs.management') }}
                    </h4>
                </div><!--col-->
            </div><!--row-->

            <div class="row mt-4">
                <div class="col">
                    <div class="table-responsive">
                        <table class="table" id="result-table">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>@lang('labels.backend.ContactUs.name')</th>
                                <th>@lang('labels.backend.ContactUs.email')</th>
                                <th>@lang('labels.backend.ContactUs.phone')</th>
                                <th>@lang('labels.backend.ContactUs.message')</th>
{{--                                <th>@lang('labels.general.actions')</th>--}}
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div><!--col-->
            </div><!--row-->
        </div><!--card-body-->
    </div><!--card-->
@endsection

@push('after-scripts')
    <script>
        $dataTable('{{ route('admin.ContactUss.index') }}', [
            {data: 'DT_RowIndex', name: 'id'},
            {data: 'name', name: 'name'},
            {data: 'email', name: 'email'},
            {data: 'phone', name: 'phone'},
            {data: 'message', name: 'message'},
            // {data: 'action', name: 'action', orderable: false, searchable: false}
        ]);
    </script>
@endpush
