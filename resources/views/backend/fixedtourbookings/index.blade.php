@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('labels.backend.fixedtourbookings.management'))

@section('content')
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        {{ __('labels.backend.fixedtourbookings.management') }}
                    </h4>
                </div><!--col-->

{{--                <div class="col-sm-7">--}}
{{--                    <div class="btn-toolbar float-right" role="toolbar" aria-label="@lang('labels.general.toolbar_btn_groups')">--}}
{{--                        <a href="{{ route('admin.fixedtourbookings.create') }}" class="btn btn-success ml-1" data-toggle="tooltip"--}}
{{--                           title="@lang('labels.general.create_new')"><i class="fas fa-plus-circle"></i></a>--}}
{{--                    </div><!--btn-toolbar-->--}}
{{--                </div><!--col-->--}}
            </div><!--row-->

            <div class="row mt-4">
                <div class="col">
                    <div class="table-responsive">
                        <table class="table" id="result-table">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>@lang('labels.backend.fixedtourbookings.customer')</th>
                                <th>@lang('labels.backend.fixedtourbookings.email')</th>
                                <th>@lang('labels.backend.fixedtourbookings.phone')</th>
                                <th>@lang('labels.backend.fixedtourbookings.fixed_tour')</th>
                                <th>@lang('labels.backend.fixedtourbookings.from')</th>
                                <th>@lang('labels.backend.fixedtourbookings.to')</th>
                                <th>@lang('labels.backend.fixedtourbookings.total_cost')</th>
                                <th>@lang('labels.backend.fixedtourbookings.status')</th>
                                <th>@lang('labels.general.actions')</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div><!--col-->
            </div><!--row-->
        </div><!--card-body-->
    </div><!--card-->
@endsection

@push('after-scripts')
    <script>
        $dataTable('{{ route('admin.fixedtourbookings.index') }}', [
            {data: 'DT_RowIndex', name: 'id'},
            {data: 'customer.name', name: 'customer.name'},
            {data: 'customer.email', name: 'customer.email'},
            {data: 'customer.phone', name: 'customer.phone'},
            {data: 'fixed_tour.name', name: 'fixedTour.translation(app()->getLocal()).name'},
            {data: 'from', name: 'from'},
            {data: 'to', name: 'to'},
            {data: 'total_cost', name: 'total_cost'},
            {data: 'status', name: 'status'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ]);
    </script>
@endpush
