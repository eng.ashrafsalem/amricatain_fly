@extends('backend.layouts.app')

@section('title', __('labels.backend.fixedtourbookings.management') . ' | ' . __('labels.backend.fixedtourbookings.edit'))
@push('after-styles')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
@endpush

@push('after-scripts')
    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
    <script>
        flatpickr('.date', {
            // enableTime: true,
            dateFormat: "Y-m-d",
        })

    </script>
@endpush

@section('content')
    {{ html()->form('POST', $data->url->update)->class('form-horizontal')->open() }}

    @method('PUT')

    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        @lang('labels.backend.fixedtourbookings.management')
                        <small class="text-muted">@lang('labels.backend.fixedtourbookings.edit')</small>
                    </h4>
                </div><!--col-->
            </div><!--row-->

            <hr>

            <div class="row mt-4 mb-4">
                <div class="col">

                    <div class="form-group row">
                        {{ html()->label(__('labels.backend.fixedtourbookings.customer'))->class('col-md-2 form-control-label')->for('customer_id') }}

                        <div class="col-md-10">
                            {{ html()->text('customer_id')
                                ->class('form-control')
                                ->attribute('readonly', true)
                                ->value($data->customer->name)
                                }}
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                        {{ html()->label(__('labels.backend.fixedtourbookings.fixed_tour'))->class('col-md-2 form-control-label')->for('fixed_tour_id') }}

                        <div class="col-md-10">
                            {{ html()->text('fixed_tour_id')
                                ->class('form-control')
                                ->placeholder(__('labels.backend.fixedtourbookings.fixed_tour_id'))
                                ->attribute('readonly', true)
                                ->value($data->fixedTour->name)
                                }}
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                        {{ html()->label(__('labels.backend.fixedtourbookings.from'))->class('col-md-2 form-control-label')->for('from') }}

                        <div class="col-md-10">
                            {{ html()->text('from')
                                ->class(['form-control', 'date'])
                                ->placeholder(__('labels.backend.fixedtourbookings.from'))
                                ->attribute('maxlength', 191)
                                ->attribute('required')
                                ->value($data->from)
                                ->autofocus() }}
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                        {{ html()->label(__('labels.backend.fixedtourbookings.to'))->class('col-md-2 form-control-label')->for('to') }}

                        <div class="col-md-10">
                            {{ html()->text('to')
                                ->class(['form-control', 'date'])
                                ->placeholder(__('labels.backend.fixedtourbookings.to'))
                                ->attribute('maxlength', 191)
                                ->attribute('required')
                                 ->value($data->to)
                                ->autofocus() }}
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                        {{ html()->label(__('labels.backend.fixedtourbookings.total_cost'))->class('col-md-2 form-control-label')->for('total_cost') }}

                        <div class="col-md-10">
                            {{ html()->text('total_cost')
                                ->class('form-control')
                                ->placeholder(__('labels.backend.fixedtourbookings.total_cost'))
                                ->attribute('maxlength', 191)
                                ->value($data->total_cost)
                                }}
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                        {{ html()->label(__('labels.backend.fixedtourbookings.adult_count'))->class('col-md-2 form-control-label')->for('adult_count') }}

                        <div class="col-md-10">
                            {{ html()->text('adult_count')
                                ->class('form-control')
                                ->placeholder(__('labels.backend.fixedtourbookings.adult_count'))
                                ->attribute('maxlength', 191)
                                ->value($data->adult_count)
                                }}
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                        {{ html()->label(__('labels.backend.fixedtourbookings.child_count'))->class('col-md-2 form-control-label')->for('child_count') }}

                        <div class="col-md-10">
                            {{ html()->text('child_count')
                                ->class('form-control')
                                ->placeholder(__('labels.backend.fixedtourbookings.child_count'))
                                ->attribute('maxlength', 191)
                                ->value($data->child_count)
                                }}
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                        {{ html()->label(__('labels.backend.fixedtourbookings.baby_count'))->class('col-md-2 form-control-label')->for('baby_count') }}

                        <div class="col-md-10">
                            {{ html()->text('baby_count')
                                ->class('form-control')
                                ->placeholder(__('labels.backend.fixedtourbookings.baby_count'))
                                ->attribute('maxlength', 191)
                                ->value($data->baby_count)
                                }}
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                        {{ html()->label(__('labels.backend.fixedtourbookings.status'))->class('col-md-2 form-control-label')->for('shown') }}

                        <div class="col-md-10">
                            <select  required class="form-control" name="status">
                                {{ html()->option(__('labels.backend.fixedtourbookings.status'))
                                        ->class('form-control')
                                        ->value('')
                                        ->attribute('disabled',true)
                                        ->selected()
                                         }}
                                {{ html()->option(__('labels.backend.fixedtourbookings.pending'))
                                      ->class('form-control')
                                      ->value('pending')
                                      ->attribute($data->status == 'pending' ? 'selected' : '')
                                       }}
                                {{ html()->option(__('labels.backend.fixedtourbookings.confirmed'))
                                      ->class('form-control')
                                      ->value('confirmed')
                                      ->attribute($data->status == 'confirmed' ? 'selected' : '')
                                       }}
                                {{ html()->option(__('labels.backend.fixedtourbookings.canceled'))
                                      ->class('form-control')
                                      ->value('canceled')
                                      ->attribute($data->status == 'canceled' ? 'selected' : '')
                                       }}
                            </select>
                        </div><!--col-->
                    </div><!--form-group-->

                </div><!--col-->
            </div><!--row-->
        </div><!--card-body-->

        <div class="card-footer clearfix">
            <div class="row">
                <div class="col">
                    {{ form_cancel(route('admin.fixedtourbookings.index'), __('buttons.general.cancel')) }}
                </div><!--col-->

                <div class="col text-right">
                    {{ form_submit(__('buttons.general.crud.edit')) }}
                </div><!--col-->
            </div><!--row-->
        </div><!--card-footer-->
    </div><!--card-->
    {{ html()->form()->close() }}
@endsection
