@extends('frontend.layouts.app')

{{--@section('title', app_name() . ' | ' . __('navs.general.home'))--}}

@push('after-styles')
    <style>
        /* .price-box
        {
            padding: 20px;
            border: 1px solid white;
            background: red;
            margin: 10px;
            width: 137px;
        } */
    </style>
@endpush

@push('after-scripts')
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBIATE_TQXlrdKhFo2QxQdIulT2kvm51AI&libraries=places" type="text/javascript"></script>
    <script>
        // In your Javascript (external .js resource or <script> tag)
        var lat={{$hotel->map_lat}};
        var lng={{$hotel->map_long}};
        $(document).ready(function() {
            map=new google.maps.Map(document.getElementById('map-canvas'),{
          center:{
            lat:lat,
            lng:lng,
          },
        zoom:15
    });

    var marker = new google.maps.Marker({
    position: {
    lat: lat, 
    lng: lng,
    },
    map: map,
    draggable:false,
});

        });

    </script>
@endpush

@section('content')

<section class="breadcrumb">
    <div class="overlay"></div>
    <div class="content text-center">
        <h1>@lang('labels.frontend.hotels.hotel_details') </h1>
        <ul class="navs">
            <li><a href="{{ route('frontend.index') }}">@lang('navs.frontend.main') </a></li>
            <span>/</span>
            <li class="active"><a href="#" class="active">@lang('labels.frontend.hotels.hotel_details')  </a></li>
        </ul>
    </div>
</section>
<section class="page-content padding-mobile">
    <div class="container">
        <div class="card-big">
            <div id="owl-demo6" class="owl-carousel">
                @foreach($slider_images as $image)
                    <div class="item">
                        <div class="img-container">
                            <img src="{{str_replace("public/", "", $image->image_url)   }}">
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="card-body">
                <ul class="features">
                    <li> {{ $hotel->city->country->name }}, {{ $hotel->city->name }}</li>
                    <li></li>
                </ul>
                <h1>{{ $hotel->name }}</h1>
                <div class="features-list clearfix">
                    <ul class="card-list left">
                       @foreach($hotel->services as $service)
                          <li>{{ $service->name }}<span><i class="fa fa-check" style="margin-left:5px"></i></span></li>
                       @endforeach
                    </ul>
                </div>
            </div>
            <div class="card-footer">
            @if(isset($hotel->map_lat))
            <div id="map-canvas" style="height:500px; width:100%;margin-bottom:30px;">

             </div>
             @endif
                <div class="button text-center">
                
                        <a class="btn btn-primary btn-lg" href="{{ route('frontend.singleHotelBooking', ['hotel' => $hotel->id]) }}">@lang('labels.frontend.hotels.register')</a>
                                   </div>
            </div>
        </div>
    </div>
</section>

@endsection
