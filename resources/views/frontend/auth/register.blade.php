@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('labels.frontend.auth.register_box_title'))

@section('content')
<section class="breadcrumb">
        <div class="overlay"></div>
        <div class="content text-center">
            <h1>@lang('labels.frontend.auth.register_box_title')</h1>
            <ul class="navs">
                <li><a href="{{ route('frontend.index') }}">@lang('navs.frontend.main')</a></li>
                <span>/</span>
                <li class="active"><a href="{{ route('frontend.auth.register') }}" class="active">@lang('labels.frontend.auth.register_box_title')</a></li>
            </ul>
        </div>
    </section>
    @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
<section class="page-content padding-mobile" style="margin-top:220px">
        <div class="container">
            <div class="card-big">
                {{ html()->form('POST',  route('frontend.register'))->open() }}
                    @csrf
                    <div class="row">
                        <div class="card-content">

                            <div class="col-lg-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <input type="text" name="name" class="form-control" placeholder="@lang('labels.frontend.customers.name')">
                                </div>
                                <div class="form-group">
                                    <input type="email" name="email" class="form-control" placeholder="@lang('labels.frontend.customers.email')">
                                </div>
                                <div class="form-group">
                                    <input type="password" name="password" class="form-control" placeholder="@lang('labels.frontend.customers.password')">
                                </div>
                                <div class="form-group">
                                    <input type="number" name="phone" min="0" class="form-control" placeholder="@lang('labels.frontend.customers.phone')">
                                </div>
                            </div>
                            <div class="col-lg-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <input type="number" name="age" min="0" class="form-control" placeholder="@lang('labels.frontend.customers.age')">
                                </div>
                                <div class="form-group">
                                    <input type="text" name="address" class="form-control" placeholder="@lang('labels.frontend.customers.address')">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">@lang('labels.frontend.customers.gender')</label>
                                    <div class="radio">
                                        <label><input type="radio" name="gender" value="male" checked>@lang('labels.frontend.customers.male')</label>
                                    </div>
                                    <div class="radio">
                                        <label><input type="radio" name="gender" value="female">@lang('labels.frontend.customers.female')</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class=""></div>
                    </div>



                <div class="button text-center">
                    <button type="submit" class="btn btn-primary btn-lg text-center mt-20" data-remodal-action="confirm">@lang('labels.frontend.customers.register')</button>
                </div>
                {{ html()->form()->close() }}
            </div>
        </div>
    </section>

@endsection

@push('after-scripts')
    @if(config('access.captcha.registration'))
        @captchaScripts
    @endif
@endpush
