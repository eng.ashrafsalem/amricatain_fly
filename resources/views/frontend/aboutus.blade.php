@extends('frontend.layouts.app')

{{--@section('title', app_name() . ' | ' . __('navs.general.home'))--}}

@section('content')
    <section class="breadcrumb">
        <div class="overlay"></div>
        <div class="content text-center">
            <h1>@lang('navs.frontend.about-us')</h1>
            <ul class="navs">
                <li><a href="{{ route('frontend.index') }}">@lang('navs.frontend.main')</a></li>
                <span>/</span>
                <li class="active"><a href="{{ route('frontend.aboutUs') }}" class="active">@lang('navs.frontend.about-us')</a></li>
            </ul>
        </div>
    </section>
    <section class="aboutus1 padding-mobile">
        <div class="container">
            <div class="row">
                @if(isset($all_about_us_section) && $all_about_us_section->count() != 0 )
                    @foreach($all_about_us_section as $about)
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="card text-center">
                                <div class="card-img">
                                    @if(isset(\App\Models\Image::where('imageable_type', \App\Models\Aboutuses\Aboutus::class)->where('imageable_id', $about->id)->first()->image_url ))
                                    <img src="{{\App\Models\Image::where('imageable_type', \App\Models\Aboutuses\Aboutus::class)->where('imageable_id', $about->id)->first()->image_url }}">
                                    @endif
                                </div>
                                <div class="card-body">
                                    <div class="content">
                                    @if($about->type_id == 1)
                                        <h4>@lang('labels.frontend.aboutUs.hint')</h4>
                                    @elseif($about->type_id == 2)
                                      <h4>@lang('labels.frontend.aboutUs.vision')</h4>
                                    @elseif($about->type_id == 3)
                                      <h4>@lang('labels.frontend.aboutUs.mission')</h4>
                                    @endif    
                                    <div class="desc"><p>{!!  $about->getTranslation(App::getLocale())['contents'] !!}</p></div>
                                      
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                 @endif
            </div>
        </div>
    </section>

@endsection
