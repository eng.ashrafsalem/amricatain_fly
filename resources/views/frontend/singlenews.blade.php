@extends('frontend.layouts.app')

{{--@section('title', app_name() . ' | ' . __('navs.general.home'))--}}
  @section('content')
<section class="breadcrumb">
        <div class="overlay"></div>
        <div class="content text-center">
            <h1>@lang('labels.frontend.news.details')</h1>
            <ul class="navs">
                <li><a href="{{route('frontend.index')}}">@lang('navs.frontend.main')</a></li>
                <span>/</span>
                <li><a href="{{ route('frontend.allnews') }}">@lang('labels.frontend.news.news')</a></li>
                <span>/</span>
                <li class="active"><a href="{{ route('frontend.singleNews', ['news' => $news->id]) }}" class="active">@lang('labels.frontend.news.details')</a></li>
            </ul>
        </div>
    </section>
    <section class="page-content padding-mobile">
        <div class="container">
            <div class="card-big">
                <div id="owl-demo6" class="owl-carousel">
                @if(isset($slider_images))
                @foreach($slider_images as $image)
                    <div class="item">
                        <div class="img-container">
                            <img src="{{str_replace("public/", "", $image->image_url)}}">
                        </div>
                    </div>
                @endforeach
                @endif
                </div>
                <div class="card-body">
{{--                    <ul class="features">--}}
{{--                        <li><i class="fa fa-calendar"></i> {{$news->news_date}} </li>--}}
{{--                    </ul>--}}

                    <h1>{{ $news->translate(app()->getLocale())['title'] }}</h1>

                    <div class="card-text">
                        <p class="lead">
                        {{ $news->translate(app()->getLocale())['description'] }}
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
