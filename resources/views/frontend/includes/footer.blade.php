<div id="subscribe" class="subscribe-section padding-mobile">
        <div class="container text-center">
            <div class="subscribe-content">
                @if(app()->getLocale() == "ar")
                <img class="logo" src="/images/logo-arabic-white.png" alt="logo" />
                @else
                <img class="logo" src="/images/logo.png" alt="logo" />
                @endif

            </div>
            <div class="clearfix"></div>
            <ul class="social-link">
                <li><a href="" id="fburl"><i class="fa fa-facebook"></i></a></li>
                <li><a href="" id="gourl"><i class="fa fa-google-plus"></i></a></li>
                <li><a href="" id="instaurl"><i class="fa fa-instagram"></i></a></li>

            </ul><!-- /.social_link -->
        </div>
    </div><!-- Subscribe Section -->
<footer class="footer-section align-center">
    <div class="container">
        <p>&copy; 2020 حقوق الملكية لدي الامريكتين</p>
    </div>
</footer><!-- /.footer_section -->
<!-- Scripts -->
@stack('before-scripts')
{{--{!! script(mix('js/manifest.js')) !!}--}}
{{--{!! script(mix('js/vendor.js')) !!}--}}
{{--{!! script(mix('js/frontend.js')) !!}--}}

<script src="{{ asset('js/frontend/jquery-2.2.4.min.js') }}"></script>
<script src="{{ asset('js/frontend/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/frontend/jquery.countTo.js') }}"></script>
<script src="{{ asset('js/frontend/owl.carousel.min.js') }}"></script>
<script src="{{ asset('js/frontend/jquery.waterwheelCarousel.js') }}"></script>
<script src="{{ asset('js/frontend/parallaxie.js') }}"></script>
<script src="{{ asset('js/frontend/plugins.js') }}"></script>
<script src="{{ asset('js/frontend/aos.js') }}"></script>
<script>
    AOS.init({
        once: true,
        duration: 1000,
        offset: 0,
    });
</script>
<script>
    $('.parallaxie').parallaxie();
</script>

<script>
$('div.alert').delay(5000).slideUp(300);
</script>
<script>
    $.ajax({
        method: 'GET',
        url: 'FaceBookUrl',
        dataType: 'json',
        data: { _method: 'GET'},
        success: function (result) {
            var a = document.getElementById('fburl'); //or grab it by tagname etc
            a.href = result.value
        },
    });
    $.ajax({
        method: 'GET',
        url: 'GoogleUrl',
        dataType: 'json',
        data: { _method: 'GET'},
        success: function (result) {
            var a = document.getElementById('gourl'); //or grab it by tagname etc
            a.href = result.value
        },
    });
    $.ajax({
        method: 'GET',
        url: 'InstagramUrl',
        dataType: 'json',
        data: { _method: 'GET'},
        success: function (result) {
            var a = document.getElementById('instaurl'); //or grab it by tagname etc
            a.href = result.value
        },
    });

</script>

@stack('after-scripts')

{{--@include('includes.partials.ga')--}}
