<!-- navbar -->

<nav class="navbar navbar-inverse affix" data-offset-top="500">
    <div class="container-fluid">
        <a class="navbar-brand" href="{{ route('frontend.index') }}">
            @if(app()->getLocale() == "ar")
             <img class="img-responsive" src="{{ asset('images/logo-arabic.png') }}">
            @else
        <img class="img-responsive" src="{{ asset('images/logo-dark.png') }}">
            @endif
        </a>
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        @if(app()->getLocale() == "ar")
            <ul class="nav navbar-nav navbar-left">
        @else
            <ul class="nav navbar-nav navbar-right">
       @endif
                <li class="active">
                    <a href="{{ route('frontend.index') }}">@lang('navs.frontend.main')<span class="sr-only">(current)</span></a>
                </li>
                <li><a href="{{ route('frontend.aboutUs') }}">@lang('navs.frontend.about-us')</a></li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="{{ route('frontend.index') }}">@lang('navs.frontend.fixed-tours')
                        <span class="caret"></span></a>
                    <ul class="dropdown-menu">

                    @if(isset($categories) && $categories->count() != 0)
                            @foreach($categories as $cat)
                                <li><a href="{{ route('frontend.fixedToursByCategory', ['category' => $cat->id ]) }}">{{ $cat->translate(app()->getLocale())['name'] }}</a></li>
                            @endforeach
                        @endif
                    </ul>
                </li>
                <li><a href="{{ route('frontend.allHotels') }}">@lang('navs.frontend.hotels')</a></li>
                <li><a href="{{ route('frontend.CustomTour') }}">@lang('navs.frontend.custom-tour')</a></li>
                <li><a href="{{ route('frontend.allnews') }}">@lang('navs.frontend.news')</a></li>
                <li><a href="{{ route('frontend.contact') }}">@lang('navs.frontend.contact-us')</a></li>
                @if(auth('customer')->user())

                    <li><a href="{{ route('frontend.customers.logout') }}" class=" btn btn-primary logout" style="
                            padding: 10px 20px;
                            margin-top: 10px;
                            color:#fff;
                           ">
                             @lang('navs.general.logout')
                        </a></li>
                @else

                    <li><button data-toggle="modal" data-target="#myModal" class="btn btn-primary">@lang('navs.frontend.login')</button></li>
{{--                    <li><a href="{{ route('frontend.logout2') }}" class="btn btn-primary">@lang('navs.frontend.logout')</a></li>--}}
                @endif

               <li>
                   @include('includes.partials.lang')
               </li>

                <!--li class="dropdown language">
                    @if(config('locale.status') && count(config('locale.languages')) > 1)
                        <li class="nav-item dropdown-toggle">
                            <a href="#" class="nav-link dropdown-toggle" id="navbarDropdownLanguageLink" data-toggle="dropdown"
                               aria-haspopup="true" aria-expanded="false">
                                <div class="flag">
                                    @lang('menus.language-picker.language')
                                    ({{ strtoupper(app()->getLocale()) }})
                                    <img class="img-responsive img-rounded" src="{{ asset('images/'.app()->getLocale().'.png') }}" style="display:inline-block;">
                                </div>
                            </a>

                        </li>
                    @endif
                </li-->
            </ul>
        </div>
    </div><!-- /.navbar-collapse -->

    @if(session('msg'))
<div class="alert alert-success">
        {{ session('msg') }}
  </div>
@endif
@if(session('error'))
  <div class="alert alert-danger">
    {{session('error')}}
  </div>
  @endif
</nav>


 <!-- modal login  -->
    <!-- ***************** -->
    @include('frontend.partials.login')
