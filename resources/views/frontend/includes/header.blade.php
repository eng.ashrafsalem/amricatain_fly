<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title> @yield('title', app_name()) </title>

    {{-- See https://laravel.com/docs/5.5/blade#stacks for usage --}}
    @stack('before-styles')

<!-- Check if the language is set to RTL, so apply the RTL layouts -->
    <!-- Otherwise apply the normal LTR layouts -->
{{--    {{ style(mix('css/frontend.css')) }}--}}
    <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    @if(app()->getLocale() == "ar")
    <link rel="stylesheet" href="css/bootstrap-rtl.min.css">
    @endif
    <link rel="stylesheet" href="{{ asset('css/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/owl.theme.default.min.css') }}">
    @if(app()->getLocale() == "ar") 
    <link rel="stylesheet" href="{{ asset('ar/css/main.css') }}">
    @else
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">
    @endif
    <link rel="stylesheet" href="{{ asset('css/aos.css') }}">
    <link href="https://fonts.googleapis.com/css?family=El+Messiri:400,500,700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.css" />

    <!--[if lt IE 9]>
    <script src="{{ asset('js/frontend/html5shiv.min.js') }}"></script>
    <script src="{{ asset('js/frontend/respond.min.js') }}"></script>
    <![endif]-->

    @stack('after-styles')


</head>
