@extends('frontend.layouts.app')

{{--@section('title', app_name() . ' | ' . __('navs.general.home'))--}}

@push('after-styles')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
@endpush

@push('after-scripts')
    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
    <script>
        flatpickr('.date', {
            // enableTime: true,
            dateFormat: "Y-m-d",
        })

    </script>
@endpush


@section('content')
    @if(auth('customer')->user())
    <section class="breadcrumb">
        <div class="overlay"></div>
        <div class="content text-center">
            <h1>@lang('labels.frontend.hotels.hotel_details') </h1>
            <ul class="navs">
                <li><a href="{{ route('frontend.index') }}">@lang('navs.frontend.main') </a></li>
                <span>/</span>
                <li class="active"><a href="#" class="active">@lang('labels.frontend.hotels.hotel_details')  </a></li>
            </ul>
        </div>
    </section>
    <section class="page-content padding-mobile">
        <div class="container">
            <div class="card-big">
                {{ html()->form('POST',  route('frontend.saveSingleHotelBooking'))->open() }}
                @csrf
                <div class="row">
                    <div class="card-content">
                        <input type="hidden" name="tour_id" value="{{ $hotel->id }}">
                        <div class="col-lg-6 col-sm-6 col-xs-12">

                            <input type="hidden" name="customer_id" value="{{auth('customer')->id()}}">
                            <div class="form-group">
                                <label for="hotel_id">@lang('labels.frontend.hotels.hotel_details')</label>
                                <input type="hidden" id="hotel_id" name="hotel_id" class="form-control" readonly  value="{{ $hotel->id }}">
                            </div>
                            <div class="form-group">
                                <label for="from">@lang('labels.frontend.hotels.from')</label>
                                <input type="test" id="from" name="from" class="form-control date" placeholder="@lang('labels.frontend.hotels.from')" required >
                            </div>
                            <div class="form-group">
                                <label for="to">@lang('labels.frontend.hotels.to')</label>
                                <input type="text" id="to" name="to" class="form-control date" placeholder="@lang('labels.frontend.hotels.to')" required>
                            </div>
                        </div>
                        <div class="col-lg-6 col-sm-6 col-xs-12">

                            <div class="form-group">
                                <label for="adult_count">@lang('labels.frontend.hotels.adult_count')</label>
                                <input type="text" id="adult_count" name="adult_count" class="form-control" placeholder="@lang('labels.frontend.fixed_tour.adult_count')" required>
                            </div>
                            <div class="form-group">
                                <label for="child_count">@lang('labels.frontend.hotels.child_count')</label>
                                <input type="text" id="child_count" name="child_count" class="form-control" placeholder="@lang('labels.frontend.fixed_tour.child_count')" required>
                            </div>
                            <div class="form-group">
                                <label for="baby_count">@lang('labels.frontend.hotels.baby_count')</label>
                                <input type="text" id="baby_count" name="baby_count" class="form-control" placeholder="@lang('labels.frontend.fixed_tour.baby_count')" required>
                            </div>
                        </div>
                    </div>
                    <div class=""></div>
                </div>



                <div class="button text-center">
                    <button type="submit" class="btn btn-primary btn-lg text-center mt-20" style="color:white" data-remodal-action="confirm">@lang('labels.frontend.customers.register')</button>
                </div>
                {{ html()->form()->close() }}
            </div>
        </div>
    </section>
    @else
        <div class="text-center" style="margin-top: 200px;">
            <a href="#myModal" data-toggle="modal" class="btn btn-primary btn-lg" style="color:white!important;">@lang('labels.frontend.fixed_tour.register')</a>
        </div>

        @include('frontend.partials.login')
    @endauth
@endsection
