@extends('frontend.layouts.app')

{{--@section('title', app_name() . ' | ' . __('navs.general.home'))--}}


@section('content')

    <section class="breadcrumb">
        <div class="overlay"></div>
        <div class="content text-center">
            <h1>@lang('labels.frontend.hotels.all_hotels')</h1>
            <ul class="navs">
                <li><a href="{{ route('frontend.index') }}">@lang('navs.frontend.main')</a></li>
                <span>/</span>
                <li class="active"><a href="{{ route('frontend.allHotels')}}" class="active">@lang('labels.frontend.hotels.all_hotels')</a></li>
            </ul>
        </div>
    </section>
    <section class="page-content padding-mobile">
        <div class="container">
            <div class="row">
                @foreach($hotels as $hotel)
                    <div class="col-lg-4 col-sm-6 col-xs-12">
                        <div class="card">
                            <div class="card-img">
                                <img class="img-responsive" src="{{str_replace("public/", "", \App\Models\Image::where('imageable_type', \App\Models\Hotels\Hotel::class)->where('imageable_id', $hotel->id)->where('main_image', 1)->first()->image_url) }}">
                            </div>
                            <div class="card-body">
                                <div class="content">
                                    <a href="{{ route('frontend.singleHotel', ['hotel' => $hotel->id]) }}">
                                        <h4>{{ $hotel->name }}</h4>
                                    </a>
                                    <p>
                                        @for($i = 0 ; $i < $hotel->level; $i++)
                                            <i class="fa fa-star"></i>
                                        @endfor
                                        {{ $hotel->level }}
                                    </p>
                                    <ul class="features">
                                        @foreach($hotel->services as $service)
                                        <li>{{ $service->name }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="price-box">

                                        <a href="{{ route('frontend.singleHotel', ['hotel' => $hotel->id]) }}" style="color:white" ><span>@lang('labels.frontend.hotels.register')</span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
@endsection
