@extends('frontend.layouts.app')

{{--@section('title', app_name() . ' | ' . __('navs.general.home'))--}}

@push('after-scripts')
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBIATE_TQXlrdKhFo2QxQdIulT2kvm51AI&libraries=places" type="text/javascript"></script>
    <script>
        // In your Javascript (external .js resource or <script> tag)
        var lat={{$tour->map_lat}};
        var lng={{$tour->map_long}};
        $(document).ready(function() {
            map=new google.maps.Map(document.getElementById('map-canvas'),{
          center:{
            lat:lat,
            lng:lng,
          },
        zoom:15
    });

    var marker = new google.maps.Marker({
    position: {
    lat: lat, 
    lng: lng,
    },
    map: map,
    draggable:false,
});

        });

    </script>
@endpush


@section('content')

    <section class="breadcrumb">
        <div class="overlay"></div>
        <div class="content text-center">
            <h1>{{ $tour->translate(app()->getLocale())->name }} @lang('labels.frontend.fixed_tour.program') </h1>
            <ul class="navs">
                <li><a href="{{ route('frontend.index') }}">@lang('navs.frontend.main')</a></li>
                <span>/</span>
                <li class="active"><a href="{{ route('frontend.fixedTour',['fixed_tour', $tour->id]) }}" class="active">@lang('labels.frontend.fixed_tour.program')</a></li>
            </ul>
        </div>
    </section>

    <section class="page-content padding-mobile">
        <div class="container">
            <div class="card-big">
                <div id="owl-demo3" class="owl-carousel">
                    @foreach($slider_images as $image)
                    <div class="item">
                        <div class="img-container">
                            <img src="{{str_replace("public/", "", $image->image_url)   }}">
                        </div>
                    </div>
                    @endforeach
                </div>
                <div class="card-body">
                    <ul class="features">
                        <li><i class="fa fa-calendar"></i>@lang('labels.frontend.fixed_tour.from') : {{ $tour->from }}</li>
                        <li><i class="fa fa-calendar"></i>@lang('labels.frontend.fixed_tour.to') : {{ $tour->to }}</li>
{{--                        <li>127 فرد</li>--}}
                        <li>{{ $tour->hotel->name }}</li>
                    </ul>
                    <h1>{{ $tour->translate(app()->getLocale())->name }}</h1>
                    <div class="card-text">
                        <p class="lead">
                           {{ $tour->translate(app()->getLocale())->description }}
                        </p>
                    </div>
                    @if(isset($tour->map_lat))
            <div id="map-canvas" style="height:500px; width:100%;margin-bottom:30px;">

             </div>
             @endif
                    <div class="button text-center">
                        <a href="{{ route('frontend.fixedTourBooking', ['fixed_tour' => $tour->id]) }}"  class="btn btn-primary btn-lg">@lang('labels.frontend.fixed_tour.register')</a>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
