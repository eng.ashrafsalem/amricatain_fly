@extends('frontend.layouts.app')

{{--@section('title', app_name() . ' | ' . __('navs.general.home'))--}}

@section('content')

<section class="breadcrumb">
        <div class="overlay"></div>
        <div class="content text-center">
            <h1>@lang('labels.frontend.news.news')</h1>
            <ul class="navs">
                <li><a href="{{route('frontend.index')}}">@lang('navs.frontend.main')</a></li>
                <span>/</span>
                <li class="active"><a href="{{route('frontend.allnews')}}" class="active">@lang('labels.frontend.news.news')</a></li>
            </ul>
        </div>
    </section>
    <section class="page-content padding-mobile">
        <div class="container">
            <div class="row">
               @foreach($allnews as $news)
                <div class="col-lg-4 col-sm-6 col-xs-12">
                    <div class="card news-card">
                        <div class="card-img">
                        @if(isset(\App\Models\Image::where('imageable_type', \App\Models\FrontNews\FrontNews::class)->where('imageable_id', $news->id)->first()->image_url))
                          <img class="img-responsive" src="{{str_replace("public/", "", \App\Models\Image::where('imageable_type', \App\Models\FrontNews\FrontNews::class)->where('imageable_id', $news->id)->first()->image_url)   }}">
                        @endif
                        </div>
                        <div class="card-body">
                            <div class="content">
{{--                                <div class="date">--}}
{{--                                    <span>{{$news->news_date}}</span>--}}
{{--                                </div>--}}
                                <a href="{{ route('frontend.singleNews', ['news' => $news->id]) }}">
                                    <h4>{{ $news->translate(app()->getLocale())['title'] }}</h4>
                                </a>
                                <div class="news-desc">
                                <p>
                                {{ $news->translate(app()->getLocale())['description'] }}
                                </p>
                                </div>

                            </div>
                        </div>
                        <div class="card-footer">
                            <a class="btn btn-primary btn-lg" href="{{ route('frontend.singleNews', ['news' => $news->id]) }}">@lang('labels.frontend.aboutUs.more')</a>
                        </div>
                    </div>
                </div>
              @endforeach
            </div>
            {{ $allnews->links() }}

        </div>
    </section>
@endsection
