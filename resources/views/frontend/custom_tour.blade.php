@extends('frontend.layouts.app')

{{--@section('title', app_name() . ' | ' . __('navs.general.home'))--}}

@push('after-styles')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
@endpush

@push('after-scripts')
    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
    <script>
        flatpickr('.date', {
            // enableTime: true,
            dateFormat: "Y-m-d",
        })

    </script>
@endpush

@section('content')
<section class="breadcrumb">
        <div class="overlay"></div>
        <div class="content text-center">
            <h1>@lang('labels.frontend.custom_tour.special_program')</h1>
            <ul class="navs">
                <li><a href="{{ route('frontend.index') }}">@lang('navs.frontend.main')</a></li>
                <span>/</span>
                <li class="active"><a href="{{ route('frontend.CustomTour') }}" class="active">@lang('labels.frontend.custom_tour.special_program')</a></li>
            </ul>
        </div>
    </section>
@if(auth('customer')->user())



    <!-- start about special program section  -->
    <!-- ********************************* -->
    <!-- ******************************** -->
    <section class="aboutus padding-mobile">
        <div class="container">

        @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="content">
                        <h1>@lang('labels.frontend.custom_tour.special_register')</h1>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="img-container">
                        <img class="img-responsive" src="/images/17.jpg">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="special-reservation padding-mobile">
        <div class="container">
            <h1 class="text-center">@lang('labels.frontend.hotelbookings.register')</h1>
            {{ html()->form('POST',  route('frontend.saveCustomTourBooking'))->open() }}
                    @csrf
            <div class="row">
                <div class="col-xs-12">
                    <div class="form-group col-lg-12">
                        <select class="form-control input-lg" name="category_id" required>
                        <option disabled selected>@lang('labels.frontend.custom_tour.category')</option>
                        @foreach($categories as $category)
                            <option value="{{$category->id}}">{{ $category->translate(app()->getLocale())->name }}</option>
                        @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <input type="test" id="from" name="from" class="form-control date" placeholder="@lang('labels.frontend.custom_tour.from')" required >
                    </div>
                    <div class="form-group">
                        <input type="text" id="to" name="to" class="form-control date" placeholder="@lang('labels.frontend.custom_tour.to')" required>
                    </div>

                    <div class="col-lg-12 col-xs-12">
                        <div class="form-group">
                            <textarea class="form-control input-lg" name="notes" placeholder="الملاحظات"></textarea>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="form-group col-lg-6 col-sm-6 col-xs-12">
                        <input type="number" class="form-control input-lg" name="adult_count" placeholder="@lang('labels.frontend.fixed_tour.adult_count')" required>
                    </div>
                    <div class="form-group col-lg-6 col-sm-6 col-xs-12">
                        <input type="number" class="form-control input-lg" name="child_count" placeholder="@lang('labels.frontend.fixed_tour.child_count')" required>
                    </div>
                    <div class="form-group col-lg-6 col-sm-6 col-xs-12">
                        <input type="number" class="form-control input-lg" name="baby_count" placeholder="@lang('labels.frontend.fixed_tour.baby_count')" required>
                    </div>
                    <div class="form-group col-lg-6 col-sm-6 col-xs-12">
                        <select class="form-control input-lg" name="hotel_id" required>
                            <option disabled selected>@lang('labels.frontend.hotels.hotel_details')</option>
                            @foreach($hotels as $hotel)
                             <option value="{{$hotel->id}}">{{$hotel->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="button text-center">
               <button type="submit" class="btn btn-primary btn-lg text-center mt-20" data-remodal-action="confirm">@lang('labels.frontend.customers.register')</button>
            </div>
        </div>
    </section>
    @else
        <div class="text-center" style="padding-top: 100px; padding-bottom: 100px; height: 100vh">
            <a href="#myModal" data-toggle="modal" class="btn btn-primary btn-lg">@lang('labels.frontend.fixed_tour.register')</a>
        </div>

        @include('frontend.partials.login')
    @endauth
@endsection
