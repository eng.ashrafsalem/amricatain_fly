@extends('frontend.layouts.app')

{{--@section('title', app_name() . ' | ' . __('navs.general.home'))--}}

@section('content')

<section class="breadcrumb">
    <div class="overlay"></div>
    <div class="content text-center">
        <h1>@lang('labels.frontend.fixed_tour.programs')</h1>
        <ul class="navs">
            <li><a href="{{ route('frontend.index') }}">@lang('navs.frontend.main')</a></li>
            <span>/</span>
            <li class="active"><a href="{{ route('frontend.fixedTours')}}" class="active">@lang('labels.frontend.fixed_tour.programs')</a></li>
        </ul>
    </div>
</section>
@if(isset($tours) && $tours->count() != 0 )

    <section class="page-content padding-mobile">
    <div class="container">
        <div class="row">
            @foreach($tours as $tour)
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="card">
                        <a href="{{ route('frontend.fixedTour', ['fixed_tour' => $tour->id]) }}">
                            <div class="card-img">
                                <img src="{{str_replace("public/", "", \App\Models\Image::where('imageable_type', \App\Models\FixedTours\FixedTour::class)->where('imageable_id', $tour->id)->first()->image_url) }}">
                                <div class="discount text-center">
                                    {{$tour->discount}}%
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="content">
                                    <h4>{{ $tour->translate(app()->getLocale())->name }}</h4>
                                    <p>
                                        @for($i = 0 ; $i < $tour->hotel->level; $i++)
                                            <i class="fa fa-star"></i>
                                        @endfor
                                        {{ $tour->hotel->level }}</p>
                                    </p>

                                    <ul class="features">

                                        <li><i class="fa fa-globe"></i> {{ $tour->category->name }}</li>
                                        <li>
                                        <i class="fa fa-map-marker"></i>
                                        @if(isset($tour->city->name))
                                         {{ $tour->city->translate(app()->getLocale())['name']  }}
                                        @endif
                                        ,
                                        @if(isset($tour->city->country->name))
                                         {{ $tour->city->country->translate(app()->getLocale())['name']  }}
                                        @endif
                                       </li>

                                        <li><i class="fa fa-clock-o"></i>{{ $tour->days_nights_counts }}</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="price-box">
                                    <span>{{ $tour->total_cost }} </span>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            @endforeach
        </div>

{{--        <div class="row">--}}
{{--            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">--}}
{{--                <div class="card">--}}
{{--                    <div class="card-img">--}}
{{--                        <img src="images/1.jpg">--}}
{{--                    </div>--}}
{{--                    <div class="card-body">--}}
{{--                        <div class="content">--}}
{{--                            <a href="program.html">--}}
{{--                                <h4>مدينة نيو يورك</h4>--}}
{{--                            </a>--}}
{{--                            <p><i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> 2.4</p>--}}
{{--                            <ul class="features">--}}
{{--                                <li><i class="fa fa-map-marker"></i> نيو يورك , الولايات المتحدة</li>--}}
{{--                                <li><i class="fa fa-clock-o"></i> 5 ايام , 3 ليالي</li>--}}
{{--                            </ul>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="card-footer">--}}
{{--                        <div class="price-box">--}}
{{--                            <span>18000 جنيه</span>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">--}}
{{--                <div class="card">--}}
{{--                    <div class="card-img">--}}
{{--                        <img src="images/1.jpg">--}}
{{--                    </div>--}}
{{--                    <div class="card-body">--}}
{{--                        <div class="content">--}}
{{--                            <a href="program.html">--}}
{{--                                <h4>مدينة نيو يورك</h4>--}}
{{--                            </a>--}}
{{--                            <p><i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> 2.4</p>--}}
{{--                            <ul class="features">--}}
{{--                                <li><i class="fa fa-map-marker"></i> نيو يورك , الولايات المتحدة</li>--}}
{{--                                <li><i class="fa fa-clock-o"></i> 5 ايام , 3 ليالي</li>--}}
{{--                            </ul>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="card-footer">--}}
{{--                        <div class="price-box">--}}
{{--                            <span>18000 جنيه</span>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">--}}
{{--                <div class="card">--}}
{{--                    <div class="card-img">--}}
{{--                        <img src="images/1.jpg">--}}
{{--                    </div>--}}
{{--                    <div class="card-body">--}}
{{--                        <div class="content">--}}
{{--                            <a href="program.html">--}}
{{--                                <h4>مدينة نيو يورك</h4>--}}
{{--                            </a>--}}
{{--                            <p><i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> 2.4</p>--}}
{{--                            <ul class="features">--}}
{{--                                <li><i class="fa fa-map-marker"></i> نيو يورك , الولايات المتحدة</li>--}}
{{--                                <li><i class="fa fa-clock-o"></i> 5 ايام , 3 ليالي</li>--}}
{{--                            </ul>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="card-footer">--}}
{{--                        <div class="price-box">--}}
{{--                            <span>18000 جنيه</span>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="pagin">--}}
{{--            <ul class="pagination pagination-lg">--}}
{{--                <li><a href="#">1</a></li>--}}
{{--                <li class="active"><a href="#">2</a></li>--}}
{{--                <li><a href="#">3</a></li>--}}
{{--                <li><a href="#">4</a></li>--}}
{{--                <li><a href="#">5</a></li>--}}
{{--            </ul>--}}
{{--        </div>--}}
    </div>
</section>
@else
    <div style="padding-top: 150px; padding-bottom: 150px" class="h1 text-center">No Programs Available</div>
@endif

@endsection
