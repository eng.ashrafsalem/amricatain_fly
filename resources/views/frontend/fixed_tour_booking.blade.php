@extends('frontend.layouts.app')

{{--@section('title', app_name() . ' | ' . __('navs.general.home'))--}}

@section('content')

@if(auth('customer')->user())

        <section class="breadcrumb">
            <div class="overlay"></div>
            <div class="content text-center">
                <h1>{{ $tour->translate(app()->getLocale())->name }} @lang('labels.frontend.fixed_tour.program') </h1>
                <ul class="navs">
                    <li><a href="{{ route('frontend.index') }}">@lang('navs.frontend.main')</a></li>
                    <span>/</span>
                    <li class="active"><a href="{{ route('frontend.fixedTour',['fixed_tour', $tour->id]) }}" class="active">@lang('labels.frontend.fixed_tour.program')</a></li>
                </ul>
            </div>
        </section>

        <section class="page-content padding-mobile">
        <div class="container">
            <div class="card-big">
                {{ html()->form('POST',  route('frontend.saveFixedTourBooking'))->open() }}
                @csrf
                <div class="row">
                    <div class="card-content">
                        <input type="hidden" name="tour_id" value="{{ $tour->id }}">
                        <div class="col-lg-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label for="fixed_tour_id">@lang('labels.frontend.fixed_tour.tour')</label>
                                <input type="test" id="fixed_tour_id" name="fixed_tour_id" class="form-control" readonly  value="{{ $tour->name }}">
                            </div>
                            <div class="form-group">
                                <label for="from">@lang('labels.frontend.fixed_tour.from')</label>
                                <input type="test" id="from" name="from" class="form-control" readonly  value="{{ $tour->from }}">
                            </div>
                            <div class="form-group">
                                <label for="to">@lang('labels.frontend.fixed_tour.to')</label>
                                <input type="text" id="to" name="to" class="form-control" readonly  value="{{ $tour->to }}">
                            </div>
                        </div>
                        <div class="col-lg-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label for="total_cost">@lang('labels.frontend.fixed_tour.total_cost')</label>
                                <input type="text" id="total_cost" name="total_cost" class="form-control" readonly  value="{{ $tour->total_cost }}">
                            </div>
                            <div class="form-group">
                                <label for="adult_count">@lang('labels.frontend.fixed_tour.adult_count')</label>
                                <input type="text" id="adult_count" name="adult_count" class="form-control" placeholder="@lang('labels.frontend.fixed_tour.adult_count')" required>
                            </div>
                            <div class="form-group">
                                <label for="child_count">@lang('labels.frontend.fixed_tour.child_count')</label>
                                <input type="text" id="child_count" name="child_count" class="form-control" placeholder="@lang('labels.frontend.fixed_tour.child_count')" required>
                            </div>
                            <div class="form-group">
                                <label for="baby_count">@lang('labels.frontend.fixed_tour.baby_count')</label>
                                <input type="text" id="baby_count" name="baby_count" class="form-control" placeholder="@lang('labels.frontend.fixed_tour.baby_count')" required>
                            </div>
                        </div>
                    </div>
                    <div class=""></div>
                </div>



                <div class="button text-center">
                    <button type="submit" class="btn btn-primary btn-lg text-center mt-20" data-remodal-action="confirm">@lang('labels.frontend.customers.register')</button>
                </div>
                {{ html()->form()->close() }}
            </div>
        </div>
    </section>
    @else
        <div class="text-center" style="margin-top: 200px">
            <a href="#myModal" data-toggle="modal" class="btn btn-primary btn-lg" >@lang('labels.frontend.fixed_tour.register')</a>
        </div>

        @include('frontend.partials.login')
    @endif

@endsection
