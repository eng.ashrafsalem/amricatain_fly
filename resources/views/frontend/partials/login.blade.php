<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <div class="">
                    <img class="img-responsive logo center-block" src="{{ asset('images/logo-dark.png') }}">
                </div>
                <p class="mt-30 mb-20 text-center">
                    @lang('labels.frontend.customers.message1')
                    @lang('labels.frontend.customers.message2')
                    <a class="register" href="/register">
                        @lang('labels.frontend.customers.register')</a>
                </p>
                {{ html()->form('POST', route('frontend.customers.login'))->open() }}
                @csrf
                <div class="form-group">
                    <label for="email">@lang('labels.frontend.customers.email')</label>
                    <input type="email" name="email" class="form-control input-lg" placeholder="mymail@gmail.com" id="email">
                </div>
                <div class="form-group">
                    <label for="pwd">@lang('labels.frontend.customers.password')</label>
                    <input type="password" name ="password" class="form-control input-lg" id="pwd" placeholder="********">
                </div>
                <div class="checkbox">
                    {{ html()->label(html()->checkbox('remember', true, 1) . ' ' . __('labels.frontend.auth.remember_me'))->for('remember') }}
                </div>

                <div class="button text-center">
                    <button type="submit" class="btn btn-primary btn-lg text-center mt-20" data-remodal-action="confirm">
                        @lang('labels.frontend.customers.register')
                    </button>
                </div>
                {{ html()->form()->close() }}
            </div>
        </div>
    </div>
</div>
