@extends('frontend.layouts.app')

{{--@section('title', app_name() . ' | ' . __('navs.general.home'))--}}

@push('after-scripts')
    <script>
        let lang = '{{App::getLocale()}}';
        if( lang == "ar")
        {
            // let owl = $("#owl-demo5");
            // owl.trigger('refresh.owl.carousel');
            // $("#owl-demo5").owlCarousel({
            //     rtl: true,
            //     item:2,
            //     loop: false,
            //     mouseDrag: false,
            //     touchDrag: false,
            //     pullDrag: false,
            //     rewind: true,
            //     autoplay: true,
            //     margin: 0,
            //     nav: true
            // });

        }
    </script>
@endpush
@section('content')

        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel" data-interval="10000">
            <div class="carousel-inner" role="listbox">
            @if(isset($front_sliders) && $front_sliders->count() != 0 )
                @foreach($front_sliders as $slider)
                 @if($front_slider->id == $slider->id)
                <div class="item active">
                    <div class="overlay"></div>
                    <img class="img-responsive" src="{{ \App\Models\Image::where('imageable_type', \App\Models\FrontSliders\FrontSlider::class)->where('imageable_id', $slider->id)->first()->image_url }}" alt="slider1">
                    <div class="carousel-caption">
                       <h1>{!! $slider->getTranslation(App::getLocale())['title'] !!}</h1>
                    </div>
                </div>
                @else
                <div class="item">
                <div class="overlay"></div>
                    <img class="img-responsive" src="{{ \App\Models\Image::where('imageable_type', \App\Models\FrontSliders\FrontSlider::class)->where('imageable_id', $slider->id)->first()->image_url }}" alt="slider1">
                    <div class="carousel-caption">
                       <h1>{!! $slider->getTranslation(App::getLocale())['title'] !!}</h1>
                    </div>
                </div>
                @endif
              @endforeach

            </div>
            <!-- Controls -->
            <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                <span class="fa fa-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                <span class="fa fa-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
            @else
                <div class="h1 text-center">No Slider Available</div>
            @endif

        </div>
        <!-- <div class="text">
            <h1 id="head" class="text-center">الامريكتين للسياحة</h1>
        </div>
        <div class="temple">
            <img id="pyramid" class="img-responsive" src="images/pyramid.png">
        </div>
        <div class="girl">
            <img id="girl" class="img-responsive" src="images/girl.png">
        </div> -->
        <!-- <div class="seperator"><img class="img-responsive" src="/images/seperator.png"></div> -->
    </section>

    <section class="aboutus padding-mobile">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="content">
                        <h1>@lang('labels.frontend.aboutUs.aboutus')</h1>
                        <p class="">
                        @if(isset($about_us_first_row))
                        {!!  $about_us_first_row->getTranslation(App::getLocale())['contents'] !!}
                        @endif
                        </p>
                        <div class="buttons">
                         <a href="{{ route('frontend.aboutUs') }}" class="btn btn-primary btn-lg">
                            @lang('labels.frontend.aboutUs.more')
                         </a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div id="owl-demo5" class="owl-carousel">
                    @foreach($all_about_us_section as $about)
                        <div class="item">
                            <div class="img-container1">
                            @if(isset(\App\Models\Image::where('imageable_type', \App\Models\Aboutuses\Aboutus::class)->where('imageable_id', $about->id)->first()->image_url))
                                <img class="img-responsive" src="{{\App\Models\Image::where('imageable_type', \App\Models\Aboutuses\Aboutus::class)->where('imageable_id', $about->id)->first()->image_url }}">
                            @endif
                            </div>
                        </div>
                     @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="programs padding-mobile">
        <div class="container">
            <h1 class="text-center">@lang('labels.frontend.fixed_tour.program')</h1>
            <div class="content">
                <div id="owl-demo" class="owl-carousel">
                @if(isset($fixed_tours))
                 @foreach($fixed_tours as $tour)
                    <div class="item">
                    <a href="{{ route('frontend.fixedTour', ['fixed_tour' => $tour->id]) }}">
                        <div class="card">
                            <div class="card-img">
                                <img src="{{str_replace("public/", "", \App\Models\Image::where('imageable_type', \App\Models\FixedTours\FixedTour::class)->where('imageable_id', $tour->id)->first()->image_url)   }}">
                                @if(isset($tour->discount))
                                <div class="discount text-center">
                                {{$tour->discount}}%
                                </div>
                                @endif
                            </div>
                            <div class="card-body">
                                <div class="content">
                                    <h4>{{ $tour->translate(app()->getLocale())['name'] }}</h4>
                                    <p>
                                    @for($i = 0 ; $i < $tour->hotel->level; $i++)
                                     <i class="fa fa-star"></i>
                                    @endfor
                                    @if(isset($tour->hotel->level))
                                    {{ $tour->hotel->level }}
                                    @endif
                                    </p>
                                    <ul class="features">
                                        <li><i class="fa fa-globe"></i>
                                        @if(isset($tour->category->name))
                                         {{ $tour->category->name }}
                                        @endif
                                        </li>

                                        <li>
                                        <i class="fa fa-map-marker"></i>
                                        @if(isset($tour->city->name))
                                         {{ $tour->city->translate(app()->getLocale())['name']  }}
                                        @endif
                                        ,
                                        @if(isset($tour->city->country->name))
                                         {{ $tour->city->country->translate(app()->getLocale())['name']  }}
                                        @endif                                   </li>
                                        <li><i class="fa fa-clock-o"></i> {{ $tour->days_nights_counts }}</li>
                                    </ul>

                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="price-box">
                                    <span>{{ $tour->total_cost }}</span>
                                </div>
                            </div>
                        </div>
                        </a>
                    </div>
                   @endforeach
                   @else
                <div class="h1">No Programs available</div>
                   @endif
                </div>
                <div class="button-all text-center">
                    <a class="btn btn-primary btn-lg" href="{{ route('frontend.fixedTours') }}">@lang('labels.frontend.fixed_tour.programs') <i class="fa fa-arrow-arrow"></i></a>
                </div>
            </div>
        </div>
    </section>
    <!-- start best destination section  -->
    <section class="destination padding-mobile">
        <div class="container">
        @if(isset($discover) && $discover->count() != 0 )
            <div class="row">
                <div class="col-lg-6">
                    <h1>{{ $discover->translate(app()->getLocale())->title }}</h1>
                </div>
                <div class="col-lg-6">
                    <p>{{ $discover->translate(app()->getLocale())->description }}</p>
                </div>
            </div>
            @else
                <div class="h1">No Discover Section</div>
            @endif
        </div>
        <div class="container-fluid">
            <div class="row">
                <div id="owl-demo1" class="owl-carousel">
                @if(isset($citiesImages) && $citiesImages->count() != 0)
                   @foreach($citiesImages as $image)
                    <div class="item">
                        <div class="card-img1">
                            <img src="{{str_replace("public/", "", \App\Models\Image::where('imageable_type', \App\Models\CitySliders\CitySlider::class)->where('imageable_id', $image->id)->first()->image_url)   }}">
                            <div class="img-desc text-center">
                                <h3>{!!   $image->translate(app()->getLocale())->title !!}</h3>
                            </div>
                        </div>
                    </div>
                    @endforeach
                  @endif
                </div>
            </div>
        </div>
    </section>
    <!-- start hotel section  -->
    <section class="hotel padding-mobile">
        <div class="container">
            <h1 class="text-center">@lang('labels.frontend.hotels.hotels')</h1>

            <div class="row">
            @if( $hotels->count() != 0)
            @foreach($hotels as $hotel)
                <div class="col-lg-4 col-sm-6 col-xs-12">
                    <div class="card">
                    <a href="{{ route('frontend.singleHotel', ['hotel' => $hotel->id]) }}">
                        <div class="card-img">
                            <img class="img-responsive" src="{{str_replace("public/", "", \App\Models\Image::where('imageable_type', \App\Models\Hotels\Hotel::class)->where('imageable_id', $hotel->id)->first()->image_url)   }}">
                        </div>
                        <div class="card-body">
                            <div class="content">
                                <h4>{{ $hotel->name }}</h4>
                                <p>
                                @for($i = 0 ; $i < $hotel->level; $i++)
                                 <i class="fa fa-star"></i>
                                @endfor
                                {{ $hotel->level }}
                                 </p>
                                <ul class="features">
                                @foreach($hotel->services as $service)
                                 <li> {{$service->name}} </li>
                                @endforeach
                                </ul>
                            </div>
                        </div>
                        <div class="card-footer">
                            <!--div class="price-box">
                                <span></span>
                            </div-->
                        </div>
                        </a>
                    </div>
                </div>
            @endforeach
            @else
                <div class="h1">No Hotels Available</div>
            @endif
            </div>
            <div class="button-all text-center">
                <a class="btn btn-primary btn-lg" href="{{ route('frontend.allHotels') }}">@lang('labels.frontend.hotels.all_hotels') <i class="fa fa-arrow-right"></i></a>
            </div>
        </div>
        </div>
        </div>
    </section>
    <!-- start testimonials section  -->
    <section class="testimonials padding-mobile">
        <div class="container">
            <h1 class="text-center">@lang('labels.frontend.customers.opinions')</h1>
            <div class="content">
                <div id="owl-demo2" class="owl-carousel">
                @if(isset($customeropinions))
                @foreach($customeropinions as $customeropinion)
                    <div class="item">
                        <div class="test-card text-center">
                            <div class="card-text">
                                <p>
                                {!! $customeropinion->description !!}
                                </p>
                                <div class="triangle"></div>
                            </div>
                            <div class="avatar">
                                <div class="avatar-img ">
                                    <img class="img-responsive center-block" src="/images/avatar.png">
                                </div>
                                <h3>{{$customeropinion->name}}</h3>
                                <p>{{$customeropinion->job_title_name}}</p>
                            </div>
                        </div>
                    </div>
                   @endforeach
                   @endif
                </div>
            </div>
        </div>
    </section>
    <!-- start blog section -->
    <section class="blog padding-mobile">
        <div class="container">
            <h1 class="text-center">@lang('labels.frontend.news.news')</h1>
            <div class="row">
                <div class="content">
                @foreach($allnews as $allnew)
                @if($allnew->id == $latestnew->id)
                @if(isset($allnew))
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="card-img1 large">
                        <?php
                    $imagenew=\App\Models\Image::where('imageable_type', \App\Models\FrontNews\FrontNews::class)->where('imageable_id', $allnew->id)->first()->image_url;
                    ?>
                      @if(isset($imagenew))
                            <img src="{{str_replace("public/", "", \App\Models\Image::where('imageable_type', \App\Models\FrontNews\FrontNews::class)->where('imageable_id', $allnew->id)->first()->image_url)   }}">
                       @endif
                            <div class="img-desc">
{{--                                <div class="date"><span>{{$allnew->news_date}}</span></div>--}}
                                <div class="card-text">
                                    <p>{{ $allnew->translate(app()->getLocale())['title'] }}</p>
                                </div>
                                <div class="button">
                                    <a class="btn btn-default" href="{{ route('frontend.singleNews', ['news' => $allnew->id]) }}">@lang('labels.frontend.aboutUs.more') <i class="fa fa-angle-left"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                   @endif
                  @endforeach
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    @foreach($allnews as $allnew)
                    @if($allnew->id != $latestnew->id)

                    <?php
                    $imagenew=\App\Models\Image::where('imageable_type', \App\Models\FrontNews\FrontNews::class)->where('imageable_id', $allnew->id)->first()->image_url;
                    ?>
                        <div class="card-img1">
                            <?php
                            $imagenew=\App\Models\Image::where('imageable_type', \App\Models\FrontNews\FrontNews::class)->where('imageable_id', $allnew->id)->first()->image_url;
                            ?>
                            @if(isset($imagenew))
                                <img src="{{str_replace("public/", "", \App\Models\Image::where('imageable_type', \App\Models\FrontNews\FrontNews::class)->where('imageable_id', $allnew->id)->first()->image_url)   }}">
                            @endif
                            <div class="img-desc">
{{--                                <div class="date"><span>{{$allnew->news_date}}</span></div>--}}
                                <div class="card-text">
                                    <p>{{ $allnew->translate(app()->getLocale())['title'] }}</p>
                                </div>
                                <div class="button">
                                    <a class="btn btn-default" href="{{ route('frontend.singleNews', ['news' => $allnew->id]) }}"> @lang('labels.frontend.aboutUs.more') <i class="fa fa-angle-left"></i></a>
                                </div>
                            </div>
                        </div>
                  @endif
                  @endforeach
                    </div>
                </div>
            </div>
            <div class="button-all text-center">
                <a class="btn btn-primary btn-lg" href="{{ route('frontend.allnews') }}">@lang('labels.frontend.news.allnews') <i class="fa fa-arrow-right"></i></a>
            </div>
        </div>
    </section>
    <!-- start Contact Us for Section  -->
    <!-- **************************************** -->
    <!-- ***************************************** -->
    <section class="contact-us padding-mobile" id="contactus">
        <div class="container">
            <h1 class="text-center"></h1>
            <div class="row">
                <div class="contact text-center">

                @foreach($settings as $setting)
                        @if( $setting->key == "question"||$setting->key == "contact"||$setting->key == "address")
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 text-center">
                        <div class="contact1">
                            <div class="icon">
                               @if( $setting->key == "question")
                                <i class="fa fa-envelope"></i>
                                @elseif( $setting->key == "contact" )
                                <i class="fa fa-phone"></i>
                                @elseif( $setting->key == "address" )
                                <i class="fa fa-map-marker"></i>
                                @endif
                            </div>
                            <div class="desc">
                                <h3>{!! $setting->translate(app()->getLocale())['title'] !!}</h3>
                                <p class="lead">{!! $setting->translate(app()->getLocale())['contents'] !!}
                                </p>
                            </div>
                        </div>
                    </div>
                        @endif
                 @endforeach

                </div>
            </div>
        </div>
    </section>
    <!-- Start Map Section  -->
    <!-- ***************************** -->
    <!-- ******************************* -->
    <section class="map">
        <div class="contact">
            <div class="container-fluid">
                <h2 class="text-center">تواصل معنا</h2>
                <form class="form-horizontal" method="post" action="{{route('frontend.storeContactUsForm')}}">
                    @csrf
                    <div class="form-group">
                        <input class="form-control input-lg" type="text" name="name" placeholder="الاسم*">
                    </div>
                    <div class="form-group">
                        <input class="form-control input-lg" type="email" name="email" placeholder="البريد الالكتروني*">
                    </div>
                    <div class="form-group">
                        <input class="form-control input-lg" type="tel" name="phone" placeholder="رقم العاتف*">
                    </div>
                    <div class="form-group">
                        <textarea class="form-control input-lg" name="message" placeholder="رسالتك*"></textarea>
                    </div>
                    <div class="button">
                    <button class="btn btn-primary btn-block text-center btn-lg">ارسال</button>
                </div>
                </form>

            </div>
        </div>
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d253.5145508877641!2d29.983917407584197!3d31.25197671767419!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x14f5dac8226b6d13%3A0x61db3b06b8f32282!2s7+El-Rahmah+Mosque%2C+As+Soyouf+Bahri%2C+Qism+El-Montaza%2C+Alexandria+Governorate!5e0!3m2!1sen!2seg!4v1540637192464" allowfullscreen></iframe>
    </section>
    @endsection
