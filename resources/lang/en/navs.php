<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Navs Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in menu items throughout the system.
    | Regardless where it is placed, a menu item can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'general' => [
        'home' => 'Home',
        'logout' => 'Logout',
    ],

    'frontend' => [
        'contact-us' => 'call us',
        'news' => 'News',
        'login' => 'Login',
        'logout' => 'Logout',
        'custom-tour' => 'Your own program',
        'hotels' => 'Hotels',
        'fixed-tours' => 'Available programs',
        'about-us' => 'About us ?',
        'main' => 'Main Page',


        'user' => [
            'account' => 'My Account',
            'administration' => 'Administration',
            'change_password' => 'Change Password',
            'my_information' => 'My Information',
            'profile' => 'Profile',
        ],
    ],
];
