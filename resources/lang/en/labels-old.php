<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Labels Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in labels throughout the system.
    | Regardless where it is placed, a label can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'general' => [
        'all' => 'All',
        'yes' => 'Yes',
        'no' => 'No',
        'copyright' => 'Copyright',
        'custom' => 'Custom',
        'actions' => 'Actions',
        'active' => 'Active',
        'buttons' => [
            'save' => 'Save',
            'update' => 'Update',
        ],
        'hide' => 'Hide',
        'inactive' => 'Inactive',
        'none' => 'None',
        'show' => 'Show',
        'toggle_navigation' => 'Toggle Navigation',
        'create_new' => 'Create New',
        'toolbar_btn_groups' => 'Toolbar with button groups',
        'more' => 'More',
    ],

    'backend' => [
        'access' => [
            'roles' => [
                'create' => 'Create Role',
                'edit' => 'Edit Role',
                'management' => 'Role Management',

                'table' => [
                    'number_of_users' => 'Number of Users',
                    'permissions' => 'Permissions',
                    'role' => 'Role',
                    'sort' => 'Sort',
                    'total' => 'role total|roles total',
                ],
            ],

            'users' => [
                'active' => 'Active Users',
                'all_permissions' => 'All Permissions',
                'change_password' => 'Change Password',
                'change_password_for' => 'Change Password for :user',
                'create' => 'Create User',
                'deactivated' => 'Deactivated Users',
                'deleted' => 'Deleted Users',
                'edit' => 'Edit User',
                'management' => 'User Management',
                'no_permissions' => 'No Permissions',
                'no_roles' => 'No Roles to set.',
                'permissions' => 'Permissions',
                'user_actions' => 'User Actions',

                'table' => [
                    'confirmed' => 'Confirmed',
                    'created' => 'Created',
                    'email' => 'E-mail',
                    'id' => 'ID',
                    'last_updated' => 'Last Updated',
                    'name' => 'Name',
                    'first_name' => 'First Name',
                    'last_name' => 'Last Name',
                    'no_deactivated' => 'No Deactivated Users',
                    'no_deleted' => 'No Deleted Users',
                    'other_permissions' => 'Other Permissions',
                    'permissions' => 'Permissions',
                    'abilities' => 'Abilities',
                    'roles' => 'Roles',
                    'social' => 'Social',
                    'total' => 'user total|users total',
                ],

                'tabs' => [
                    'titles' => [
                        'overview' => 'Overview',
                        'history' => 'History',
                    ],

                    'content' => [
                        'overview' => [
                            'avatar' => 'Avatar',
                            'confirmed' => 'Confirmed',
                            'created_at' => 'Created At',
                            'deleted_at' => 'Deleted At',
                            'email' => 'E-mail',
                            'last_login_at' => 'Last Login At',
                            'last_login_ip' => 'Last Login IP',
                            'last_updated' => 'Last Updated',
                            'name' => 'Name',
                            'first_name' => 'First Name',
                            'last_name' => 'Last Name',
                            'status' => 'Status',
                            'timezone' => 'Timezone',
                        ],
                    ],
                ],

                'view' => 'View User',
            ],
        ],
        'none' => 'none',
        'customeropinions' => [
            'management' => 'Customer opinions',
            'create' => 'Create customer opinion',
            'edit' => 'Edit on customer opinion',
            'description' => 'Description',
            'name' => 'Name',
            'job_title_name' => 'job title name',
            'main_image' => 'Image',
        ],

        'countries' => [
            'management' => 'Country manage',
            'create' => 'Add a new Country',
            'edit' => 'Edit Country',
            'name' => 'Name',
            'ar_name' => 'Name In ar',
            'en_name' => 'Name In en',
            'fr_name' => 'Name In fr',
            'it_name' => 'Name In it',
            'es_name' => 'Name In sp',
        ],
        'customers' => [
            'management' => 'customers',
            'create' => 'create customer',
            'edit' => 'edit customer',
            'image_url' => 'image',
            'name' => 'name',
            'email' => 'email',
            'phone' => 'phone',
            'address' => 'address',
            'gender' => 'gender',
            'male' => 'male',
            'female' => 'female',
            'password' => 'password',
            'age' => 'age',
            'isAdmin' => 'is Admin',
            'Admin' => 'Admin',
            'User' => 'User',
            'register' => 'register'
        ],
        'users' => [
            'management' => 'users management',
            'create' => 'create users',
            'edit' => 'edit users',
            'first_name' => 'first name',
            'last_name' => 'last name',
            'image_url' => 'image',
            'email' => 'email',
            'phone' => 'phone',
            'address' => 'address',
            'gender' => 'gender',
            'male' => 'male',
            'female' => 'female',
            'password' => 'password',
            'age' => 'age',

        ],
        'discovers' => [
            'management' => 'Discovers  manage',
            'create' => 'create discover',
            'edit' => 'Edit discover',
            'title' => 'Title',
            'description' => 'Description',
            'ar_title' => 'Arabic Title',
            'en_title' => 'English Title',
            'fr_title' => 'French Title',
            'it_title' => 'Italy Title',
            'es_title' => 'Spain Title ',
            'ar_description' => 'Arabic description',
            'en_description' => 'English description',
            'fr_description' => 'French description',
            'it_description' => 'Italy description',
            'es_description' => 'Spain description',
        ],
        'services' => [
            'management' => 'services manage',
            'create' => 'create service',
            'edit' => 'edit service',
            'name' => 'name',
            'icon' => 'Icon',
            'ar_name' => 'Arabic Name',
            'en_name' => 'English Name',
            'fr_name' => 'French Name',
            'it_name' => 'Italy Name',
            'es_name' => 'Spain Name',
        ],
        'categories' => [
            'management' => 'categories manage',
            'create' => 'create category',
            'edit' => 'edit category',
            'name' => 'name',
            'ar_name' => 'Arabic Name',
            'en_name' => 'English Name',
            'fr_name' => 'French Name',
            'it_name' => 'Italy Name',
            'es_name' => 'Spain Name',
        ],
        'currencyexchanges' => [
            'management' => 'currency exchanges',
            'create' => 'create currency exchanges',
            'edit' => 'edit currency exchanges',
            'name' => 'name',
            'exchange_rate' => 'exchange rate',
            'ar_name' => 'Arabic Name',
            'en_name' => 'English Name',
            'fr_name' => 'French Name',
            'it_name' => 'Italy Name',
            'es_name' => 'Spain Name',
        ],

        'cities' => [
            'management' => 'City manage',
            'create' => 'Add a new city',
            'edit' => 'Edit City',
            'name' => 'Name',
            'country_name' => 'Country Name',
            'ar_name' => 'Name in ar',
            'en_name' => 'Name in en',
            'fr_name' => 'Name in fr',
            'it_name' => 'Name in it',
            'es_name' => 'Name in es',
        ],
        'fixedtours' => [
            'management' => 'fixed tours manage',
            'create' => 'create fixed tours',
            'edit' => 'edit create fixed tours',
            'image_url' => 'image',
            'category' => 'category',
            'hotel' => 'hotel',
            'from' => 'from',
            'to' => 'to',
            'totalCost' => 'total Cost',
            'active' => 'active',
            'daysAndNights' => 'days And Nights count',
            'after_or_equal' => 'End date must be after start date',
        ],
        'frontsliders' => [
            'management' => 'front sliders manage',
            'create' => 'create slider',
            'edit' => 'edit slider',
            'title' => 'title',
            'main_image' => 'image',
            'ar_title' => 'Arabic Title',
            'en_title' => 'English Title',
            'fr_title' => 'French Title',
            'it_title' => 'Italy Title',
            'es_title' => 'Spain Title ',
        ],
        'citysliders' => [
            'management' => 'city sliders manage',
            'create' => 'create city sliders',
            'edit' => 'edit city sliders',
            'title' => 'title',
            'main_image' => 'image',
            'ar_title' => 'Arabic Title',
            'en_title' => 'English Title',
            'fr_title' => 'French Title',
            'it_title' => 'Italy Title',
            'es_title' => 'Spain Title ',
        ],
        'frontnews' => [
            'management' => 'News manage',
            'create' => 'create news',
            'edit' => 'edit news',
            'news_date' => 'news date',
            'title' => 'title',
            'description' => 'description',
            'main_image' => 'image',
            'ar_title' => 'Arabic Title',
            'en_title' => 'English Title',
            'fr_title' => 'French Title',
            'it_title' => 'Italy Title',
            'es_title' => 'Spain Title ',
            'ar_description' => 'Arabic description',
            'en_description' => 'English description',
            'fr_description' => 'French description',
            'it_description' => 'Italy description',
            'es_description' => 'Spain description',
        ],
        'aboutuses' => [
            'management' => 'about us manage',
            'create' => 'create about us',
            'edit' => 'edit about us',
            'type' => 'type',
            'contents' => 'contents',
            'main_image' => 'image',
            'ar_contents' => 'Arabic contents',
            'en_contents' => 'English contents',
            'fr_contents' => 'French contents',
            'it_contents' => 'Italy contents',
            'es_contents' => 'Spain contents',
        ],
        'settings' => [
            'management' => 'settings manage',
            'create' => 'create settings',
            'edit' => 'edit settings ',
            'key' => 'name',
            'value' => 'value',
            'title' => 'title',
            'contents' => 'contents',
            'ar_title' => 'Arabic Title',
            'en_title' => 'English Title',
            'fr_title' => 'French Title',
            'it_title' => 'Italy Title',
            'es_title' => 'Spain Title ',
            'ar_contents' => 'Arabic contents',
            'en_contents' => 'English contents',
            'fr_contents' => 'French contents',
            'it_contents' => 'Italy contents',
            'es_contents' => 'Spain contents',
        ],
        'fixedtourbookings' => [
            'management' => 'fixed tour bookings manage',
            'create' => 'create fixed tour bookings',
            'edit' => 'edit fixed tour bookings',
            'customer' => 'customer name',
            'phone' => 'phone',
            'email' => 'email',
            'status' => 'status',
            'fixed_tour' => 'fixed tour title',
            'from' => 'from',
            'to' => 'to',
            'total_cost' => ' total cost',
            'adult_count' => 'adult count',
            'child_count' => 'child count',
            'baby_count' => 'baby count',
            'pending' => 'pending',
            'confirmed' => 'confirmed',
            'canceled' => 'canceled',
        ],
        'hotels' => [
            'management' => 'hotels management',
            'create' => 'create hotel',
            'edit' => 'edit hotel',
            'name' => 'name',
            'ar_name' => 'Name in ar',
            'en_name' => 'Name in en',
            'fr_name' => 'Name in fr',
            'it_name' => 'Name in it',
            'es_name' => 'Name in es',
            'cities' => 'cities',
            'map_long' => 'longitude',
            'map_lat' => 'latitude',
            'level' => 'level',
            'star' => 'star',
            '1star' => '1',
            '2star' => '2',
            '3star' => '3',
            '4star' => '4',
            '5star' => '5',
            'shown' => 'shown',
            'main_image' => 'main image',
            'slider_images' => 'slider images',
            'choose-shown' => 'choose shown',
            'active' => 'active',
            'in-active' => 'in active',
            'services' => 'services',
        ],
        'customtourbookings' => [
            'management' => 'custom tour bookings',
            'create' => 'create custom tour booking',
            'edit' => 'edit custom tour booking',
            'customer' => 'customer',
            'phone' => 'phone',
            'category' => 'category',
            'hotel' => 'hotel',
            'email' => 'email',
            'status' => 'status',
            'fixed_tour' => 'fixed tour',
            'from' => 'from',
            'to' => 'to',
            'total_cost' => 'total cost',
            'adult_count' => 'adult count',
            'child_count' => 'child count',
            'baby_count' => 'baby count',
            'pending' => 'pending',
            'confirmed' => 'confirmed',
            'canceled' => 'canceled',
        ],
        'ContactUs' => [
            'management' => 'Contact Us manage',
            'name' => 'name',
            'email' => 'email',
            'phone' => 'phone',
            'message' => 'message',
        ],
        'hotelbookings' => [
            'management' => 'hotel bookings manage',
            'edit' => 'edit booking',
            'hotels' => 'Hotels and Resorts',
            'all_hotels' => 'All hotels',
            'hotel_details' => 'Hotel Details',
            'from' => 'From',
            'to' => 'To',
            'phone' => 'phone',
            'email' => 'email',
            'register' => 'Register Now',
            'customer' => 'Customer',
            'tour' => 'Tour',
            'total_cost' => 'Total Cost',
            'adult_count' => 'Adult Count',
            'child_count' => 'Child Count',
            'baby_count' => 'Baby Count',
            'status' => 'Request Status',
        ],
    ],

    'frontend' => [
        'auth' => [
            'login_box_title' => 'Login',
            'login_button' => 'Login',
            'login_with' => 'Login with :social_media',
            'register_box_title' => 'Register',
            'register_button' => 'Register',
            'remember_me' => 'Remember Me',
        ],

        'contact' => [
            'box_title' => 'Contact Us',
            'button' => 'Send Information',
        ],

        'passwords' => [
            'expired_password_box_title' => 'Your password has expired.',
            'forgot_password' => 'Forgot Your Password?',
            'reset_password_box_title' => 'Reset Password',
            'reset_password_button' => 'Reset Password',
            'update_password_button' => 'Update Password',
            'send_password_reset_link_button' => 'Send Password Reset Link',
        ],

        'user' => [
            'passwords' => [
                'change' => 'Change Password',
            ],

            'profile' => [
                'avatar' => 'Avatar',
                'created_at' => 'Created At',
                'edit_information' => 'Edit Information',
                'email' => 'E-mail',
                'last_updated' => 'Last Updated',
                'name' => 'Name',
                'first_name' => 'First Name',
                'last_name' => 'Last Name',
                'update_information' => 'Update Information',
            ],
        ],

        'customers' => [
            'name' => 'Name',
            'password' => 'Password',
            'email' => 'E-Mail',
            'phone' => 'Phone',
            'address' => 'Address',
            'gender' => 'Gender',
            'male' => 'Male',
            'female' => 'Female',
            'age' => 'Age',
            'register' => 'Register Now',
            'message1' => 'Log in now and start your journey -',
            'message2' => 'You do not have an account',
            'opinions' => 'customers opinions',
        ],

        'aboutUs' => [
            'more' => 'More',
            'aboutus' => 'About Us',
            'vision' => 'our vision',
            'mission' => 'our mission',
            'hint' => 'About us',
        ],

        'fixed_tour' => [
            'program' => 'Program',
            'programs' => 'All Programs',
            'from' => 'From',
            'to' => 'To',
            'register' => 'Register Now',
            'customer' => 'Customer',
            'tour' => 'Tour',
            'total_cost' => 'Total Cost',
            'adult_count' => 'Adult Count',
            'child_count' => 'Child Count',
            'baby_count' => 'Baby Count',
        ],

        'custom_tour' => [
            'special_program' => 'Special program',
            'category' => 'Category',
            'special_register' => 'Book a program on your mood',
        ],

        'hotels' => [
            'hotels' => 'Hotels and Resorts',
            'all_hotels' => 'All hotels',
            'hotel_details' => 'Hotel Details',
            'from' => 'From',
            'to' => 'To',
            'register' => 'Register Now',
            'customer' => 'Customer',
            'tour' => 'Tour',
            'total_cost' => 'Total Cost',
            'adult_count' => 'Adult Count',
            'child_count' => 'Child Count',
            'baby_count' => 'Baby Count',
        ],

        'hotelbookings' => [
            'hotels' => 'Hotels and Resorts',
            'all_hotels' => 'All hotels',
            'hotel_details' => 'Hotel Details',
            'from' => 'From',
            'to' => 'To',
            'register' => 'Register Now',
            'customer' => 'Customer',
            'tour' => 'Tour',
            'total_cost' => 'Total Cost',
            'adult_count' => 'Adult Count',
            'child_count' => 'Child Count',
            'baby_count' => 'Baby Count',
            'status' => 'Request Status',
        ],


    ],
];
