<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Navs Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in menu items throughout the system.
    | Regardless where it is placed, a menu item can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'general' => [
        'home' => 'Home',
        'logout' => 'Logout',
    ],

    'frontend' => [
        'login' => 'Accesso',
        'logout' => 'disconnettersi',
        'contact-us' => 'chiamaci',
        'news' => 'notizia',
        'custom-tour' => 'Il tuo programma',
        'hotels' => 'Alberghi',
        'fixed-tours' => 'Programmi disponibili',
        'about-us' => 'Riguardo a noi ?',
        'main' => 'Pagina principale',

        'user' => [
            'account' => 'My Account',
            'administration' => 'Amministrazione',
            'change_password' => 'Cambio Password',
            'my_information' => 'Profilo',
            'profile' => 'Profile',
        ],
    ],
];
