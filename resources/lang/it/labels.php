<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Labels Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in labels throughout the system.
    | Regardless where it is placed, a label can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'general' => [
        'all' => 'Tutti',
        'yes' => 'Sì',
        'no' => 'No',
        'custom' => 'Custom', // TODO TRANSLATION
        'actions' => 'Azioni',
        'active' => 'Active',
        'buttons' => [
            'save' => 'Salva',
            'update' => 'Aggiorna',
        ],
        'hide' => 'Nascondi',
        'inactive' => 'Inactive',
        'none' => 'Nessuno',
        'show' => 'Visualizza',
        'toggle_navigation' => 'Menu Navigazione',
    ],

    'backend' => [
        'access' => [
            'roles' => [
                'create' => 'Crea ruolo',
                'edit' => 'Modifica ruolo',
                'management' => 'Gestione ruolo',

                'table' => [
                    'number_of_users' => 'Numero di utenti',
                    'permissions' => 'Permessi',
                    'role' => 'Ruolo',
                    'sort' => 'Ordina',
                    'total' => 'Ruolo|Totale ruoli',
                ],
            ],

            'users' => [
                'active' => 'Utenti attivi',
                'all_permissions' => 'Tutti i permessi',
                'change_password' => 'Cambia password',
                'change_password_for' => 'Cambia password per :user',
                'create' => 'Crea utente',
                'deactivated' => 'Utenti disattivati',
                'deleted' => 'Utenti eliminati',
                'edit' => 'Modifica utente',
                'management' => 'Gestione utente',
                'no_permissions' => 'Nessun permesso',
                'no_roles' => 'Nessuno ruolo da assegnare.',
                'permissions' => 'Permessi',

                'table' => [
                    'confirmed' => 'Confermato',
                    'created' => 'Creato',
                    'email' => 'E-mail',
                    'id' => 'ID',
                    'last_updated' => 'Ultimo aggiornamento',
                    'name' => 'Nome',
                    'no_deactivated' => 'Nessun utente disattivato',
                    'no_deleted' => 'Nessun utente eliminato',
                    'roles' => 'Ruoli',
                    'social' => 'Social',
                    'total' => 'utente(i) totali', // TODO: pluralization
                ],

                'tabs' => [
                    'titles' => [
                        'overview' => 'Overview',
                        'history' => 'History',
                    ],

                    'content' => [
                        'overview' => [
                            'avatar' => 'Avatar',
                            'confirmed' => 'Confirmed',
                            'created_at' => 'Created At',
                            'deleted_at' => 'Deleted At',
                            'email' => 'E-mail',
                            'last_login_at' => 'Last Login At',
                            'last_login_ip' => 'Last Login IP',
                            'last_updated' => 'Last Updated',
                            'name' => 'Name',
                            'status' => 'Status',
                            'timezone' => 'Timezone',
                        ],
                    ],
                ],

                'view' => 'View User',
            ],
        ],

        'countries' => [
            'management' => 'Gestione del Paese',
            'create' => 'Aggiungi un nuovo Paese',
            'edit' => 'Modifica Paese',
            'name' => 'Nome',
            'ar_name' => 'Nome In ar',
            'en_name' => 'Nome In en',
            'fr_name' => 'Nome In fr',
            'it_name' => 'Nome In it',
            'es_name' => 'Nome In sp',
        ],
        'hotelbookings' => [
            'hotels' => 'Alberghi e resort',
            'all_hotels' => 'Tutti gli hotel',
            'hotel_details' => 'Dettagli dell\'hotel',
            'from' => 'a partire dal',
            'to' => 'per',
            'register' => 'Iscriviti ora',
            'customer' => 'cliente',
            'tour' => 'giro',
            'total_cost' => 'costo totale',
            'adult_count' => 'conteggio degli adulti',
            'child_count' => 'conteggio dei bambini',
            'baby_count' => 'conteggio del bambino',
            'status' => 'Stato richiesta',
        ],
    ],

    'frontend' => [
        'auth' => [
            'login_box_title' => 'Login',
            'login_button' => 'Login',
            'login_with' => 'Login tramite :social_media',
            'register_box_title' => 'Registrazione',
            'register_button' => 'Registrati',
            'remember_me' => 'Ricordami',
        ],

        'contact' => [
            'box_title' => 'Contattaci',
            'button' => 'Invia informazioni',
        ],

        'passwords' => [
            'forgot_password' => 'Password dimenticata?',
            'reset_password_box_title' => 'Reset password',
            'reset_password_button' => 'Reset password',
            'send_password_reset_link_button' => 'Invia link per il reset della password',
        ],

        'user' => [
            'passwords' => [
                'change' => 'Change Password',
            ],

            'profile' => [
                'avatar' => 'Avatar',
                'created_at' => 'Data di creazione',
                'edit_information' => 'Modifica informazioni',
                'email' => 'E-mail',
                'last_updated' => 'Ultimo aggiornamento',
                'name' => 'Nome',
                'update_information' => 'Aggiorna informazioni',
                'message1'   => 'Accedi ora e inizia il tuo viaggio -',
                'message2'   => 'Non hai un account'
            ],
        ],

        'customers' => [
            'name'       => 'nome',
            'password'   => 'parola d\'ordine',
            'email'      => 'e-mail',
            'phone'      => 'Telefono',
            'address'    => 'indirizzo',
            'gender'     => 'Genere',
            'male'       => 'maschio',
            'female'     => 'femmina',
            'age'        => 'età',
            'register'   => 'Registrati',
            'opinions'=>'Opinioni dei clienti',
        ],
     
        'aboutUs' => [
            'more' => 'più',
            'aboutus'=>'Chi siamo',
            'vision'=>'La nostra visione',
            'mission'=>'Il nostro messaggio',
            'hint'=>'Chi siamo',
        ],
        
        'fixed_tour' => [
            'program' => 'programma',
            'programs' => 'programmi',
            'from' => 'a partire dal',
            'to' => 'per',
            'register' => 'Iscriviti ora',
            'customer' => 'cliente',
            'tour' => 'giro',
            'total_cost' => 'costo totale',
            'adult_count' => 'conteggio degli adulti',
            'child_count' => 'conteggio dei bambini',
            'baby_count' => 'conteggio del bambino',
        ],

        'custom_tour' => [
            'special_program' => 'Programma speciale',
            'category' => 'gruppo',
            'special_register' => 'Prenota un programma sul tuo umore',
        ],

        'hotels' => [
            'hotels' => 'Alberghi e resort',
            'all_hotels' => 'Tutti gli hotel',
            'hotel_details' => 'Dettagli dell\'hotel',
            'from' => 'a partire dal',
            'to' => 'per',
            'register' => 'Iscriviti ora',
            'customer' => 'cliente',
            'tour' => 'giro',
            'total_cost' => 'costo totale',
            'adult_count' => 'conteggio degli adulti',
            'child_count' => 'conteggio dei bambini',
            'baby_count' => 'conteggio del bambino',
        ],

        'news' => [
            'news' => 'notizie',
            'news_date' => 'تاريخ الخبر',
            'title' => 'البيان',
            'description' => 'الوصف',
            'main_image' => 'الصورة',
            'ar_title'       => 'البيان بالعربى',
            'en_title'       => 'البيان بالإنجليزى',
            'fr_title'       => 'البيان بالفرنسية',
            'it_title'       => 'البيان الايطالبة',
            'es_title'       => 'البيان بالاسبانية',
            'ar_description'       => 'الوصف بالعربى',
            'en_description'       => 'الوصف بالإنجليزى',
            'fr_description'       => 'الوصف بالفرنسية',
            'it_description'       => 'الوصف الايطالبة',
            'es_description'       => 'الوصف بالاسبانية',
            'latestnews'           =>'Ultime notizie',
            'allnews'           =>'Tutte le notizie',
            'details'           =>'Dettagli notizie',
        ],

        'hotelbookings' => [
            'hotels' => 'Alberghi e resort',
            'all_hotels' => 'Tutti gli hotel',
            'hotel_details' => 'Dettagli dell\'hotel',
            'from' => 'a partire dal',
            'to' => 'per',
            'register' => 'Iscriviti ora',
            'customer' => 'cliente',
            'tour' => 'giro',
            'total_cost' => 'costo totale',
            'adult_count' => 'conteggio degli adulti',
            'child_count' => 'conteggio dei bambini',
            'baby_count' => 'conteggio del bambino',
            'status' => 'Stato richiesta',
        ],
    ],
];
