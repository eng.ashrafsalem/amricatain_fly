<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Navs Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in menu items throughout the system.
    | Regardless where it is placed, a menu item can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'general' => [
        'home' => 'Accueil',
        'logout' => 'Déconnexion',
    ],

    'frontend' => [
        'login' => 'Connexion',
        'logout' => 'Se déconnecter',
        'contact-us' => 'appelez-nous',
        'news' => 'Nouvelles',
        'custom-tour' => 'Votre propre programme',
        'hotels' => 'Hôtels',
        'fixed-tours' => 'Programmes disponibles',
        'about-us' => 'À propos de nous ?',
        'main' => 'Page d\'accueil',

        'user' => [
            'account' => 'Mon compte',
            'administration' => 'Administration',
            'change_password' => 'Changer mon mot de passe',
            'my_information' => 'Mes informations',
            'profile' => 'Profil',
        ],
    ],
];
