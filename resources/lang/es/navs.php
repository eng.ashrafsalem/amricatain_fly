<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Navs Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in menu items throughout the system.
    | Regardless where it is placed, a menu item can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'general' => [
        'home' => 'Inicio',
        'logout' => 'Cerrar Sesión',
    ],

    'frontend' => [
        'login' => 'Iniciar Sesión',
        'logout' => 'cerrar sesión',
        'contact-us' => 'llámanos',
        'news' => 'Noticias',
        'custom-tour' => 'Tu propio programa',
        'hotels' => 'Hotels',
        'fixed-tours' => 'Programas disponibles',
        'about-us' => 'Sobre nosotros ?',
        'main' => 'Pagina principal',

        'user' => [
            'account' => 'Mi Cuenta',
            'administration' => 'Administración',
            'change_password' => 'Cambiar la contraseña',
            'my_information' => 'Mi Cuenta',
            'profile' => 'Perfil',
        ],
    ],
];
