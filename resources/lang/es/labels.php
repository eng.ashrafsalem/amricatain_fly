<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Labels Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in labels throughout the system.
    | Regardless where it is placed, a label can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'general' => [
        'all' => 'Todos',
        'yes' => 'Sí',
        'no' => 'No',
        'copyright' => 'Copyright',
        'custom' => 'Personalizado',
        'actions' => 'Acciones',
        'active' => 'Activo',
        'buttons' => [
            'save' => 'Guardar',
            'update' => 'Actualizar',
        ],
        'hide' => 'Ocultar',
        'inactive' => 'Inactivo',
        'none' => 'Ningúno',
        'show' => 'Mostrar',
        'toggle_navigation' => 'Abrir/Cerrar menú de navegación',
    ],

    'backend' => [
        'access' => [
            'roles' => [
                'create' => 'Crear Rol',
                'edit' => 'Modificar Rol',
                'management' => 'Administración de Roles',

                'table' => [
                    'number_of_users' => 'Número de Usuarios',
                    'permissions' => 'Permisos',
                    'role' => 'Rol',
                    'sort' => 'Orden',
                    'total' => 'Todos los Roles',
                ],
            ],

            'users' => [
                'active' => 'Usuarios activos',
                'all_permissions' => 'Todos los Permisos',
                'change_password' => 'Cambiar la contraseña',
                'change_password_for' => 'Cambiar la contraseña para :user',
                'create' => 'Crear Usuario',
                'deactivated' => 'Usuarios desactivados',
                'deleted' => 'Usuarios eliminados',
                'edit' => 'Modificar Usuario',
                'management' => 'Administración de Usuarios',
                'no_permissions' => 'Sin Permisos',
                'no_roles' => 'No hay Roles disponibles.',
                'permissions' => 'Permisos',

                'table' => [
                    'confirmed' => 'Confirmado',
                    'created' => 'Creado',
                    'email' => 'Correo',
                    'id' => 'ID',
                    'last_updated' => 'Última modificación',
                    'name' => 'Nombre',
                    'first_name' => 'Nombre',
                    'last_name' => 'Apellidos',
                    'no_deactivated' => 'Ningún Usuario desactivado disponible',
                    'no_deleted' => 'Ningún Usuario eliminado disponible',
                    'other_permissions' => 'Otros Permisos',
                    'permissions' => 'Permisos',
                    'roles' => 'Roles',
                    'social' => 'Cuenta Social',
                    'total' => 'Todos los Usuarios',
                ],

                'tabs' => [
                    'titles' => [
                        'overview' => 'Resúmen',
                        'history' => 'Historia',
                    ],

                    'content' => [
                        'overview' => [
                            'avatar' => 'Avatar',
                            'confirmed' => 'Confirmado',
                            'created_at' => 'Creación',
                            'deleted_at' => 'Eliminación',
                            'email' => 'E-mail',
                            'last_login_at' => 'Último login',
                            'last_login_ip' => 'IP último login',
                            'last_updated' => 'Última Actualización',
                            'name' => 'Nombre',
                            'first_name' => 'Nombre',
                            'last_name' => 'Apellidos',
                            'status' => 'Estado',
                            'timezone' => 'Zona horaria',
                        ],
                    ],
                ],

                'view' => 'Ver Usuario',
            ],
        ],

        'countries' => [
            'management' => 'Gestión del país',
            'create' => 'Agregar un nuevo país',
            'edit' => 'Editar país',
            'name' => 'Nombre',
            'ar_name' => 'Nombre en ar',
            'en_name' => 'Nombre en en',
            'fr_name' => 'Nombre en fr',
            'it_name' => 'Nombre en it',
            'es_name' => 'Nombre en sp',
        ],
        'cities' => [
            'management' => 'Gestión de la ciudad',
            'create' => 'Agregar una nueva ciudad',
            'edit' => 'Editar ciudad',
            'name' => 'Nombre',
            'country_name' => 'Nombre del país',
            'ar_name' => 'Name in ar',
            'en_name' => 'Name in en',
            'fr_name' => 'Name in fr',
            'it_name' => 'Name in it',
            'es_name' => 'Name in es',
        ],
        'hotelbookings' => [
            'hotels' => 'Hoteles y Resorts',
            'all_hotels' => 'Todos los hoteles',
            'hotel_details' => 'Detalles del hotel',
            'from' => 'desde',
            'to' => 'a',
            'register' => 'Regístrate ahora',
            'customer' => 'Cliente',
            'tour' => 'Tour',
            'total_cost' => 'Coste total',
            'adult_count' => 'Recuento de adultos',
            'child_count' => 'Recuento de niños',
            'baby_count' => 'cuenta bebé',
            'status' => 'Estado de la solicitud',
        ],
    ],

    'frontend' => [
        'auth' => [
            'login_box_title' => 'Iniciar Sesión',
            'login_button' => 'Iniciar Sesión',
            'login_with' => 'Iniciar Sesión mediante :social_media',
            'register_box_title' => 'Registrarse',
            'register_button' => 'Registrarse',
            'remember_me' => 'Recordarme',
        ],

        'contact' => [
            'box_title' => 'Contáctenos',
            'button' => 'Enviar información',
        ],

        'passwords' => [
            'expired_password_box_title' => 'Su contraseña ha expirado',
            'forgot_password' => '¿Ha olvidado su contraseña?',
            'reset_password_box_title' => 'Reiniciar contraseña',
            'reset_password_button' => 'Reiniciar contraseña',
            'update_password_button' => 'Actualizar contraseña',
            'send_password_reset_link_button' => 'Enviar el correo de verificación',
        ],

        'user' => [
            'passwords' => [
                'change' => 'Cambiar la contraseña',
            ],

            'profile' => [
                'avatar' => 'Avatar',
                'created_at' => 'Creado el',
                'edit_information' => 'Modificar la información',
                'email' => 'Correo',
                'last_updated' => 'Última modificación',
                'name' => 'Nombre',
                'first_name' => 'Nombre',
                'last_name' => 'Apellidos',
                'update_information' => 'Actualizar la información',
            ],
        ],

        'customers' => [
            'name'       => 'nombre',
            'password'   => 'contraseña',
            'email'      => 'Correo electrónico',
            'phone'      => 'Teléfono',
            'address'    => 'habla a',
            'gender'     => 'Género',
            'male'       => 'Masculino',
            'female'     => 'Hembra',
            'age'        => 'Años',
            'register'   => 'Regístrate ahora',
            'message1'   => 'Inicie sesión ahora y comience su viaje -',
            'message2'   => 'No tienes una cuenta',
            'opinions'=>'Opiniones de los clientes',
        ],

        'aboutUs' => [
            'more' => 'Más',
            'aboutus'=>'Quienes somos',
            'vision'=>'Nuestra vision',
            'mission'=>'Nuestro mensaje',
            'hint'=>'Quienes somos',
        ],
        
        'fixed_tour' => [
            'program' => 'programa',
            'programs' => 'programas',
            'from' => 'desde',
            'to' => 'a',
            'register' => 'Regístrate ahora',
            'customer' => 'Cliente',
            'tour' => 'Tour',
            'total_cost' => 'Coste total',
            'adult_count' => 'Recuento de adultos',
            'child_count' => 'Recuento de niños',
            'baby_count' => 'cuenta bebé',
        ],

        'custom_tour' => [
            'special_program' => 'Programa especial',
            'category' => 'Categoría',
            'special_register' => 'Reserve un programa sobre su estado de ánimo',
        ],

        'hotels' => [
            'hotels' => 'Hoteles y Resorts',
            'all_hotels' => 'Todos los hoteles',
            'hotel_details' => 'Detalles del hotel',
            'from' => 'desde',
            'to' => 'a',
            'register' => 'Regístrate ahora',
            'customer' => 'Cliente',
            'tour' => 'Tour',
            'total_cost' => 'Coste total',
            'adult_count' => 'Recuento de adultos',
            'child_count' => 'Recuento de niños',
            'baby_count' => 'cuenta bebé',
        ],

        'news' => [
            'news' => 'Noticias',
            'news_date' => 'تاريخ الخبر',
            'title' => 'البيان',
            'description' => 'الوصف',
            'main_image' => 'الصورة',
            'ar_title'       => 'البيان بالعربى',
            'en_title'       => 'البيان بالإنجليزى',
            'fr_title'       => 'البيان بالفرنسية',
            'it_title'       => 'البيان الايطالبة',
            'es_title'       => 'البيان بالاسبانية',
            'ar_description'       => 'الوصف بالعربى',
            'en_description'       => 'الوصف بالإنجليزى',
            'fr_description'       => 'الوصف بالفرنسية',
            'it_description'       => 'الوصف الايطالبة',
            'es_description'       => 'الوصف بالاسبانية',
            'latestnews'           =>'Últimas noticias',
            'allnews'           =>'Todas las noticias',
            'details'           =>'Detalles de noticias',
        ],

        'hotelbookings' => [
            'hotels' => 'Hoteles y Resorts',
            'all_hotels' => 'Todos los hoteles',
            'hotel_details' => 'Detalles del hotel',
            'from' => 'desde',
            'to' => 'a',
            'register' => 'Regístrate ahora',
            'customer' => 'Cliente',
            'tour' => 'Tour',
            'total_cost' => 'Coste total',
            'adult_count' => 'Recuento de adultos',
            'child_count' => 'Recuento de niños',
            'baby_count' => 'cuenta bebé',
            'status' => 'Estado de la solicitud',
        ],
    ],
];
