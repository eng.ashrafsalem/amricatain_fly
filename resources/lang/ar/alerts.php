<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Alert Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain alert messages for various scenarios
    | during CRUD operations. You are free to modify these language lines
    | according to your application's requirements.
    |
    */

    'backend' => [
        'roles' => [
            'created' => '.لقد تم إضافة الدور الجديد بنجاح',
            'deleted' => 'لقد تم مسح الدور بنجاح.',
            'updated' => 'تم تعديل الدور بنجاح.',
        ],

        'users' => [
            'cant_resend_confirmation' => 'The application is currently set to manually approve users.',
            'confirmation_email' => 'لقد تم إرسال رسالة تأكيد جديدة إلى عنوان البريد الألكتروني الموجود في الملف الشخصي.',
            'confirmed' => 'The user was successfully confirmed.',
            'created' => 'لقد تم إنشاء المستخدم الجديد بنجاح.',
            'deleted' => 'لقد تم إزالة المستخدم بنجاح.',
            'deleted_permanently' => 'لقد تم حذف المستخدم نهائيا بنجاح.',
            'restored' => 'لقد تمت استعادة المستخدم بنجاح.',
            'session_cleared' => "The user's session was successfully cleared.",
            'social_deleted' => 'Social Account Successfully Removed',
            'unconfirmed' => 'The user was successfully un-confirmed',
            'updated' => 'لقد تم تعديل المستخدم بنجاح.',
            'updated_password' => 'لقد تم تعديل كلمة مرور المستخدم بنجاح.',
        ],
        'countries' => [
            'created' => '.لقد تم إضافة الدولة الجديدة بنجاح',
            'deleted' => 'لقد تم مسح الدولة بنجاح.',
            'updated' => 'تم تعديل الدولة بنجاح.',
        ],

        'cities' => [
            'created' => '.لقد تم إضافة المدينة الجديدة بنجاح',
            'deleted' => 'لقد تم مسح المدينة بنجاح.',
            'updated' => 'تم تعديل المدبنة بنجاح.',
        ],

        'services' => [
            'created' => '.لقد تم إضافة الخدمة الفندقبة الجديدة بنجاح',
            'deleted' => 'لقد تم مسح الخدمة الفندقية بنجاح.',
            'updated' => 'تم تعديل الخدمة الفندقية بنجاح.',
        ],

        'categories' => [
            'created' => '.لقد تم إضافة القسم الجديد بنجاح',
            'deleted' => 'لقد تم مسح القسم بنجاح.',
            'updated' => 'تم تعديل القسم بنجاح.',
        ],

        'currencyexchanges' => [
            'created' => '.لقد تم إضافة معامل تغيير لعملة جديدة بنجاح',
            'deleted' => 'لقد تم مسح معامل التغيير بنجاح.',
            'updated' => 'تم تعديل معامل التغيير بنجاح.',
        ],

        'customers' => [
            'created' => '.لقد تم إضافة العميل بنجاح',
            'deleted' => 'لقد تم مسح العميل بنجاح.',
            'updated' => 'تم تعديل العميل بنجاح.',
        ],

        'hotels' => [
            'created' => '.لقد تم إضافة الفندق الجديد بنجاح',
            'deleted' => 'لقد تم مسح الفندق بنجاح.',
            'updated' => 'تم تعديل الفندق بنجاح.',
        ],

        'fixedtours' => [
            'created' => '.لقد تم إضافة الرحلة بنجاح',
            'deleted' => 'لقد تم مسح الرحلة بنجاح.',
            'updated' => 'تم تعديل الرحلة بنجاح.',
        ],

        'frontsliders' => [
            'created' => '.لقد تم إضافة الصورة بنجاح',
            'deleted' => 'لقد تم مسح الصورة بنجاح.',
            'updated' => 'تم تعديل الصورة بنجاح.',
        ],

        'aboutuses' => [
            'created' => '.لقد تم ادخال عن الموقع بنجاح',
            'deleted' => 'لقد تم مسح عن الموقع بنجاح.',
            'updated' => 'تم تعديل عن الموقع بنجاح.',
        ],

        'customeropinions' => [
            'created' => '.لقد تم ادخال راى العميل بنجاح',
            'deleted' => 'لقد تم مسح راى العميل بنجاح.',
            'updated' => 'تم تعديل راى العميل بنجاح.',
        ],

        'discovers' => [
            'created' => '.لقد تم ادخال اكتشف معنا بنجاح',
            'deleted' => 'لقد تم مسح اكتشف معنا بنجاح.',
            'updated' => 'تم تعديل اكتشف معنا بنجاح.',
        ],

        'citysliders' => [
            'created' => '.لقد تم ادخال صورة المدينة بنجاح',
            'deleted' => 'لقد تم مسح صورة المدينة بنجاح.',
            'updated' => 'تم تعديل صورة المدينة بنجاح.',
        ],

        'frontnews' => [
            'created' => '.لقد تم ادخال خبر جديد بنجاح',
            'deleted' => 'لقد تم مسح الخبر بنجاح.',
            'updated' => 'تم تعديل الخبر بنجاح.',
        ],

        'settings' => [
            'created' => '.لقد تم ادخال اعداد جديد بنجاح',
            'deleted' => 'لقد تم مسح الاعداد بنجاح.',
            'updated' => 'تم تعديل الاعداد بنجاح.',
        ],

        'messages' => [
            'deleted' => 'لقد تم مسح الرسالة بنجاح.',
        ],

        'fixedtourbookings' => [
            'created' => '.لقد تم حجز الرحلة بنجاح',
            'deleted' => 'لقد تم مسح بيانات الرحلة بنجاح.',
            'updated' => 'تم تعديل بيانات الرحلة بنجاح.',
        ],

        'hotelbookings' => [
            'created' => '.لقد تم حجز الفندق بنجاح',
            'deleted' => 'لقد تم مسح بيانات الفندق بنجاح.',
            'updated' => 'تم تعديل بيانات الفندق بنجاح.',
        ],
    ],

    'frontend' => [
        'contact' => [
            'sent' => 'Your information was successfully sent. We will respond back to the e-mail provided as soon as we can.',
        ],
    ],
];
