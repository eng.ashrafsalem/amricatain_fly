<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Navs Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in menu items throughout the system.
    | Regardless where it is placed, a menu item can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'general' => [
        'home' => 'الرئيسية',
        'logout' => 'تسجيل خروج',
    ],

    'frontend' => [
        'contact-us' => 'اتصل بنا',
        'news' => 'الاخبار',
        'login' => 'تسجيل دخول',
        'logout' => 'تسجيل خروج',
        'custom-tour' => 'برنامجك الخاص',
        'hotels' => 'الفنادق',
        'fixed-tours' => 'البرامج',
        'about-us' => 'من نحن ؟',
        'main' => 'الرئيسية',

        'user' => [
            'account' => 'My Account',
            'administration' => 'الإدارة',
            'change_password' => 'تغيير كلمة المرور',
            'my_information' => 'بياناتي',
            'profile' => 'Profile',
        ],
    ],
];
