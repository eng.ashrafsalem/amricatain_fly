<?php

use App\Http\Controllers\Frontend\Auth\LoginController;
use App\Http\Controllers\Frontend\ContactController;
use App\Http\Controllers\Frontend\HomeController;
use App\Http\Controllers\Frontend\User\AccountController;
use App\Http\Controllers\Frontend\User\DashboardController;
use App\Http\Controllers\Frontend\User\ProfileController;

/*
 * Frontend Controllers
 * All route names are prefixed with 'frontend.'.
 */
Route::get('/', [HomeController::class, 'index'])->name('index');
Route::post('/customerregister', [HomeController::class, 'register'])->name('register');
Route::post('customers/login','CustomerLoginController@login')->name('customers.login');
Route::get('customers/logout','CustomerLoginController@logout')->name('customers.logout');

Route::get('/about_us', [HomeController::class, 'aboutUs'])->name('aboutUs');
Route::get('/fixed_tour/{fixed_tour}', [HomeController::class, 'fixedTour'])->name('fixedTour');
Route::get('/fixed_tours', [HomeController::class, 'fixedTours'])->name('fixedTours');
//
Route::get('/fixed_tours/{category}', [HomeController::class, 'fixedToursByCategory'])->name('fixedToursByCategory');
Route::get('/fixed_tour_booking/{fixed_tour}', [HomeController::class, 'fixedTourBooking'])->name('fixedTourBooking');
Route::post('/fixed_tour_booking', [HomeController::class, 'saveFixedTourBooking'])->name('saveFixedTourBooking');
Route::get('/single_hotel/{hotel}', [HomeController::class, 'singleHotel'])->name('singleHotel');
Route::get('/single_news/{news}', [HomeController::class, 'singleNews'])->name('singleNews');
Route::get('/all_hotel', [HomeController::class, 'allHotels'])->name('allHotels');
Route::get('/all_news', [HomeController::class, 'allnews'])->name('allnews');
Route::get('/single_hotel_booking/{hotel}', [HomeController::class, 'singleHotelBooking'])->name('singleHotelBooking');
Route::post('/single_hotel_booking', [HomeController::class, 'saveSingleHotelBooking'])->name('saveSingleHotelBooking');
Route::get('/custom_tour', [HomeController::class, 'customTour'])->name('CustomTour');
Route::post('/save_custom_tour', [HomeController::class, 'saveCustomTourBooking'])->name('saveCustomTourBooking');
Route::post('/storeContactUsForm', [HomeController::class, 'storeContactUsForm'])->name('storeContactUsForm');
Route::get('contact', [ContactController::class, 'index'])->name('contact');
Route::post('contact/send', [ContactController::class, 'send'])->name('contact.send');
//Route::get('logout', [LoginController::class, 'logout2'])->name('logout2');
Route::get('FaceBookUrl', [HomeController::class, 'FaceBookUrl'])->name('FaceBookUrl');
Route::get('GoogleUrl', [HomeController::class, 'GoogleUrl'])->name('GoogleUrl');
Route::get('InstagramUrl', [HomeController::class, 'InstagramUrl'])->name('InstagramUrl');
/*
 * These frontend controllers require the user to be logged in
 * All route names are prefixed with 'frontend.'
 * These routes can not be hit if the password is expired
 */
Route::group(['middleware' => ['auth', 'password_expires']], function () {
    Route::group(['namespace' => 'User', 'as' => 'user.'], function () {
        // User Dashboard Specific
        Route::get('dashboard', [DashboardController::class, 'index'])->name('dashboard');

        // User Account Specific
        Route::get('account', [AccountController::class, 'index'])->name('account');

        // User Profile Specific
        Route::patch('profile/update', [ProfileController::class, 'update'])->name('profile.update');
    });
});
