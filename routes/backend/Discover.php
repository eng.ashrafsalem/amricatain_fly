<?php

// All route names are prefixed with 'admin.auth'.
Route::group([
    'namespace' => 'Discover',
    'middleware' => 'role:' . config('access.users.admin_role'),
], function () {

    Route::resource('discovers', 'DiscoverController');

});
