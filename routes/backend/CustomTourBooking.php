<?php

// All route names are prefixed with 'admin.auth'.
Route::group([
    'namespace' => 'CustomTourBooking',
    'middleware' => 'role:' . config('access.users.admin_role'),
], function () {

    Route::resource('customtourbookings', 'CustomTourBookingController');

});
