<?php

// All route names are prefixed with 'admin.auth'.
Route::group([
    'namespace' => 'Country',
    'middleware' => 'role:' . config('access.users.admin_role'),
], function () {

    Route::resource('countries', 'CountryController');

});
