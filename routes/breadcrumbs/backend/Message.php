<?php

Breadcrumbs::for('admin.messages.index', function ($trail) {
    $trail->push(__('labels.backend.messages.management'), route('admin.messages.index'));
});

Breadcrumbs::for('admin.messages.create', function ($trail) {
    $trail->parent('admin.messages.index');
    $trail->push(__('labels.backend.messages.create'), route('admin.messages.index'));
});


Breadcrumbs::for('admin.messages.edit', function ($trail) {
    $trail->parent('admin.messages.index');
    $trail->push(__('labels.backend.messages.edit'), route('admin.messages.index'));
});
