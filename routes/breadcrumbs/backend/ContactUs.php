<?php

Breadcrumbs::for('admin.ContactUss.index', function ($trail) {
    $trail->push(__('labels.backend.ContactUs.management'), route('admin.ContactUss.index'));
});

Breadcrumbs::for('admin.ContactUss.create', function ($trail) {
    $trail->parent('admin.ContactUss.index');
    $trail->push(__('labels.backend.ContactUs.create'), route('admin.ContactUss.index'));
});


Breadcrumbs::for('admin.ContactUss.edit', function ($trail) {
    $trail->parent('admin.ContactUss.index');
    $trail->push(__('labels.backend.ContactUs.edit'), route('admin.ContactUss.index'));
});
