<?php

Breadcrumbs::for('admin.customers.index', function ($trail) {
    $trail->push(__('labels.backend.customers.management'), route('admin.customers.index'));
});

Breadcrumbs::for('admin.customers.create', function ($trail) {
    $trail->parent('admin.customers.index');
    $trail->push(__('labels.backend.customers.create'), route('admin.customers.index'));
});


Breadcrumbs::for('admin.customers.edit', function ($trail) {
    $trail->parent('admin.customers.index');
    $trail->push(__('labels.backend.customers.edit'), route('admin.customers.index'));
});
