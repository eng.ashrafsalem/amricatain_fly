<?php

Breadcrumbs::for('admin.discovers.index', function ($trail) {
    $trail->push(__('labels.backend.discovers.management'), route('admin.discovers.index'));
});

Breadcrumbs::for('admin.discovers.create', function ($trail) {
    $trail->parent('admin.discovers.index');
    $trail->push(__('labels.backend.discovers.create'), route('admin.discovers.index'));
});


Breadcrumbs::for('admin.discovers.edit', function ($trail) {
    $trail->parent('admin.discovers.index');
    $trail->push(__('labels.backend.discovers.edit'), route('admin.discovers.index'));
});
