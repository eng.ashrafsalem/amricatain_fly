<?php

Breadcrumbs::for('admin.customeropinions.index', function ($trail) {
    $trail->push(__('labels.backend.customeropinions.management'), route('admin.customeropinions.index'));
});

Breadcrumbs::for('admin.customeropinions.create', function ($trail) {
    $trail->parent('admin.customeropinions.index');
    $trail->push(__('labels.backend.customeropinions.create'), route('admin.customeropinions.index'));
});


Breadcrumbs::for('admin.customeropinions.edit', function ($trail) {
    $trail->parent('admin.customeropinions.index');
    $trail->push(__('labels.backend.customeropinions.edit'), route('admin.customeropinions.index'));
});
