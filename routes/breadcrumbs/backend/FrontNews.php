<?php

Breadcrumbs::for('admin.frontnews.index', function ($trail) {
    $trail->push(__('labels.backend.frontnews.management'), route('admin.frontnews.index'));
});

Breadcrumbs::for('admin.frontnews.create', function ($trail) {
    $trail->parent('admin.frontnews.index');
    $trail->push(__('labels.backend.frontnews.create'), route('admin.frontnews.index'));
});


Breadcrumbs::for('admin.frontnews.edit', function ($trail) {
    $trail->parent('admin.frontnews.index');
    $trail->push(__('labels.backend.frontnews.edit'), route('admin.frontnews.index'));
});
