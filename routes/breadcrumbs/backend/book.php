<?php

Breadcrumbs::for('admin.books.index', function ($trail) {
    $trail->push(__('labels.backend.books.management'), route('admin.books.index'));
});

Breadcrumbs::for('admin.books.create', function ($trail) {
    $trail->parent('admin.books.index');
    $trail->push(__('labels.backend.books.create'), route('admin.books.index'));
});


Breadcrumbs::for('admin.books.edit', function ($trail) {
    $trail->parent('admin.books.index');
    $trail->push(__('labels.backend.books.edit'), route('admin.books.index'));
});
