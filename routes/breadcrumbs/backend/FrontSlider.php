<?php

Breadcrumbs::for('admin.frontsliders.index', function ($trail) {
    $trail->push(__('labels.backend.frontsliders.management'), route('admin.frontsliders.index'));
});

Breadcrumbs::for('admin.frontsliders.create', function ($trail) {
    $trail->parent('admin.frontsliders.index');
    $trail->push(__('labels.backend.frontsliders.create'), route('admin.frontsliders.index'));
});


Breadcrumbs::for('admin.frontsliders.edit', function ($trail) {
    $trail->parent('admin.frontsliders.index');
    $trail->push(__('labels.backend.frontsliders.edit'), route('admin.frontsliders.index'));
});
