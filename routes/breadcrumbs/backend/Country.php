<?php

Breadcrumbs::for('admin.countries.index', function ($trail) {
    $trail->push(__('labels.backend.countries.management'), route('admin.countries.index'));
});

Breadcrumbs::for('admin.countries.create', function ($trail) {
    $trail->parent('admin.countries.index');
    $trail->push(__('labels.backend.countries.create'), route('admin.countries.index'));
});


Breadcrumbs::for('admin.countries.edit', function ($trail) {
    $trail->parent('admin.countries.index');
    $trail->push(__('labels.backend.countries.edit'), route('admin.countries.index'));
});
