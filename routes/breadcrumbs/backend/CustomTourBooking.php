<?php

Breadcrumbs::for('admin.customtourbookings.index', function ($trail) {
    $trail->push(__('labels.backend.customtourbookings.management'), route('admin.customtourbookings.index'));
});

Breadcrumbs::for('admin.customtourbookings.create', function ($trail) {
    $trail->parent('admin.customtourbookings.index');
    $trail->push(__('labels.backend.customtourbookings.create'), route('admin.customtourbookings.index'));
});


Breadcrumbs::for('admin.customtourbookings.edit', function ($trail) {
    $trail->parent('admin.customtourbookings.index');
    $trail->push(__('labels.backend.customtourbookings.edit'), route('admin.customtourbookings.index'));
});
