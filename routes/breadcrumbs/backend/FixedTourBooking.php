<?php

Breadcrumbs::for('admin.fixedtourbookings.index', function ($trail) {
    $trail->push(__('labels.backend.fixedtourbookings.management'), route('admin.fixedtourbookings.index'));
});

Breadcrumbs::for('admin.fixedtourbookings.create', function ($trail) {
    $trail->parent('admin.fixedtourbookings.index');
    $trail->push(__('labels.backend.fixedtourbookings.create'), route('admin.fixedtourbookings.index'));
});


Breadcrumbs::for('admin.fixedtourbookings.edit', function ($trail) {
    $trail->parent('admin.fixedtourbookings.index');
    $trail->push(__('labels.backend.fixedtourbookings.edit'), route('admin.fixedtourbookings.index'));
});
