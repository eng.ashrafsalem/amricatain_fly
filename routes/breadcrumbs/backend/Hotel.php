<?php

Breadcrumbs::for('admin.hotels.index', function ($trail) {
    $trail->push(__('labels.backend.hotels.management'), route('admin.hotels.index'));
});

Breadcrumbs::for('admin.hotels.create', function ($trail) {
    $trail->parent('admin.hotels.index');
    $trail->push(__('labels.backend.hotels.create'), route('admin.hotels.index'));
});


Breadcrumbs::for('admin.hotels.edit', function ($trail) {
    $trail->parent('admin.hotels.index');
    $trail->push(__('labels.backend.hotels.edit'), route('admin.hotels.index'));
});
