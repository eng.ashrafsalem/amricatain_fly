<?php

Breadcrumbs::for('admin.dashboard', function ($trail) {
    $trail->push(__('strings.backend.dashboard.title'), route('admin.dashboard'));
});

require __DIR__.'/auth.php';
require __DIR__.'/log-viewer.php';
require __DIR__.'/Country.php';
require __DIR__.'/City.php';
require __DIR__.'/Service.php';
require __DIR__.'/Hotel.php';
require __DIR__.'/Category.php';
require __DIR__.'/}عقCurrancyExchange.php';
require __DIR__.'/CurrencyExchange.php';
require __DIR__.'/Customer.php';
require __DIR__.'/ContactUs.php';
require __DIR__.'/User.php';
require __DIR__.'/FixedTour.php';
require __DIR__.'/FrontSlider.php';
require __DIR__.'/Aboutus.php';
require __DIR__.'/CustomerOpinions.php';
require __DIR__.'/Discover.php';
require __DIR__.'/CitySlider.php';
require __DIR__.'/FrontNews.php';
require __DIR__.'/Setting.php';
require __DIR__.'/Message.php';
require __DIR__.'/FixedTourBooking.php';
require __DIR__.'/HotelBooking.php';
require __DIR__.'/CustomTourBooking.php';
require __DIR__.'/book.php';
