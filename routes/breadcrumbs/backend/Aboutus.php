<?php

Breadcrumbs::for('admin.aboutuses.index', function ($trail) {
    $trail->push(__('labels.backend.aboutuses.management'), route('admin.aboutuses.index'));
});

Breadcrumbs::for('admin.aboutuses.create', function ($trail) {
    $trail->parent('admin.aboutuses.index');
    $trail->push(__('labels.backend.aboutuses.create'), route('admin.aboutuses.index'));
});


Breadcrumbs::for('admin.aboutuses.edit', function ($trail) {
    $trail->parent('admin.aboutuses.index');
    $trail->push(__('labels.backend.aboutuses.edit'), route('admin.aboutuses.index'));
});
