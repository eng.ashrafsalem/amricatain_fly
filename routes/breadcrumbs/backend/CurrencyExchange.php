<?php

Breadcrumbs::for('admin.currencyexchanges.index', function ($trail) {
    $trail->push(__('labels.backend.currencyexchanges.management'), route('admin.currencyexchanges.index'));
});

Breadcrumbs::for('admin.currencyexchanges.create', function ($trail) {
    $trail->parent('admin.currencyexchanges.index');
    $trail->push(__('labels.backend.currencyexchanges.create'), route('admin.currencyexchanges.index'));
});


Breadcrumbs::for('admin.currencyexchanges.edit', function ($trail) {
    $trail->parent('admin.currencyexchanges.index');
    $trail->push(__('labels.backend.currencyexchanges.edit'), route('admin.currencyexchanges.index'));
});
