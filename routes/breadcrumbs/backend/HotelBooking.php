<?php

Breadcrumbs::for('admin.hotelbookings.index', function ($trail) {
    $trail->push(__('labels.backend.hotelbookings.management'), route('admin.hotelbookings.index'));
});

Breadcrumbs::for('admin.hotelbookings.create', function ($trail) {
    $trail->parent('admin.hotelbookings.index');
    $trail->push(__('labels.backend.hotelbookings.create'), route('admin.hotelbookings.index'));
});


Breadcrumbs::for('admin.hotelbookings.edit', function ($trail) {
    $trail->parent('admin.hotelbookings.index');
    $trail->push(__('labels.backend.hotelbookings.edit'), route('admin.hotelbookings.index'));
});
