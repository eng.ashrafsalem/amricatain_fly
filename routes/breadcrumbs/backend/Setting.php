<?php

Breadcrumbs::for('admin.settings.index', function ($trail) {
    $trail->push(__('labels.backend.settings.management'), route('admin.settings.index'));
});

Breadcrumbs::for('admin.settings.create', function ($trail) {
    $trail->parent('admin.settings.index');
    $trail->push(__('labels.backend.settings.create'), route('admin.settings.index'));
});


Breadcrumbs::for('admin.settings.edit', function ($trail) {
    $trail->parent('admin.settings.index');
    $trail->push(__('labels.backend.settings.edit'), route('admin.settings.index'));
});
