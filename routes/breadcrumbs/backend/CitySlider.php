<?php

Breadcrumbs::for('admin.citysliders.index', function ($trail) {
    $trail->push(__('labels.backend.citysliders.management'), route('admin.citysliders.index'));
});

Breadcrumbs::for('admin.citysliders.create', function ($trail) {
    $trail->parent('admin.citysliders.index');
    $trail->push(__('labels.backend.citysliders.create'), route('admin.citysliders.index'));
});


Breadcrumbs::for('admin.citysliders.edit', function ($trail) {
    $trail->parent('admin.citysliders.index');
    $trail->push(__('labels.backend.citysliders.edit'), route('admin.citysliders.index'));
});
