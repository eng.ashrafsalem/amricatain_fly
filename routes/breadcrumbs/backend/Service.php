<?php

Breadcrumbs::for('admin.services.index', function ($trail) {
    $trail->push(__('labels.backend.services.management'), route('admin.services.index'));
});

Breadcrumbs::for('admin.services.create', function ($trail) {
    $trail->parent('admin.services.index');
    $trail->push(__('labels.backend.services.create'), route('admin.services.index'));
});


Breadcrumbs::for('admin.services.edit', function ($trail) {
    $trail->parent('admin.services.index');
    $trail->push(__('labels.backend.services.edit'), route('admin.services.index'));
});
