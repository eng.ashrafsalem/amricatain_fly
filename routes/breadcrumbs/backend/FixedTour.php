<?php

Breadcrumbs::for('admin.fixedtours.index', function ($trail) {
    $trail->push(__('labels.backend.fixedtours.management'), route('admin.fixedtours.index'));
});

Breadcrumbs::for('admin.fixedtours.create', function ($trail) {
    $trail->parent('admin.fixedtours.index');
    $trail->push(__('labels.backend.fixedtours.create'), route('admin.fixedtours.index'));
});


Breadcrumbs::for('admin.fixedtours.edit', function ($trail) {
    $trail->parent('admin.fixedtours.index');
    $trail->push(__('labels.backend.fixedtours.edit'), route('admin.fixedtours.index'));
});
