@extends('frontend.layouts.app')

{{--@section('title', app_name() . ' | ' . __('navs.general.home'))--}}

@push('after-scripts')
    <script>
        let lang = '{{App::getLocale()}}';
        if( lang == "ar")
        {
            // let owl = $("#owl-demo5");
            // owl.trigger('refresh.owl.carousel');
            // $("#owl-demo5").owlCarousel({
            //     rtl: true,
            //     item:2,
            //     loop: false,
            //     mouseDrag: false,
            //     touchDrag: false,
            //     pullDrag: false,
            //     rewind: true,
            //     autoplay: true,
            //     margin: 0,
            //     nav: true
            // });

        }
    </script>
@endpush

@section('content')
    <section id="carousel_slider_front">
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel"  style="margin-top: 100px">
        <div class="carousel-inner" role="listbox">
            @if(isset($front_sliders) && $front_sliders->count() != 0 )
                @foreach($front_sliders as $slider)
                    <div class="item active">
                        <div class="overlay"></div>
                        <img class="img-responsive" src="{{ \App\Models\Image::where('imageable_type', \App\Models\FrontSliders\FrontSlider::class)->where('imageable_id', $slider->id)->first()->image_url }}" alt="slider1">
                        <div class="carousel-caption">
                            {!! $slider->getTranslation(App::getLocale())['title'] !!}
                        </div>
                    </div>
                @endforeach

                    <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                        <span class="fa fa-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                        <span class="fa fa-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>


                    <div class="seperator"><img class="img-responsive" src="{{ asset('images/seperator.png')}}"></div>
            @else
                <div class="h1 text-center">No Slider Available</div>
            @endif

        </div>
        <!-- Controls -->

    </div>

    </section>

    <!-- about us section -->
    <section class="aboutus padding-mobile">
        <div class="container">
            @if(isset($all_about_us_section) && $all_about_us_section->count() != 0 )
                <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="content">
                        {!!  $about_us_first_row->getTranslation(App::getLocale())['contents'] !!}
                        <div class="buttons">
                            <a href="{{ route('frontend.aboutUs') }}" class="btn btn-primary btn-lg">
                                @lang('labels.frontend.aboutUs.more')
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div id="owl-demo5" class="owl-carousel">
                        @foreach($all_about_us_section as $about)
                        <div class="item">
                            <div class="img-container1">
                                <img class="img-responsive" src="{{\App\Models\Image::where('imageable_type', \App\Models\Aboutuses\Aboutus::class)->where('imageable_id', $about->id)->first()->image_url }}" alt="">
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
            @else
                <div class="h1"> No About us Section Available </div>
            @endif
        </div>
    </section>

    <!-- fixed tours section -->
    <section class="programs padding-mobile">
        <div class="container">
            <h1 class="text-center">@lang('labels.frontend.fixed_tour.program')</h1>
            @if(isset($fixed_tours) && $fixed_tours->count() != 0 )
            <div class="content">
                <div id="owl-demo" class="owl-carousel">
                    @foreach($fixed_tours as $tour)

                        <div class="item">
                            <a href="{{ route('frontend.fixedTour', ['fixed_tour' => $tour->id]) }}">
                                <div class="card">
                            <div class="card-img">
                                <img src="{{str_replace("public/", "", \App\Models\Image::where('imageable_type', \App\Models\FixedTours\FixedTour::class)->where('imageable_id', $tour->id)->first()->image_url)   }}">
                            </div>
                            <div class="card-body">
                                <div class="content">
                                    <h4>{{ $tour->translate(app()->getLocale())['name'] }}</h4>
                                    <p>
                                        @for($i = 0 ; $i < $tour->hotel->level; $i++)
                                        <i class="fa fa-star"></i>
                                        @endfor
                                        {{ $tour->hotel->level }}
                                    </p>
                                    <ul class="features">
                                        <li><i class="fa fa-map-marker"></i> {{ $tour->category->name }}</li>
                                        <li><i class="fa fa-clock-o"></i>{{ $tour->days_nights_counts }}</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="price-box">
                                    <span>{{ $tour->total_cost }} </span>
                                </div>
                            </div>
                        </div>
                            </a>
                        </div>
                    @endforeach
                </div>
                <div class="button-all text-center">
                    <a class="btn btn-primary btn-lg" href="{{ route('frontend.fixedTours') }}">@lang('labels.frontend.fixed_tour.programs') <i class="fa fa-arrow-arrow"></i></a>
                </div>
            </div>
            @else
                <div class="h1">No Programs available</div>
            @endif
        </div>
    </section>

    <section class="destination padding-mobile">
        <div class="container">
            @if(isset($discover) && $discover->count() != 0 )
            <div class="row">
                <div class="col-lg-6">
                    <h1>
                        {{ $discover->translate(app()->getLocale())->title }}
                    </h1>
                </div>
                <div class="col-lg-6">
                    <p>
                        {{ $discover->translate(app()->getLocale())->description }}
                    </p>
                </div>
            </div>
            @else
                <div class="h1">No Discover Section</div>
            @endif
        </div>
        <div class="container-fluid">
            @if(isset($citiesImages) && $citiesImages->count() != 0)
            <div class="row">
                <div id="owl-demo1" class="owl-carousel">
                    @foreach($citiesImages as $image)
                        <div class="item">
                        <div class="card-img1">
                            <img src="{{str_replace("public/", "", \App\Models\Image::where('imageable_type', \App\Models\CitySliders\CitySlider::class)->where('imageable_id', $image->id)->first()->image_url)   }}">
                            <div class="img-desc text-center">
                                <h3>{!!   $image->translate(app()->getLocale())->title !!}</h3>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
            @else
                <div class="h1">No City Slider available</div>
            @endif
        </div>
    </section>

    <!-- start hotel section  -->
    <section class="hotel padding-mobile">
        <div class="container">
            <div class="text-center h1 text-red">
                 @lang('labels.frontend.hotels.hotels')
            </div>
            @if(isset($citiesImages) && $citiesImages->count() != 0)
                <div class="row">
                    @foreach($hotels as $hotel)
                        <div class="col-lg-4 col-sm-6 col-xs-12">
                        <div class="card">
                            <a href="{{ route('frontend.singleHotel', ['hotel' => $hotel->id]) }}">
                                <div class="card-img">
                                    <img class="img-responsive" src="{{str_replace("public/", "", \App\Models\Image::where('imageable_type', \App\Models\Hotels\Hotel::class)->where('imageable_id', $hotel->id)->first()->image_url)   }}">
                                </div>
                                <div class="card-body">
                                    <div class="content">
                                        <h4>{{ $hotel->name }}</h4>
                                        <p>
                                            @for($i = 0 ; $i < $hotel->level; $i++)
                                                <i class="fa fa-star"></i>
                                            @endfor
                                            {{ $hotel->level }}
                                        </p>
                                        <ul class="features">
                                            @foreach($hotel->services as $service)
                                            <li> {{$service->name}} </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
{{--                                <div class="card-footer">--}}
{{--                                    <div class="price-box">--}}
{{--                                        <span>800 جنيه</span>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
                            </a>
                        </div>
                    </div>
                    @endforeach
                </div>
                <div class="button-all text-center">
                    <a class="btn btn-primary btn-lg" href="{{ route('frontend.allHotels') }}">@lang('labels.frontend.hotels.all_hotels')  <i class="fa fa-arrow-right"></i></a>
                </div>
            @else
                <div class="h1">No Hotels Available</div>
            @endif
        </div>
    </section>
@endsection
