-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 21, 2020 at 05:26 PM
-- Server version: 5.7.29
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `flyware_amricatain`
--

-- --------------------------------------------------------

--
-- Table structure for table `cache`
--

CREATE TABLE `cache` (
  `key` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `expiration` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '2020-02-25 17:27:09', '2020-02-25 17:27:09', NULL),
(2, '2020-02-25 17:27:45', '2020-02-25 17:27:45', NULL),
(4, '2020-04-15 13:17:54', '2020-04-15 13:17:54', NULL),
(5, '2020-04-15 13:18:25', '2020-04-15 13:18:25', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `categories_translations`
--

CREATE TABLE `categories_translations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `locale` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories_translations`
--

INSERT INTO `categories_translations` (`id`, `category_id`, `name`, `locale`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'حج', 'ar', '2020-02-25 17:27:09', '2020-02-25 17:27:09', NULL),
(2, 1, 'hag', 'en', '2020-02-25 17:27:09', '2020-02-25 17:27:09', NULL),
(3, 1, 'hag', 'fr', '2020-02-25 17:27:09', '2020-02-25 17:27:09', NULL),
(4, 1, 'hag', 'it', '2020-02-25 17:27:09', '2020-02-25 17:27:09', NULL),
(5, 1, 'hag', 'es', '2020-02-25 17:27:09', '2020-02-25 17:27:09', NULL),
(6, 2, 'عمرة', 'ar', '2020-02-25 17:27:45', '2020-02-25 17:27:45', NULL),
(7, 2, 'omra', 'en', '2020-02-25 17:27:45', '2020-02-25 17:27:45', NULL),
(8, 2, 'omra', 'fr', '2020-02-25 17:27:45', '2020-02-25 17:27:45', NULL),
(9, 2, 'omra', 'it', '2020-02-25 17:27:45', '2020-02-25 17:27:45', NULL),
(10, 2, 'omra', 'es', '2020-02-25 17:27:45', '2020-02-25 17:27:45', NULL),
(11, 3, 'عمرة', 'ar', '2020-02-25 17:43:41', '2020-02-25 17:43:41', NULL),
(12, 3, 'omra', 'en', '2020-02-25 17:43:41', '2020-02-25 17:43:41', NULL),
(13, 3, 'omra', 'fr', '2020-02-25 17:43:41', '2020-02-25 17:43:41', NULL),
(14, 3, 'omra', 'it', '2020-02-25 17:43:41', '2020-02-25 17:43:41', NULL),
(15, 3, 'omra', 'es', '2020-02-25 17:43:41', '2020-02-25 17:43:41', NULL),
(16, 4, 'سياحة داخلية', 'ar', '2020-04-15 13:17:54', '2020-04-15 13:17:54', NULL),
(17, 4, 'Domestic Tourism', 'en', '2020-04-15 13:17:54', '2020-04-15 13:17:54', NULL),
(18, 4, 'Domestic Tourism', 'fr', '2020-04-15 13:17:54', '2020-04-15 13:17:54', NULL),
(19, 4, 'Domestic Tourism', 'it', '2020-04-15 13:17:54', '2020-04-15 13:17:54', NULL),
(20, 4, 'Domestic Tourism', 'es', '2020-04-15 13:17:54', '2020-04-15 13:17:54', NULL),
(21, 5, 'سياحة خارجية', 'ar', '2020-04-15 13:18:25', '2020-04-15 13:18:25', NULL),
(22, 5, 'Foreign tourism', 'en', '2020-04-15 13:18:25', '2020-04-15 13:18:25', NULL),
(23, 5, 'Foreign tourism', 'fr', '2020-04-15 13:18:25', '2020-04-15 13:18:25', NULL),
(24, 5, 'Foreign tourism', 'it', '2020-04-15 13:18:25', '2020-04-15 13:18:25', NULL),
(25, 5, 'Foreign tourism', 'es', '2020-04-15 13:18:25', '2020-04-15 13:18:25', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE `cities` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `country_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`id`, `country_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(3, 4, '2020-04-15 13:15:12', '2020-04-15 13:15:12', NULL),
(2, 2, '2020-02-25 17:22:23', '2020-02-25 17:22:23', NULL),
(4, 5, '2020-04-15 13:15:44', '2020-04-15 13:15:44', NULL),
(5, 2, '2020-04-15 13:16:14', '2020-04-15 13:16:14', NULL),
(6, 6, '2020-04-15 13:17:16', '2020-04-15 13:17:16', NULL),
(7, 3, '2020-04-15 13:41:36', '2020-04-15 13:41:36', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cities_translations`
--

CREATE TABLE `cities_translations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `city_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `locale` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cities_translations`
--

INSERT INTO `cities_translations` (`id`, `city_id`, `name`, `locale`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'swecwe', 'ar', '2020-02-20 17:29:52', '2020-02-20 17:29:52', NULL),
(2, 1, 'wvwrvr', 'en', '2020-02-20 17:29:52', '2020-02-20 17:29:52', NULL),
(3, 1, 'vwrvwrv', 'fr', '2020-02-20 17:29:52', '2020-02-20 17:29:52', NULL),
(4, 1, 'vwrvrwv', 'it', '2020-02-20 17:29:52', '2020-02-20 17:29:52', NULL),
(5, 1, 'vwrvwrv', 'es', '2020-02-20 17:29:52', '2020-02-20 17:29:52', NULL),
(6, 2, 'الاسكندرية', 'ar', '2020-02-25 17:22:23', '2020-02-25 17:22:23', NULL),
(7, 2, 'alexandria', 'en', '2020-02-25 17:22:23', '2020-02-25 17:22:23', NULL),
(8, 2, 'alexandria', 'fr', '2020-02-25 17:22:23', '2020-02-25 17:22:23', NULL),
(9, 2, 'alexandria', 'it', '2020-02-25 17:22:23', '2020-02-25 17:22:23', NULL),
(10, 2, 'alexandria', 'es', '2020-02-25 17:22:23', '2020-02-25 17:22:23', NULL),
(11, 3, 'مدريد', 'ar', '2020-04-15 13:15:12', '2020-04-15 13:15:12', NULL),
(12, 3, 'Madrid', 'en', '2020-04-15 13:15:12', '2020-04-15 13:15:12', NULL),
(13, 3, 'Madrid', 'fr', '2020-04-15 13:15:12', '2020-04-15 13:15:12', NULL),
(14, 3, 'Madrid', 'it', '2020-04-15 13:15:12', '2020-04-15 13:15:12', NULL),
(15, 3, 'Madrid', 'es', '2020-04-15 13:15:12', '2020-04-15 13:15:12', NULL),
(16, 4, 'باريس', 'ar', '2020-04-15 13:15:44', '2020-04-15 13:15:44', NULL),
(17, 4, 'Paris', 'en', '2020-04-15 13:15:44', '2020-04-15 13:15:44', NULL),
(18, 4, 'Paris', 'fr', '2020-04-15 13:15:44', '2020-04-15 13:15:44', NULL),
(19, 4, 'Paris', 'it', '2020-04-15 13:15:44', '2020-04-15 13:15:44', NULL),
(20, 4, 'Paris', 'es', '2020-04-15 13:15:44', '2020-04-15 13:15:44', NULL),
(21, 5, 'الغردقة', 'ar', '2020-04-15 13:16:14', '2020-04-15 13:16:14', NULL),
(22, 5, 'Hurghada', 'en', '2020-04-15 13:16:14', '2020-04-15 13:16:14', NULL),
(23, 5, 'Hurghada', 'fr', '2020-04-15 13:16:14', '2020-04-15 13:16:14', NULL),
(24, 5, 'Hurghada', 'it', '2020-04-15 13:16:14', '2020-04-15 13:16:14', NULL),
(25, 5, 'Hurghada', 'es', '2020-04-15 13:16:14', '2020-04-15 13:16:14', NULL),
(26, 6, 'مكة', 'ar', '2020-04-15 13:17:16', '2020-04-15 13:17:16', NULL),
(27, 6, 'Makkah', 'en', '2020-04-15 13:17:16', '2020-04-15 13:17:16', NULL),
(28, 6, 'Makkah', 'fr', '2020-04-15 13:17:16', '2020-04-15 13:17:16', NULL),
(29, 6, 'Makkah', 'it', '2020-04-15 13:17:16', '2020-04-15 13:17:16', NULL),
(30, 6, 'Makkah', 'es', '2020-04-15 13:17:16', '2020-04-15 13:17:16', NULL),
(31, 7, 'روما', 'ar', '2020-04-15 13:41:36', '2020-04-15 13:41:36', NULL),
(32, 7, 'Rome', 'en', '2020-04-15 13:41:36', '2020-04-15 13:41:36', NULL),
(33, 7, 'Rome', 'fr', '2020-04-15 13:41:36', '2020-04-15 13:41:36', NULL),
(34, 7, 'Rome', 'it', '2020-04-15 13:41:36', '2020-04-15 13:41:36', NULL),
(35, 7, 'Rome', 'es', '2020-04-15 13:41:36', '2020-04-15 13:41:36', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(4, '2020-04-15 13:14:04', '2020-04-15 13:14:04', NULL),
(2, '2020-02-25 17:21:24', '2020-02-25 17:21:24', NULL),
(3, '2020-04-15 13:13:36', '2020-04-15 13:13:36', NULL),
(5, '2020-04-15 13:14:32', '2020-04-15 13:14:32', NULL),
(6, '2020-04-15 13:16:49', '2020-04-15 13:16:49', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `countries_translations`
--

CREATE TABLE `countries_translations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `country_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `locale` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `countries_translations`
--

INSERT INTO `countries_translations` (`id`, `country_id`, `name`, `locale`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'saudia', 'ar', '2020-02-20 17:29:16', '2020-02-20 17:29:16', NULL),
(2, 1, 'saudia', 'en', '2020-02-20 17:29:16', '2020-02-20 17:29:16', NULL),
(3, 1, 'saudia', 'fr', '2020-02-20 17:29:16', '2020-02-20 17:29:16', NULL),
(4, 1, 'saudia', 'it', '2020-02-20 17:29:16', '2020-02-20 17:29:16', NULL),
(5, 1, 'saudia', 'es', '2020-02-20 17:29:16', '2020-02-20 17:29:16', NULL),
(6, 2, 'مصر', 'ar', '2020-02-25 17:21:24', '2020-02-25 17:21:24', NULL),
(7, 2, 'egypt', 'en', '2020-02-25 17:21:24', '2020-02-25 17:21:24', NULL),
(8, 2, 'egypt', 'fr', '2020-02-25 17:21:24', '2020-02-25 17:21:24', NULL),
(9, 2, 'egypt', 'it', '2020-02-25 17:21:24', '2020-02-25 17:21:24', NULL),
(10, 2, 'egypt', 'es', '2020-02-25 17:21:24', '2020-02-25 17:21:24', NULL),
(11, 3, 'ايطاليا', 'ar', '2020-04-15 13:13:36', '2020-04-15 13:13:36', NULL),
(12, 3, 'Italy', 'en', '2020-04-15 13:13:36', '2020-04-15 13:13:36', NULL),
(13, 3, 'Italy', 'fr', '2020-04-15 13:13:36', '2020-04-15 13:13:36', NULL),
(14, 3, 'Italy', 'it', '2020-04-15 13:13:36', '2020-04-15 13:13:36', NULL),
(15, 3, 'Italy', 'es', '2020-04-15 13:13:36', '2020-04-15 13:13:36', NULL),
(16, 4, 'اسبانيا', 'ar', '2020-04-15 13:14:04', '2020-04-15 13:14:04', NULL),
(17, 4, 'Spain', 'en', '2020-04-15 13:14:04', '2020-04-15 13:14:04', NULL),
(18, 4, 'Spain', 'fr', '2020-04-15 13:14:04', '2020-04-15 13:14:04', NULL),
(19, 4, 'Spain', 'it', '2020-04-15 13:14:04', '2020-04-15 13:14:04', NULL),
(20, 4, 'Spain', 'es', '2020-04-15 13:14:04', '2020-04-15 13:14:04', NULL),
(21, 5, 'فرنسا', 'ar', '2020-04-15 13:14:32', '2020-04-15 13:14:32', NULL),
(22, 5, 'France', 'en', '2020-04-15 13:14:32', '2020-04-15 13:14:32', NULL),
(23, 5, 'France', 'fr', '2020-04-15 13:14:32', '2020-04-15 13:14:32', NULL),
(24, 5, 'France', 'it', '2020-04-15 13:14:32', '2020-04-15 13:14:32', NULL),
(25, 5, 'France', 'es', '2020-04-15 13:14:32', '2020-04-15 13:14:32', NULL),
(26, 6, 'السعودية', 'ar', '2020-04-15 13:16:49', '2020-04-15 13:16:49', NULL),
(27, 6, 'Saudi Arabia', 'en', '2020-04-15 13:16:49', '2020-04-15 13:16:49', NULL),
(28, 6, 'Saudi Arabia', 'fr', '2020-04-15 13:16:49', '2020-04-15 13:16:49', NULL),
(29, 6, 'Saudi Arabia', 'it', '2020-04-15 13:16:49', '2020-04-15 13:16:49', NULL),
(30, 6, 'Saudi Arabia', 'es', '2020-04-15 13:16:49', '2020-04-15 13:16:49', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `currency_exchanges`
--

CREATE TABLE `currency_exchanges` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `exchange_rate` double NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `currency_exchanges`
--

INSERT INTO `currency_exchanges` (`id`, `name`, `exchange_rate`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'ريال', 4.4, '2020-02-25 17:28:21', '2020-02-25 17:28:21', NULL),
(2, 'دولار', 16.1, '2020-02-25 17:30:59', '2020-02-25 17:30:59', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `age` smallint(6) NOT NULL,
  `isAdmin` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `name`, `email`, `phone`, `address`, `gender`, `password`, `age`, `isAdmin`, `created_at`, `updated_at`, `deleted_at`) VALUES
(5, 'hassan', 'etshhassan@mail.com', '1276521657', 'الاسكندرية', 'male', '$2y$10$d4cvmc4YDdXTA7AlcbRJs.tewAZgCVLDASziWqPD963lZNsnRBrSS', 28, 0, '2020-04-18 16:15:50', '2020-04-18 16:15:50', NULL),
(2, 'hassan mohamed', 'etshhassan9@gmail.com', '01555284924', 'الاسكندرية', 'male', '$2y$10$dU/vyoRDVHgccrap/FaqB.ix/LedVV.BBwfYS2/7al8M2ZLt3hsdC', 27, 0, '2020-03-09 13:25:25', '2020-03-09 13:25:25', NULL),
(3, 'hassan', 'etshhassan9@gmail.com', '01555284924', 'savcahghs;k]vdm', 'male', '$2y$10$TSQJwfV1mDqO43z/a1VkCOx2iJP5FfkeuSzIk1K3PSz3BhUnwC4hq', 27, 0, '2020-03-18 16:30:33', '2020-03-18 16:30:33', NULL),
(4, 'hassan', 'as@mail.com', '01555284987', 'jg', 'male', '$2y$10$WbY7MPrmbL6MTTo/3mpB3OLe2X7QCDwYy9eP66MX5ZsC1uJOTqEkK', 27, 0, '2020-03-22 17:27:36', '2020-03-22 17:27:36', NULL),
(6, 'basem mohamed', 'bassem.mohamed@enozom.com', '01276755942', 'sedi beshr', 'male', '$2y$10$MLas4QyA5qiG6LIGduOp../1s4R6vlYOcQOqzk38Av/GLjHOp1Lla', 23, 0, '2020-04-18 18:04:32', '2020-04-18 18:04:32', NULL),
(7, 'kamal', 'midokcis@gmail.com', '123456', 'test', 'male', '$2y$10$DzfZ1M4j0ZDpi.WVRnowE.LjcBwct69uYzGN.FcLee4ALzr.EAGiO', 33, 0, '2020-04-22 00:39:18', '2020-04-22 00:39:18', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `custom_tour_bookings`
--

CREATE TABLE `custom_tour_bookings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `customer_id` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `from` datetime NOT NULL,
  `to` datetime NOT NULL,
  `total_cost` double NOT NULL,
  `adult_count` smallint(6) NOT NULL,
  `child_count` smallint(6) NOT NULL,
  `baby_count` smallint(6) NOT NULL,
  `status` enum('pending','confirmed','canceled') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending',
  `currency_exchange_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `hotel_id` int(11) DEFAULT NULL,
  `notes` text COLLATE utf8mb4_unicode_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `custom_tour_bookings`
--

INSERT INTO `custom_tour_bookings` (`id`, `customer_id`, `category_id`, `from`, `to`, `total_cost`, `adult_count`, `child_count`, `baby_count`, `status`, `currency_exchange_id`, `created_at`, `updated_at`, `deleted_at`, `hotel_id`, `notes`) VALUES
(1, 5, 4, '2020-04-16 00:00:00', '2020-04-30 00:00:00', 0, 6, 52, 65, 'pending', NULL, '2020-04-18 16:17:06', '2020-04-18 16:17:06', NULL, 2, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `custom_tour_booking_details`
--

CREATE TABLE `custom_tour_booking_details` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `custom_tour_booking_id` bigint(20) UNSIGNED NOT NULL,
  `hotel_id` bigint(20) UNSIGNED NOT NULL,
  `nights_count` smallint(6) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `custom_tour_payments`
--

CREATE TABLE `custom_tour_payments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `custom_tour_booking_id` bigint(20) UNSIGNED NOT NULL,
  `amount` double NOT NULL,
  `payment_date` datetime NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fixed_tours`
--

CREATE TABLE `fixed_tours` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `hotel_id` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `from` datetime NOT NULL,
  `to` datetime NOT NULL,
  `total_cost` double NOT NULL,
  `active` tinyint(1) NOT NULL,
  `days_nights_counts` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `discount` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `map_lat` double(8,4) DEFAULT NULL,
  `map_long` double(8,4) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `fixed_tours`
--

INSERT INTO `fixed_tours` (`id`, `hotel_id`, `category_id`, `from`, `to`, `total_cost`, `active`, `days_nights_counts`, `created_at`, `updated_at`, `deleted_at`, `city_id`, `discount`, `map_lat`, `map_long`) VALUES
(2, 1, 5, '2020-04-18 00:00:00', '2020-04-29 00:00:00', 17500, 1, '7', '2020-04-15 13:24:51', '2020-04-15 13:24:51', NULL, 3, '10', 31.2560, 29.9844),
(3, 1, 4, '2020-04-16 00:00:00', '2020-04-18 00:00:00', 3000, 1, '3', '2020-04-15 13:28:52', '2020-04-15 13:28:52', NULL, 5, '20', 27.3484, 33.8296),
(4, 1, 2, '2020-04-16 00:00:00', '2020-04-25 00:00:00', 25000, 1, '8', '2020-04-15 13:35:31', '2020-04-15 13:35:49', NULL, 6, '25', 31.2560, 29.9844),
(5, 1, 5, '2020-04-16 00:00:00', '2020-04-24 00:00:00', 40000, 1, '8', '2020-04-15 13:40:38', '2020-04-15 13:40:38', NULL, 4, '30', 31.2560, 29.9844),
(6, 1, 5, '2020-04-16 00:00:00', '2020-04-22 00:00:00', 47500, 1, '8', '2020-04-15 13:43:27', '2020-04-15 13:43:27', NULL, 7, '22', 41.8827, 12.8457);

-- --------------------------------------------------------

--
-- Table structure for table `fixed_tours_translations`
--

CREATE TABLE `fixed_tours_translations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `fixed_tour_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `locale` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `fixed_tours_translations`
--

INSERT INTO `fixed_tours_translations` (`id`, `fixed_tour_id`, `name`, `description`, `locale`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'رحلة الاسكندرية', 'رحلة الاسكندرية', 'ar', '2020-02-25 18:07:46', '2020-02-25 18:07:46', NULL),
(2, 1, 'alexandria trip', 'alexandria trip', 'en', '2020-02-25 18:07:46', '2020-02-25 18:07:46', NULL),
(3, 1, 'alexandria trip', 'alexandria trip', 'fr', '2020-02-25 18:07:46', '2020-02-25 18:07:46', NULL),
(4, 1, 'alexandria trip', 'alexandria trip', 'it', '2020-02-25 18:07:46', '2020-02-25 18:07:46', NULL),
(5, 1, 'alexandria trip', 'alexandria trip', 'es', '2020-02-25 18:07:46', '2020-02-25 18:07:46', NULL),
(6, 2, 'رحلة مدريد', 'رحلة مدينة مدريد الاسبانية شامة الاقامه و التاشية و الانتقالات و الطياران', 'ar', '2020-04-15 13:24:51', '2020-04-15 13:24:51', NULL),
(7, 2, 'Madrid', 'Madrid city trip, Spain, including residence, accommodation, transfers, and pilots', 'en', '2020-04-15 13:24:51', '2020-04-15 13:24:51', NULL),
(8, 2, 'Madrid', 'Madrid city trip, Spain, including residence, accommodation, transfers, and pilots', 'fr', '2020-04-15 13:24:51', '2020-04-15 13:24:51', NULL),
(9, 2, 'Madrid', 'Madrid city trip, Spain, including residence, accommodation, transfers, and pilots', 'it', '2020-04-15 13:24:51', '2020-04-15 13:24:51', NULL),
(10, 2, 'Madrid', 'Madrid city trip, Spain, including residence, accommodation, transfers, and pilots', 'es', '2020-04-15 13:24:51', '2020-04-15 13:24:51', NULL),
(11, 3, 'رحلة مدينة الغردقة', 'رحلة مدينة الغردقة المصرية شامة الاقامه و التاشية و الانتقالات و الطياران', 'ar', '2020-04-15 13:28:52', '2020-04-15 13:28:52', NULL),
(12, 3, 'Hurghada trip', 'The Egyptian city of Hurghada trip, including residence, livestock, transfers, and pilots', 'en', '2020-04-15 13:28:52', '2020-04-15 13:28:52', NULL),
(13, 3, 'Hurghada trip', 'The Egyptian city of Hurghada trip, including residence, livestock, transfers, and pilots', 'fr', '2020-04-15 13:28:52', '2020-04-15 13:28:52', NULL),
(14, 3, 'Hurghada trip', 'The Egyptian city of Hurghada trip, including residence, livestock, transfers, and pilots', 'it', '2020-04-15 13:28:52', '2020-04-15 13:28:52', NULL),
(15, 3, 'Hurghada trip', 'The Egyptian city of Hurghada trip, including residence, livestock, transfers, and pilots', 'es', '2020-04-15 13:28:52', '2020-04-15 13:28:52', NULL),
(16, 4, 'عمرة نصف ابريل', 'عمرة نصف ابريل شامة الاقامه و التاشية و الانتقالات و الطياران', 'ar', '2020-04-15 13:35:31', '2020-04-15 13:35:31', NULL),
(17, 4, 'Umrah half of April', 'The half-April pilgrimage, including residence, livelihood, transfers, and pilots', 'en', '2020-04-15 13:35:31', '2020-04-15 13:35:31', NULL),
(18, 4, 'Umrah half of April', 'The half-April pilgrimage, including residence, livelihood, transfers, and pilots', 'fr', '2020-04-15 13:35:31', '2020-04-15 13:35:31', NULL),
(19, 4, 'Umrah half of April', 'The half-April pilgrimage, including residence, livelihood, transfers, and pilots', 'it', '2020-04-15 13:35:31', '2020-04-15 13:35:31', NULL),
(20, 4, 'Umrah half of April', 'The half-April pilgrimage, including residence, livelihood, transfers, and pilots', 'es', '2020-04-15 13:35:31', '2020-04-15 13:35:31', NULL),
(21, 5, 'رحلة مدينة باريس الفرنسية', 'رحلة مدينة باريس الفرنسية شاملة التاشيرة و الطياران و الاقامة و الانتقالات', 'ar', '2020-04-15 13:40:38', '2020-04-15 13:40:38', NULL),
(22, 5, 'French city of Paris trip', 'A trip to Paris, France, including visas, pilots, accommodation and transfers', 'en', '2020-04-15 13:40:38', '2020-04-15 13:40:38', NULL),
(23, 5, 'French city of Paris trip', 'A trip to Paris, France, including visas, pilots, accommodation and transfers', 'fr', '2020-04-15 13:40:38', '2020-04-15 13:40:38', NULL),
(24, 5, 'French city of Paris trip', 'A trip to Paris, France, including visas, pilots, accommodation and transfers', 'it', '2020-04-15 13:40:38', '2020-04-15 13:40:38', NULL),
(25, 5, 'French city of Paris trip', 'A trip to Paris, France, including visas, pilots, accommodation and transfers', 'es', '2020-04-15 13:40:38', '2020-04-15 13:40:38', NULL),
(26, 6, 'رحلة روما الايطالية', 'رحلة مدينة روما الايطاليةشاملة التاشيرة و الطياران و الاقامة و الانتقالات', 'ar', '2020-04-15 13:43:27', '2020-04-15 13:43:27', NULL),
(27, 6, 'Rome Italian trip', 'Trip to the Italian city of Rome, including visa, pilots, accommodation and transfers', 'en', '2020-04-15 13:43:27', '2020-04-15 13:43:27', NULL),
(28, 6, 'Rome Italian trip', 'Trip to the Italian city of Rome, including visa, pilots, accommodation and transfers', 'fr', '2020-04-15 13:43:27', '2020-04-15 13:43:27', NULL),
(29, 6, 'Rome Italian trip', 'Trip to the Italian city of Rome, including visa, pilots, accommodation and transfers', 'it', '2020-04-15 13:43:27', '2020-04-15 13:43:27', NULL),
(30, 6, 'Rome Italian trip', 'Trip to the Italian city of Rome, including visa, pilots, accommodation and transfers', 'es', '2020-04-15 13:43:27', '2020-04-15 13:43:27', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `fixed_tour_bookings`
--

CREATE TABLE `fixed_tour_bookings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `customer_id` bigint(20) UNSIGNED NOT NULL,
  `fixed_tour_id` bigint(20) UNSIGNED NOT NULL,
  `from` datetime NOT NULL,
  `to` datetime NOT NULL,
  `total_cost` double NOT NULL,
  `adult_count` smallint(6) NOT NULL,
  `child_count` smallint(6) NOT NULL,
  `baby_count` smallint(6) NOT NULL,
  `status` enum('pending','confirmed','canceled') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending',
  `currency_exchange_id` bigint(20) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `fixed_tour_bookings`
--

INSERT INTO `fixed_tour_bookings` (`id`, `customer_id`, `fixed_tour_id`, `from`, `to`, `total_cost`, `adult_count`, `child_count`, `baby_count`, `status`, `currency_exchange_id`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 5, 3, '2020-04-16 00:00:00', '2020-04-18 00:00:00', 3000, 5, 1, 3, 'pending', NULL, NULL, '2020-04-18 16:16:29', '2020-04-18 16:16:29');

-- --------------------------------------------------------

--
-- Table structure for table `fixed_tour_payments`
--

CREATE TABLE `fixed_tour_payments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `fixed_tour_booking_id` bigint(20) UNSIGNED NOT NULL,
  `amount` double NOT NULL,
  `payment_date` datetime NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `front_about_us`
--

CREATE TABLE `front_about_us` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `front_about_us`
--

INSERT INTO `front_about_us` (`id`, `created_at`, `updated_at`, `type_id`) VALUES
(3, '2020-04-15 12:23:16', '2020-04-15 12:23:16', 1),
(4, '2020-04-15 12:24:13', '2020-04-15 12:24:13', 2),
(5, '2020-04-15 12:24:50', '2020-04-15 12:24:50', 3);

-- --------------------------------------------------------

--
-- Table structure for table `front_about_us_translations`
--

CREATE TABLE `front_about_us_translations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `aboutus_id` bigint(20) UNSIGNED NOT NULL,
  `contents` text COLLATE utf8mb4_unicode_ci,
  `locale` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `front_about_us_translations`
--

INSERT INTO `front_about_us_translations` (`id`, `aboutus_id`, `contents`, `locale`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, '<div>عنا</div>', 'ar', '2020-02-20 22:20:59', '2020-02-20 22:20:59', NULL),
(2, 1, '<div>asdasds</div>', 'en', '2020-02-20 22:20:59', '2020-02-20 22:20:59', NULL),
(3, 1, '<div>asdasds</div>', 'fr', '2020-02-20 22:20:59', '2020-02-20 22:20:59', NULL),
(4, 1, '<div>asdasds</div>', 'it', '2020-02-20 22:20:59', '2020-02-20 22:20:59', NULL),
(5, 1, '<div>asdasds</div>', 'es', '2020-02-20 22:20:59', '2020-02-20 22:20:59', NULL),
(6, 2, '<div>ءياتل تلارار غعتالار ربرتربر</div>', 'ar', '2020-02-25 18:12:07', '2020-02-25 18:12:07', NULL),
(7, 2, '<div>dth ge&nbsp; rge geg gw gersggeg gerge tgse r</div>', 'en', '2020-02-25 18:12:07', '2020-02-25 18:12:07', NULL),
(8, 2, '<div>dth ge&nbsp; rge geg gw gersggeg gerge tgse r</div>', 'fr', '2020-02-25 18:12:07', '2020-02-25 18:12:07', NULL),
(9, 2, '<div>dth ge&nbsp; rge geg gw gersggeg gerge tgse r</div>', 'it', '2020-02-25 18:12:07', '2020-02-25 18:12:07', NULL),
(10, 2, '<div>dth ge&nbsp; rge geg gw gersggeg gerge tgse r</div>', 'es', '2020-02-25 18:12:07', '2020-02-25 18:12:07', NULL),
(11, 3, '<div>شركة الامريكتين من الشركات الرائدة و ذات خبرة مميزة و لحرصها على تحقيق التميز تنتقى فريق عمل متميز فى كافة الخدمات القائمة عليها الشركة بخلاف العمرة و الحج مثل حجوزات الطيران و الشركة&nbsp;</div>', 'ar', '2020-04-15 12:23:16', '2020-04-15 12:23:16', NULL),
(12, 3, '<div>The Americas company is one of the leading companies with </div><div><br><br></div><div><br></div><div><br></div>', 'en', '2020-04-15 12:23:16', '2020-04-15 12:30:18', NULL),
(13, 3, '<div>The Americas company is one of the leading companies with&nbsp;</div>', 'fr', '2020-04-15 12:23:16', '2020-04-15 12:30:18', NULL),
(14, 3, '<div>The Americas company is one of the leading companies with&nbsp;</div>', 'it', '2020-04-15 12:23:16', '2020-04-15 12:30:18', NULL),
(15, 3, '<div>The Americas company is one of the leading companies with&nbsp;</div>', 'es', '2020-04-15 12:23:16', '2020-04-15 12:30:18', NULL),
(16, 4, '<div>شركة الامريكتين من الشركات الرائدة و ذات خبرة مميزة و لحرصها على تحقيق التميز تنتقى فريق عمل متميز فى كافة الخدمات القائمة عليها الشركة بخلاف العمرة و الحج مثل حجوزات الطيران و الشركة&nbsp;</div>', 'ar', '2020-04-15 12:24:13', '2020-04-15 12:24:13', NULL),
(17, 4, '<div>The Americas company is one of the leading companies with&nbsp;</div>', 'en', '2020-04-15 12:24:13', '2020-04-15 12:30:42', NULL),
(18, 4, '<div>The Americas company is one of the leading companies with&nbsp;</div>', 'fr', '2020-04-15 12:24:13', '2020-04-15 12:30:42', NULL),
(19, 4, '<div>The Americas company is one of the leading companies with&nbsp;</div>', 'it', '2020-04-15 12:24:13', '2020-04-15 12:30:42', NULL),
(20, 4, '<div>The Americas company is one of the leading companies with&nbsp;</div>', 'es', '2020-04-15 12:24:13', '2020-04-15 12:30:42', NULL),
(21, 5, '<div>شركة الامريكتين من الشركات الرائدة و ذات خبرة مميزة و لحرصها على تحقيق التميز تنتقى فريق عمل متميز فى كافة الخدمات القائمة عليها الشركة بخلاف العمرة و الحج مثل حجوزات الطيران و الشركة&nbsp;</div>', 'ar', '2020-04-15 12:24:50', '2020-04-15 12:24:50', NULL),
(22, 5, '<div>The Americas company is one of the leading companies with&nbsp;</div>', 'en', '2020-04-15 12:24:50', '2020-04-15 12:31:03', NULL),
(23, 5, '<div>The Americas company is one of the leading companies with&nbsp;</div>', 'fr', '2020-04-15 12:24:50', '2020-04-15 12:31:03', NULL),
(24, 5, '<div>The Americas company is one of the leading companies with&nbsp;</div>', 'it', '2020-04-15 12:24:50', '2020-04-15 12:31:03', NULL),
(25, 5, '<div>The Americas company is one of the leading companies with&nbsp;</div>', 'es', '2020-04-15 12:24:50', '2020-04-15 12:31:03', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `front_city_slider`
--

CREATE TABLE `front_city_slider` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `front_city_slider`
--

INSERT INTO `front_city_slider` (`id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(3, '2020-04-15 12:43:21', '2020-04-15 12:43:21', NULL),
(4, '2020-04-15 12:45:23', '2020-04-15 12:45:23', NULL),
(5, '2020-04-15 12:48:59', '2020-04-15 12:48:59', NULL),
(6, '2020-04-15 12:56:37', '2020-04-15 12:56:37', NULL),
(7, '2020-04-15 12:57:33', '2020-04-15 12:57:33', NULL),
(8, '2020-04-15 12:59:14', '2020-04-15 12:59:14', NULL),
(9, '2020-04-15 13:05:38', '2020-04-15 13:05:38', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `front_city_translations`
--

CREATE TABLE `front_city_translations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `city_slider_id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `locale` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `front_city_translations`
--

INSERT INTO `front_city_translations` (`id`, `city_slider_id`, `title`, `locale`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, '<div>بفت قيعف ق5غ غق5ث ث5غغ&nbsp;</div>', 'ar', '2020-02-25 18:16:23', '2020-02-25 18:16:23', NULL),
(2, 1, '<div>abrehsd uasfgsjk b jugafvmnsfv&nbsp; jkgvjaw vuy</div>', 'en', '2020-02-25 18:16:23', '2020-02-25 18:16:23', NULL),
(3, 1, '<div>abrehsd uasfgsjk b jugafvmnsfv&nbsp; jkgvjaw vuy</div>', 'fr', '2020-02-25 18:16:23', '2020-02-25 18:16:23', NULL),
(4, 1, '<div>abrehsd uasfgsjk b jugafvmnsfv&nbsp; jkgvjaw vuy</div>', 'it', '2020-02-25 18:16:23', '2020-02-25 18:16:23', NULL),
(5, 1, '<div>abrehsd uasfgsjk b jugafvmnsfv&nbsp; jkgvjaw vuy</div>', 'es', '2020-02-25 18:16:23', '2020-02-25 18:16:23', NULL),
(6, 2, '<div>ثيلسف يبليقب&nbsp;</div>', 'ar', '2020-03-09 13:34:18', '2020-03-09 13:34:18', NULL),
(7, 2, '<div>SFvdafb dfgsbb&nbsp; trs&nbsp; r&nbsp; &nbsp; &nbsp; rsrt</div>', 'en', '2020-03-09 13:34:18', '2020-03-09 13:34:18', NULL),
(8, 2, '<div>SFvdafb dfgsbb&nbsp; trs&nbsp; r&nbsp; &nbsp; &nbsp; rsrt</div>', 'fr', '2020-03-09 13:34:18', '2020-03-09 13:34:18', NULL),
(9, 2, '<div>SFvdafb dfgsbb&nbsp; trs&nbsp; r&nbsp; &nbsp; &nbsp; rsrt</div>', 'it', '2020-03-09 13:34:18', '2020-03-09 13:34:18', NULL),
(10, 2, '<div>SFvdafb dfgsbb&nbsp; trs&nbsp; r&nbsp; &nbsp; &nbsp; rsrt</div>', 'es', '2020-03-09 13:34:18', '2020-03-09 13:34:18', NULL),
(11, 3, '<div>باريس</div>', 'ar', '2020-04-15 12:43:21', '2020-04-15 12:43:21', NULL),
(12, 3, '<pre>Paris</pre><div><br></div>', 'en', '2020-04-15 12:43:21', '2020-04-15 12:43:21', NULL),
(13, 3, '<pre>Paris</pre><div><br></div>', 'fr', '2020-04-15 12:43:21', '2020-04-15 12:43:21', NULL),
(14, 3, '<pre>Paris</pre><div><br></div>', 'it', '2020-04-15 12:43:21', '2020-04-15 12:43:21', NULL),
(15, 3, '<pre>Paris</pre><div><br></div>', 'es', '2020-04-15 12:43:21', '2020-04-15 12:43:21', NULL),
(16, 4, '<div>روما</div>', 'ar', '2020-04-15 12:45:23', '2020-04-15 12:45:23', NULL),
(17, 4, '<pre>Rome</pre><div><br></div>', 'en', '2020-04-15 12:45:23', '2020-04-15 12:45:23', NULL),
(18, 4, '<pre>Rome</pre><div><br></div>', 'fr', '2020-04-15 12:45:23', '2020-04-15 12:45:23', NULL),
(19, 4, '<pre>Rome</pre><div><br></div>', 'it', '2020-04-15 12:45:23', '2020-04-15 12:45:23', NULL),
(20, 4, '<pre>Rome</pre><div><br></div>', 'es', '2020-04-15 12:45:23', '2020-04-15 12:45:23', NULL),
(21, 5, '<div>نيويورك</div>', 'ar', '2020-04-15 12:48:59', '2020-04-15 12:48:59', NULL),
(22, 5, '<pre>New York</pre><div><br></div>', 'en', '2020-04-15 12:48:59', '2020-04-15 12:48:59', NULL),
(23, 5, '<pre>New York</pre><div><br></div>', 'fr', '2020-04-15 12:48:59', '2020-04-15 12:48:59', NULL),
(24, 5, '<pre>New York</pre><div><br></div>', 'it', '2020-04-15 12:48:59', '2020-04-15 12:48:59', NULL),
(25, 5, '<pre>New York</pre><div><br></div>', 'es', '2020-04-15 12:48:59', '2020-04-15 12:48:59', NULL),
(26, 6, '<div>القاهرة</div>', 'ar', '2020-04-15 12:56:37', '2020-04-15 12:56:37', NULL),
(27, 6, '<pre>Cairo</pre><div><br></div>', 'en', '2020-04-15 12:56:37', '2020-04-15 12:56:37', NULL),
(28, 6, '<pre>Cairo</pre><div><br></div>', 'fr', '2020-04-15 12:56:37', '2020-04-15 12:56:37', NULL),
(29, 6, '<pre>Cairo</pre><div><br></div>', 'it', '2020-04-15 12:56:37', '2020-04-15 12:56:37', NULL),
(30, 6, '<pre>Cairo</pre><div><br></div>', 'es', '2020-04-15 12:56:37', '2020-04-15 12:56:37', NULL),
(31, 7, '<div>دبي</div>', 'ar', '2020-04-15 12:57:33', '2020-04-15 12:57:33', NULL),
(32, 7, '<pre>Dubai</pre><div><br></div>', 'en', '2020-04-15 12:57:33', '2020-04-15 12:57:33', NULL),
(33, 7, '<pre>Dubai</pre><div><br></div>', 'fr', '2020-04-15 12:57:33', '2020-04-15 12:57:33', NULL),
(34, 7, '<pre>Dubai</pre><div><br></div>', 'it', '2020-04-15 12:57:33', '2020-04-15 12:57:33', NULL),
(35, 7, '<pre>Dubai</pre><div><br></div>', 'es', '2020-04-15 12:57:33', '2020-04-15 12:57:33', NULL),
(36, 8, '<div>نابولي</div>', 'ar', '2020-04-15 12:59:14', '2020-04-15 12:59:14', NULL),
(37, 8, '<div>Napoli</div>', 'en', '2020-04-15 12:59:14', '2020-04-15 12:59:14', NULL),
(38, 8, '<div>Napoli</div>', 'fr', '2020-04-15 12:59:14', '2020-04-15 12:59:14', NULL),
(39, 8, '<div>Napoli</div>', 'it', '2020-04-15 12:59:14', '2020-04-15 12:59:14', NULL),
(40, 8, '<div>Napoli</div>', 'es', '2020-04-15 12:59:14', '2020-04-15 12:59:14', NULL),
(41, 9, '<div>مدريد</div>', 'ar', '2020-04-15 13:05:38', '2020-04-15 13:05:38', NULL),
(42, 9, '<pre>Madrid</pre><div><br></div>', 'en', '2020-04-15 13:05:38', '2020-04-15 13:05:38', NULL),
(43, 9, '<pre>Madrid</pre><div><br></div>', 'fr', '2020-04-15 13:05:38', '2020-04-15 13:05:38', NULL),
(44, 9, '<pre>Madrid</pre><div><br></div>', 'it', '2020-04-15 13:05:38', '2020-04-15 13:05:38', NULL),
(45, 9, '<pre>Madrid</pre><div><br></div>', 'es', '2020-04-15 13:05:38', '2020-04-15 13:05:38', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `front_contact_us`
--

CREATE TABLE `front_contact_us` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `map_lang` bigint(20) UNSIGNED NOT NULL,
  `map_lat` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `front_contact_us_translations`
--

CREATE TABLE `front_contact_us_translations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `front_contact_us_id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `locale` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `front_customers_opinions`
--

CREATE TABLE `front_customers_opinions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `job_title_name` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `front_customers_opinions`
--

INSERT INTO `front_customers_opinions` (`id`, `description`, `name`, `job_title_name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(3, '<div>افضل شركة سياحة في الاسكندرية</div>', 'حسن محمد', 'مهندس', '2020-04-15 14:00:45', '2020-04-15 14:00:45', NULL),
(4, '<div>شركة في غاية الروعه و الخدنة ممتازة</div>', 'محمد كمال', 'مهندس', '2020-04-15 14:01:18', '2020-04-15 14:01:18', NULL),
(5, '<div>خدمة العملاء و المتابعة ممتازة</div>', 'محمد عبد الجواد', 'دكتور', '2020-04-15 14:01:58', '2020-04-15 14:01:58', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `front_discover`
--

CREATE TABLE `front_discover` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `front_discover`
--

INSERT INTO `front_discover` (`id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2, '2020-04-15 12:35:22', '2020-04-15 12:35:22', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `front_discover_translations`
--

CREATE TABLE `front_discover_translations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `discover_id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `locale` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `front_discover_translations`
--

INSERT INTO `front_discover_translations` (`id`, `discover_id`, `title`, `description`, `locale`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'بفقاقف', 'ابؤلءؤ يسبغلؤلايؤء ؤرلؤءيبء', 'ar', '2020-02-25 18:14:08', '2020-02-25 18:14:08', NULL),
(2, 1, 'rtdy6  er6y56erfvxgxd', 'rtdy6  er6y56erfvxgxd', 'en', '2020-02-25 18:14:08', '2020-02-25 18:14:08', NULL),
(3, 1, 'rtdy6  er6y56erfvxgxd', 'rtdy6  er6y56erfvxgxd', 'fr', '2020-02-25 18:14:08', '2020-02-25 18:14:08', NULL),
(4, 1, 'rtdy6  er6y56erfvxgxd', 'rtdy6  er6y56erfvxgxd', 'it', '2020-02-25 18:14:08', '2020-02-25 18:14:08', NULL),
(5, 1, 'rtdy6  er6y56erfvxgxd', 'rtdy6  er6y56erfvxgxd', 'es', '2020-02-25 18:14:08', '2020-02-25 18:14:08', NULL),
(6, 2, 'اكتشف العالم معنا', 'اكتشف العالم معنا', 'ar', '2020-04-15 12:35:22', '2020-04-15 12:35:22', NULL),
(7, 2, 'Discover the world with us', 'Discover the world with us', 'en', '2020-04-15 12:35:22', '2020-04-15 12:35:22', NULL),
(8, 2, 'Discover the world with us', 'Discover the world with us', 'fr', '2020-04-15 12:35:22', '2020-04-15 12:35:22', NULL),
(9, 2, 'Discover the world with us', 'Discover the world with us', 'it', '2020-04-15 12:35:22', '2020-04-15 12:35:22', NULL),
(10, 2, 'Discover the world with us', 'Discover the world with us', 'es', '2020-04-15 12:35:22', '2020-04-15 12:35:22', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `front_navbar`
--

CREATE TABLE `front_navbar` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `front_navbar_translations`
--

CREATE TABLE `front_navbar_translations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `front_navbar_id` bigint(20) UNSIGNED NOT NULL,
  `contents` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `locale` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `front_news`
--

CREATE TABLE `front_news` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `news_date` datetime NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `front_news`
--

INSERT INTO `front_news` (`id`, `news_date`, `created_at`, `updated_at`, `deleted_at`) VALUES
(3, '2020-04-15 00:00:00', '2020-04-15 14:07:10', '2020-04-15 14:07:10', NULL),
(2, '2020-04-15 00:00:00', '2020-04-15 14:05:17', '2020-04-15 14:05:17', NULL),
(4, '2020-04-09 00:00:00', '2020-04-15 14:09:54', '2020-04-15 14:09:54', NULL),
(5, '2020-04-08 00:00:00', '2020-04-15 14:12:05', '2020-04-15 14:16:34', NULL),
(6, '2020-04-07 00:00:00', '2020-04-15 14:15:48', '2020-04-15 14:15:48', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `front_news_translations`
--

CREATE TABLE `front_news_translations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `front_news_id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `locale` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `front_news_translations`
--

INSERT INTO `front_news_translations` (`id`, `front_news_id`, `title`, `description`, `locale`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'ؤبلةمة ينىفبى  ىلال', 'ِلآتارتشصق لارتشرصب عتر لاشقبلشثق', 'ar', '2020-02-25 18:17:47', '2020-02-25 18:17:47', NULL),
(2, 1, 'aetg raeg ratg gerr e ege5er', 'aetg raeg ratg gerr e ege5er', 'en', '2020-02-25 18:17:47', '2020-02-25 18:17:47', NULL),
(3, 1, 'aetg raeg ratg gerr e ege5er', 'aetg raeg ratg gerr e ege5er', 'fr', '2020-02-25 18:17:47', '2020-02-25 18:17:47', NULL),
(4, 1, 'aetg raeg ratg gerr e ege5er', 'aetg raeg ratg gerr e ege5er', 'it', '2020-02-25 18:17:47', '2020-02-25 18:17:47', NULL),
(5, 1, 'aetg raeg ratg gerr e ege5er', 'aetg raeg ratg gerr e ege5er', 'es', '2020-02-25 18:17:47', '2020-02-25 18:17:47', NULL),
(6, 2, 'مصر تخفض توقعات إيرادات السياحة إلى 11 مليار دولار', 'توقعت وزيرة التخطيط المصرية، هالة السعيد، الاثنين، أن تصل إيرادات السياحة، أحد المصادر الرئيسية للنقد الأجنبي، في السنة المالية الحالية 2019-2020 لنحو 11 مليار دولار، بدلا من 16 مليارا كانت متوقعة قبل أزمة كورونا.', 'ar', '2020-04-15 14:05:17', '2020-04-15 14:05:17', NULL),
(7, 2, 'Egypt reduces tourism revenue forecasts to 11 billion dollars', 'Egyptian Minister of Planning, Hala Al-Saeed, expected, on Monday, that tourism revenues, one of the main sources of foreign exchange, in the current fiscal year 2019-2020 will reach about 11 billion dollars, instead of the 16 billion expected before the Corona crisis.', 'en', '2020-04-15 14:05:17', '2020-04-15 14:05:17', NULL),
(8, 2, 'Egypt reduces tourism revenue forecasts to 11 billion dollars', 'Egyptian Minister of Planning, Hala Al-Saeed, expected, on Monday, that tourism revenues, one of the main sources of foreign exchange, in the current fiscal year 2019-2020 will reach about 11 billion dollars, instead of the 16 billion expected before the Corona crisis.', 'fr', '2020-04-15 14:05:17', '2020-04-15 14:05:17', NULL),
(9, 2, 'Egypt reduces tourism revenue forecasts to 11 billion dollars', 'Egyptian Minister of Planning, Hala Al-Saeed, expected, on Monday, that tourism revenues, one of the main sources of foreign exchange, in the current fiscal year 2019-2020 will reach about 11 billion dollars, instead of the 16 billion expected before the Corona crisis.', 'it', '2020-04-15 14:05:17', '2020-04-15 14:05:17', NULL),
(10, 2, 'Egypt reduces tourism revenue forecasts to 11 billion dollars', 'Egyptian Minister of Planning, Hala Al-Saeed, expected, on Monday, that tourism revenues, one of the main sources of foreign exchange, in the current fiscal year 2019-2020 will reach about 11 billion dollars, instead of the 16 billion expected before the Corona crisis.', 'es', '2020-04-15 14:05:17', '2020-04-15 14:05:17', NULL),
(11, 3, 'وزراء السياحة العالمية يضعون 5 حلول لتعافي القطاع من الأزمة الحالية', 'وزراء السياحة العالمية يضعون 5 حلول لتعافي القطاع من الأزمة الحالية\r\n\r\nالدكتور خالد العناني وزير السياحة\r\n\r\nكتب يوسف عفيفي:\r\nشارك الدكتور خالد العناني وزير السياحة والآثار، في أول اجتماع وزاري لوزراء السياحة بمنظمة السياحة العالمية التابعة للأمم المتحدة (UNWTO) عبر الفيديو كونفرانس.\r\n\r\nوشارك في الاجتماع أيضا، غادة شلبي نائب الوزير لشئون السياحة والسفير ماجد مصلح المشرف العالم على إدارة العلاقات الخارجية بالوزارة.\r\n\r\nوناقش الاجتماع، الإجراءات التي اتخذتها الدول والحكومات لتخفيف الأعباء الاقتصادية والتبعات السلبية على القطاع السياحي من تداعيات فيروس كورونا المستجد، بالإضافة إلى تبادل الآراء حول الحلول المختلفة لعمل خطط مستقبلية للنهوض بالقطاع السياحي بعد التعافي من الأزمة.', 'ar', '2020-04-15 14:07:10', '2020-04-15 14:07:10', NULL),
(12, 3, 'Global tourism ministers are developing 5 solutions to recover the sector from the current crisis', 'Global tourism ministers are developing 5 solutions to recover the sector from the current crisis\r\n\r\nDr. Khaled Al-Anani, Minister of Tourism\r\n\r\nYoussef Afifi wrote:\r\nDr. Khaled Al-Anani, Minister of Tourism and Antiquities, participated in the first ministerial meeting of Tourism Ministers of the United Nations World Tourism Organization (UNWTO) via video conference.\r\n\r\nGhada Shalaby, Deputy Minister for Tourism Affairs, and Ambassador Majed Musleh, the world supervisor of the Ministry\'s External Relations Department, participated in the meeting.\r\n\r\nThe meeting discussed the measures taken by countries and governments to alleviate the economic burdens and negative consequences on the tourism sector from the repercussions of the emerging Corona virus, in addition to exchanging views on various solutions to make future plans to advance the tourism sector after recovering from the crisis.', 'en', '2020-04-15 14:07:10', '2020-04-15 14:07:10', NULL),
(13, 3, 'Global tourism ministers are developing 5 solutions to recover the sector from the current crisis', 'Global tourism ministers are developing 5 solutions to recover the sector from the current crisis\r\n\r\nDr. Khaled Al-Anani, Minister of Tourism\r\n\r\nYoussef Afifi wrote:\r\nDr. Khaled Al-Anani, Minister of Tourism and Antiquities, participated in the first ministerial meeting of Tourism Ministers of the United Nations World Tourism Organization (UNWTO) via video conference.\r\n\r\nGhada Shalaby, Deputy Minister for Tourism Affairs, and Ambassador Majed Musleh, the world supervisor of the Ministry\'s External Relations Department, participated in the meeting.\r\n\r\nThe meeting discussed the measures taken by countries and governments to alleviate the economic burdens and negative consequences on the tourism sector from the repercussions of the emerging Corona virus, in addition to exchanging views on various solutions to make future plans to advance the tourism sector after recovering from the crisis.', 'fr', '2020-04-15 14:07:10', '2020-04-15 14:07:10', NULL),
(14, 3, 'Global tourism ministers are developing 5 solutions to recover the sector from the current crisis', 'Global tourism ministers are developing 5 solutions to recover the sector from the current crisis\r\n\r\nDr. Khaled Al-Anani, Minister of Tourism\r\n\r\nYoussef Afifi wrote:\r\nDr. Khaled Al-Anani, Minister of Tourism and Antiquities, participated in the first ministerial meeting of Tourism Ministers of the United Nations World Tourism Organization (UNWTO) via video conference.\r\n\r\nGhada Shalaby, Deputy Minister for Tourism Affairs, and Ambassador Majed Musleh, the world supervisor of the Ministry\'s External Relations Department, participated in the meeting.\r\n\r\nThe meeting discussed the measures taken by countries and governments to alleviate the economic burdens and negative consequences on the tourism sector from the repercussions of the emerging Corona virus, in addition to exchanging views on various solutions to make future plans to advance the tourism sector after recovering from the crisis.', 'it', '2020-04-15 14:07:10', '2020-04-15 14:07:10', NULL),
(15, 3, 'Global tourism ministers are developing 5 solutions to recover the sector from the current crisis', 'Global tourism ministers are developing 5 solutions to recover the sector from the current crisis\r\n\r\nDr. Khaled Al-Anani, Minister of Tourism\r\n\r\nYoussef Afifi wrote:\r\nDr. Khaled Al-Anani, Minister of Tourism and Antiquities, participated in the first ministerial meeting of Tourism Ministers of the United Nations World Tourism Organization (UNWTO) via video conference.\r\n\r\nGhada Shalaby, Deputy Minister for Tourism Affairs, and Ambassador Majed Musleh, the world supervisor of the Ministry\'s External Relations Department, participated in the meeting.\r\n\r\nThe meeting discussed the measures taken by countries and governments to alleviate the economic burdens and negative consequences on the tourism sector from the repercussions of the emerging Corona virus, in addition to exchanging views on various solutions to make future plans to advance the tourism sector after recovering from the crisis.', 'es', '2020-04-15 14:07:10', '2020-04-15 14:07:10', NULL),
(16, 4, 'مليار دولار شهريًا .. كيف أثر فيروس كورونا على قطاع السياحة بالبحر الأحمر؟', 'قدر عدد من خبراء السياحة، حجم الخسائر التي ستلحق بالقطاع شهريًا لشركات السياحة والفنادق والقرى السياحية ومراكز الغوص والأنشطة المرتبطة بالسياحة، بنحو مليار دولار شهريًا، متمثلة في توقف رحلات نحو ٤٠٠ ألف سائح شهريًا كانت تستقبلهم منتجعات البحر الأحمر شهريًا ويقضون نحو ٤ مليون ليلة سياحية، بمعدل متوسط ١٠ ليال لكل سائح، في الوقت الذي أغلقت مختلف الفنادق والشركات والمطاعم والبازارات والكافيهات أبوابها منذ بداية انتشار فيروس كورونا، وفقد نحو ٢٠٠ ألف عامل وظائفهم، بالإضافة إلى الآلاف الذين يعملون في الخدمات السياحية.', 'ar', '2020-04-15 14:09:54', '2020-04-15 14:09:54', NULL),
(17, 4, 'One billion dollars a month .. How did the Corona virus affect the tourism sector in the Red Sea?', 'A number of tourism experts estimated the size of the losses that will be inflicted on the sector every month to tourism companies, hotels, tourist villages, diving centers and tourism-related activities, at about one billion dollars per month, represented by stopping the trips of about 400 thousand tourists per month, which were received by the Red Sea resorts every month and they spend about 4 million tourist nights, With an average rate of 10 nights per tourist, at a time when various hotels, companies, restaurants, bazaars and cafes have closed their doors since the beginning of the Coruna virus, around 200,000 workers lost their jobs, in addition to the thousands who work in tourism services.', 'en', '2020-04-15 14:09:54', '2020-04-15 14:09:54', NULL),
(18, 4, 'One billion dollars a month .. How did the Corona virus affect the tourism sector in the Red Sea?', 'A number of tourism experts estimated the size of the losses that will be inflicted on the sector every month to tourism companies, hotels, tourist villages, diving centers and tourism-related activities, at about one billion dollars per month, represented by stopping the trips of about 400 thousand tourists per month, which were received by the Red Sea resorts every month and they spend about 4 million tourist nights, With an average rate of 10 nights per tourist, at a time when various hotels, companies, restaurants, bazaars and cafes have closed their doors since the beginning of the Coruna virus, around 200,000 workers lost their jobs, in addition to the thousands who work in tourism services.', 'fr', '2020-04-15 14:09:54', '2020-04-15 14:09:54', NULL),
(19, 4, 'One billion dollars a month .. How did the Corona virus affect the tourism sector in the Red Sea?', 'A number of tourism experts estimated the size of the losses that will be inflicted on the sector every month to tourism companies, hotels, tourist villages, diving centers and tourism-related activities, at about one billion dollars per month, represented by stopping the trips of about 400 thousand tourists per month, which were received by the Red Sea resorts every month and they spend about 4 million tourist nights, With an average rate of 10 nights per tourist, at a time when various hotels, companies, restaurants, bazaars and cafes have closed their doors since the beginning of the Coruna virus, around 200,000 workers lost their jobs, in addition to the thousands who work in tourism services.', 'it', '2020-04-15 14:09:54', '2020-04-15 14:09:54', NULL),
(20, 4, 'One billion dollars a month .. How did the Corona virus affect the tourism sector in the Red Sea?', 'A number of tourism experts estimated the size of the losses that will be inflicted on the sector every month to tourism companies, hotels, tourist villages, diving centers and tourism-related activities, at about one billion dollars per month, represented by stopping the trips of about 400 thousand tourists per month, which were received by the Red Sea resorts every month and they spend about 4 million tourist nights, With an average rate of 10 nights per tourist, at a time when various hotels, companies, restaurants, bazaars and cafes have closed their doors since the beginning of the Coruna virus, around 200,000 workers lost their jobs, in addition to the thousands who work in tourism services.', 'es', '2020-04-15 14:09:54', '2020-04-15 14:09:54', NULL),
(21, 5, 'مقدمة بحث عن السياحة للصف الخامس الابتدائي 2020 موقع وزارة التعليم', 'نعرض من موقع كورة اون مقدمة بحث عن السياحة لطلاب الصف الخامس الابتدائي في العام الدراسي الجاري 2020 حيث عرضت وزارة التعليم نماذج استرشادية لاربع مشروعات بحثية لطلاب المرحلة الابتدائية، لكن تختلف العناصر الاساسية لكل موضوع من عام دراسي إلى آخر ونعرض هذا الموضوع مقدمة عن بحث السياحة.', 'ar', '2020-04-15 14:12:05', '2020-04-15 14:12:05', NULL),
(22, 5, 'Introduction to a research on tourism for the fifth grade of primary school 2020, Ministry of Education', 'From the Koura On website, we offer an introduction to a research paper on tourism for fifth-grade primary students in the current academic year 2020, where the Ministry of Education presented pilot models for four research projects for primary school students, but the basic elements of each topic differ from one academic year to another and we offer this topic an introduction on tourism research.', 'en', '2020-04-15 14:12:05', '2020-04-15 14:12:05', NULL),
(23, 5, 'Introduction to a research on tourism for the fifth grade of primary school 2020, Ministry of Education', 'From the Koura On website, we offer an introduction to a research paper on tourism for fifth-grade primary students in the current academic year 2020, where the Ministry of Education presented pilot models for four research projects for primary school students, but the basic elements of each topic differ from one academic year to another and we offer this topic an introduction on tourism research.', 'fr', '2020-04-15 14:12:05', '2020-04-15 14:12:05', NULL),
(24, 5, 'Introduction to a research on tourism for the fifth grade of primary school 2020, Ministry of Education', 'From the Koura On website, we offer an introduction to a research paper on tourism for fifth-grade primary students in the current academic year 2020, where the Ministry of Education presented pilot models for four research projects for primary school students, but the basic elements of each topic differ from one academic year to another and we offer this topic an introduction on tourism research.', 'it', '2020-04-15 14:12:05', '2020-04-15 14:12:05', NULL),
(25, 5, 'Introduction to a research on tourism for the fifth grade of primary school 2020, Ministry of Education', 'From the Koura On website, we offer an introduction to a research paper on tourism for fifth-grade primary students in the current academic year 2020, where the Ministry of Education presented pilot models for four research projects for primary school students, but the basic elements of each topic differ from one academic year to another and we offer this topic an introduction on tourism research.', 'es', '2020-04-15 14:12:05', '2020-04-15 14:12:05', NULL),
(26, 6, 'السياحة تستمر في أعمال التعقيم في المناطق الأثرية', 'وجاء ذلك استمرارا لأعمال تطهير وتعقيم المتاحف والمواقع الأثرية التي تقوم بها وزارة السياحة والآثار ضمن الإجراءات الاحترازية والوقائية التي تتخذها من تداعيات فيروس كورونا المستجد.\r\n\r\nالموافق د. أسامة طلعت رئيس قطاع الآثار الإسلامية والقبطية واليهودية ، انه تم الانتهاء من تعقيم وتطهير عددا من الآثار القبطية منها منطقة آثار شجرة مريم بالمطرية ، دير السيدة العذراء بالقوصية ودير السيدة العذراء بجبل الطير بسمالوط ، ومنطقة ابو مينا بالإسكندرية ودير الأنبا سمعان بمحافظة أسوان ، بالإضافة إلى جميع الآثار الإسلامية الموجودة في شارع المعز لدين الله بالقاهرة التاريخية وقلعة صلاح الدين ومنطقة السلطان حسن والرفاعي ومنطقة الأزهر', 'ar', '2020-04-15 14:15:48', '2020-04-15 14:15:48', NULL),
(27, 6, 'Tourism continues to sterilize the archaeological sites', 'This came as a continuation of the disinfection and sterilization of museums and archaeological sites carried out by the Ministry of Tourism and Antiquities, as part of the precautionary and preventive measures that they are taking from the repercussions of the emerging Corona virus.', 'en', '2020-04-15 14:15:48', '2020-04-15 14:15:48', NULL),
(28, 6, 'Tourism continues to sterilize the archaeological sites', 'This came as a continuation of the disinfection and sterilization of museums and archaeological sites carried out by the Ministry of Tourism and Antiquities, as part of the precautionary and preventive measures that they are taking from the repercussions of the emerging Corona virus.', 'fr', '2020-04-15 14:15:48', '2020-04-15 14:15:48', NULL),
(29, 6, 'Tourism continues to sterilize the archaeological sites', 'This came as a continuation of the disinfection and sterilization of museums and archaeological sites carried out by the Ministry of Tourism and Antiquities, as part of the precautionary and preventive measures that they are taking from the repercussions of the emerging Corona virus.', 'it', '2020-04-15 14:15:48', '2020-04-15 14:15:48', NULL),
(30, 6, 'Tourism continues to sterilize the archaeological sites', 'This came as a continuation of the disinfection and sterilization of museums and archaeological sites carried out by the Ministry of Tourism and Antiquities, as part of the precautionary and preventive measures that they are taking from the repercussions of the emerging Corona virus.', 'es', '2020-04-15 14:15:48', '2020-04-15 14:15:48', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `front_services`
--

CREATE TABLE `front_services` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `front_services_translations`
--

CREATE TABLE `front_services_translations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `front_service_id` bigint(20) UNSIGNED NOT NULL,
  `contents` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `locale` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `front_sliders`
--

CREATE TABLE `front_sliders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `front_sliders`
--

INSERT INTO `front_sliders` (`id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(3, '2020-04-15 12:12:19', '2020-04-15 12:12:19', NULL),
(4, '2020-04-15 12:17:00', '2020-04-15 12:17:00', NULL),
(5, '2020-04-15 12:18:31', '2020-04-15 12:18:31', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `front_sliders_translations`
--

CREATE TABLE `front_sliders_translations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `front_slider_id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `locale` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `front_sliders_translations`
--

INSERT INTO `front_sliders_translations` (`id`, `front_slider_id`, `title`, `locale`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, '<div>يقءلاابرسالر ءيالاالاري ارئءرت رئيائر</div>', 'ar', '2020-02-19 16:01:38', '2020-02-19 16:01:38', NULL),
(2, 1, '<div>jhzsfjhfa jbSFHEVsfv ugfgvf jbvje bufgagv</div>', 'en', '2020-02-19 16:01:38', '2020-02-19 16:01:38', NULL),
(3, 1, '<div>jhzsfjhfa jbSFHEVsfv ugfgvf jbvje bufgagv</div>', 'fr', '2020-02-19 16:01:38', '2020-02-19 16:01:38', NULL),
(4, 1, '<div>jhzsfjhfa jbSFHEVsfv ugfgvf jbvje bufgagv</div>', 'it', '2020-02-19 16:01:38', '2020-02-19 16:01:38', NULL),
(5, 1, '<div>jhzsfjhfa jbSFHEVsfv ugfgvf jbvje bufgagv</div>', 'es', '2020-02-19 16:01:38', '2020-02-19 16:01:38', NULL),
(6, 2, '<div>tمصىتسيبتالا شتاسلتاير ى تشسيارر</div>', 'ar', '2020-02-25 18:08:44', '2020-02-25 18:08:44', NULL),
(7, 2, '<div>strhfgx rgtregfbxb trsbv trgserg btt tsr</div>', 'en', '2020-02-25 18:08:44', '2020-02-25 18:08:44', NULL),
(8, 2, '<div>strhfgx rgtregfbxb trsbv trgserg btt tsr</div>', 'fr', '2020-02-25 18:08:44', '2020-02-25 18:08:44', NULL),
(9, 2, '<div>strhfgx rgtregfbxb trsbv trgserg btt tsr</div>', 'it', '2020-02-25 18:08:44', '2020-02-25 18:08:44', NULL),
(10, 2, '<div>strhfgx rgtregfbxb trsbv trgserg btt tsr</div>', 'es', '2020-02-25 18:08:44', '2020-02-25 18:08:44', NULL),
(11, 3, '<div>استمتع بافضل وجهة سياحية</div>', 'ar', '2020-04-15 12:12:19', '2020-04-15 12:12:19', NULL),
(12, 3, '<pre>Enjoy the best tourist destination</pre><div><br></div>', 'en', '2020-04-15 12:12:19', '2020-04-15 12:12:19', NULL),
(13, 3, '<pre>Enjoy the best tourist destination</pre><div><br></div>', 'fr', '2020-04-15 12:12:19', '2020-04-15 12:12:19', NULL),
(14, 3, '<pre>Enjoy the best tourist destination</pre><div><br></div>', 'it', '2020-04-15 12:12:19', '2020-04-15 12:12:19', NULL),
(15, 3, '<pre>Enjoy the best tourist destination</pre><div><br></div>', 'es', '2020-04-15 12:12:19', '2020-04-15 12:12:19', NULL),
(16, 4, '<div>استمتع بافضل وجهة سياحية خارجية</div>', 'ar', '2020-04-15 12:17:00', '2020-04-15 12:17:00', NULL),
(17, 4, '<pre>Enjoy the best foreign tourist destination</pre><div><br></div>', 'en', '2020-04-15 12:17:00', '2020-04-15 12:17:00', NULL),
(18, 4, '<pre>Enjoy the best foreign tourist destination</pre><div><br></div>', 'fr', '2020-04-15 12:17:00', '2020-04-15 12:17:00', NULL),
(19, 4, '<pre>Enjoy the best foreign tourist destination</pre><div><br></div>', 'it', '2020-04-15 12:17:00', '2020-04-15 12:17:00', NULL),
(20, 4, '<pre>Enjoy the best foreign tourist destination</pre><div><br></div>', 'es', '2020-04-15 12:17:00', '2020-04-15 12:17:00', NULL),
(21, 5, '<div>افضل عروض الحج و العمرة</div>', 'ar', '2020-04-15 12:18:31', '2020-04-15 12:18:31', NULL),
(22, 5, '<pre>The best offers of Hajj and Umrah</pre><div><br></div>', 'en', '2020-04-15 12:18:31', '2020-04-15 12:18:31', NULL),
(23, 5, '<pre>The best offers of Hajj and Umrah</pre><div><br></div>', 'fr', '2020-04-15 12:18:31', '2020-04-15 12:18:31', NULL),
(24, 5, '<pre>The best offers of Hajj and Umrah</pre><div><br></div>', 'it', '2020-04-15 12:18:31', '2020-04-15 12:18:31', NULL),
(25, 5, '<pre>The best offers of Hajj and Umrah</pre><div><br></div>', 'es', '2020-04-15 12:18:31', '2020-04-15 12:18:31', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `hotels`
--

CREATE TABLE `hotels` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city_id` bigint(20) UNSIGNED NOT NULL,
  `map_long` bigint(20) UNSIGNED NOT NULL,
  `map_lat` bigint(20) UNSIGNED NOT NULL,
  `level` double NOT NULL,
  `shown` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `hotels`
--

INSERT INTO `hotels` (`id`, `name`, `city_id`, `map_long`, `map_lat`, `level`, `shown`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'orbittest', 2, 30, 31, 5, 1, '2020-02-20 22:19:05', '2020-04-15 13:47:41', NULL),
(2, 'فندق هيلتون الغردقة', 5, 34, 27, 5, 1, '2020-04-15 13:47:23', '2020-04-15 13:48:17', NULL),
(3, 'Ciampino Hotel', 7, 30, 31, 4, 1, '2020-04-15 13:51:54', '2020-04-15 13:51:54', NULL),
(4, 'Hostal Ballesta', 3, 30, 31, 4, 1, '2020-04-15 13:55:53', '2020-04-15 13:55:53', NULL),
(5, 'فندق تيرا صدقي', 6, 30, 31, 1, 1, '2020-04-15 13:57:04', '2020-04-15 13:57:04', NULL),
(6, 'فندق إيلاف السلام', 6, 30, 31, 3, 1, '2020-04-15 13:58:29', '2020-04-15 13:58:29', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `hotel_bookings`
--

CREATE TABLE `hotel_bookings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `hotel_id` bigint(20) UNSIGNED NOT NULL,
  `from` datetime NOT NULL,
  `to` datetime NOT NULL,
  `total_cost` double NOT NULL,
  `adult_count` smallint(6) NOT NULL,
  `child_count` smallint(6) NOT NULL,
  `baby_count` smallint(6) NOT NULL,
  `status` enum('pending','confirmed','canceled') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending',
  `currency_exchange_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `customer_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `hotel_bookings`
--

INSERT INTO `hotel_bookings` (`id`, `hotel_id`, `from`, `to`, `total_cost`, `adult_count`, `child_count`, `baby_count`, `status`, `currency_exchange_id`, `created_at`, `updated_at`, `deleted_at`, `customer_id`) VALUES
(1, 1, '2020-03-10 00:00:00', '2020-03-31 00:00:00', 0, 2, 2, 2, 'pending', NULL, '2020-03-09 13:25:49', '2020-03-09 13:25:49', NULL, 2),
(2, 1, '2020-03-18 00:00:00', '2020-03-31 00:00:00', 0, 2, 0, 0, 'pending', NULL, '2020-03-18 16:35:08', '2020-03-18 16:35:08', NULL, 3);

-- --------------------------------------------------------

--
-- Table structure for table `hotel_booking_payments`
--

CREATE TABLE `hotel_booking_payments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `hotel_booking_id` bigint(20) UNSIGNED NOT NULL,
  `amount` double NOT NULL,
  `payment_date` datetime NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `hotel_service`
--

CREATE TABLE `hotel_service` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `hotel_id` bigint(20) UNSIGNED NOT NULL,
  `service_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `hotel_service`
--

INSERT INTO `hotel_service` (`id`, `hotel_id`, `service_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2, 2, 2, NULL, NULL, NULL),
(3, 3, 2, NULL, NULL, NULL),
(4, 4, 2, NULL, NULL, NULL),
(5, 5, 2, NULL, NULL, NULL),
(6, 6, 2, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `image_url` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `main_image` tinyint(1) NOT NULL,
  `imageable_id` int(10) UNSIGNED NOT NULL,
  `imageable_type` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`id`, `image_url`, `main_image`, `imageable_id`, `imageable_type`, `created_at`, `updated_at`, `deleted_at`) VALUES
(15, 'uploads/1AUPAhVxNx0KPGyiQ0QsT1apaGZSlRzp9QlnYRrH.jpeg', 1, 3, 'App\\Models\\FrontSliders\\FrontSlider', '2020-04-15 12:12:19', '2020-04-15 12:12:19', NULL),
(54, 'public/uploads/7kmlxv1Zrin54uaFRNGpxkcUCOe4vAA5swMIFyrw.jpeg', 1, 1, 'App\\Models\\Hotels\\Hotel', '2020-04-15 13:48:58', '2020-04-15 13:48:58', NULL),
(55, 'public/uploads/7GOg48t2a9PNdawbBxft7McjBdSWQR6EwvjNDIbA.jpeg', 0, 1, 'App\\Models\\Hotels\\Hotel', '2020-04-15 13:48:58', '2020-04-15 13:48:58', NULL),
(18, 'uploads/A88kSG9zl97Z5KCYZZTt3UNJsjRmGWvFmrdzhFqI.jpeg', 1, 3, 'App\\Models\\Aboutuses\\Aboutus', '2020-04-15 12:23:16', '2020-04-15 12:23:16', NULL),
(31, 'public/uploads/Aam3v23uUCHCNzcjvI4QihrCCDon9KWaqFiG8Esr.jpeg', 1, 2, 'App\\Models\\FixedTours\\FixedTour', '2020-04-15 13:24:51', '2020-04-15 13:24:51', NULL),
(32, 'public/uploads/JJKcy4pT1499E4j4108vIHeSguSrQdgAyNm9DQyH.jpeg', 0, 2, 'App\\Models\\FixedTours\\FixedTour', '2020-04-15 13:24:51', '2020-04-15 13:24:51', NULL),
(16, 'uploads/v6WJRpuBxOyHPeq4tv49xde51gKxpmOkdOYrjQkC.jpeg', 1, 4, 'App\\Models\\FrontSliders\\FrontSlider', '2020-04-15 12:17:00', '2020-04-15 12:17:00', NULL),
(19, 'uploads/9bVeQS8P4uFv9IMVbyOj7yz6UXvVjJHsjVmMLN6a.jpeg', 1, 4, 'App\\Models\\Aboutuses\\Aboutus', '2020-04-15 12:24:13', '2020-04-15 12:24:13', NULL),
(71, 'uploads/pOuIOhzWaCJQKeJFaD0cDSY4Vw3nlK0bVNIAv3ux.png', 1, 3, 'App\\Models\\CustomerOpinions\\CustomerOpinions', '2020-04-15 14:00:45', '2020-04-15 14:00:45', NULL),
(72, 'uploads/ItWMhkPjb1tZoTNe82DZh6BQFeYVudktMqUN4TOp.png', 1, 4, 'App\\Models\\CustomerOpinions\\CustomerOpinions', '2020-04-15 14:01:18', '2020-04-15 14:01:18', NULL),
(21, 'uploads/V6cU0NAfzjsJqby53LT2FW5fZ6BW89AI5tZmL2vJ.jpeg', 1, 3, 'App\\Models\\CitySliders\\CitySlider', '2020-04-15 12:43:21', '2020-04-15 12:43:21', NULL),
(74, 'uploads/B757eNSroyn2uaRpP9ynJ46NizwVlaBisWbkla5G.jpeg', 1, 3, 'App\\Models\\FrontNews\\FrontNews', '2020-04-15 14:07:10', '2020-04-15 14:07:10', NULL),
(22, 'uploads/1bs54ypFTPhG1vhhfo2UE2yazFDaEtmLFBfmL8fF.webp', 1, 4, 'App\\Models\\CitySliders\\CitySlider', '2020-04-15 12:45:23', '2020-04-15 12:45:23', NULL),
(17, 'uploads/rBDdz9fyxJmgyKmJn6RJxS1e8J4ER3s3vC4sOvCH.jpeg', 1, 5, 'App\\Models\\FrontSliders\\FrontSlider', '2020-04-15 12:18:31', '2020-04-15 12:18:31', NULL),
(20, 'uploads/wfxBSWlONuruLpEVDGFnRcqsgBWmvhqiPRcqfPRs.jpeg', 1, 5, 'App\\Models\\Aboutuses\\Aboutus', '2020-04-15 12:24:50', '2020-04-15 12:24:50', NULL),
(28, 'uploads/wQZsrmeaECrkl4AegCLCU5lt9AZJZpNCdu2C1RMb.jpeg', 1, 5, 'App\\Models\\CitySliders\\CitySlider', '2020-04-15 13:01:29', '2020-04-15 13:01:29', NULL),
(29, 'uploads/gG2gv13kSFA46rswAaxnLfQhsSh7n5MKtkCG0qs4.jpeg', 1, 6, 'App\\Models\\CitySliders\\CitySlider', '2020-04-15 13:02:38', '2020-04-15 13:02:38', NULL),
(25, 'uploads/LSOZ05QaGg7n0rFV2BzIWfdkFg273e9mRvL9CDin.jpeg', 1, 7, 'App\\Models\\CitySliders\\CitySlider', '2020-04-15 12:57:33', '2020-04-15 12:57:33', NULL),
(27, 'uploads/rHo0d6PhvGO4gagvnhEyY8Aty3iNRb3fJE01F43M.jpeg', 1, 8, 'App\\Models\\CitySliders\\CitySlider', '2020-04-15 13:00:15', '2020-04-15 13:00:15', NULL),
(30, 'uploads/guQ8S2L8OMjIrNBMBWzw8AyGrBBHsC8wBgOeUd6i.jpeg', 1, 9, 'App\\Models\\CitySliders\\CitySlider', '2020-04-15 13:05:38', '2020-04-15 13:05:38', NULL),
(33, 'public/uploads/PcHIbo4fFQrY1eBIcNi1JIfGSkgYs5VB2x8Uw38Q.jpeg', 0, 2, 'App\\Models\\FixedTours\\FixedTour', '2020-04-15 13:24:51', '2020-04-15 13:24:51', NULL),
(34, 'public/uploads/KSxzicrKGUZzK8w1Z7oujpxNVGXszEysGuYD0LYW.jpeg', 0, 2, 'App\\Models\\FixedTours\\FixedTour', '2020-04-15 13:24:51', '2020-04-15 13:24:51', NULL),
(35, 'public/uploads/msRzR8s0iDW1Drzzst1vQcyOWuUylnUf634ugiRo.jpeg', 1, 3, 'App\\Models\\FixedTours\\FixedTour', '2020-04-15 13:28:52', '2020-04-15 13:28:52', NULL),
(36, 'public/uploads/PzszejZGn15DheDZunT0pmgGK59cdjfSfAZoIiwY.jpeg', 0, 3, 'App\\Models\\FixedTours\\FixedTour', '2020-04-15 13:28:52', '2020-04-15 13:28:52', NULL),
(37, 'public/uploads/bxrYHstXBztODIfeZnDbYaEpdhqn9SLy24mZJFNS.jpeg', 0, 3, 'App\\Models\\FixedTours\\FixedTour', '2020-04-15 13:28:52', '2020-04-15 13:28:52', NULL),
(38, 'public/uploads/0SQFDIsBHtFt7GzwalChGBdKZJ3Wt5patC1xFPX8.jpeg', 0, 3, 'App\\Models\\FixedTours\\FixedTour', '2020-04-15 13:28:52', '2020-04-15 13:28:52', NULL),
(39, 'public/uploads/1knaARrCZ8o52gV6J4DEaH3LD3DrNyng6ryvhlb5.jpeg', 1, 4, 'App\\Models\\FixedTours\\FixedTour', '2020-04-15 13:35:31', '2020-04-15 13:35:31', NULL),
(40, 'public/uploads/Y3VR0NwQbPVsjXlF6E2lbtrf9HlRQ6rcggzgV58L.jpeg', 0, 4, 'App\\Models\\FixedTours\\FixedTour', '2020-04-15 13:35:31', '2020-04-15 13:35:31', NULL),
(41, 'public/uploads/EGQ2E9uPmKcC2u430rp8NjOgXHlOLuiW4eG4Lu2P.jpeg', 0, 4, 'App\\Models\\FixedTours\\FixedTour', '2020-04-15 13:35:31', '2020-04-15 13:35:31', NULL),
(42, 'public/uploads/jjMw5xfDnGxgwOJP5mg4US4IAO9oy93ZSoc5dYOO.jpeg', 0, 4, 'App\\Models\\FixedTours\\FixedTour', '2020-04-15 13:35:31', '2020-04-15 13:35:31', NULL),
(43, 'public/uploads/xneEpyXElJ5h8SNxXwcx6qiDh2vXDQ7GJk6BeKjQ.jpeg', 1, 5, 'App\\Models\\FixedTours\\FixedTour', '2020-04-15 13:40:38', '2020-04-15 13:40:38', NULL),
(44, 'public/uploads/xezOJd4eEpi3OhxClHwcO3XRcMNenfBjMYbZX6NT.jpeg', 0, 5, 'App\\Models\\FixedTours\\FixedTour', '2020-04-15 13:40:38', '2020-04-15 13:40:38', NULL),
(45, 'public/uploads/5zVkWzQV9SUJZzhr0JDtoAsLcMluWgKjQxx8kUtm.jpeg', 0, 5, 'App\\Models\\FixedTours\\FixedTour', '2020-04-15 13:40:38', '2020-04-15 13:40:38', NULL),
(46, 'public/uploads/m8np96YHtAEaIt88b1lehT3I066bWPepPfAWjEoP.jpeg', 1, 6, 'App\\Models\\FixedTours\\FixedTour', '2020-04-15 13:43:27', '2020-04-15 13:43:27', NULL),
(47, 'public/uploads/vSwRrOdD5qEk0MzkO7HQq5iOl18TxfQ6Duf6NvTP.jpeg', 0, 6, 'App\\Models\\FixedTours\\FixedTour', '2020-04-15 13:43:27', '2020-04-15 13:43:27', NULL),
(48, 'public/uploads/Jqbmgr0FYQE7ymh1BoS5ZYmTBOv98oK5WISt65T4.jpeg', 0, 6, 'App\\Models\\FixedTours\\FixedTour', '2020-04-15 13:43:27', '2020-04-15 13:43:27', NULL),
(49, 'public/uploads/xeMyhjS1NsM30rQ1jN98VHg0Vu0Vwg2AxYJ3dmer.jpeg', 0, 6, 'App\\Models\\FixedTours\\FixedTour', '2020-04-15 13:43:27', '2020-04-15 13:43:27', NULL),
(50, 'public/uploads/pQ14UnQV9jo3g0MURWbp91Owg4yTBcPtNrlxnCZ3.jpeg', 1, 2, 'App\\Models\\Hotels\\Hotel', '2020-04-15 13:47:23', '2020-04-15 13:47:23', NULL),
(51, 'public/uploads/iIjkbC27ZxLtCraDQxSsdhdhfpqe5L0MALXdZdlD.jpeg', 0, 2, 'App\\Models\\Hotels\\Hotel', '2020-04-15 13:47:23', '2020-04-15 13:47:23', NULL),
(52, 'public/uploads/iy7GSFtBJ99c2vYn9M2AGvjdMd3PXBIvDsKDexVu.jpeg', 0, 2, 'App\\Models\\Hotels\\Hotel', '2020-04-15 13:47:23', '2020-04-15 13:47:23', NULL),
(53, 'public/uploads/CnCi9qZG4tkrqbvBYdtM2JoruziZbRhrYksz8tBt.jpeg', 0, 2, 'App\\Models\\Hotels\\Hotel', '2020-04-15 13:47:23', '2020-04-15 13:47:23', NULL),
(56, 'public/uploads/GiV2MsrCbTX0mYuu3NnrRjKv6xSPLSzp1DyxmNIA.jpeg', 0, 1, 'App\\Models\\Hotels\\Hotel', '2020-04-15 13:48:58', '2020-04-15 13:48:58', NULL),
(57, 'public/uploads/BNKHchTTVFadpSkikBkO4bwTSzdD5QtU56WE8gB3.jpeg', 1, 3, 'App\\Models\\Hotels\\Hotel', '2020-04-15 13:51:54', '2020-04-15 13:51:54', NULL),
(58, 'public/uploads/AtXx3urA7NpcSvoBjYpoBMS7h3JoieaYwwTC6PfD.jpeg', 0, 3, 'App\\Models\\Hotels\\Hotel', '2020-04-15 13:51:54', '2020-04-15 13:51:54', NULL),
(59, 'public/uploads/z7guTjHhpnksKzInsch5KYPPCIQckrTV5zD6NhbY.jpeg', 0, 3, 'App\\Models\\Hotels\\Hotel', '2020-04-15 13:51:54', '2020-04-15 13:51:54', NULL),
(60, 'public/uploads/QxW8Vpo9U3Vq405BEOvyoWBLCvG5uPmMwdx3DBGP.jpeg', 0, 3, 'App\\Models\\Hotels\\Hotel', '2020-04-15 13:51:54', '2020-04-15 13:51:54', NULL),
(61, 'public/uploads/9BgoEhNd0KqjSSQlOeJzLUc11v235ikmGlnxXBLo.jpeg', 1, 4, 'App\\Models\\Hotels\\Hotel', '2020-04-15 13:55:53', '2020-04-15 13:55:53', NULL),
(62, 'public/uploads/81TA7P0Nurann6Zl4J7gypTKfSjCSvbI4gW4n2dc.webp', 0, 4, 'App\\Models\\Hotels\\Hotel', '2020-04-15 13:55:53', '2020-04-15 13:55:53', NULL),
(63, 'public/uploads/mRBaWorDMVgu45VXYzS2Hhp3rUlMH6N3xnWFLT0D.jpeg', 0, 4, 'App\\Models\\Hotels\\Hotel', '2020-04-15 13:55:53', '2020-04-15 13:55:53', NULL),
(64, 'public/uploads/D7W7fncjQEmcF6YZO92reFqmNHNeHOcrhie29RUf.jpeg', 1, 5, 'App\\Models\\Hotels\\Hotel', '2020-04-15 13:57:04', '2020-04-15 13:57:04', NULL),
(65, 'public/uploads/tp7zzeICGdYPp58fbOrd9kvFOHsGSmANNDdQCSzb.jpeg', 0, 5, 'App\\Models\\Hotels\\Hotel', '2020-04-15 13:57:04', '2020-04-15 13:57:04', NULL),
(66, 'public/uploads/97A8WA53w7pWQrzxK2zObWhkgSfGdsvJ4i4bLq5X.jpeg', 0, 5, 'App\\Models\\Hotels\\Hotel', '2020-04-15 13:57:04', '2020-04-15 13:57:04', NULL),
(67, 'public/uploads/ODqinVE1S9q6GmtZIO1Zis3cedYfuR5iANX07LpX.jpeg', 1, 6, 'App\\Models\\Hotels\\Hotel', '2020-04-15 13:58:29', '2020-04-15 13:58:29', NULL),
(68, 'public/uploads/tYrhqjLcMjC4QSM7xVhj2aFQfgba6AMHrCvctJZo.webp', 0, 6, 'App\\Models\\Hotels\\Hotel', '2020-04-15 13:58:29', '2020-04-15 13:58:29', NULL),
(69, 'public/uploads/noYPudcRJNlmBY9KvtHpoOpjfYDj5XjkcVcxSCYl.jpeg', 0, 6, 'App\\Models\\Hotels\\Hotel', '2020-04-15 13:58:29', '2020-04-15 13:58:29', NULL),
(70, 'public/uploads/MXsMoJ5AxqPgf1VEvB3HZAc8bIEpnnyC9IttzLXS.jpeg', 0, 6, 'App\\Models\\Hotels\\Hotel', '2020-04-15 13:58:29', '2020-04-15 13:58:29', NULL),
(73, 'uploads/9ZmiIspffBntW3jiqg25ncQoBV4Z6qYMsGaohKmJ.jpeg', 1, 2, 'App\\Models\\FrontNews\\FrontNews', '2020-04-15 14:05:17', '2020-04-15 14:05:17', NULL),
(75, 'uploads/krA9D3EZQkU5pU6G4J0PoJIW6EBpAYaVwqJFzth1.jpeg', 1, 4, 'App\\Models\\FrontNews\\FrontNews', '2020-04-15 14:09:54', '2020-04-15 14:09:54', NULL),
(76, 'uploads/xdTUi6RdBAf7xfjrsT3n9B6tlThD1eHBrBKPkD7V.jpeg', 1, 5, 'App\\Models\\FrontNews\\FrontNews', '2020-04-15 14:12:05', '2020-04-15 14:12:05', NULL),
(77, 'uploads/1EfXpTh9tellbGBYgXKGHa0mBghjMZiWSz9olJrp.jpeg', 1, 6, 'App\\Models\\FrontNews\\FrontNews', '2020-04-15 14:15:48', '2020-04-15 14:15:48', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `queue` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint(3) UNSIGNED NOT NULL,
  `reserved_at` int(10) UNSIGNED DEFAULT NULL,
  `available_at` int(10) UNSIGNED NOT NULL,
  `created_at` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ledgers`
--

CREATE TABLE `ledgers` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_type` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `recordable_type` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `recordable_id` bigint(20) UNSIGNED NOT NULL,
  `context` tinyint(3) UNSIGNED NOT NULL,
  `event` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `properties` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `modified` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `pivot` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `extra` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` text COLLATE utf8mb4_unicode_ci,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `signature` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ledgers`
--

INSERT INTO `ledgers` (`id`, `user_type`, `user_id`, `recordable_type`, `recordable_id`, `context`, `event`, `properties`, `modified`, `pivot`, `extra`, `url`, `ip_address`, `user_agent`, `signature`, `created_at`, `updated_at`) VALUES
(1, 'App\\Models\\Auth\\User', 1, 'App\\Models\\Auth\\User', 1, 4, 'updated', '{\"id\":1,\"uuid\":\"e57a1a52-00a9-43c0-9dfe-beca1e5be7e0\",\"first_name\":\"Super\",\"last_name\":\"Admin\",\"email\":\"admin@admin.com\",\"avatar_type\":\"gravatar\",\"avatar_location\":null,\"password\":\"$2y$10$Cmvk08sqKKt5KwGE5w89nOoXYyiBJ2WdMyuZPR8aOnZYGE8eyBL1W\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"6590d23fbbe79c99dd1849f8a3febe6e\",\"confirmed\":1,\"timezone\":null,\"last_login_at\":null,\"last_login_ip\":null,\"to_be_logged_out\":0,\"remember_token\":\"q7JxyNxTgKvL0C2cgf1aJpZHPPG2x6X69qvDZqXbpeIve91EBW8xD1oYdanU\",\"created_at\":\"2020-02-19 04:34:10\",\"updated_at\":\"2020-02-19 04:34:10\",\"deleted_at\":null}', '[\"remember_token\"]', '[]', '[]', 'http://amricatain.flyfoxsoftware.com/login', '156.218.119.69', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:72.0) Gecko/20100101 Firefox/72.0', '8edb8d75dfd1cc29a03c051ea5a10617228199b4e4dc5480f863da27095346cd95ae95e088000c19052cef9cee454df64c9542e1d47257d0595d65224b051ba7', '2020-02-19 15:56:18', '2020-02-19 15:56:18'),
(2, 'App\\Models\\Auth\\User', 1, 'App\\Models\\Auth\\User', 1, 4, 'updated', '{\"id\":1,\"uuid\":\"e57a1a52-00a9-43c0-9dfe-beca1e5be7e0\",\"first_name\":\"Super\",\"last_name\":\"Admin\",\"email\":\"admin@admin.com\",\"avatar_type\":\"gravatar\",\"avatar_location\":null,\"password\":\"$2y$10$Cmvk08sqKKt5KwGE5w89nOoXYyiBJ2WdMyuZPR8aOnZYGE8eyBL1W\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"6590d23fbbe79c99dd1849f8a3febe6e\",\"confirmed\":1,\"timezone\":\"Africa\\/Cairo\",\"last_login_at\":\"2020-02-19 10:56:18\",\"last_login_ip\":\"156.218.119.69\",\"to_be_logged_out\":0,\"remember_token\":\"q7JxyNxTgKvL0C2cgf1aJpZHPPG2x6X69qvDZqXbpeIve91EBW8xD1oYdanU\",\"created_at\":\"2020-02-19 04:34:10\",\"updated_at\":\"2020-02-19 10:56:18\",\"deleted_at\":null}', '[\"timezone\",\"last_login_at\",\"last_login_ip\",\"updated_at\"]', '[]', '[]', 'http://amricatain.flyfoxsoftware.com/login', '156.218.119.69', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:72.0) Gecko/20100101 Firefox/72.0', '729c47d089e231ebcfc51ebd33ae1a06029aab7e7ab5b7fb78cec7a5f5bf5a6f3c1a94b2e89591c0ba171338b0b49baf218953ad16174b1caf7744ff41340465', '2020-02-19 15:56:18', '2020-02-19 15:56:18'),
(3, 'App\\Models\\Auth\\User', 1, 'App\\Models\\Auth\\User', 1, 4, 'updated', '{\"id\":1,\"uuid\":\"e57a1a52-00a9-43c0-9dfe-beca1e5be7e0\",\"first_name\":\"Super\",\"last_name\":\"Admin\",\"email\":\"admin@admin.com\",\"avatar_type\":\"gravatar\",\"avatar_location\":null,\"password\":\"$2y$10$Cmvk08sqKKt5KwGE5w89nOoXYyiBJ2WdMyuZPR8aOnZYGE8eyBL1W\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"6590d23fbbe79c99dd1849f8a3febe6e\",\"confirmed\":1,\"timezone\":\"Africa\\/Cairo\",\"last_login_at\":\"2020-02-19 10:56:18\",\"last_login_ip\":\"156.218.119.69\",\"to_be_logged_out\":0,\"remember_token\":\"iKDpow5Qx0OWqRdgu8MFNARo84TkmWK17wsCmnuWrNxzOwQCqjIrR8PSMsGB\",\"created_at\":\"2020-02-19 04:34:10\",\"updated_at\":\"2020-02-19 10:56:18\",\"deleted_at\":null}', '[\"remember_token\"]', '[]', '[]', 'http://amricatain.flyfoxsoftware.com/logout', '156.218.132.149', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:72.0) Gecko/20100101 Firefox/72.0', 'ebd7c5a93f127dba1e116fec9a680ddd7f4d92441bcfd6e3a422922691ee3b740970dbbb5a36fa977e5b422f36ad4db735070fb3fd7f14d5600073f1dec9314f', '2020-02-19 19:44:41', '2020-02-19 19:44:41'),
(4, 'App\\Models\\Auth\\User', 1, 'App\\Models\\Auth\\User', 1, 4, 'updated', '{\"id\":1,\"uuid\":\"e57a1a52-00a9-43c0-9dfe-beca1e5be7e0\",\"first_name\":\"Super\",\"last_name\":\"Admin\",\"email\":\"admin@admin.com\",\"avatar_type\":\"gravatar\",\"avatar_location\":null,\"password\":\"$2y$10$Cmvk08sqKKt5KwGE5w89nOoXYyiBJ2WdMyuZPR8aOnZYGE8eyBL1W\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"6590d23fbbe79c99dd1849f8a3febe6e\",\"confirmed\":1,\"timezone\":\"Africa\\/Cairo\",\"last_login_at\":\"2020-02-20 10:49:43\",\"last_login_ip\":\"156.218.66.121\",\"to_be_logged_out\":0,\"remember_token\":\"iKDpow5Qx0OWqRdgu8MFNARo84TkmWK17wsCmnuWrNxzOwQCqjIrR8PSMsGB\",\"created_at\":\"2020-02-19 04:34:10\",\"updated_at\":\"2020-02-20 10:49:43\",\"deleted_at\":null}', '[\"last_login_at\",\"last_login_ip\",\"updated_at\"]', '[]', '[]', 'http://amricatain.flyfoxsoftware.com/login', '156.218.66.121', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.116 Safari/537.36', 'cdc70a58d852e0e4e2c3bae4a18b2a34e29e19b368202efee4bf76d81b54d0deed96c9a42f497fa7a5bef9a504a31432fc13341c552bf09cdf29ab41cdc06986', '2020-02-20 15:49:43', '2020-02-20 15:49:43'),
(5, 'App\\Models\\Auth\\User', 1, 'App\\Models\\Auth\\User', 1, 4, 'updated', '{\"id\":1,\"uuid\":\"e57a1a52-00a9-43c0-9dfe-beca1e5be7e0\",\"first_name\":\"Super\",\"last_name\":\"Admin\",\"email\":\"admin@admin.com\",\"avatar_type\":\"gravatar\",\"avatar_location\":null,\"password\":\"$2y$10$Cmvk08sqKKt5KwGE5w89nOoXYyiBJ2WdMyuZPR8aOnZYGE8eyBL1W\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"6590d23fbbe79c99dd1849f8a3febe6e\",\"confirmed\":1,\"timezone\":\"Africa\\/Cairo\",\"last_login_at\":\"2020-02-20 10:49:43\",\"last_login_ip\":\"156.218.66.121\",\"to_be_logged_out\":0,\"remember_token\":\"C7BJW7BBdYzMhwIiXtuviPicvkKwfW2BHM4ovJxy3EE93ygqDfArTCYnBwjW\",\"created_at\":\"2020-02-19 04:34:10\",\"updated_at\":\"2020-02-20 10:49:43\",\"deleted_at\":null}', '[\"remember_token\"]', '[]', '[]', 'http://amricatain.flyfoxsoftware.com/logout', '156.218.66.121', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.116 Safari/537.36', 'd6fdfaab699e19211999d7bd67c8162bd0dc99d3c5806f108b7cdd7e29a5808988303a8f9996603c8935d433d3ce6f53da370cf652c0672c3d187a85cca063d2', '2020-02-20 17:09:02', '2020-02-20 17:09:02'),
(6, 'App\\Models\\Auth\\User', 1, 'App\\Models\\Auth\\User', 1, 4, 'updated', '{\"id\":1,\"uuid\":\"e57a1a52-00a9-43c0-9dfe-beca1e5be7e0\",\"first_name\":\"Super\",\"last_name\":\"Admin\",\"email\":\"admin@admin.com\",\"avatar_type\":\"gravatar\",\"avatar_location\":null,\"password\":\"$2y$10$Cmvk08sqKKt5KwGE5w89nOoXYyiBJ2WdMyuZPR8aOnZYGE8eyBL1W\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"6590d23fbbe79c99dd1849f8a3febe6e\",\"confirmed\":1,\"timezone\":\"Africa\\/Cairo\",\"last_login_at\":\"2020-02-20 12:23:31\",\"last_login_ip\":\"156.218.66.121\",\"to_be_logged_out\":0,\"remember_token\":\"C7BJW7BBdYzMhwIiXtuviPicvkKwfW2BHM4ovJxy3EE93ygqDfArTCYnBwjW\",\"created_at\":\"2020-02-19 04:34:10\",\"updated_at\":\"2020-02-20 12:23:32\",\"deleted_at\":null}', '[\"last_login_at\",\"updated_at\"]', '[]', '[]', 'http://amricatain.flyfoxsoftware.com/login', '156.218.66.121', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.116 Safari/537.36', 'df5874192e4c8c79f7c38aebb852703803091d6c0ee739ca91d87bd754a239ace3c9977bb5b6c70ce4250cf6bc4212ea379aac089ffd60b2b72bd6e732c387da', '2020-02-20 17:23:32', '2020-02-20 17:23:32'),
(7, 'App\\Models\\Auth\\User', 1, 'App\\Models\\Auth\\User', 1, 4, 'updated', '{\"id\":1,\"uuid\":\"e57a1a52-00a9-43c0-9dfe-beca1e5be7e0\",\"first_name\":\"Super\",\"last_name\":\"Admin\",\"email\":\"admin@admin.com\",\"avatar_type\":\"gravatar\",\"avatar_location\":null,\"password\":\"$2y$10$Cmvk08sqKKt5KwGE5w89nOoXYyiBJ2WdMyuZPR8aOnZYGE8eyBL1W\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"6590d23fbbe79c99dd1849f8a3febe6e\",\"confirmed\":1,\"timezone\":\"Africa\\/Cairo\",\"last_login_at\":\"2020-02-20 12:23:31\",\"last_login_ip\":\"156.218.66.121\",\"to_be_logged_out\":0,\"remember_token\":\"MbiddLzSLeSufYXni6cPm3E1twgVMLh0fVorRDjQvg04A5sK4v7NZVTwq2oA\",\"created_at\":\"2020-02-19 04:34:10\",\"updated_at\":\"2020-02-20 12:23:32\",\"deleted_at\":null}', '[\"remember_token\"]', '[]', '[]', 'http://amricatain.flyfoxsoftware.com/logout', '156.218.66.121', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.116 Safari/537.36', '5d6f58e301fc54c379ef04fcce4bd5bb8b8e76efe6f14b3efbbef13094e78000ef710194bc34131641a35510a39c833f4605d749a603401636df25ce6bb4cc74', '2020-02-20 17:23:54', '2020-02-20 17:23:54'),
(8, 'App\\Models\\Auth\\User', 1, 'App\\Models\\Auth\\User', 1, 4, 'updated', '{\"id\":1,\"uuid\":\"e57a1a52-00a9-43c0-9dfe-beca1e5be7e0\",\"first_name\":\"Super\",\"last_name\":\"Admin\",\"email\":\"admin@admin.com\",\"avatar_type\":\"gravatar\",\"avatar_location\":null,\"password\":\"$2y$10$Cmvk08sqKKt5KwGE5w89nOoXYyiBJ2WdMyuZPR8aOnZYGE8eyBL1W\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"6590d23fbbe79c99dd1849f8a3febe6e\",\"confirmed\":1,\"timezone\":\"Africa\\/Cairo\",\"last_login_at\":\"2020-02-20 12:27:30\",\"last_login_ip\":\"156.218.66.121\",\"to_be_logged_out\":0,\"remember_token\":\"MbiddLzSLeSufYXni6cPm3E1twgVMLh0fVorRDjQvg04A5sK4v7NZVTwq2oA\",\"created_at\":\"2020-02-19 04:34:10\",\"updated_at\":\"2020-02-20 12:27:30\",\"deleted_at\":null}', '[\"last_login_at\",\"updated_at\"]', '[]', '[]', 'http://amricatain.flyfoxsoftware.com/login', '156.218.66.121', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.116 Safari/537.36', 'f01fead423e229373942dd8011e611de46f87814c6a93a5c87b62099035d797944c1f7ac687151b56b54f3e029c1241e25cb12f3349d69427184e81ad02f1146', '2020-02-20 17:27:30', '2020-02-20 17:27:30'),
(9, 'App\\Models\\Auth\\User', 1, 'App\\Models\\Auth\\User', 1, 4, 'updated', '{\"id\":1,\"uuid\":\"e57a1a52-00a9-43c0-9dfe-beca1e5be7e0\",\"first_name\":\"Super\",\"last_name\":\"Admin\",\"email\":\"admin@admin.com\",\"avatar_type\":\"gravatar\",\"avatar_location\":null,\"password\":\"$2y$10$Cmvk08sqKKt5KwGE5w89nOoXYyiBJ2WdMyuZPR8aOnZYGE8eyBL1W\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"6590d23fbbe79c99dd1849f8a3febe6e\",\"confirmed\":1,\"timezone\":\"Africa\\/Cairo\",\"last_login_at\":\"2020-02-20 12:27:30\",\"last_login_ip\":\"156.218.66.121\",\"to_be_logged_out\":0,\"remember_token\":\"18sgLKGvzzlWR8T7ZxTkE4lxS9FHSlYP5rDKfNz1c8wna8tdgererl5QdvPD\",\"created_at\":\"2020-02-19 04:34:10\",\"updated_at\":\"2020-02-20 12:27:30\",\"deleted_at\":null}', '[\"remember_token\"]', '[]', '[]', 'http://amricatain.flyfoxsoftware.com/logout', '156.218.66.121', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.116 Safari/537.36', '79106901a189d1826de54c4a6d96e79490b1c7ff4d29f5146498dc062e6c73463f079cfdd08db9d9048be5e185385cc9bb09129fd56ceebf06c8468108a75219', '2020-02-20 17:34:15', '2020-02-20 17:34:15'),
(10, 'App\\Models\\Auth\\User', 1, 'App\\Models\\Auth\\User', 1, 4, 'updated', '{\"id\":1,\"uuid\":\"e57a1a52-00a9-43c0-9dfe-beca1e5be7e0\",\"first_name\":\"Super\",\"last_name\":\"Admin\",\"email\":\"admin@admin.com\",\"avatar_type\":\"gravatar\",\"avatar_location\":null,\"password\":\"$2y$10$Cmvk08sqKKt5KwGE5w89nOoXYyiBJ2WdMyuZPR8aOnZYGE8eyBL1W\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"6590d23fbbe79c99dd1849f8a3febe6e\",\"confirmed\":1,\"timezone\":\"Africa\\/Cairo\",\"last_login_at\":\"2020-02-20 12:34:38\",\"last_login_ip\":\"156.218.66.121\",\"to_be_logged_out\":0,\"remember_token\":\"18sgLKGvzzlWR8T7ZxTkE4lxS9FHSlYP5rDKfNz1c8wna8tdgererl5QdvPD\",\"created_at\":\"2020-02-19 04:34:10\",\"updated_at\":\"2020-02-20 12:34:38\",\"deleted_at\":null}', '[\"last_login_at\",\"updated_at\"]', '[]', '[]', 'http://amricatain.flyfoxsoftware.com/login', '156.218.66.121', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.116 Safari/537.36', '797192dd6c2c3b1ab7f9c3d96d23165da4d9a46cf46f862a9529a871414df9babc8d06dc212716e1542b8272291369d7823e49e47860a8444c091cdc357775b4', '2020-02-20 17:34:38', '2020-02-20 17:34:38'),
(11, 'App\\Models\\Auth\\User', 1, 'App\\Models\\Auth\\User', 1, 4, 'updated', '{\"id\":1,\"uuid\":\"e57a1a52-00a9-43c0-9dfe-beca1e5be7e0\",\"first_name\":\"Super\",\"last_name\":\"Admin\",\"email\":\"admin@admin.com\",\"avatar_type\":\"gravatar\",\"avatar_location\":null,\"password\":\"$2y$10$Cmvk08sqKKt5KwGE5w89nOoXYyiBJ2WdMyuZPR8aOnZYGE8eyBL1W\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"6590d23fbbe79c99dd1849f8a3febe6e\",\"confirmed\":1,\"timezone\":\"Africa\\/Cairo\",\"last_login_at\":\"2020-02-20 17:17:33\",\"last_login_ip\":\"156.218.66.121\",\"to_be_logged_out\":0,\"remember_token\":\"18sgLKGvzzlWR8T7ZxTkE4lxS9FHSlYP5rDKfNz1c8wna8tdgererl5QdvPD\",\"created_at\":\"2020-02-19 04:34:10\",\"updated_at\":\"2020-02-20 17:17:33\",\"deleted_at\":null}', '[\"last_login_at\",\"updated_at\"]', '[]', '[]', 'http://amricatain.flyfoxsoftware.com/login', '156.218.66.121', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.116 Safari/537.36', 'd3474b707703f76df4b5ab428604c70fe98490dbae7a30154c47dc3a979bce44569d91c327b3e0edddb4a6d0a865154f7cab403e0cc684f5264f09c90a5ebaf8', '2020-02-20 22:17:33', '2020-02-20 22:17:33'),
(12, 'App\\Models\\Auth\\User', 1, 'App\\Models\\Auth\\User', 1, 4, 'updated', '{\"id\":1,\"uuid\":\"e57a1a52-00a9-43c0-9dfe-beca1e5be7e0\",\"first_name\":\"Super\",\"last_name\":\"Admin\",\"email\":\"admin@admin.com\",\"avatar_type\":\"gravatar\",\"avatar_location\":null,\"password\":\"$2y$10$Cmvk08sqKKt5KwGE5w89nOoXYyiBJ2WdMyuZPR8aOnZYGE8eyBL1W\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"6590d23fbbe79c99dd1849f8a3febe6e\",\"confirmed\":1,\"timezone\":\"Africa\\/Cairo\",\"last_login_at\":\"2020-02-25 12:17:55\",\"last_login_ip\":\"102.43.120.227\",\"to_be_logged_out\":0,\"remember_token\":\"18sgLKGvzzlWR8T7ZxTkE4lxS9FHSlYP5rDKfNz1c8wna8tdgererl5QdvPD\",\"created_at\":\"2020-02-19 04:34:10\",\"updated_at\":\"2020-02-25 12:17:55\",\"deleted_at\":null}', '[\"last_login_at\",\"last_login_ip\",\"updated_at\"]', '[]', '[]', 'http://amricatain.flyfoxsoftware.com/login', '102.43.120.227', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:73.0) Gecko/20100101 Firefox/73.0', 'f77a2ce69ceca8fd307060f2a20ca720ef95190fb70381d3036db2c6da9760e0862a8407aa2efa6d2bffbb24145d48b2ded87fbd45a1f907b97807b1a2cd280a', '2020-02-25 17:17:55', '2020-02-25 17:17:55'),
(13, 'App\\Models\\Auth\\User', 1, 'App\\Models\\Auth\\User', 1, 4, 'updated', '{\"id\":1,\"uuid\":\"e57a1a52-00a9-43c0-9dfe-beca1e5be7e0\",\"first_name\":\"Super\",\"last_name\":\"Admin\",\"email\":\"admin@admin.com\",\"avatar_type\":\"gravatar\",\"avatar_location\":null,\"password\":\"$2y$10$Cmvk08sqKKt5KwGE5w89nOoXYyiBJ2WdMyuZPR8aOnZYGE8eyBL1W\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"6590d23fbbe79c99dd1849f8a3febe6e\",\"confirmed\":1,\"timezone\":\"Africa\\/Cairo\",\"last_login_at\":\"2020-02-25 12:17:55\",\"last_login_ip\":\"102.43.120.227\",\"to_be_logged_out\":0,\"remember_token\":\"19xLXx9AFhprTF3TTvCKEvWGQyYs0SmNFLuEPKJVzPDAppQKKcZex38f2srP\",\"created_at\":\"2020-02-19 04:34:10\",\"updated_at\":\"2020-02-25 12:17:55\",\"deleted_at\":null}', '[\"remember_token\"]', '[]', '[]', 'http://amricatain.flyfoxsoftware.com/logout', '102.43.120.227', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:73.0) Gecko/20100101 Firefox/73.0', '056d2366d1bac40e4b7e3f84932ac0780395bd4ee0facf18d2898ff6aca19f88074326b7aa1fac184bb528ada6660a472041be0afd98810768a10d36672eb5a0', '2020-02-25 17:23:31', '2020-02-25 17:23:31'),
(14, 'App\\Models\\Auth\\User', 1, 'App\\Models\\Auth\\User', 1, 4, 'updated', '{\"id\":1,\"uuid\":\"e57a1a52-00a9-43c0-9dfe-beca1e5be7e0\",\"first_name\":\"Super\",\"last_name\":\"Admin\",\"email\":\"admin@admin.com\",\"avatar_type\":\"gravatar\",\"avatar_location\":null,\"password\":\"$2y$10$Cmvk08sqKKt5KwGE5w89nOoXYyiBJ2WdMyuZPR8aOnZYGE8eyBL1W\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"6590d23fbbe79c99dd1849f8a3febe6e\",\"confirmed\":1,\"timezone\":\"Africa\\/Cairo\",\"last_login_at\":\"2020-02-25 12:17:55\",\"last_login_ip\":\"102.43.120.227\",\"to_be_logged_out\":0,\"remember_token\":\"fWFM9d4SqslZBF2QRtgFKsjqfU1zwObsUDGtHnfuwYV0N2uIlVmpFHRYqKr7\",\"created_at\":\"2020-02-19 04:34:10\",\"updated_at\":\"2020-02-25 12:17:55\",\"deleted_at\":null}', '[\"remember_token\"]', '[]', '[]', 'http://amricatain.flyfoxsoftware.com/logout', '102.43.120.227', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:73.0) Gecko/20100101 Firefox/73.0', 'f90134c6c24675cb2e60c150728383fc971878370cc297356d6dd8d00403e83896bd805784126e9dcbad9ceafacc862e873fd93b1699cd5c97b73cda978fafd0', '2020-02-25 17:23:32', '2020-02-25 17:23:32'),
(15, 'App\\Models\\Auth\\User', 1, 'App\\Models\\Auth\\User', 1, 4, 'updated', '{\"id\":1,\"uuid\":\"e57a1a52-00a9-43c0-9dfe-beca1e5be7e0\",\"first_name\":\"Super\",\"last_name\":\"Admin\",\"email\":\"admin@admin.com\",\"avatar_type\":\"gravatar\",\"avatar_location\":null,\"password\":\"$2y$10$Cmvk08sqKKt5KwGE5w89nOoXYyiBJ2WdMyuZPR8aOnZYGE8eyBL1W\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"6590d23fbbe79c99dd1849f8a3febe6e\",\"confirmed\":1,\"timezone\":\"Africa\\/Cairo\",\"last_login_at\":\"2020-02-25 12:23:43\",\"last_login_ip\":\"102.43.120.227\",\"to_be_logged_out\":0,\"remember_token\":\"fWFM9d4SqslZBF2QRtgFKsjqfU1zwObsUDGtHnfuwYV0N2uIlVmpFHRYqKr7\",\"created_at\":\"2020-02-19 04:34:10\",\"updated_at\":\"2020-02-25 12:23:43\",\"deleted_at\":null}', '[\"last_login_at\",\"updated_at\"]', '[]', '[]', 'http://amricatain.flyfoxsoftware.com/login', '102.43.120.227', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:73.0) Gecko/20100101 Firefox/73.0', 'edc2457ac32a0ab84008f782906edb9e5455bd6ebbb9904baefb9e59d63feb2e9b4e00d908bf64318cfb83a6ad20d57c7f5bf37d52b956d0e4ae36ff324f1e8c', '2020-02-25 17:23:43', '2020-02-25 17:23:43'),
(16, 'App\\Models\\Auth\\User', 1, 'App\\Models\\Auth\\User', 3, 4, 'created', '{\"first_name\":\"hassan\",\"email\":\"hassan@mail.com\",\"password\":\"$2y$10$X5BENphXbwk83TxjpTziZ.COAMyv1961h3VWnApKfB95sjQdZvsUG\",\"confirmed\":1,\"uuid\":\"9a5e87df-f523-4d82-afbf-bdf513bac257\",\"updated_at\":\"2020-02-25 12:36:44\",\"created_at\":\"2020-02-25 12:36:44\",\"id\":3}', '[\"first_name\",\"email\",\"password\",\"confirmed\",\"uuid\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://amricatain.flyfoxsoftware.com/admin/customers', '102.43.120.227', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:73.0) Gecko/20100101 Firefox/73.0', '4783e6f0458df84e4ffe4fe2e37b697145ab1b010ff864da2afc062fa288b7bebc8783c4633c5caad3c69e2a06034c452371ddb3a056b4e792b2ff3784eae4be', '2020-02-25 17:36:44', '2020-02-25 17:36:44'),
(17, 'App\\Models\\Auth\\User', 1, 'App\\Models\\Auth\\User', 3, 4, 'synced', '{\"first_name\":\"hassan\",\"email\":\"hassan@mail.com\",\"password\":\"$2y$10$X5BENphXbwk83TxjpTziZ.COAMyv1961h3VWnApKfB95sjQdZvsUG\",\"confirmed\":1,\"uuid\":\"9a5e87df-f523-4d82-afbf-bdf513bac257\",\"updated_at\":\"2020-02-25 12:36:44\",\"created_at\":\"2020-02-25 12:36:44\",\"id\":3}', '[]', '{\"relation\":\"roles\",\"properties\":[{\"model_id\":3,\"model_type\":\"App\\\\Models\\\\Auth\\\\User\",\"role_id\":2}]}', '[]', 'http://amricatain.flyfoxsoftware.com/admin/customers', '102.43.120.227', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:73.0) Gecko/20100101 Firefox/73.0', '0d77f88a87ed29a95a30d884526b45833654ed0f18cd2ada0476cc45b46e2577ea41279241e7042113dbb845e02c9bdc03620a5b961d2f6aac224fb2430f9570', '2020-02-25 17:36:45', '2020-02-25 17:36:45'),
(18, 'App\\Models\\Auth\\User', 1, 'App\\Models\\Auth\\User', 1, 4, 'updated', '{\"id\":1,\"uuid\":\"e57a1a52-00a9-43c0-9dfe-beca1e5be7e0\",\"first_name\":\"Super\",\"last_name\":\"Admin\",\"email\":\"admin@admin.com\",\"avatar_type\":\"gravatar\",\"avatar_location\":null,\"password\":\"$2y$10$Cmvk08sqKKt5KwGE5w89nOoXYyiBJ2WdMyuZPR8aOnZYGE8eyBL1W\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"6590d23fbbe79c99dd1849f8a3febe6e\",\"confirmed\":1,\"timezone\":\"Africa\\/Cairo\",\"last_login_at\":\"2020-02-25 12:23:43\",\"last_login_ip\":\"102.43.120.227\",\"to_be_logged_out\":0,\"remember_token\":\"qa1Y1J41On03G5zfelWBlsj1Qt5KuFRZerikJ9z8KYTw3hP2EXQVM7SAYmSE\",\"created_at\":\"2020-02-19 04:34:10\",\"updated_at\":\"2020-02-25 12:23:43\",\"deleted_at\":null}', '[\"remember_token\"]', '[]', '[]', 'http://amricatain.flyfoxsoftware.com/logout', '41.47.67.231', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:73.0) Gecko/20100101 Firefox/73.0', '7e115cdda9d213fa5bfeebd61a0173dea7752b0c4e1396876b1529cd35cc0936c5a10442d6d6033ef5ac9c543ff735a557eeff87bcbf02137ecef38f597899bd', '2020-03-09 13:23:59', '2020-03-09 13:23:59'),
(19, NULL, NULL, 'App\\Models\\Auth\\User', 4, 4, 'created', '{\"first_name\":\"hassan mohamed\",\"email\":\"etshhassan9@gmail.com\",\"password\":\"$2y$10$jjQluzvHPJAkiJZH1jpAXOu8a5l.nfrku0f8Bn667W8eqlsyVicrS\",\"confirmed\":1,\"uuid\":\"fc25997f-1a0c-40ca-a492-9664c96dcb92\",\"updated_at\":\"2020-03-09 09:25:25\",\"created_at\":\"2020-03-09 09:25:25\",\"id\":4}', '[\"first_name\",\"email\",\"password\",\"confirmed\",\"uuid\",\"updated_at\",\"created_at\",\"id\"]', '[]', '[]', 'http://amricatain.flyfoxsoftware.com/register', '41.47.67.231', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:73.0) Gecko/20100101 Firefox/73.0', '55800c3c8bbf0c4cbf154291ab4f077724ebeaf8da4b4ae1f027980e4c67717b414737e49e50f9f723edb0f61a9f5ca384df783abee6600734e7eeac417fc56d', '2020-03-09 13:25:25', '2020-03-09 13:25:25'),
(20, 'App\\Models\\Auth\\User', 1, 'App\\Models\\Auth\\User', 1, 4, 'updated', '{\"id\":1,\"uuid\":\"e57a1a52-00a9-43c0-9dfe-beca1e5be7e0\",\"first_name\":\"Super\",\"last_name\":\"Admin\",\"email\":\"admin@admin.com\",\"avatar_type\":\"gravatar\",\"avatar_location\":null,\"password\":\"$2y$10$Cmvk08sqKKt5KwGE5w89nOoXYyiBJ2WdMyuZPR8aOnZYGE8eyBL1W\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"6590d23fbbe79c99dd1849f8a3febe6e\",\"confirmed\":1,\"timezone\":\"Africa\\/Cairo\",\"last_login_at\":\"2020-03-09 09:32:45\",\"last_login_ip\":\"41.47.67.231\",\"to_be_logged_out\":0,\"remember_token\":\"qa1Y1J41On03G5zfelWBlsj1Qt5KuFRZerikJ9z8KYTw3hP2EXQVM7SAYmSE\",\"created_at\":\"2020-02-19 04:34:10\",\"updated_at\":\"2020-03-09 09:32:45\",\"deleted_at\":null}', '[\"last_login_at\",\"last_login_ip\",\"updated_at\"]', '[]', '[]', 'http://amricatain.flyfoxsoftware.com/login', '41.47.67.231', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:73.0) Gecko/20100101 Firefox/73.0', 'b1d3500c5e0f643ef543c32b01f6afb148050d0950b335c9e376e45d23b79cfe79daaac71b42ed81c34c605bac9e7d977fd00509b15a89e237c410665f750cc2', '2020-03-09 13:32:45', '2020-03-09 13:32:45'),
(21, 'App\\Models\\Auth\\User', 1, 'App\\Models\\Auth\\User', 1, 4, 'updated', '{\"id\":1,\"uuid\":\"e57a1a52-00a9-43c0-9dfe-beca1e5be7e0\",\"first_name\":\"Super\",\"last_name\":\"Admin\",\"email\":\"admin@admin.com\",\"avatar_type\":\"gravatar\",\"avatar_location\":null,\"password\":\"$2y$10$Cmvk08sqKKt5KwGE5w89nOoXYyiBJ2WdMyuZPR8aOnZYGE8eyBL1W\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"6590d23fbbe79c99dd1849f8a3febe6e\",\"confirmed\":1,\"timezone\":\"Africa\\/Cairo\",\"last_login_at\":\"2020-03-11 10:44:02\",\"last_login_ip\":\"41.47.67.231\",\"to_be_logged_out\":0,\"remember_token\":\"qa1Y1J41On03G5zfelWBlsj1Qt5KuFRZerikJ9z8KYTw3hP2EXQVM7SAYmSE\",\"created_at\":\"2020-02-19 04:34:10\",\"updated_at\":\"2020-03-11 10:44:02\",\"deleted_at\":null}', '[\"last_login_at\",\"updated_at\"]', '[]', '[]', 'http://amricatain.flyfoxsoftware.com/login', '41.47.67.231', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36', 'f81fe92719fab6b48a3c69d705e0068c9c1a6f1e19b2206ec8a65ca1f06c4a0a20388bc2073ad6876d13a273ad9cb54762829f9cd855c64a33ac6fb03837c6d4', '2020-03-11 14:44:02', '2020-03-11 14:44:02'),
(22, 'App\\Models\\Auth\\User', 1, 'App\\Models\\Auth\\User', 1, 4, 'updated', '{\"id\":1,\"uuid\":\"e57a1a52-00a9-43c0-9dfe-beca1e5be7e0\",\"first_name\":\"Super\",\"last_name\":\"Admin\",\"email\":\"admin@admin.com\",\"avatar_type\":\"gravatar\",\"avatar_location\":null,\"password\":\"$2y$10$Cmvk08sqKKt5KwGE5w89nOoXYyiBJ2WdMyuZPR8aOnZYGE8eyBL1W\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"6590d23fbbe79c99dd1849f8a3febe6e\",\"confirmed\":1,\"timezone\":\"Africa\\/Cairo\",\"last_login_at\":\"2020-03-11 10:44:02\",\"last_login_ip\":\"41.47.67.231\",\"to_be_logged_out\":0,\"remember_token\":\"0ipBnwivHA1WfKCSpVBjcrpLspjEC4sST3BtxsC7Ucwai2QrIOPWfF2J7nPC\",\"created_at\":\"2020-02-19 04:34:10\",\"updated_at\":\"2020-03-11 10:44:02\",\"deleted_at\":null}', '[\"remember_token\"]', '[]', '[]', 'http://amricatain.flyfoxsoftware.com/logout', '41.47.67.231', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36', 'a50a44ab411b2175ceda77361017914b5fac5dab38b2e80e8b7ae28e24e2e9950f0470391d4cddb55a46046a217798499644e9025c3f2f6b84b544983cff61ae', '2020-03-11 14:44:29', '2020-03-11 14:44:29'),
(23, 'App\\Models\\Auth\\User', 1, 'App\\Models\\Auth\\User', 1, 4, 'updated', '{\"id\":1,\"uuid\":\"e57a1a52-00a9-43c0-9dfe-beca1e5be7e0\",\"first_name\":\"Super\",\"last_name\":\"Admin\",\"email\":\"admin@admin.com\",\"avatar_type\":\"gravatar\",\"avatar_location\":null,\"password\":\"$2y$10$Cmvk08sqKKt5KwGE5w89nOoXYyiBJ2WdMyuZPR8aOnZYGE8eyBL1W\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"6590d23fbbe79c99dd1849f8a3febe6e\",\"confirmed\":1,\"timezone\":\"Africa\\/Cairo\",\"last_login_at\":\"2020-03-19 11:11:55\",\"last_login_ip\":\"41.239.114.253\",\"to_be_logged_out\":0,\"remember_token\":\"0ipBnwivHA1WfKCSpVBjcrpLspjEC4sST3BtxsC7Ucwai2QrIOPWfF2J7nPC\",\"created_at\":\"2020-02-19 04:34:10\",\"updated_at\":\"2020-03-19 11:11:55\",\"deleted_at\":null}', '[\"last_login_at\",\"last_login_ip\",\"updated_at\"]', '[]', '[]', 'http://amricatain.flyfoxsoftware.com/login', '41.239.114.253', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:74.0) Gecko/20100101 Firefox/74.0', 'ac90f40c481a1d8b366a38ae1c628be54e272ab1ce494f3584575d605b785994829a2e49baf37758008d552e2753081dd0cb1a8bf7e0e992e866ebdb8c668872', '2020-03-19 15:11:55', '2020-03-19 15:11:55'),
(24, 'App\\Models\\Auth\\User', 1, 'App\\Models\\Auth\\User', 1, 4, 'updated', '{\"id\":1,\"uuid\":\"e57a1a52-00a9-43c0-9dfe-beca1e5be7e0\",\"first_name\":\"Super\",\"last_name\":\"Admin\",\"email\":\"admin@admin.com\",\"avatar_type\":\"gravatar\",\"avatar_location\":null,\"password\":\"$2y$10$Cmvk08sqKKt5KwGE5w89nOoXYyiBJ2WdMyuZPR8aOnZYGE8eyBL1W\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"6590d23fbbe79c99dd1849f8a3febe6e\",\"confirmed\":1,\"timezone\":\"Africa\\/Cairo\",\"last_login_at\":\"2020-03-31 09:03:01\",\"last_login_ip\":\"197.165.241.212\",\"to_be_logged_out\":0,\"remember_token\":\"0ipBnwivHA1WfKCSpVBjcrpLspjEC4sST3BtxsC7Ucwai2QrIOPWfF2J7nPC\",\"created_at\":\"2020-02-19 04:34:10\",\"updated_at\":\"2020-03-31 09:03:01\",\"deleted_at\":null}', '[\"last_login_at\",\"last_login_ip\",\"updated_at\"]', '[]', '[]', 'http://amricatain.flyfoxsoftware.com/login', '197.165.241.212', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:74.0) Gecko/20100101 Firefox/74.0', 'c181040fb2885402516eca7f89fee09615ada50bc66c5418f08b26f1ae8d205809b4ba8c079d38f0ff8d37d38a20eef6ee0df48980d27c4960a2d26642833092', '2020-03-31 13:03:01', '2020-03-31 13:03:01'),
(25, 'App\\Models\\Auth\\User', 1, 'App\\Models\\Auth\\User', 1, 4, 'updated', '{\"id\":1,\"uuid\":\"e57a1a52-00a9-43c0-9dfe-beca1e5be7e0\",\"first_name\":\"Super\",\"last_name\":\"Admin\",\"email\":\"admin@admin.com\",\"avatar_type\":\"gravatar\",\"avatar_location\":null,\"password\":\"$2y$10$Cmvk08sqKKt5KwGE5w89nOoXYyiBJ2WdMyuZPR8aOnZYGE8eyBL1W\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"6590d23fbbe79c99dd1849f8a3febe6e\",\"confirmed\":1,\"timezone\":\"Africa\\/Cairo\",\"last_login_at\":\"2020-04-15 08:06:24\",\"last_login_ip\":\"197.48.114.137\",\"to_be_logged_out\":0,\"remember_token\":\"0ipBnwivHA1WfKCSpVBjcrpLspjEC4sST3BtxsC7Ucwai2QrIOPWfF2J7nPC\",\"created_at\":\"2020-02-19 04:34:10\",\"updated_at\":\"2020-04-15 08:06:24\",\"deleted_at\":null}', '[\"last_login_at\",\"last_login_ip\",\"updated_at\"]', '[]', '[]', 'http://amricatain.flyfoxsoftware.com/login', '197.48.114.137', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', '2d46c6d37bb84afe7c40c261c5f9a3af40cc67097bb20b8d892b20b58491131c3b2301d86575733e391afaccff7dd1bdfb7d33fc76fbf81daf810adef6dd48df', '2020-04-15 12:06:24', '2020-04-15 12:06:24'),
(26, 'App\\Models\\Auth\\User', 1, 'App\\Models\\Auth\\User', 1, 4, 'updated', '{\"id\":1,\"uuid\":\"e57a1a52-00a9-43c0-9dfe-beca1e5be7e0\",\"first_name\":\"Super\",\"last_name\":\"Admin\",\"email\":\"admin@admin.com\",\"avatar_type\":\"gravatar\",\"avatar_location\":null,\"password\":\"$2y$10$Cmvk08sqKKt5KwGE5w89nOoXYyiBJ2WdMyuZPR8aOnZYGE8eyBL1W\",\"password_changed_at\":null,\"active\":1,\"confirmation_code\":\"6590d23fbbe79c99dd1849f8a3febe6e\",\"confirmed\":1,\"timezone\":\"Africa\\/Cairo\",\"last_login_at\":\"2020-04-18 12:18:26\",\"last_login_ip\":\"197.48.148.137\",\"to_be_logged_out\":0,\"remember_token\":\"0ipBnwivHA1WfKCSpVBjcrpLspjEC4sST3BtxsC7Ucwai2QrIOPWfF2J7nPC\",\"created_at\":\"2020-02-19 04:34:10\",\"updated_at\":\"2020-04-18 12:18:26\",\"deleted_at\":null}', '[\"last_login_at\",\"last_login_ip\",\"updated_at\"]', '[]', '[]', 'http://amricatain.flyfoxsoftware.com/login', '197.48.148.137', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0', 'b92a918b16363afc0785b8ac3fd97fc97a98b85877a311b510a15d34525bbac2532e2e3f84c5c443663241b15f51a5c7141adc59e25f6d845a5a16ffbf889946', '2020-04-18 16:18:27', '2020-04-18 16:18:27');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2017_09_03_144628_create_permission_tables', 1),
(4, '2017_09_11_174816_create_social_accounts_table', 1),
(5, '2017_09_26_140332_create_cache_table', 1),
(6, '2017_09_26_140528_create_sessions_table', 1),
(7, '2017_09_26_140609_create_jobs_table', 1),
(8, '2018_04_08_033256_create_password_histories_table', 1),
(9, '2018_11_21_000001_create_ledgers_table', 1),
(10, '2019_08_19_000000_create_failed_jobs_table', 1),
(11, '2020_01_29_084936_create__country_table', 1),
(12, '2020_01_29_085011_create__city_table', 1),
(13, '2020_01_29_085206_create__hotel_table', 1),
(14, '2020_01_29_085347_create__services_table', 1),
(15, '2020_01_29_085627_create_hotel_service_table', 1),
(16, '2020_01_29_085859_create_hotelbooking_table', 1),
(17, '2020_01_29_090101_create_hotelbooking_payments_table', 1),
(18, '2020_01_29_090132_create_customer_table', 1),
(19, '2020_01_29_090159_create_category_table', 1),
(20, '2020_01_29_090258_create_fixedtoure_table', 1),
(21, '2020_01_29_090402_create_fixedtoure_bookings_table', 1),
(22, '2020_01_29_090435_create_fixedtoure_payments_table', 1),
(23, '2020_01_29_090530_create_customtour_booking_table', 1),
(24, '2020_01_29_090610_create_customtour_booking_details_table', 1),
(25, '2020_01_29_090649_create_customtour_payments_table', 1),
(26, '2020_01_29_090808_create_currency_exchanges_table', 1),
(27, '2020_01_29_090941_create_front_sliders_table', 1),
(28, '2020_01_29_091010_create_front_services_table', 1),
(29, '2020_01_29_091058_create_front_contact_us_table', 1),
(30, '2020_01_29_091121_create_front_about_us_table', 1),
(31, '2020_01_29_091347_create_front_discver_table', 1),
(32, '2020_01_29_091408_create_front_navbar_table', 1),
(33, '2020_01_29_091446_create_front_city_slider_table', 1),
(34, '2020_01_29_091556_create_front_customers_opinions_table', 1),
(35, '2020_01_29_091627_create_front_news_table', 1),
(36, '2020_01_30_120631_create_images_tabel', 1),
(37, '2020_01_30_120832_create_settings_tabel', 1),
(38, '2020_02_13_103020_create_messages_table', 1),
(39, '2020_02_20_143513_alter_table_hotel_bookings_add_customer', 2),
(40, '2020_03_03_120758_add_city_id_to_fixed_tours', 3),
(41, '2020_03_03_134702_add_type_to_front_about_us', 3),
(42, '2020_03_05_084550_add_hotel_id_to_custom_tour_bookings', 4),
(43, '2020_03_08_084347_add_notes_to_custom_tour_bookings', 4),
(44, '2020_03_17_162640_add_discount_to_fixed_tours', 4),
(45, '2020_03_30_170651_add_map_lat_and_coupon_map_long_to_fixed_tours', 4);

-- --------------------------------------------------------

--
-- Table structure for table `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
(1, 'App\\Models\\Auth\\User', 1),
(2, 'App\\Models\\Auth\\User', 2),
(2, 'App\\Models\\Auth\\User', 3);

-- --------------------------------------------------------

--
-- Table structure for table `password_histories`
--

CREATE TABLE `password_histories` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `password` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_histories`
--

INSERT INTO `password_histories` (`id`, `user_id`, `password`, `created_at`, `updated_at`) VALUES
(1, 1, '$2y$10$Cmvk08sqKKt5KwGE5w89nOoXYyiBJ2WdMyuZPR8aOnZYGE8eyBL1W', '2020-02-19 09:34:10', '2020-02-19 09:34:10'),
(2, 2, '$2y$10$92U3RGUFL0RTWHpceluIvORNTNl99pjweZq7hvSptY1KtNnIGoIUa', '2020-02-19 09:34:11', '2020-02-19 09:34:11'),
(3, 3, '$2y$10$X5BENphXbwk83TxjpTziZ.COAMyv1961h3VWnApKfB95sjQdZvsUG', '2020-02-25 17:36:45', '2020-02-25 17:36:45'),
(4, 4, '$2y$10$jjQluzvHPJAkiJZH1jpAXOu8a5l.nfrku0f8Bn667W8eqlsyVicrS', '2020-03-09 13:25:25', '2020-03-09 13:25:25');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'view backend', 'web', '2020-02-19 09:34:11', '2020-02-19 09:34:11');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'administrator', 'web', '2020-02-19 09:34:11', '2020-02-19 09:34:11'),
(2, 'user', 'web', '2020-02-19 09:34:11', '2020-02-19 09:34:11');

-- --------------------------------------------------------

--
-- Table structure for table `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `icon` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `icon`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2, 'واي فاي', '2020-02-25 17:24:42', '2020-02-25 17:26:10', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `services_translations`
--

CREATE TABLE `services_translations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `service_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `locale` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `services_translations`
--

INSERT INTO `services_translations` (`id`, `service_id`, `name`, `locale`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'حج', 'ar', '2020-02-20 17:35:47', '2020-02-20 17:35:47', NULL),
(2, 1, 'kjvjkv', 'en', '2020-02-20 17:35:47', '2020-02-20 17:35:47', NULL),
(3, 1, 'jbvkhv', 'fr', '2020-02-20 17:35:47', '2020-02-20 17:35:47', NULL),
(4, 1, 'ljbjkbvkv', 'it', '2020-02-20 17:35:47', '2020-02-20 17:35:47', NULL),
(5, 1, 'jbjkb;kn', 'es', '2020-02-20 17:35:47', '2020-02-20 17:35:47', NULL),
(6, 2, 'واي فاي', 'ar', '2020-02-25 17:24:42', '2020-02-25 17:26:10', NULL),
(7, 2, 'wifi', 'en', '2020-02-25 17:24:42', '2020-02-25 17:26:10', NULL),
(8, 2, 'wifi', 'fr', '2020-02-25 17:24:42', '2020-02-25 17:26:10', NULL),
(9, 2, 'wifi', 'it', '2020-02-25 17:24:42', '2020-02-25 17:26:10', NULL),
(10, 2, 'wifi', 'es', '2020-02-25 17:24:42', '2020-02-25 17:26:10', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `id` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci,
  `payload` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `key` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `settings_translations`
--

CREATE TABLE `settings_translations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `setting_id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contents` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `locale` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `social_accounts`
--

CREATE TABLE `social_accounts` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `provider` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `provider_id` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` text COLLATE utf8mb4_unicode_ci,
  `avatar` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_name` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar_type` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'gravatar',
  `avatar_location` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password_changed_at` timestamp NULL DEFAULT NULL,
  `active` tinyint(3) UNSIGNED NOT NULL DEFAULT '1',
  `confirmation_code` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `confirmed` tinyint(1) NOT NULL DEFAULT '0',
  `timezone` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_login_at` timestamp NULL DEFAULT NULL,
  `last_login_ip` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `to_be_logged_out` tinyint(1) NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `uuid`, `first_name`, `last_name`, `email`, `avatar_type`, `avatar_location`, `password`, `password_changed_at`, `active`, `confirmation_code`, `confirmed`, `timezone`, `last_login_at`, `last_login_ip`, `to_be_logged_out`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'e57a1a52-00a9-43c0-9dfe-beca1e5be7e0', 'Super', 'Admin', 'admin@admin.com', 'gravatar', NULL, '$2y$10$Cmvk08sqKKt5KwGE5w89nOoXYyiBJ2WdMyuZPR8aOnZYGE8eyBL1W', NULL, 1, '6590d23fbbe79c99dd1849f8a3febe6e', 1, 'Africa/Cairo', '2020-04-18 16:18:26', '197.48.148.137', 0, '0ipBnwivHA1WfKCSpVBjcrpLspjEC4sST3BtxsC7Ucwai2QrIOPWfF2J7nPC', '2020-02-19 09:34:10', '2020-04-18 16:18:26', NULL),
(2, '24199d5c-da7c-4fc9-9341-ef3f0f9a92b4', 'Default', 'User', 'user@user.com', 'gravatar', NULL, '$2y$10$92U3RGUFL0RTWHpceluIvORNTNl99pjweZq7hvSptY1KtNnIGoIUa', NULL, 1, '96293bae731bd34a9880cc51b49fdfd2', 1, NULL, NULL, NULL, 0, NULL, '2020-02-19 09:34:11', '2020-02-19 09:34:11', NULL),
(3, '9a5e87df-f523-4d82-afbf-bdf513bac257', 'hassan', NULL, 'hassan@mail.com', 'gravatar', NULL, '$2y$10$X5BENphXbwk83TxjpTziZ.COAMyv1961h3VWnApKfB95sjQdZvsUG', NULL, 1, NULL, 1, NULL, NULL, NULL, 0, NULL, '2020-02-25 17:36:44', '2020-04-15 14:26:14', '2020-04-15 14:26:14'),
(4, 'fc25997f-1a0c-40ca-a492-9664c96dcb92', 'hassan mohamed', NULL, 'etshhassan9@gmail.com', 'gravatar', NULL, '$2y$10$jjQluzvHPJAkiJZH1jpAXOu8a5l.nfrku0f8Bn667W8eqlsyVicrS', NULL, 1, NULL, 1, NULL, NULL, NULL, 0, NULL, '2020-03-09 13:25:25', '2020-03-09 13:25:25', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cache`
--
ALTER TABLE `cache`
  ADD UNIQUE KEY `cache_key_unique` (`key`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories_translations`
--
ALTER TABLE `categories_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categories_translations_category_id_locale_unique` (`category_id`,`locale`),
  ADD KEY `categories_translations_locale_index` (`locale`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cities_country_id_foreign` (`country_id`);

--
-- Indexes for table `cities_translations`
--
ALTER TABLE `cities_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `cities_translations_city_id_locale_unique` (`city_id`,`locale`),
  ADD KEY `cities_translations_locale_index` (`locale`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries_translations`
--
ALTER TABLE `countries_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `countries_translations_country_id_locale_unique` (`country_id`,`locale`),
  ADD KEY `countries_translations_locale_index` (`locale`);

--
-- Indexes for table `currency_exchanges`
--
ALTER TABLE `currency_exchanges`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `custom_tour_bookings`
--
ALTER TABLE `custom_tour_bookings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `custom_tour_bookings_customer_id_foreign` (`customer_id`),
  ADD KEY `custom_tour_bookings_category_id_foreign` (`category_id`);

--
-- Indexes for table `custom_tour_booking_details`
--
ALTER TABLE `custom_tour_booking_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `custom_tour_booking_details_custom_tour_booking_id_foreign` (`custom_tour_booking_id`),
  ADD KEY `custom_tour_booking_details_hotel_id_foreign` (`hotel_id`);

--
-- Indexes for table `custom_tour_payments`
--
ALTER TABLE `custom_tour_payments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `custom_tour_payments_custom_tour_booking_id_foreign` (`custom_tour_booking_id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fixed_tours`
--
ALTER TABLE `fixed_tours`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fixed_tours_hotel_id_foreign` (`hotel_id`),
  ADD KEY `fixed_tours_category_id_foreign` (`category_id`);

--
-- Indexes for table `fixed_tours_translations`
--
ALTER TABLE `fixed_tours_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `fixed_tours_translations_fixed_tour_id_locale_unique` (`fixed_tour_id`,`locale`),
  ADD KEY `fixed_tours_translations_locale_index` (`locale`);

--
-- Indexes for table `fixed_tour_bookings`
--
ALTER TABLE `fixed_tour_bookings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fixed_tour_bookings_customer_id_foreign` (`customer_id`),
  ADD KEY `fixed_tour_bookings_fixed_tour_id_foreign` (`fixed_tour_id`);

--
-- Indexes for table `fixed_tour_payments`
--
ALTER TABLE `fixed_tour_payments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fixed_tour_payments_fixed_tour_booking_id_foreign` (`fixed_tour_booking_id`);

--
-- Indexes for table `front_about_us`
--
ALTER TABLE `front_about_us`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `front_about_us_translations`
--
ALTER TABLE `front_about_us_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `front_about_us_translations_aboutus_id_locale_unique` (`aboutus_id`,`locale`),
  ADD KEY `front_about_us_translations_locale_index` (`locale`);

--
-- Indexes for table `front_city_slider`
--
ALTER TABLE `front_city_slider`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `front_city_translations`
--
ALTER TABLE `front_city_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `front_city_translations_city_slider_id_locale_unique` (`city_slider_id`,`locale`),
  ADD KEY `front_city_translations_locale_index` (`locale`);

--
-- Indexes for table `front_contact_us`
--
ALTER TABLE `front_contact_us`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `front_contact_us_translations`
--
ALTER TABLE `front_contact_us_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `front_contact_us_translations_front_contact_us_id_locale_unique` (`front_contact_us_id`,`locale`),
  ADD KEY `front_contact_us_translations_locale_index` (`locale`);

--
-- Indexes for table `front_customers_opinions`
--
ALTER TABLE `front_customers_opinions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `front_discover`
--
ALTER TABLE `front_discover`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `front_discover_translations`
--
ALTER TABLE `front_discover_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `front_discover_translations_discover_id_locale_unique` (`discover_id`,`locale`),
  ADD KEY `front_discover_translations_locale_index` (`locale`);

--
-- Indexes for table `front_navbar`
--
ALTER TABLE `front_navbar`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `front_navbar_translations`
--
ALTER TABLE `front_navbar_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `front_navbar_translations_front_navbar_id_locale_unique` (`front_navbar_id`,`locale`),
  ADD KEY `front_navbar_translations_locale_index` (`locale`);

--
-- Indexes for table `front_news`
--
ALTER TABLE `front_news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `front_news_translations`
--
ALTER TABLE `front_news_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `front_news_translations_front_news_id_locale_unique` (`front_news_id`,`locale`),
  ADD KEY `front_news_translations_locale_index` (`locale`);

--
-- Indexes for table `front_services`
--
ALTER TABLE `front_services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `front_services_translations`
--
ALTER TABLE `front_services_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `front_services_translations_front_service_id_locale_unique` (`front_service_id`,`locale`),
  ADD KEY `front_services_translations_locale_index` (`locale`);

--
-- Indexes for table `front_sliders`
--
ALTER TABLE `front_sliders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `front_sliders_translations`
--
ALTER TABLE `front_sliders_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `front_sliders_translations_front_slider_id_locale_unique` (`front_slider_id`,`locale`),
  ADD KEY `front_sliders_translations_locale_index` (`locale`);

--
-- Indexes for table `hotels`
--
ALTER TABLE `hotels`
  ADD PRIMARY KEY (`id`),
  ADD KEY `hotels_city_id_foreign` (`city_id`);

--
-- Indexes for table `hotel_bookings`
--
ALTER TABLE `hotel_bookings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `hotel_bookings_hotel_id_foreign` (`hotel_id`),
  ADD KEY `hotel_bookings_customer_id_foreign` (`customer_id`);

--
-- Indexes for table `hotel_booking_payments`
--
ALTER TABLE `hotel_booking_payments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `hotel_booking_payments_hotel_booking_id_foreign` (`hotel_booking_id`);

--
-- Indexes for table `hotel_service`
--
ALTER TABLE `hotel_service`
  ADD PRIMARY KEY (`id`),
  ADD KEY `hotel_service_hotel_id_foreign` (`hotel_id`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jobs_queue_index` (`queue`);

--
-- Indexes for table `ledgers`
--
ALTER TABLE `ledgers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ledgers_recordable_type_recordable_id_index` (`recordable_type`,`recordable_id`),
  ADD KEY `ledgers_user_id_user_type_index` (`user_id`,`user_type`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_type_model_id_index` (`model_type`,`model_id`);

--
-- Indexes for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_type_model_id_index` (`model_type`,`model_id`);

--
-- Indexes for table `password_histories`
--
ALTER TABLE `password_histories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `password_histories_user_id_foreign` (`user_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `roles_name_index` (`name`);

--
-- Indexes for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `services_translations`
--
ALTER TABLE `services_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `services_translations_service_id_locale_unique` (`service_id`,`locale`),
  ADD KEY `services_translations_locale_index` (`locale`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD UNIQUE KEY `sessions_id_unique` (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings_translations`
--
ALTER TABLE `settings_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `settings_translations_setting_id_locale_unique` (`setting_id`,`locale`),
  ADD KEY `settings_translations_locale_index` (`locale`);

--
-- Indexes for table `social_accounts`
--
ALTER TABLE `social_accounts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `social_accounts_user_id_foreign` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `categories_translations`
--
ALTER TABLE `categories_translations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `cities_translations`
--
ALTER TABLE `cities_translations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `countries_translations`
--
ALTER TABLE `countries_translations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `currency_exchanges`
--
ALTER TABLE `currency_exchanges`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `custom_tour_bookings`
--
ALTER TABLE `custom_tour_bookings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `custom_tour_booking_details`
--
ALTER TABLE `custom_tour_booking_details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `custom_tour_payments`
--
ALTER TABLE `custom_tour_payments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fixed_tours`
--
ALTER TABLE `fixed_tours`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `fixed_tours_translations`
--
ALTER TABLE `fixed_tours_translations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `fixed_tour_bookings`
--
ALTER TABLE `fixed_tour_bookings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `fixed_tour_payments`
--
ALTER TABLE `fixed_tour_payments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `front_about_us`
--
ALTER TABLE `front_about_us`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `front_about_us_translations`
--
ALTER TABLE `front_about_us_translations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `front_city_slider`
--
ALTER TABLE `front_city_slider`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `front_city_translations`
--
ALTER TABLE `front_city_translations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `front_contact_us`
--
ALTER TABLE `front_contact_us`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `front_contact_us_translations`
--
ALTER TABLE `front_contact_us_translations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `front_customers_opinions`
--
ALTER TABLE `front_customers_opinions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `front_discover`
--
ALTER TABLE `front_discover`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `front_discover_translations`
--
ALTER TABLE `front_discover_translations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `front_navbar`
--
ALTER TABLE `front_navbar`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `front_navbar_translations`
--
ALTER TABLE `front_navbar_translations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `front_news`
--
ALTER TABLE `front_news`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `front_news_translations`
--
ALTER TABLE `front_news_translations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `front_services`
--
ALTER TABLE `front_services`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `front_services_translations`
--
ALTER TABLE `front_services_translations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `front_sliders`
--
ALTER TABLE `front_sliders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `front_sliders_translations`
--
ALTER TABLE `front_sliders_translations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `hotels`
--
ALTER TABLE `hotels`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `hotel_bookings`
--
ALTER TABLE `hotel_bookings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `hotel_booking_payments`
--
ALTER TABLE `hotel_booking_payments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `hotel_service`
--
ALTER TABLE `hotel_service`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=78;

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ledgers`
--
ALTER TABLE `ledgers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `password_histories`
--
ALTER TABLE `password_histories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `services_translations`
--
ALTER TABLE `services_translations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `settings_translations`
--
ALTER TABLE `settings_translations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `social_accounts`
--
ALTER TABLE `social_accounts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
