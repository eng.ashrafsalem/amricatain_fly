<?php

namespace App\Models\ContactUs;

use App\Traits\HasRoute;
use Illuminate\Database\Eloquent\Model;

class ContactUs extends Model
{
    use HasRoute;

    protected $table='contact_us_form';
    protected $fillable = ['name','email','phone','message'];

    protected $routeName = 'admin.ContactUss';
}
