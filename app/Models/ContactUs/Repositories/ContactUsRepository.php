<?php

namespace App\Models\ContactUs\Repositories;

use App\Models\ContactUs\ContactUs;
use App\Repositories\BaseRepository;


class ContactUsRepository extends BaseRepository
{

	public function __construct(ContactUs $contactUs)
    {
        $this->model = $contactUs;
    }

}

