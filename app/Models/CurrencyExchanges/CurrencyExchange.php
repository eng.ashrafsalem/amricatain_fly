<?php

namespace App\Models\CurrencyExchanges;

use Illuminate\Database\Eloquent\Model;
use App\Traits\HasRoute;

class CurrencyExchange extends Model
{
    use HasRoute;

    protected $table='currency_exchanges';

    protected $fillable = ['name', 'exchange_rate'];

    protected $routeName = 'admin.currencyexchanges';
}


