<?php

namespace App\Models\CurrencyExchanges\Repositories;

use App\Models\CurrencyExchanges\CurrencyExchange;
use App\Repositories\BaseRepository;

class CurrencyExchangeRepository extends BaseRepository
{

    //    protected $with = [''];

	public function __construct(CurrencyExchange $currencyexchange)
    {
        $this->model = $currencyexchange;
    }

    public function createData()
    {
        return [
//            'example' => app(ExampleRepository::class)->all(),
        ];
    }

    public function editData($id)
    {
        $data = parent::editData($id);

//        $data['example'] = app(ExampleRepository::class)->all();

        return $data;
    }

}

