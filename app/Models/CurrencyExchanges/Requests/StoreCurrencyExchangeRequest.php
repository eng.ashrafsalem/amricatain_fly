<?php

namespace App\Models\CurrencyExchanges\Requests;
use Illuminate\Foundation\Http\FormRequest;

class StoreCurrencyExchangeRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {

        $validation['name'] = 'required|max:190';
        $validation['exchange_rate'] = 'required|max:190';

        return $validation;

    }

    public function attributes()
    {
        return [
            'name' => __('labels.backend.currencyexchanges.ar_name'),
            'exchange_rate' => __('labels.backend.currencyexchanges.exchange_rate'),
        ];
    }
}
