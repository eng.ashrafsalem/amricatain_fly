<?php

namespace App\Models\FixedTourBookings\Requests;
use Illuminate\Foundation\Http\FormRequest;

class StoreFixedTourBookingRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {

        $validation['customer_id'] = 'required';
        $validation['fixed_tour_id'] = 'required';
        $validation['from'] = 'required|date';
        $validation['to'] = 'required|date|after_or_equal:from';
        $validation['total_cost'] = 'required';
        $validation['adult_count'] = 'required';
        $validation['child_count'] = 'required';
        $validation['baby_count'] = 'required|image';

        return $validation;

    }

    public function attributes()
    {
        return [
            'customer_id' => __('labels.backend.fixedtourbookings.customer'),
            'fixed_tour_id' => __('labels.backend.fixedtourbookings.fixed_tour'),
            'from' => __('labels.backend.fixedtourbookings.from'),
            'to' => __('labels.backend.fixedtourbookings.to'),
            'total_cost' => __('labels.backend.fixedtourbookings.total_cost'),
            'adult_count' => __('labels.backend.fixedtourbookings.adult_count'),
            'child_count' => __('labels.backend.fixedtourbookings.child_count'),
            'baby_count' => __('labels.backend.fixedtourbookings.baby_count'),
        ];
    }
}
