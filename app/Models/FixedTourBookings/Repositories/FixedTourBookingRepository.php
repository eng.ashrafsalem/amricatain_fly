<?php

namespace App\Models\FixedTourBookings\Repositories;

use App\Models\Customers\Customer;
use App\Models\FixedTourBookings\FixedTourBooking;
use App\Models\FixedTours\FixedTour;
use App\Repositories\BaseRepository;

class FixedTourBookingRepository extends BaseRepository
{

        protected $with = ['fixedTour', 'customer'];

	public function __construct(FixedTourBooking $fixedtourbooking)
    {
        $this->model = $fixedtourbooking;
    }

    public function createData()
    {
        return [
//            'example' => app(ExampleRepository::class)->all(),
        ];
    }

    public function editData($id)
    {
        $data = parent::editData($id);

//        $data['example'] = app(ExampleRepository::class)->all();

        return $data;
    }
    public function updateById($id, array $data, array $options = [])
    {

        $FixedTourBooking = FixedTourBooking::find($id);
        $data['customer_id'] = Customer::where('id', $FixedTourBooking->customer_id)->first()->id;
        $data['fixed_tour_id'] = FixedTour::where('id', $FixedTourBooking->fixed_tour_id)->first()->id;

        parent::updateById($id, $data);
    }
}

