<?php

namespace App\Models\FixedTourBookings;

use App\Models\Customers\Customer;
use App\Models\FixedTours\FixedTour;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use App\Traits\HasRoute;

class FixedTourBooking extends Model
{
    use HasRoute;

    protected $guarded = [''];

    protected $table = 'fixed_tour_bookings';

    protected $routeName = 'admin.fixedtourbookings';

    public function fixedTour()
    {
        return $this->belongsTo(FixedTour::class);
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function getToAttribute($value){
        return Carbon::parse($value)->format("Y-m-d");
    }

    public function getFromAttribute($value){
        return Carbon::parse($value)->format("Y-m-d");
    }
    public function setToAttribute($value){
        $this->attributes['to']=Carbon::parse($value)->format("Y-m-d");
    }

    public function setFromAttribute($value){
        $this->attributes['from']=Carbon::parse($value)->format("Y-m-d");
    }
}


