<?php

namespace App\Models\FixedTours\Repositories;

use App\Models\Categories\Repositories\CategoryRepository;
use App\Models\Countries\Repositories\CountryRepository;
use App\Models\FixedTours\FixedTour;
use App\Models\Hotels\Repositories\HotelRepository;
use App\Models\Image;
use App\Repositories\BaseRepository;
use App\Models\Cities\Repositories\CityRepository;
class FixedTourRepository extends BaseRepository
{

    protected $with = ['category', 'hotel'];

	public function __construct(FixedTour $fixedtour)
    {
        $this->model = $fixedtour;
    }

    public function createData()
    {
        $data = [];
        $data['categories'] = app(CategoryRepository::class)->all();
        $data['hotels'] =  app(HotelRepository::class)->all();
        $data['cities'] = app(CityRepository::class)->all();

        return $data;
    }

    public function create(array $data)
    {
        // table fixed_tours doesn't have m_image or s_image columns so exclude them
        $test = $data;
        unset($test["m_image"]);
        unset($test["s_image"]);

        // save
        $fixed_tour = parent::create($test);

        // store main image
        if(request()->hasFile('m_image')) {
            // save the main image
            saveImage($fixed_tour->id, FixedTour::class, $data['m_image']->store('public/uploads'), 1);
        } else  {
            // save the avatar
            saveImage($fixed_tour->id, FixedTour::class, 'public/uploads/tour.png', 1);
        }

        // store slider image
        if(request()->hasFile('s_image')) {
            foreach ($data['s_image'] as $img) {
                // save the slider images
                saveImage($fixed_tour->id, FixedTour::class, $img->store('public/uploads'), 0);
            }
        }
    }

    public function editData($id)
    {

        $data = parent::editData($id);
        $data['categories'] = app(CategoryRepository::class)->all();
        $data['hotels'] =  app(HotelRepository::class)->all();
        $data['cities'] = app(CityRepository::class)->all();

        return $data;
    }

    public function updateById($id, array $data, array $options = [])
    {
        $fixed_tour = FixedTour::find($id);

        // table fixed_tours doesn't have m_image or s_image columns so exclude them
        $test = $data;
        unset($test["m_image"]);
        unset($test["s_image"]);

        parent::updateById($id, $test);

        // store main image
        if(request()->hasFile('m_image')) {
            // delete the old one
            deleteMainImageOnly($id, FixedTour::class);
            // save the new one
            saveImage($id, FixedTour::class, $data['m_image']->store('public/uploads'), 1);
        }

        // store slider image
        if(request()->hasFile('s_image')) {
            // first delete the old ones
            deleteSliderImagesOnly($id, FixedTour::class);

            // second insert new ones
            foreach ($data['s_image'] as $img) {
                // get the image
                saveImage($id, FixedTour::class, $img->store('public/uploads'), 0);
            }
        }
    }

    public function deleteById($id)
    {
        // delete all related images first
        deleteAllFromBothTableAndStorage($id, FixedTour::class);

        // then delete the rest
        parent::unsetClauses();
        return parent::deleteById($id);
    }
}

