<?php

namespace App\Models\FixedTours;

use Illuminate\Database\Eloquent\Model;

class FixedTourTranslation extends Model
{

    protected $fillable = ['name', 'description'];

    protected $table = 'fixed_tours_translations';

}
