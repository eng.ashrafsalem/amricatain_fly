<?php

namespace App\Models\FixedTours\Requests;
use Illuminate\Foundation\Http\FormRequest;

class UpdateFixedTourRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }


    public function rules()
    {

        $validation['ar.name'] = 'required|max:190';
        $validation['en.name'] = 'required|max:190';
        $validation['fr.name'] = 'required|max:190';
        $validation['it.name'] = 'required|max:190';
        $validation['es.name'] = 'required|max:190';
        $validation['ar.description'] = 'required';
        $validation['en.description'] = 'required';
        $validation['fr.description'] = 'required';
        $validation['it.description'] = 'required';
        $validation['es.description'] = 'required';
        $validation['category_id'] = 'required';
        $validation['hotel_id'] = 'required';
        $validation['from'] = 'required|date';
        $validation['to'] = 'required|date|after_or_equal:from';
        $validation['total_cost'] = 'required';
        $validation['active'] = 'required';
        $validation['days_nights_counts'] = 'required';

        return $validation;

    }

    public function attributes()
    {
        return [
            'name' => __('labels.backend.fixedtours.name'),
            'description' => __('labels.backend.fixedtours.description'),
            'category_id' => __('labels.backend.fixedtours.category'),
            'hotel_id' => __('labels.backend.fixedtours.hotel'),
            'from' => __('labels.backend.fixedtours.from'),
            'to' => __('labels.backend.fixedtours.to'),
            'total_cost' => __('labels.backend.fixedtours.totalCost'),
            'active' => __('labels.backend.fixedtours.active'),
            'days_nights_counts' => __('labels.backend.fixedtours.days_nights_counts'),
            'm_image' => __('labels.backend.fixedtours.main_image'),
            's_image' => __('labels.backend.fixedtours.slider_image'),
        ];
    }
}
