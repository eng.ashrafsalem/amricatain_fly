<?php

namespace App\Models\FixedTours;

use App\Models\Categories\Category;
use App\Models\FixedTourBookings\FixedTourBooking;
use App\Models\Hotels\Hotel;
use App\Models\Cities\City;
use App\Models\Image;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use App\Traits\HasRoute;
use Astrotomic\Translatable\Translatable;

class FixedTour extends Model
{
    use HasRoute, Translatable;

    protected $translatedAttributes = ['name', 'description'];

    protected $with = ['translations'];

    protected $guarded = [''];

    protected $routeName = 'admin.fixedtours';

    // reverse one-to-many relationship with hotel table
    public function hotel()
    {
        return $this->belongsTo(Hotel::class)->withTrashed();
    }

    // reverse one-to-many relationship with Category table
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function fixedTourBookings()
    {
        return $this->hasMany(FixedTourBooking::class);
    }

    public function getToAttribute($value)
    {
        return Carbon::parse($value)->format("Y-m-d");
    }

    public function getFromAttribute($value)
    {
        return Carbon::parse($value)->format("Y-m-d");
    }
}


