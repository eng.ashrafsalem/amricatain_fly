<?php

namespace App\Models\Discovers;

use Illuminate\Database\Eloquent\Model;

class DiscoverTranslation extends Model
{

    protected $fillable = ['title', 'description'];

    protected $table = 'front_discover_translations';

}
