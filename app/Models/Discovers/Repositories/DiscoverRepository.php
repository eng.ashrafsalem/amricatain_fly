<?php

namespace App\Models\Discovers\Repositories;

use App\Models\Discovers\Discover;
use App\Repositories\BaseRepository;

class DiscoverRepository extends BaseRepository
{

    //    protected $with = [''];

	public function __construct(Discover $discover)
    {
        $this->model = $discover;
    }

    public function createData()
    {
        return [
//            'example' => app(ExampleRepository::class)->all(),
        ];
    }

    public function editData($id)
    {
        $data = parent::editData($id);

//        $data['example'] = app(ExampleRepository::class)->all();

        return $data;
    }

}

