<?php

namespace App\Models\Discovers;

use Illuminate\Database\Eloquent\Model;
use App\Traits\HasRoute;
use Astrotomic\Translatable\Translatable;

class Discover extends Model
{
    use HasRoute, Translatable;

    protected $guarded = ['discover_id'];

    protected $table = 'front_discover';

    protected $routeName = 'admin.discovers';

    protected $translatedAttributes = ['title', 'description'];

    protected $with = ['translations'];
}


