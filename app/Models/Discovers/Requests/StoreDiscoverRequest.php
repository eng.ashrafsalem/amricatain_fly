<?php

namespace App\Models\Discovers\Requests;
use Illuminate\Foundation\Http\FormRequest;

class StoreDiscoverRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $validation['ar.title'] = 'required|max:190';
        $validation['en.title'] = 'required|max:190';
        $validation['fr.title'] = 'required|max:190';
        $validation['it.title'] = 'required|max:190';
        $validation['es.title'] = 'required|max:190';

        $validation['ar.description'] = 'required';
        $validation['en.description'] = 'required';
        $validation['fr.description'] = 'required';
        $validation['it.description'] = 'required';
        $validation['es.description'] = 'required';

        return $validation;

    }

    public function attributes()
    {
        return [
            'ar.title' => __('labels.backend.discover.ar_title'),
            'en.title' => __('labels.backend.discover.en_title'),
            'fr.title' => __('labels.backend.discover.fr_title'),
            'it.title' => __('labels.backend.discover.it_title'),
            'es.title' => __('labels.backend.discover.es_title'),

            'ar.description' => __('labels.backend.discover.ar_description'),
            'en.description' => __('labels.backend.discover.en_description'),
            'fr.description' => __('labels.backend.discover.fr_description'),
            'it.description' => __('labels.backend.discover.it_description'),
            'es.description' => __('labels.backend.discover.es_description'),
        ];
    }
}
