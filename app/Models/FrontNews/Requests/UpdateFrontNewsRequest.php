<?php

namespace App\Models\FrontNews\Requests;
use Illuminate\Foundation\Http\FormRequest;

class UpdateFrontNewsRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $validation['news_date'] = 'required|date';
        $validation['ar.title'] = 'required|max:190';
        $validation['en.title'] = 'required|max:190';
        $validation['fr.title'] = 'required|max:190';
        $validation['it.title'] = 'required|max:190';
        $validation['es.title'] = 'required|max:190';

        $validation['ar.description'] = 'required';
        $validation['en.description'] = 'required';
        $validation['fr.description'] = 'required';
        $validation['it.description'] = 'required';
        $validation['es.description'] = 'required';

        return $validation;

    }

    public function attributes()
    {
        return [
            'news_date' => __('labels.backend.frontnews.news_date'),
            'ar.title' => __('labels.backend.frontnews.ar_title'),
            'en.title' => __('labels.backend.frontnews.en_title'),
            'fr.title' => __('labels.backend.frontnews.fr_title'),
            'it.title' => __('labels.backend.frontnews.it_title'),
            'es.title' => __('labels.backend.frontnews.es_title'),

            'ar.description' => __('labels.backend.frontnews.ar_description'),
            'en.description' => __('labels.backend.frontnews.en_description'),
            'fr.description' => __('labels.backend.frontnews.fr_description'),
            'it.description' => __('labels.backend.frontnews.it_description'),
            'es.description' => __('labels.backend.frontnews.es_description'),
        ];
    }
}
