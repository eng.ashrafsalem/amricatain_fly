<?php

namespace App\Models\FrontNews\Repositories;

use App\Models\FrontNews\FrontNews;
use App\Repositories\BaseRepository;

class FrontNewsRepository extends BaseRepository
{

    //    protected $with = [''];

	public function __construct(FrontNews $frontnews)
    {
        $this->model = $frontnews;
    }

    public function createData()
    {
        return [
//            'example' => app(ExampleRepository::class)->all(),
        ];
    }

    public function create(array $data)
    {
        $test = $data;
        unset($test["image_url"]);
        // save
        $news = parent::create($test);
        // store image
        if(request()->hasFile('image_url')) {
            // save the main image
            saveImage($news->id, FrontNews::class, $data['image_url']->store('public/uploads'), 1);
        }

//        return ;
    }

    public function editData($id)
    {
        $data = parent::editData($id);

//        $data['example'] = app(ExampleRepository::class)->all();

        return $data;
    }

    public function updateById($id, array $data, array $options = [])
    {
        $test = $data;
        unset($test["image_url"]);

        // store main image if any
        if(request()->hasFile('image_url')) {
            // delete the old one
            deleteMainImageOnly($id, FrontNews::class);
            // save the new one
            saveImage($id, FrontNews::class, $data['image_url']->store('public/uploads'), 1);
        }

        parent::updateById($id, $test);
    }

    public function deleteById($id)
    {
        // delete the old one
        deleteMainImageOnly($id, FrontNews::class);

        parent::unsetClauses();

        return parent::deleteById($id);
    }
}

