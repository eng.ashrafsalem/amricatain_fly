<?php

namespace App\Models\FrontNews;

use Illuminate\Database\Eloquent\Model;

class FrontNewsTranslation extends Model
{

    protected $fillable = ['title', 'description'];

    protected $table = 'front_news_translations';

}
