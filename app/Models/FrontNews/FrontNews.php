<?php

namespace App\Models\FrontNews;

use Illuminate\Database\Eloquent\Model;
use App\Traits\HasRoute;
use Astrotomic\Translatable\Translatable;

class FrontNews extends Model
{
    use HasRoute, Translatable;

    protected $guarded = ['front_news_id'];

    protected $routeName = 'admin.frontnews';

    protected $translatedAttributes = ['title'];

    protected $with = ['translations'];
}


