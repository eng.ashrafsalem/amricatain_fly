<?php

namespace App\Models\Users\Requests;
use Illuminate\Foundation\Http\FormRequest;

class UpdateUserRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {

        $validation['first_name'] = 'required|max:190';
        $validation['last_name'] = 'required|max:190';
        $validation['gender'] = 'required|in:male,female';
        $validation['email'] = 'required|email|unique:users,email,' . request()->route('user');
        $validation['password'] = 'sometimes|nullable|min:6|max:30';
        $validation['phone'] = 'required';
        $validation['image_url'] = 'nullable|image';
        $validation['age'] = 'required|max:3';

        return $validation;

    }

    public function attributes()
    {
        return [
            'first_name' => __('labels.backend.customers.first_name'),
            'last_name' => __('labels.backend.customers.last_name'),
            'gender' => __('labels.backend.customers.gender'),
            'email' => __('labels.backend.customers.email'),
            'password' => __('labels.backend.customers.password'),
            'phone' => __('labels.backend.customers.phone'),
            'image_url' => __('labels.backend.customers.image_url'),
            'age' => __('labels.backend.customers.age'),
        ];
    }


}
