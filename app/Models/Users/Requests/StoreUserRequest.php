<?php

namespace App\Models\Users\Requests;
use Illuminate\Foundation\Http\FormRequest;

class StoreUserRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {

        $validation['first_name'] = 'required|max:190';
        $validation['last_name'] = 'required|max:190';
        $validation['address'] = 'required|max:190';
        $validation['gender'] = 'nullable|in:male,female';
        $validation['email'] = 'nullable|email|unique:users';
        $validation['password'] = 'required|min:6|max:30';
        $validation['phone'] = 'required|unique:customers';
        $validation['image_url'] = 'nullable|image';
        $validation['age'] = 'required|max:3';

        return $validation;

    }

    public function attributes()
    {
        return [
            'first_name' => __('labels.backend.customers.first_name'),
            'last_name' => __('labels.backend.customers.last_name'),
            'address' => __('labels.backend.customers.address'),
            'gender' => __('labels.backend.customers.gender'),
            'email' => __('labels.backend.customers.email'),
            'password' => __('labels.backend.customers.password'),
            'phone' => __('labels.backend.customers.phone'),
            'image_url' => __('labels.backend.customers.image_url'),
            'age' => __('labels.backend.customers.age'),
        ];
    }
}
