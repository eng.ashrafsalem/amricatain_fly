<?php

namespace App\Models\Users\Repositories;


use App\Models\Users\User;
use App\Repositories\BaseRepository;
use Hash;

class UserRepository extends BaseRepository
{

        protected $with = ['image'];

	public function __construct(User $user)
    {
        $this->model = $user;
    }

    public function createData()
    {
        return [
//            'example' => app(ExampleRepository::class)->all(),
        ];
    }

    public function create(array $data)
    {
        // save
//        dd($data);
        $data['confirmed']=1;
        $user = parent::create($data);

        // store image
        if(request()->hasFile('image_url')) {
            $image = new \App\Models\Image(['image_url' => $data['image_url'], 'main_image' => 1]);
            $user->image()->save($image);
        } else {
            $image = new \App\Models\Image(['image_url' => 'uploads/avatar.png', 'main_image' => 1]);
            $user->image()->save($image);
        }

        $user->assignRole(config('access.users.admin_role'));

    }

    public function editData($id)
    {
        return [
            'data' => $this->getById($id->id),
        ];
////        dd($id);
//        $data = parent::editData($id);
////        $data['example'] = app(ExampleRepository::class)->all();
//
//        return $data;
    }

    public function updateById($id, array $data, array $options = [])
    {

//        dd($data[]);
        $user = User::find($id->id);

        if(!$data['password']) {
//            dd($user->password);
            $data['password'] = $user->password;
        }
        // store image if any
        if(request()->hasFile('image_url')) {
            // here if the image is unique , first delete the record from
            // image table
            // if not unique , no need to delete the old one
            $user->image()->delete();

            $image = new \App\Models\Image(['image_url' => $data['image_url'], 'main_image' => 1]);

            $user->image()->save($image);
        }

            $user->assignRole(config('access.users.admin_role'));




        $this->unsetClauses();
        $model = $this->getById($id->id);
        $model->update($data, $options);

    }

    public function deleteById($id)
    {
//        dd($id);
        $user = parent::getById($id->id);
//          $user=User::find($id->id);
//        dd($user);
//        $user = User::where('email', $customer->email)->first();
//         dd($user);
        if(isset($user->image->image_url))
        {
            // keep the avatar image
            if($user->image->image_url != '/storage/uploads/avatar.png') {
                $path = public_path($user->image->image_url);
                unlink($path);
            }
            $user->image()->delete();
        }

        parent::unsetClauses();
        // delete the user from user table first
        return $user->delete();
    }
}

