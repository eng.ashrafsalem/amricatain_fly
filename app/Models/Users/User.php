<?php

namespace App\Models\Users;


use App\Models\Traits\Uuid;
use App\Traits\HasRoute;
use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Traits\HasRoles;

class User extends Model
{
    use HasRoute,Uuid,HasRoles;
    protected $guard_name = 'web';
    protected $fillable = ['first_name','last_name', 'email', 'phone', 'address', 'gender',
        'password', 'age'];

    protected $routeName = 'admin.users';
    // one-to-one morphic-relation with images table
    public function image()
    {

        return $this->morphOne('App\Models\Image', 'imageable');
    }

    // bcrypt the password
    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = \Hash::make($value);
    }

}
