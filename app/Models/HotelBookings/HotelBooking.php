<?php

namespace App\Models\HotelBookings;

use App\Models\Customers\Customer;
use App\Models\Hotels\Hotel;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use App\Traits\HasRoute;

class HotelBooking extends Model
{
    use HasRoute;

    protected $guarded= [''];

    protected $table = 'hotel_bookings';

    protected $routeName = 'admin.hotelbookings';

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function hotel()
    {
        return $this->belongsTo(Hotel::class)->withTrashed();
    }

    public function getToAttribute($value){
        return Carbon::parse($value)->format("Y-m-d");
    }

    public function getFromAttribute($value){
        return Carbon::parse($value)->format("Y-m-d");
    }
}


