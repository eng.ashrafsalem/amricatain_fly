<?php

namespace App\Models\HotelBookings\Repositories;

use App\Models\Customers\Customer;
use App\Models\HotelBookings\HotelBooking;
use App\Models\Hotels\Hotel;
use App\Repositories\BaseRepository;

class HotelBookingRepository extends BaseRepository
{

        protected $with = ['customer', 'hotel'];

	public function __construct(HotelBooking $hotelbooking)
    {
        $this->model = $hotelbooking;
    }

    public function createData()
    {
        return [
//            'example' => app(ExampleRepository::class)->all(),
        ];
    }

    public function editData($id)
    {
        $data = parent::editData($id);

//        $data['example'] = app(ExampleRepository::class)->all();

        return $data;
    }

    public function updateById($id, array $data, array $options = [])
    {

        $hotelBooking = HotelBooking::find($id);

        $data['customer_id'] = Customer::where('id', $hotelBooking->customer_id)->first()->id;
        $data['hotel_id'] = Hotel::where('id', $hotelBooking ->hotel_id)->first()->id;

        parent::updateById($id, $data);
    }

}

