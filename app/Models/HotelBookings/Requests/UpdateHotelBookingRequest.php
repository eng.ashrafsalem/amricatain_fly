<?php

namespace App\Models\HotelBookings\Requests;
use Illuminate\Foundation\Http\FormRequest;

class UpdateHotelBookingRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {

        $validation[''] = '';

        return $validation;

    }

    public function attributes()
    {
        return [
            '' => __('labels.backend.hotelbookings.'),
        ];
    }
}
