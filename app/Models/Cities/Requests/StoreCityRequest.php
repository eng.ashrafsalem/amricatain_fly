<?php

namespace App\Models\Cities\Requests;
use Illuminate\Foundation\Http\FormRequest;

class StoreCityRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {

        $validation['country_id'] = 'required';
        $validation['ar.name'] = 'required|max:190';
        $validation['en.name'] = 'required|max:190';
        $validation['fr.name'] = 'required|max:190';
        $validation['it.name'] = 'required|max:190';
        $validation['es.name'] = 'required|max:190';

        return $validation;

    }

    public function attributes()
    {
        return [
            'country' => __('labels.backend.cities.country'),
            'ar.name' => __('labels.backend.cities.ar_name'),
            'en.name' => __('labels.backend.cities.en_name'),
            'fr.name' => __('labels.backend.cities.fr_name'),
            'it.name' => __('labels.backend.cities.it_name'),
            'es.name' => __('labels.backend.cities.es_name'),
        ];
    }
}
