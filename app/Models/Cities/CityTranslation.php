<?php

namespace App\Models\Cities;

use Illuminate\Database\Eloquent\Model;

class CityTranslation extends Model
{

    protected $fillable = ['name'];

    protected $table = 'cities_translations';

}
