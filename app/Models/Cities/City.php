<?php

namespace App\Models\Cities;

use App\Models\Countries\Country;
use App\Models\Hotels\Hotel;
use Illuminate\Database\Eloquent\Model;
use App\Traits\HasRoute;
use App\Models\FixedTours\FixedTour;
use Astrotomic\Translatable\Translatable;

class City extends Model
{
    use HasRoute, Translatable;

    protected $fillable = ['name', 'country_id'];

    protected $routeName = 'admin.cities';

    protected $translatedAttributes = ['name'];

    protected $with = ['translations'];

    // reverse relation with country
    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    // one to many relationship with hotel
    public function hotels()
    {
        return $this->belongsToMany(Hotel::class);
    }

    public function FixedTour()
    {
        return $this->hasMany(FixedTour::class);
    }
    
}


