<?php

namespace App\Models\Cities\Repositories;

use App\Models\Cities\City;
use App\Models\Countries\Country;
use App\Models\Countries\Repositories\CountryRepository;
use App\Repositories\BaseRepository;

class CityRepository extends BaseRepository
{
        // if there is relation with other . sit the relation function here in $with
        protected $with = ['country'];

	public function __construct(City $city)
    {
        $this->model = $city;
    }

    public function createData()
    {
        $data = [];

        $data['countries'] = app(CountryRepository::class)->all();

        return $data;
    }

    public function editData($id)
    {
        $data = parent::editData($id);

        $data['countries'] = app(CountryRepository::class)->all();

        return $data;
    }

}

