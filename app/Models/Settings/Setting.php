<?php

namespace App\Models\Settings;

use Illuminate\Database\Eloquent\Model;
use App\Traits\HasRoute;
use Astrotomic\Translatable\Translatable;

class Setting extends Model
{
    use HasRoute, Translatable;

    protected $fillable = ['key', 'value'];

    protected $routeName = 'admin.settings';

    protected $translatedAttributes = ['title', 'contents'];

    protected $with = ['translations'];
}


