<?php

namespace App\Models\Settings\Repositories;

use App\Models\Settings\Setting;
use App\Repositories\BaseRepository;

class SettingRepository extends BaseRepository
{

    //    protected $with = [''];

	public function __construct(Setting $setting)
    {
        $this->model = $setting;
    }

    public function createData()
    {
        return [
//            'example' => app(ExampleRepository::class)->all(),
        ];
    }

    public function editData($id)
    {
        $data = parent::editData($id);

//        $data['example'] = app(ExampleRepository::class)->all();

        return $data;
    }

}

