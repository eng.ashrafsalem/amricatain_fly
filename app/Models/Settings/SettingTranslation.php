<?php

namespace App\Models\Settings;

use Illuminate\Database\Eloquent\Model;

class SettingTranslation extends Model
{

    protected $fillable = ['title', 'contents'];

    protected $table = 'settings_translations';

}
