<?php

namespace App\Models\Settings\Requests;
use Illuminate\Foundation\Http\FormRequest;

class StoreSettingRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $validation['key'] = 'required|max:190';
        $validation['value'] = 'nullable|max:190';
        $validation['ar.title'] = 'nullable|max:190';
        $validation['en.title'] = 'nullable|max:190';
        $validation['fr.title'] = 'nullable|max:190';
        $validation['it.title'] = 'nullable|max:190';
        $validation['es.title'] = 'nullable|max:190';

        $validation['ar.contents'] = 'nullable';
        $validation['en.contents'] = 'nullable';
        $validation['fr.contents'] = 'nullable';
        $validation['it.contents'] = 'nullable';
        $validation['es.contents'] = 'nullable';

        return $validation;

    }

    public function attributes()
    {
        return [
            'key' => __('labels.backend.settings.key'),
            'value' => __('labels.backend.settings.value'),
            'ar.title' => __('labels.backend.settings.ar_title'),
            'en.title' => __('labels.backend.settings.en_title'),
            'fr.title' => __('labels.backend.settings.fr_title'),
            'it.title' => __('labels.backend.settings.it_title'),
            'es.title' => __('labels.backend.settings.es_title'),

            'ar.contents' => __('labels.backend.settings.ar_contents'),
            'en.contents' => __('labels.backend.settings.en_contents'),
            'fr.contents' => __('labels.backend.settings.fr_contents'),
            'it.contents' => __('labels.backend.settings.it_contents'),
            'es.contents' => __('labels.backend.settings.es_contents'),
        ];
    }
}
