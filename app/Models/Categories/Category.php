<?php

namespace App\Models\Categories;

use App\Models\CustomTourBookings\CustomTourBooking;
use App\Models\FixedTours\FixedTour;
use Illuminate\Database\Eloquent\Model;
use App\Traits\HasRoute;
use Astrotomic\Translatable\Translatable;

class Category extends Model
{
    use HasRoute, Translatable;

    protected $fillable = ['name'];

    protected $routeName = 'admin.categories';

    protected $translatedAttributes = ['name'];

    protected $with = ['translations'];

    // one-to-many relationship with fixed_Tour table
    public function fixedTour()
    {
        return $this->hasMany(FixedTour::class);
    }

    public function customTourBooking()
    {
        return $this->hasMany(CustomTourBooking::class);
    }
}


