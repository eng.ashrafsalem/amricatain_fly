<?php

namespace App\Models\Categories;

use Illuminate\Database\Eloquent\Model;

class CategoryTranslation extends Model
{

    protected $fillable = ['name'];

    protected $table = 'categories_translations';

}
