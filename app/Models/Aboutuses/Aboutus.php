<?php

namespace App\Models\Aboutuses;

use Illuminate\Database\Eloquent\Model;
use App\Traits\HasRoute;
use Astrotomic\Translatable\Translatable;

class Aboutus extends Model
{
    use HasRoute, Translatable;

    protected $fillable = ['type_id'];

    protected $table = 'front_about_us';

    protected $routeName = 'admin.aboutuses';

    protected $translatedAttributes = ['contents'];

    protected $with = ['translations'];
}


