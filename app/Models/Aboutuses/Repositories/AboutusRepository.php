<?php

namespace App\Models\Aboutuses\Repositories;

use App\Models\Aboutuses\Aboutus;
use App\Repositories\BaseRepository;

class AboutusRepository extends BaseRepository
{

    //    protected $with = [''];

	public function __construct(Aboutus $aboutus)
    {
        $this->model = $aboutus;
    }

    public function createData()
    {
        return [
//            'example' => app(ExampleRepository::class)->all(),
        ];
    }

    public function create(array $data)
    {
        $test = $data;
        unset($test["image_url"]);
        // save
        $aboutus = parent::create($test);
        // store image
        if(request()->hasFile('image_url')) {
            // save the main image
            saveImage($aboutus->id, Aboutus::class, $data['image_url']->store('public/uploads'), 1);
        }

//        return ;
    }

    public function editData($id)
    {
        $data = parent::editData($id);

//        $data['example'] = app(ExampleRepository::class)->all();

        return $data;
    }

    public function updateById($id, array $data, array $options = [])
    {
        $test = $data;
        unset($test["image_url"]);

        // store main image if any
        if(request()->hasFile('image_url')) {
            // delete the old one
            deleteMainImageOnly($id, Aboutus::class);
            // save the new one
            saveImage($id, Aboutus::class, $data['image_url']->store('public/uploads'), 1);
        }

        parent::updateById($id, $test);
    }

    public function deleteById($id)
    {
        // delete the old one
        deleteMainImageOnly($id, Aboutus::class);

        parent::unsetClauses();

        return parent::deleteById($id);
    }

}

