<?php

namespace App\Models\Aboutuses;

use Illuminate\Database\Eloquent\Model;

class AboutusTranslation extends Model
{

    protected $fillable = ['contents'];

    protected $table = 'front_about_us_translations';

}
