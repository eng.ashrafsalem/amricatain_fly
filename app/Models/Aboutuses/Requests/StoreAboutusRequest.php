<?php

namespace App\Models\Aboutuses\Requests;
use Illuminate\Foundation\Http\FormRequest;

class StoreAboutusRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $validation['ar.contents'] = 'required';
        $validation['en.contents'] = 'required';
        $validation['fr.contents'] = 'required';
        $validation['it.contents'] = 'required';
        $validation['es.contents'] = 'required';
        $validation['image_url'] = 'required';

        return $validation;

    }

    public function attributes()
    {
        return [
            'ar.contents' => __('labels.backend.aboutuses.ar_contents'),
            'en.contents' => __('labels.backend.aboutuses.en_contents'),
            'fr.contents' => __('labels.backend.aboutuses.fr_contents'),
            'es.contents' => __('labels.backend.aboutuses.es_contents'),
            'it.contents' => __('labels.backend.aboutuses.it_contents'),
            'image_url' => __('labels.backend.aboutuses.main_image'),
        ];
    }
}
