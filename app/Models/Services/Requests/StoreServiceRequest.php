<?php

namespace App\Models\Services\Requests;
use Illuminate\Foundation\Http\FormRequest;

class StoreServiceRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {

        $validation['ar.name'] = 'required|max:190';
        $validation['en.name'] = 'required|max:190';
        $validation['fr.name'] = 'required|max:190';
        $validation['it.name'] = 'required|max:190';
        $validation['es.name'] = 'required|max:190';
        $validation['icon'] = 'required|max:190';

        return $validation;

    }

    public function attributes()
    {
        return [
            'ar.name' => __('labels.backend.services.ar_name'),
            'en.name' => __('labels.backend.services.en_name'),
            'fr.name' => __('labels.backend.services.fr_name'),
            'it.name' => __('labels.backend.services.it_name'),
            'es.name' => __('labels.backend.services.es_name'),
            'icon' => __('labels.backend.services.icon'),
        ];
    }
}
