<?php

namespace App\Models\Services\Requests;
use Illuminate\Foundation\Http\FormRequest;

class UpdateServiceRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {

        $validation['ar.name'] = 'required|max:190';
        $validation['en.name'] = 'required|max:190';
        $validation['fr.name'] = 'required|max:190';
        $validation['it.name'] = 'required|max:190';
        $validation['es.name'] = 'required|max:190';
        $validation['icon'] = 'required|max:190';

        return $validation;

    }

    public function attributes()
    {
        return [
            'ar.name' => __('labels.backend.categories.ar_name'),
            'en.name' => __('labels.backend.categories.en_name'),
            'fr.name' => __('labels.backend.categories.fr_name'),
            'it.name' => __('labels.backend.categories.it_name'),
            'es.name' => __('labels.backend.categories.es_name'),
            'icon' => __('labels.backend.services.icon'),
        ];
    }
}
