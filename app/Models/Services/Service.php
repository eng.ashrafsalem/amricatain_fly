<?php

namespace App\Models\Services;

use App\Models\Hotels\Hotel;
use Illuminate\Database\Eloquent\Model;
use App\Traits\HasRoute;
use Astrotomic\Translatable\Translatable;

class Service extends Model
{
    use HasRoute, Translatable;

    protected $fillable = ['name', 'icon'];

    protected $routeName = 'admin.services';

    protected $translatedAttributes = ['name'];

    protected $with = ['translations'];

    // reverse many-to-many relationship with hotels table
    public function hotels()
    {
        $this->belongsToMany(Hotel::class);
    }
}


