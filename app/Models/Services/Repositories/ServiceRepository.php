<?php

namespace App\Models\Services\Repositories;

use App\Models\Services\Service;
use App\Repositories\BaseRepository;

class ServiceRepository extends BaseRepository
{

    //    protected $with = [''];

	public function __construct(Service $service)
    {
        $this->model = $service;
    }

    public function createData()
    {
        return [
//            'example' => app(ExampleRepository::class)->all(),
        ];
    }

    public function editData($id)
    {
        $data = parent::editData($id);

//        $data['example'] = app(ExampleRepository::class)->all();

        return $data;
    }

}

