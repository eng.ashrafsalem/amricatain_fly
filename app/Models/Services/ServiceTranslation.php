<?php

namespace App\Models\Services;

use Illuminate\Database\Eloquent\Model;

class ServiceTranslation extends Model
{

    protected $fillable = ['name'];

    protected $table = 'services_translations';

}
