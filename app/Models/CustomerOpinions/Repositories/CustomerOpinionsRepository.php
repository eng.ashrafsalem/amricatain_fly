<?php

namespace App\Models\CustomerOpinions\Repositories;

use App\Models\CustomerOpinions\CustomerOpinions;
use App\Repositories\BaseRepository;

class CustomerOpinionsRepository extends BaseRepository
{

    //    protected $with = [''];

	public function __construct(CustomerOpinions $customeropinions)
    {
        $this->model = $customeropinions;
    }

    public function createData()
    {
        return [
//            'example' => app(ExampleRepository::class)->all(),
        ];
    }

    public function create(array $data)
    {
        $test = $data;
        unset($test["image_url"]);
        // save
        $customerOpinion = parent::create($test);
        // store image
        if(request()->hasFile('image_url')) {
            // save the main image
            saveImage($customerOpinion->id, CustomerOpinions::class, $data['image_url']->store('public/uploads'), 1);
        }

//        return ;
    }

    public function editData($id)
    {
        $data = parent::editData($id);

//        $data['example'] = app(ExampleRepository::class)->all();

        return $data;
    }


    public function updateById($id, array $data, array $options = [])
    {
        $openion=CustomerOpinions::find($id);
        $test = $data;
        unset($test["image_url"]);
//        dd(request()->hasFile('image_url'));
        // store main image if any
        if(request()->hasFile('image_url')) {
            // delete the old one
//            if(isset($openion->image->image_url)) {
                deleteMainImageOnly($id, CustomerOpinions::class);
//            }
            // save the new one
            saveImage($id, CustomerOpinions::class, $data['image_url']->store('public/uploads'), 1);
        }

        parent::updateById($id, $test);
    }

    public function deleteById($id)
    {
        $openion=CustomerOpinions::find($id);
//        dd($id);
        // delete the old one
//        if(isset($openion->image->image_url)){
            deleteMainImageOnly($id, CustomerOpinions::class);
//        }


        parent::unsetClauses();

        return parent::deleteById($id);
    }
}

