<?php

namespace App\Models\CustomerOpinions;

use Illuminate\Database\Eloquent\Model;
use App\Traits\HasRoute;

class CustomerOpinions extends Model
{
    use HasRoute;

    protected $guarded = [''];

    protected $table = 'front_customers_opinions';

    protected $routeName = 'admin.customeropinions';
}


