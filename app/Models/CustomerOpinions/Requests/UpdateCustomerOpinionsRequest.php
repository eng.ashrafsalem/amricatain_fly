<?php

namespace App\Models\CustomerOpinions\Requests;
use Illuminate\Foundation\Http\FormRequest;

class UpdateCustomerOpinionsRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {

        $validation['description'] = 'required';
        $validation['name'] = 'required|max:191';
        $validation['job_title_name'] = 'required|max:191';

        return $validation;

    }

    public function attributes()
    {
        return [
            'description' => __('labels.backend.customeropinions.description'),
            'name' => __('labels.backend.customeropinions.name'),
            'job_title_name' => __('labels.backend.customeropinions.job_title_name'),
        ];
    }
}
