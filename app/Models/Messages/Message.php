<?php

namespace App\Models\Messages;

use Illuminate\Database\Eloquent\Model;
use App\Traits\HasRoute;

class Message extends Model
{
    use HasRoute;

    protected $fillable = [''];

    protected $routeName = 'admin.messages';
}


