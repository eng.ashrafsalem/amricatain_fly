<?php

namespace App\Models\Messages\Repositories;

use App\Models\Messages\AlertMessage;
use App\Repositories\BaseRepository;

class MessageRepository extends BaseRepository
{

    //    protected $with = [''];

	public function __construct(AlertMessage $message)
    {
        $this->model = $message;
    }

    public function createData()
    {
        return [
//            'example' => app(ExampleRepository::class)->all(),
        ];
    }

    public function editData($id)
    {
        $data = parent::editData($id);

//        $data['example'] = app(ExampleRepository::class)->all();

        return $data;
    }

}

