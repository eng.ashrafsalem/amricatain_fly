<?php

namespace App\Models\FrontSliders\Requests;
use Illuminate\Foundation\Http\FormRequest;

class UpdateFrontSliderRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $validation['ar.title'] = 'required';
        $validation['en.title'] = 'required';
        $validation['fr.title'] = 'required';
        $validation['it.title'] = 'required';
        $validation['es.title'] = 'required';

        return $validation;

    }

    public function attributes()
    {
        return [
            'ar.title' => __('labels.backend.frontsliders.ar_title'),
            'en.title' => __('labels.backend.frontsliders.en_title'),
            'fr.title' => __('labels.backend.frontsliders.fr_title'),
            'es.title' => __('labels.backend.frontsliders.es_title'),
            'it.title' => __('labels.backend.frontsliders.it_title'),
        ];
    }
}
