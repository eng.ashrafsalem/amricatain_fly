<?php

namespace App\Models\FrontSliders\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreFrontSliderRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $validation['ar.title'] = 'nullable';
        $validation['en.title'] = 'nullable';
        $validation['fr.title'] = 'nullable';
        $validation['it.title'] = 'nullable';
        $validation['es.title'] = 'nullable';
        $validation['image_url'] = 'required';

        return $validation;

    }

    public function attributes()
    {
        return [
            'ar.title' => __('labels.backend.frontsliders.ar_title'),
            'en.title' => __('labels.backend.frontsliders.en_title'),
            'fr.title' => __('labels.backend.frontsliders.fr_title'),
            'es.title' => __('labels.backend.frontsliders.es_title'),
            'it.title' => __('labels.backend.frontsliders.it_title'),
            'image_url' => __('labels.backend.frontsliders.main_image'),
        ];
    }
}
