<?php

namespace App\Models\FrontSliders;

use Illuminate\Database\Eloquent\Model;
use App\Traits\HasRoute;
use Astrotomic\Translatable\Translatable;

class FrontSlider extends Model
{
    use HasRoute, Translatable;

    protected $fillable = [''];

    protected $routeName = 'admin.frontsliders';

    protected $translatedAttributes = ['title'];

    protected $with = ['translations'];

}


