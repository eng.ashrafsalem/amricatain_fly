<?php

namespace App\Models\FrontSliders;

use Illuminate\Database\Eloquent\Model;

class FrontSliderTranslation extends Model
{

    protected $fillable = ['title'];

    protected $table = 'front_sliders_translations';

}
