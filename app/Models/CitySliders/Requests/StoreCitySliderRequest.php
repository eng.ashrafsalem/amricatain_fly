<?php

namespace App\Models\CitySliders\Requests;
use Illuminate\Foundation\Http\FormRequest;

class StoreCitySliderRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {

        $validation['image_url'] = 'required|image';
        $validation['ar.title'] = 'required|max:190';
        $validation['en.title'] = 'required|max:190';
        $validation['fr.title'] = 'required|max:190';
        $validation['it.title'] = 'required|max:190';
        $validation['es.title'] = 'required|max:190';

        return $validation;

    }

    public function attributes()
    {
        return [
            'image_url' => __('labels.backend.citysliders.main_image'),
            'ar.title' => __('labels.backend.citysliders.ar_title'),
            'en.title' => __('labels.backend.citysliders.en_title'),
            'fr.title' => __('labels.backend.citysliders.fr_title'),
            'it.title' => __('labels.backend.citysliders.it_title'),
            'es.title' => __('labels.backend.citysliders.es_title'),
        ];
    }
}
