<?php

namespace App\Models\CitySliders;

use Illuminate\Database\Eloquent\Model;
use App\Traits\HasRoute;
use Astrotomic\Translatable\Translatable;

class CitySlider extends Model
{
    use HasRoute, Translatable;

    protected $guarded = ['city_slider_id'];

    protected $table = 'front_city_slider';

    protected $routeName = 'admin.citysliders';

    protected $translatedAttributes = ['title'];

    protected $with = ['translations'];
}


