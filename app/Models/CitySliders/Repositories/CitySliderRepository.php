<?php

namespace App\Models\CitySliders\Repositories;

use App\Models\CitySliders\CitySlider;
use App\Repositories\BaseRepository;

class CitySliderRepository extends BaseRepository
{

    //    protected $with = [''];

	public function __construct(CitySlider $cityslider)
    {
        $this->model = $cityslider;
    }

    public function createData()
    {
        return [
//            'example' => app(ExampleRepository::class)->all(),
        ];
    }

    public function create(array $data)
    {
        $test = $data;
        unset($test["image_url"]);
        // save
        $slider = parent::create($test);
        // store image
        if(request()->hasFile('image_url')) {
            // save the main image
            saveImage($slider->id, CitySlider::class, $data['image_url']->store('public/uploads'), 1);
        }

//        return ;
    }

    public function editData($id)
    {
        $data = parent::editData($id);

//        $data['example'] = app(ExampleRepository::class)->all();

        return $data;
    }

    public function updateById($id, array $data, array $options = [])
    {
        $test = $data;
        unset($test["image_url"]);

        // store main image if any
        if(request()->hasFile('image_url')) {
            // delete the old one
            deleteMainImageOnly($id, CitySlider::class);
            // save the new one
            saveImage($id, CitySlider::class, $data['image_url']->store('public/uploads'), 1);
        }

        parent::updateById($id, $test);
    }

    public function deleteById($id)
    {
        // delete the old one
        deleteMainImageOnly($id, CitySlider::class);

        parent::unsetClauses();

        return parent::deleteById($id);
    }

}

