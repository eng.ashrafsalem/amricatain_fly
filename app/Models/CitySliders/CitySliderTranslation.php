<?php

namespace App\Models\CitySliders;

use Illuminate\Database\Eloquent\Model;

class CitySliderTranslation extends Model
{

    protected $fillable = ['title'];

    protected $table = 'front_city_translations';

}
