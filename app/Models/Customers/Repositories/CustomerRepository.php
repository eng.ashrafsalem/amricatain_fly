<?php

namespace App\Models\Customers\Repositories;

use App\Models\Auth\User;
use App\Models\Customers\Customer;
use App\Models\HotelBookings\HotelBooking;
use App\Repositories\BaseRepository;
use Hash;

class CustomerRepository extends BaseRepository
{

        protected $with = ['image'];

	public function __construct(Customer $customer)
    {
        $this->model = $customer;
    }

    public function createData()
    {
        return [
//            'example' => app(ExampleRepository::class)->all(),
        ];
    }

    public function create(array $data)
    {
        // save
//        dd($data);
        $customer = parent::create($data);

        // store image
        if(request()->hasFile('image_url')) {
            $image = new \App\Models\Image(['image_url' => $data['image_url'], 'main_image' => 1]);
            $customer->image()->save($image);
        } else {
            $image = new \App\Models\Image(['image_url' => 'uploads/avatar.png', 'main_image' => 1]);
            $customer->image()->save($image);
        }


    }

    public function editData($id)
    {
        $data = parent::editData($id);
//        $data['example'] = app(ExampleRepository::class)->all();

        return $data;
    }

    public function updateById($id, array $data, array $options = [])
    {
        $customer = Customer::find($id);
        // if there is no password use the old one
        if(!$data['password']) {
            $data['password'] = $customer->first()->password;
        }
        // store image if any
        if(request()->hasFile('image_url')) {
            // here if the image is unique , first delete the record from
            // image table
            // if not unique , no need to delete the old one
            $customer->image()->delete();

            $image = new \App\Models\Image(['image_url' => $data['image_url'], 'main_image' => 1]);

            $customer->image()->save($image);
        }

        parent::updateById($id, $data);

    }

    public function deleteById($id)
    {

        $customer = parent::getById($id);

        if(isset($customer->image->image_url))
        {
            // keep the avatar image
            if($customer->image->image_url != '/storage/uploads/avatar.png') {
                $path = public_path($customer->image->image_url);
                unlink($path);
            }
            $customer->image()->delete();
        }

        parent::unsetClauses();
        $customer->hotelBookings()->delete();
        $customer->fixedTourBookings()->delete();
        $customer->customTourBookings()->delete();
        // then delete from customer table
        return $customer->delete();
    }
}

