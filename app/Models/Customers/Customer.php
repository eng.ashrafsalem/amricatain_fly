<?php

namespace App\Models\Customers;

use App\Models\CustomTourBookings\CustomTourBooking;
use App\Models\FixedTourBookings\FixedTourBooking;
use App\Models\HotelBookings\HotelBooking;
use Illuminate\Database\Eloquent\Model;
use App\Traits\HasRoute;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Customer extends Authenticatable
{
    use HasRoute;

    protected $fillable = ['name', 'email', 'phone', 'address', 'gender',
        'password', 'age','isAdmin'];

    protected $routeName = 'admin.customers';

    // one-to-one morphic-relation with images table
    public function image()
    {

        return $this->morphOne('App\Models\Image', 'imageable');
    }

    // bcrypt the password
    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = \Hash::make($value);
    }

    public function fixedTourBookings()
    {
        return $this->hasMany(FixedTourBooking::class);
    }

    public function hotelBookings()
    {
        return $this->hasMany(HotelBooking::class);
    }

    public function customTourBookings()
    {
        return $this->hasMany(CustomTourBooking::class);
    }
}


