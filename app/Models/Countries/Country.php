<?php

namespace App\Models\Countries;

use App\Models\Cities\City;
use Illuminate\Database\Eloquent\Model;
use App\Traits\HasRoute;
use Astrotomic\Translatable\Translatable;

class Country extends Model
{
    use HasRoute, Translatable;

    protected $fillable = ['name'];

    protected $routeName = 'admin.countries';

    protected $translatedAttributes = ['name'];

    protected $with = ['translations'];

    // one-to-many relationship with table city
    public function cities()
    {
        return $this->hasMany(City::class);
    }
}


