<?php

namespace App\Models\Countries\Repositories;

use App\Models\Countries\Country;
use App\Repositories\BaseRepository;
use Yajra\DataTables\DataTables;

class CountryRepository extends BaseRepository
{

    //    protected $with = [''];

	public function __construct(Country $country)
    {
        $this->model = $country;
    }

    public function apiGet($table_id)
    {
        return $this->model
            ->whereHas('menu', function ($q) use ($table_id) {
                $q->whereHas('store', function ($q) use ($table_id) {
                    $q->whereHas('tables', function ($q) use ($table_id) {
                        $q->where('id', $table_id);
                    });
                });
            })->paginate(10, ['id', 'logo']);
    }

    public function getWithDatatable()
    {
        $model = $this->model->withOut('translations')->select(
            [
                'countries_translations.name',
                'countries.id',
            ])
            ->join('countries_translations', 'countries_translations.country_id', '=', 'countries.id')
            ->where('countries_translations.locale', app()->getLocale());


        return DataTables::of($model)
            ->addIndexColumn()
            ->addColumn('action', function ($data) {
                return view('backend.includes.partials.datatables.action', ['action' => $this->setDatatableAction($data)])->render();
            })->make(true);
    }

    public function createData()
    {
        return [
//            'example' => app(ExampleRepository::class)->all(),
        ];
    }

    public function editData($id)
    {
        $data = parent::editData($id);

//        $data['example'] = app(ExampleRepository::class)->all();

        return $data;
    }

}

