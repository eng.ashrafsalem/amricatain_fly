<?php

namespace App\Models\Countries;

use Illuminate\Database\Eloquent\Model;

class CountryTranslation extends Model
{

    protected $fillable = ['name'];

    protected $table = 'countries_translations';

}
