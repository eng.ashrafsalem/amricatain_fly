<?php

namespace App\Models\AlertMessages;

use Illuminate\Database\Eloquent\Model;


class AlertMessageTranslation extends Model
{

    protected $fillable = ['name'];
    protected $table='alert_messages_translation';
//    protected $routeName = 'admin.messages';
}


