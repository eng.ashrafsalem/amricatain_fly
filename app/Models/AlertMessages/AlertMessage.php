<?php

namespace App\Models\AlertMessages;

use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use App\Traits\HasRoute;

class AlertMessage extends Model
{
    use HasRoute, Translatable;

    protected $translatedAttributes = ['name'];
//    protected $fillable = [''];
    protected $with = ['translations'];
    protected $table='alert_messages';
//    protected $routeName = 'admin.messages';
}


