<?php

namespace App\Models\CustomTourBookings\Requests;
use Illuminate\Foundation\Http\FormRequest;

class StoreCustomTourBookingRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {

        $validation[''] = '';

        return $validation;

    }

    public function attributes()
    {
        return [
            '' => __('labels.backend.customtourbookings.'),
        ];
    }
}
