<?php

namespace App\Models\CustomTourBookings\Repositories;

use App\Models\Categories\Category;
use App\Models\Customers\Customer;
use App\Models\CustomTourBookings\CustomTourBooking;
use App\Models\Hotels\Hotel;
use App\Repositories\BaseRepository;

class CustomTourBookingRepository extends BaseRepository
{

        protected $with = ['customer','category','hotel'];

	public function __construct(CustomTourBooking $customtourbooking)
    {
        $this->model = $customtourbooking;
    }

    public function createData()
    {
        return [
//            'example' => app(ExampleRepository::class)->all(),
        ];
    }

    public function editData($id)
    {
        $data = parent::editData($id);

//        $data['example'] = app(ExampleRepository::class)->all();

        return $data;
    }

    public function updateById($id, array $data, array $options = [])
    {

        $CustomTourBooking = CustomTourBooking::find($id);
//        dd($CustomTourBooking);
        $data['customer_id'] = Customer::where('id', $CustomTourBooking->customer_id)->first()->id;
        $data['hotel_id'] = Hotel::where('id', $CustomTourBooking ->hotel_id)->first()->id;
        $data['category_id'] = Category::where('id', $CustomTourBooking ->hotel_id)->first()->id;
        parent::updateById($id, $data);
    }

}

