<?php

namespace App\Models\CustomTourBookings;

use App\Models\Categories\Category;
use App\Models\Customers\Customer;
use App\Models\Hotels\Hotel;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use App\Traits\HasRoute;

class CustomTourBooking extends Model
{
    use HasRoute;

    protected $guarded = [''];

    protected $table = 'custom_tour_bookings';

    protected $routeName = 'admin.customtourbookings';

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function hotel()
    {
        return $this->belongsTo(Hotel::class)->withTrashed();
    }

    public function getToAttribute($value){
        return Carbon::parse($value)->format("Y-m-d");
    }

    public function getFromAttribute($value){
        return Carbon::parse($value)->format("Y-m-d");
    }

}


