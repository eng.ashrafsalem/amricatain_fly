<?php

namespace App\Models\CustomTourBookings;

use App\Models\Categories\Category;
use App\Models\Customers\Customer;
use App\Models\Hotels\Hotel;
use Illuminate\Database\Eloquent\Model;
use App\Traits\HasRoute;

class CustomTourBookingDetails extends Model
{
//    use HasRoute;

    protected $guarded = [''];

    protected $table = 'custom_tour_bookings_details';

    protected $routeName = 'admin.customtourbookings';



}


