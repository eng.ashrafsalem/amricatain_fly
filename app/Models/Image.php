<?php

namespace App\Models;

use QCod\ImageUp\HasImageUploads;
use Illuminate\Database\Eloquent\Model;

class Image extends Model
{

    use HasImageUploads;

    protected $fillable = [
        'image_url', 'main_image'
    ];

    protected static $imageFields = [
        'image_url'
    ];

    // one-to-many morphic relation
    public function imageable()
    {
        return $this->morphTo();
    }

    public function getImageUrlAttribute($value){
        return "/storage/".$value;
    }


}
