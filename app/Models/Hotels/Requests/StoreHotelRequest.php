<?php

namespace App\Models\Hotels\Requests;
use Illuminate\Foundation\Http\FormRequest;

class StoreHotelRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {

        $validation['ar.name'] = 'required|max:190';
        $validation['en.name'] = 'required|max:190';
        $validation['fr.name'] = 'required|max:190';
        $validation['it.name'] = 'required|max:190';
        $validation['es.name'] = 'required|max:190';
        $validation['city_id'] = 'required';
        $validation['map_long'] = 'required';
        $validation['map_lat'] = 'required';
        $validation['level'] = 'required';
        $validation['shown'] = 'required';
        $validation['m_image'] = 'required|image';

        return $validation;

    }

    public function attributes()
    {
        return [
            'name' => __('labels.backend.hotels.name'),
            'city_id' => __('labels.backend.hotels.cities'),
            'map_long' => __('labels.backend.hotels.map_long'),
            'map_lat' => __('labels.backend.hotels.map_lat'),
            'level' => __('labels.backend.hotels.level'),
            'shown' => __('labels.backend.hotels.shown'),
            'm_image' => __('labels.backend.hotels.main_image'),
            's_image' => __('labels.backend.hotels.slider_image'),
        ];
    }
}
