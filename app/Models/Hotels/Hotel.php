<?php

namespace App\Models\Hotels;

use App\Models\Cities\City;
use App\Models\FixedTours\FixedTour;
use App\Models\HotelBookings\HotelBooking;
use App\Models\Image;
use App\Models\Services\Service;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use App\Traits\HasRoute;
use Illuminate\Database\Eloquent\SoftDeletes;

class Hotel extends Model
{
    use HasRoute, Translatable, SoftDeletes;

    protected $translatedAttributes = ['name'];

    protected $with = ['translations'];

    protected $guarded = [];

    protected $routeName = 'admin.hotels';

    // many-to-many relationship with services table
    public function services()
    {
        return $this->belongsToMany(Service::class);
    }

    // reverse relation many-to-one relation with city
    public function city()
    {
        return $this->belongsTo(City::class);
    }

    // one-to-many relationship with fixed_tours table
    public function fixedTours()
    {
        return $this->hasMany(FixedTour::class);
    }

    public function hasService($serviceId)
    {
        return in_array($serviceId, $this->services->pluck('id')->toArray());
    }

    public function  hotelBookings()
    {
        return $this->hasMany(HotelBooking::class);
    }
}


