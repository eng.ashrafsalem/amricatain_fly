<?php

namespace App\Models\Hotels;

use Illuminate\Database\Eloquent\Model;

class HotelTranslation extends Model
{
    protected $fillable = ['name'];

    protected $table = 'hotels_translations';
}
