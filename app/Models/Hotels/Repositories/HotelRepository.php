<?php

namespace App\Models\Hotels\Repositories;

use App\Models\Cities\Repositories\CityRepository;
use App\Models\Hotels\Hotel;
use App\Models\Image;
use App\Models\Services\Repositories\ServiceRepository;
use App\Repositories\BaseRepository;
use function GuzzleHttp\Psr7\parse_request;
use DB;
use Yajra\DataTables\DataTables;

class HotelRepository extends BaseRepository
{

    protected $with = ['city', 'services'];

	public function __construct(Hotel $hotel)
    {
        $this->model = $hotel;
    }

    public function createData()
    {
        $data = [];

        $data['cities'] = app(CityRepository::class)->all();
        $data['services'] =  app(ServiceRepository::class)->all() ;

        return $data;
    }

    public function create(array $data)
    {
        // table hotels doesn't have m_image or s_image or services columns so exclude them
        $test = $data;
        unset($test["services"]);
        unset($test["m_image"]);
        unset($test["s_image"]);

        // save
        $hotel = parent::create($test);

        // adding services if available
        if($data['services'])
        {
            $hotel->services()->attach($data['services']);
        }

        // store main image
        if(request()->hasFile('m_image')) {
            // save the main image
            saveImage($hotel->id, Hotel::class, $data['m_image']->store('public/uploads'), 1);
        } else  {
            // save the avatar
            saveImage($hotel->id, Hotel::class, 'public/uploads/main_hotel.png', 1);
        }

        // store slider image
        if(request()->hasFile('s_image')) {
            foreach ($data['s_image'] as $img) {
                // save the slider images
                saveImage($hotel->id, Hotel::class, $img->store('public/uploads'), 0);
            }
        }
    }

    public function editData($id)
    {
        $data = parent::editData($id);
        $data['cities'] = app(CityRepository::class)->all();
        $data['services'] =  app(ServiceRepository::class)->all() ;

        return $data;
    }

    public function updateById($id, array $data, array $options = [])
    {

        $hotel = Hotel::find($id);

        $test = $data;

        if(isset($data['services']))
        {
            unset($test['services']);
        }
        unset($test["m_image"]);
        unset($test["s_image"]);

        parent::updateById($id, $test);

        if(isset($data['services']))
        {
            $hotel->services()->sync($data['services']);
        }
        else
        {
            // delete if he d-select the services
            DB::table('hotel_service')->where('hotel_id', $id)->delete();
        }

        // store main image
        if(request()->hasFile('m_image')) {
            // delete the old one
            deleteMainImageOnly($id, Hotel::class);
            // save the new one
            saveImage($id, Hotel::class, $data['m_image']->store('public/uploads'), 1);
        }

        // store slider image
        if(request()->hasFile('s_image')) {
            // first delete the old ones
            deleteSliderImagesOnly($id, Hotel::class);

            // second insert new ones
            foreach ($data['s_image'] as $img) {
                // get the image
                saveImage($id, Hotel::class, $img->store('public/uploads'), 0);
            }
        }
    }

    public function deleteById($id)
    {
        // delete all related images first
//        deleteAllFromBothTableAndStorage($id, Hotel::class);

        // then delete the rest
        parent::unsetClauses();

        return parent::deleteById($id);
    }

}

