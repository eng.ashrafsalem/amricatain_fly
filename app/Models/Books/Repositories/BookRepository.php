<?php

namespace App\Models\Books\Repositories;

use App\Models\Books\Book;
use App\Repositories\BaseRepository;

class BookRepository extends BaseRepository
{

    //    protected $with = [''];

	public function __construct(Book $book)
    {
        $this->model = $book;
    }

    public function createData()
    {
        return [
//            'example' => app(ExampleRepository::class)->all(),
        ];
    }

    public function editData($id)
    {
        $data = parent::editData($id);

//        $data['example'] = app(ExampleRepository::class)->all();

        return $data;
    }

}

