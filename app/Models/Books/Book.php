<?php

namespace App\Models\Books;

use Illuminate\Database\Eloquent\Model;
use App\Traits\HasRoute;

class Book extends Model
{
    use HasRoute;

    protected $fillable = [''];

    protected $routeName = 'admin.books';
}


