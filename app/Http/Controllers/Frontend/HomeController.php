<?php

namespace App\Http\Controllers\Frontend;

use App\Events\Frontend\Auth\UserRegistered;
use App\Http\Controllers\Controller;
use App\Models\Aboutuses\Aboutus;
use App\Models\AlertMessages\AlertMessage;
use App\Models\Auth\User;
use App\Models\Categories\Category;
use App\Models\CitySliders\CitySlider;
use App\Models\ContactUs\ContactUs;
use App\Models\Customers\Customer;
use App\Models\CustomerOpinions\CustomerOpinions;
use App\Models\Discovers\Discover;
use App\Models\FixedTourBookings\FixedTourBooking;
use App\Models\FixedTours\FixedTour;
use App\Models\FrontSliders\FrontSlider;
use App\Models\HotelBookings\HotelBooking;
use App\Models\Hotels\Hotel;
use App\Models\CustomTourBookings\CustomTourBooking;
use App\Models\FrontNews\FrontNews;
use App\Models\Settings\Setting;
use App\Models\Image;
use DB;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
/**
 * Class HomeController.
 */
class HomeController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $about_us_first_record = Aboutus::where('type_id',1)->first();
        $all_about_us = Aboutus::all();
        $news = FrontNews::orderBy('id', 'desc')->take(3)->get();
        $latestnew=FrontNews::orderBy('id', 'desc')->first();
        return view('frontend.index', [
            'categories' => Category::all(),
            'front_sliders' => FrontSlider::all(),
            'front_slider' =>FrontSlider::all()->first(),
            'about_us_first_row' => $about_us_first_record,
            'all_about_us_section' => $all_about_us,
            'fixed_tours' => FixedTour::where('active', 1)->get(),
            'discover' => Discover::all()->first(),
            'citiesImages' => CitySlider::all(),
            'hotels' => Hotel::where('shown', 1)->get(),
            'customeropinions' => CustomerOpinions::all(),
            'allnews'=>$news,
            'latestnew'=>$latestnew,
            'settings' => Setting::all(),
        ]);
    }

    public function aboutUs()
    {
        $all_about_us = Aboutus::all();
        return view('frontend.aboutus',
            [
                'all_about_us_section' => $all_about_us,
            'categories' => Category::all(),
            ]);
    }

    public function allnews(){
        $allnews = FrontNews::paginate(6);
        return view('frontend.allnews', ['allnews' => $allnews]);
    }

    public function fixedTour($fixed_tour)
    {
        $tour = FixedTour::find($fixed_tour);
        $slider_images = Image::where('imageable_type', FixedTour::class)->where('imageable_id', $tour->id)->get();
        return view('frontend.fixed_tour')
            ->with('tour', $tour)
            ->with('slider_images', $slider_images)
            ->with('categories', Category::all());
    }

    public function fixedTours()
    {
        return view('frontend.all_fixed_tours')
            ->with('tours', FixedTour::all())
            ->with('categories', Category::all());
    }

    public function fixedToursByCategory($category)
    {
        return view('frontend.all_fixed_tours')
            ->with('tours', FixedTour::where('category_id', $category)->get())
            ->with('categories', Category::all());
    }

    public function fixedTourBooking($fixed_tour)
    {

        $tour = FixedTour::find($fixed_tour);
        if(!$tour) {
            abort(404);
        }
        if ($tour->from < date('Y-m-d')){
        return back()->with(['msg' =>AlertMessage::find(4)->translate(app()->getLocale())->name]);
        }
        return view('frontend.fixed_tour_booking')
            ->with('tour', $tour)
            ->with('categories', Category::all());
    }

    public function saveFixedTourBooking()
    {

        $data['customer_id'] = auth('customer')->id();
        $data['fixed_tour_id'] = request()->tour_id;
        $data['from'] = request()->from;
        $data['to'] = request()->to;
        $data['total_cost'] = request()->total_cost;
        $data['adult_count'] = request()->adult_count;
        $data['child_count'] = request()->child_count;
        $data['baby_count'] = request()->baby_count;
//        $x=AlertMessage::find(1);
//        dd(AlertMessage::find(1)->translate(app()->getLocale())->name);
        FixedTourBooking::create($data);
        return redirect(route('frontend.index'))->with(['msg' =>AlertMessage::find(1)->translate(app()->getLocale())->name]);
    }

    public function singleHotel($hotel)
    {
        $hotel = Hotel::find($hotel);
        $slider_images = Image::where('imageable_type', Hotel::class)->where('imageable_id', $hotel->id)->get();
        return view('frontend.single_hotel')
            ->with('hotel', $hotel)
            ->with('slider_images', $slider_images)
            ->with('categories', Category::all());
    }

    public function singleNews($news)
    {
        $news = FrontNews::find($news);
        $slider_images = Image::where('imageable_type', FrontNews::class)
        ->where('imageable_id', $news->id)->get();
        return view('frontend.singlenews')
            ->with('news', $news)
            ->with('slider_images', $slider_images);
    }

    public function singleHotelBooking($hotel)
    {
        return view('frontend.single_hotel_booking',
            [
                'hotel' => Hotel::find($hotel),
                'categories' => Category::all()
            ]);
    }

    public function saveSingleHotelBooking()
    {
        $data['customer_id'] = auth('customer')->id();
        // dd(request('hotel_id'));
        $data['hotel_id'] = Hotel::where('id', request()->hotel_id)->first()->id;
    //  dd($data['hotel_id']);
        $data['from'] = request()->from;
        $data['to'] = request()->to;
        $data['total_cost'] = 0;
        $data['adult_count'] = request()->adult_count;
        $data['child_count'] = request()->child_count;
        $data['baby_count'] = request()->baby_count;

        HotelBooking::create($data);
        return redirect(route('frontend.index'))->with(['msg' =>AlertMessage::find(1)->translate(app()->getLocale())->name]);
    }

    public function saveCustomTourBooking(Request $request)
    {
        $request->validate([
            'adult_count'=>'required|numeric',
            'child_count' =>'required|numeric',
            'baby_count' =>'required|numeric',
           ]);

        $data['customer_id'] = auth('customer')->id();
        // dd(request('hotel_id'));
        $data['hotel_id'] = request()->hotel_id;
        $data['category_id'] = request()->category_id;
    //  dd($data['hotel_id']);
        $data['from'] = request()->from;
        $data['to'] = request()->to;
        $data['total_cost'] = 0;
        $data['adult_count'] = request()->adult_count;
        $data['child_count'] = request()->child_count;
        $data['baby_count'] = request()->baby_count;

        CustomTourBooking::create($data);
        return redirect(route('frontend.index'))->with(['msg' =>AlertMessage::find(1)->translate(app()->getLocale())->name]);
    }

    public function allHotels()
    {
        return view('frontend.all_hotels')
            ->with('hotels', Hotel::where('shown', 1)->get())
            ->with('categories', Category::all());
    }

    public function customTour()
    {
        return view('frontend.custom_tour',
            [
                'categories' => Category::all(),
                'hotels' => Hotel::where('shown', 1)->get(),
                'categories' => Category::all()
            ]);
    }

    public function register(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string'],
            'email' => ['required', 'string', 'email', Rule::unique('customers')],
            'password' => ['required',
                'string',
                'min:6'],
                'address' => ['required', 'string'],
                'phone' => ['required', 'numeric'],
                'age' => ['required', 'numeric'],
            'g-recaptcha-response' => ['required_if:captcha_status,true', 'captcha'],
        ]);

       $user = Customer::create(request()->all());

        auth()->guard('customer')->login($user);

       // event(new UserRegistered($user));

       return redirect(route('frontend.index'))->with(['msg' =>AlertMessage::find(2)->translate(app()->getLocale())->name]);
    }

    public function storeContactUsForm(Request $request){
        $request->validate([
            'name'=>'required',
            'email'=>'required|email',
            'phone'=>'required',
            'message'=>'required|max:190',
        ]);
        ContactUs::create($request->all());
        return redirect(route('frontend.index'))->with(['msg' =>AlertMessage::find(3)->translate(app()->getLocale())->name]);
    }
public function FaceBookUrl(Request $request){
        $value=Setting::select('value')->where('key','facebook')->first();
        return $value;
}
    public function GoogleUrl(){
        $value=Setting::select('value')->where('key','google')->first();
        return $value;
    }

    public function InstagramUrl(){
        $value=Setting::select('value')->where('key','instagram')->first();
        return $value;
    }
}
