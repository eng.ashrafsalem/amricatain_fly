<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Hash;
use App\Models\Customers\Customer;
use Illuminate\Http\Request;
use LangleyFoxall\LaravelNISTPasswordRules\PasswordRules;
use App\Exceptions\GeneralException;

class CustomerLoginController extends Controller
{

public function login(Request $request)
   {
      $request->validate([
         'email' => 'required|string',
         'password' => PasswordRules::login(),
         'g-recaptcha-response' => ['required_if:captcha_status,true', 'captcha'],
     ], [
         'g-recaptcha-response.required_if' => __('validation.required', ['attribute' => 'captcha']),
     ]);

       $user = Customer::where('email',  $request->email)->first();
       if($user != null){
       if (Hash::check($request->password, $user->password))
       {
            auth()->guard('customer')->loginUsingId($user->id);   
            return redirect(route('frontend.index'));
           
       }
      }

      return redirect(route('frontend.index'))->with(['error' => 'invalid Login']);
   }

   public function logout()
   {
      // dd(1);
      auth()->guard('customer')->logout();
//   dd(auth()->guard('customer'));
      return redirect(route('frontend.index'));
   }
}