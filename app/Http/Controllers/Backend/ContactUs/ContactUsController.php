<?php

namespace App\Http\Controllers\Backend\ContactUs;

use App\Http\Controllers\Backend\CustomController;
use App\Http\Controllers\Controller;
use App\Models\ContactUs\Repositories\ContactUsRepository;
use Illuminate\Http\Request;

class ContactUsController extends CustomController
{
    protected $view = 'backend.contactus';

    protected $route = 'admin.ContactUss';

    public function __construct(ContactUsRepository $repository)
    {
        parent::__construct($repository);
    }
}
