<?php

namespace App\Http\Controllers\Backend\Discover;

use App\Http\Controllers\Backend\CustomController;
use App\Models\Discovers\Repositories\DiscoverRepository;
use App\Models\Discovers\Requests\StoreDiscoverRequest;
use App\Models\Discovers\Requests\UpdateDiscoverRequest;


class DiscoverController extends CustomController
{
    protected $view = 'backend.discovers';

    protected $route = 'admin.discovers';

    protected $storeRequestFile = StoreDiscoverRequest::class;

    protected $updateRequestFile = UpdateDiscoverRequest::class;

    public function __construct(DiscoverRepository $repository)
    {
        parent::__construct($repository);
    }

}


