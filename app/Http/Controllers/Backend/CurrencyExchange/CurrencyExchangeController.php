<?php

namespace App\Http\Controllers\Backend\CurrencyExchange;

use App\Http\Controllers\Backend\CustomController;
use App\Models\CurrencyExchanges\Repositories\CurrencyExchangeRepository;
use App\Models\CurrencyExchanges\Requests\StoreCurrencyExchangeRequest;
use App\Models\CurrencyExchanges\Requests\UpdateCurrencyExchangeRequest;


class CurrencyExchangeController extends CustomController
{
    protected $view = 'backend.currencyexchanges';

    protected $route = 'admin.currencyexchanges';

    protected $storeRequestFile = StoreCurrencyExchangeRequest::class;

    protected $updateRequestFile = UpdateCurrencyExchangeRequest::class;

    public function __construct(CurrencyExchangeRepository $repository)
    {
        parent::__construct($repository);
    }

}


