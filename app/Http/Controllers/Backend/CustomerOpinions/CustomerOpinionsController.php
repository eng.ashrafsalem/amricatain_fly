<?php

namespace App\Http\Controllers\Backend\CustomerOpinions;

use App\Http\Controllers\Backend\CustomController;
use App\Models\CustomerOpinions\Repositories\CustomerOpinionsRepository;
use App\Models\CustomerOpinions\Requests\StoreCustomerOpinionsRequest;
use App\Models\CustomerOpinions\Requests\UpdateCustomerOpinionsRequest;


class CustomerOpinionsController extends CustomController
{
    protected $view = 'backend.customeropinions';

    protected $route = 'admin.customeropinions';

    protected $storeRequestFile = StoreCustomerOpinionsRequest::class;

    protected $updateRequestFile = UpdateCustomerOpinionsRequest::class;

    public function __construct(CustomerOpinionsRepository $repository)
    {
        parent::__construct($repository);
    }

}


