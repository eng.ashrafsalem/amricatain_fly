<?php

namespace App\Http\Controllers\Backend\FixedTourBooking;

use App\Http\Controllers\Backend\CustomController;
use App\Models\FixedTourBookings\Repositories\FixedTourBookingRepository;
use App\Models\FixedTourBookings\Requests\StoreFixedTourBookingRequest;
use App\Models\FixedTourBookings\Requests\UpdateFixedTourBookingRequest;


class FixedTourBookingController extends CustomController
{
    protected $view = 'backend.fixedtourbookings';

    protected $route = 'admin.fixedtourbookings';

    protected $storeRequestFile = StoreFixedTourBookingRequest::class;

    protected $updateRequestFile = UpdateFixedTourBookingRequest::class;

    public function __construct(FixedTourBookingRepository $repository)
    {
        parent::__construct($repository);
    }

}


