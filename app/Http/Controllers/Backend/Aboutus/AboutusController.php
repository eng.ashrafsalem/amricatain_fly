<?php

namespace App\Http\Controllers\Backend\Aboutus;

use App\Http\Controllers\Backend\CustomController;
use App\Models\Aboutuses\Repositories\AboutusRepository;
use App\Models\Aboutuses\Requests\StoreAboutusRequest;
use App\Models\Aboutuses\Requests\UpdateAboutusRequest;


class AboutusController extends CustomController
{
    protected $view = 'backend.aboutuses';

    protected $route = 'admin.aboutuses';

    protected $storeRequestFile = StoreAboutusRequest::class;

    protected $updateRequestFile = UpdateAboutusRequest::class;

    public function __construct(AboutusRepository $repository)
    {
        parent::__construct($repository);
    }

}


