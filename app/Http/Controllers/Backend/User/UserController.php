<?php

namespace App\Http\Controllers\Backend\User;

use App\Http\Controllers\Backend\CustomController;
use App\Http\Controllers\Controller;
use App\Models\Users\Repositories\UserRepository;
use App\Models\Users\Requests\StoreUserRequest;
use App\Models\Users\Requests\UpdateUserRequest;
use Illuminate\Http\Request;

class UserController extends CustomController
{
    protected $view = 'backend.users';

    protected $route = 'admin.users';

    protected $storeRequestFile = StoreUserRequest::class;

    protected $updateRequestFile = UpdateUserRequest::class;

    public function __construct(UserRepository $repository)
    {
        parent::__construct($repository);
    }
}
