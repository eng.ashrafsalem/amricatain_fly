<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App;
/**
 * Class DashboardController.
 */
class DashboardController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        // change the language to arabic in case we access the control panel
        App::setLocale('ar');
        return view('backend.dashboard');
    }
}
