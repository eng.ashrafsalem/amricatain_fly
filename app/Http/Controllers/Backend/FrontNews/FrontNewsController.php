<?php

namespace App\Http\Controllers\Backend\FrontNews;

use App\Http\Controllers\Backend\CustomController;
use App\Models\FrontNews\Repositories\FrontNewsRepository;
use App\Models\FrontNews\Requests\StoreFrontNewsRequest;
use App\Models\FrontNews\Requests\UpdateFrontNewsRequest;


class FrontNewsController extends CustomController
{
    protected $view = 'backend.frontnews';

    protected $route = 'admin.frontnews';

    protected $storeRequestFile = StoreFrontNewsRequest::class;

    protected $updateRequestFile = UpdateFrontNewsRequest::class;

    public function __construct(FrontNewsRepository $repository)
    {
        parent::__construct($repository);
    }

}


