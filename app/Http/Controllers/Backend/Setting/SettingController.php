<?php

namespace App\Http\Controllers\Backend\Setting;

use App\Http\Controllers\Backend\CustomController;
use App\Models\Settings\Repositories\SettingRepository;
use App\Models\Settings\Requests\StoreSettingRequest;
use App\Models\Settings\Requests\UpdateSettingRequest;


class SettingController extends CustomController
{
    protected $view = 'backend.settings';

    protected $route = 'admin.settings';

    protected $storeRequestFile = StoreSettingRequest::class;

    protected $updateRequestFile = UpdateSettingRequest::class;

    public function __construct(SettingRepository $repository)
    {
        parent::__construct($repository);
    }

}


