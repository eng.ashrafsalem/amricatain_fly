<?php

namespace App\Http\Controllers\Backend\FrontSlider;

use App\Http\Controllers\Backend\CustomController;
use App\Models\FrontSliders\Repositories\FrontSliderRepository;
use App\Models\FrontSliders\Requests\StoreFrontSliderRequest;
use App\Models\FrontSliders\Requests\UpdateFrontSliderRequest;


class FrontSliderController extends CustomController
{
    protected $view = 'backend.frontsliders';

    protected $route = 'admin.frontsliders';

    protected $storeRequestFile = StoreFrontSliderRequest::class;

    protected $updateRequestFile = UpdateFrontSliderRequest::class;

    public function __construct(FrontSliderRepository $repository)
    {
        parent::__construct($repository);
    }

}


