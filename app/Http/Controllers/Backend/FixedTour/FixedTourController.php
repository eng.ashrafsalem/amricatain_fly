<?php

namespace App\Http\Controllers\Backend\FixedTour;

use App\Http\Controllers\Backend\CustomController;
use App\Models\FixedTours\Repositories\FixedTourRepository;
use App\Models\FixedTours\Requests\StoreFixedTourRequest;
use App\Models\FixedTours\Requests\UpdateFixedTourRequest;


class FixedTourController extends CustomController
{
    protected $view = 'backend.fixedtours';

    protected $route = 'admin.fixedtours';

    protected $storeRequestFile = StoreFixedTourRequest::class;

    protected $updateRequestFile = UpdateFixedTourRequest::class;

    public function __construct(FixedTourRepository $repository)
    {
        parent::__construct($repository);
    }

}


