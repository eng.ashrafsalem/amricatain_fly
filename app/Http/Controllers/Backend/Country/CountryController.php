<?php

namespace App\Http\Controllers\Backend\Country;

use App\Http\Controllers\Backend\CustomController;
use App\Models\Countries\Repositories\CountryRepository;
use App\Models\Countries\Requests\StoreCountryRequest;
use App\Models\Countries\Requests\UpdateCountryRequest;


class CountryController extends CustomController
{
    protected $view = 'backend.countries';

    protected $route = 'admin.countries';

    protected $storeRequestFile = StoreCountryRequest::class;

    protected $updateRequestFile = UpdateCountryRequest::class;

    public function __construct(CountryRepository $repository)
    {
        parent::__construct($repository);
    }

}


