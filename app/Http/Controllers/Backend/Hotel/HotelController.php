<?php

namespace App\Http\Controllers\Backend\Hotel;

use App\Http\Controllers\Backend\CustomController;
use App\Models\Hotels\Repositories\HotelRepository;
use App\Models\Hotels\Requests\StoreHotelRequest;
use App\Models\Hotels\Requests\UpdateHotelRequest;


class HotelController extends CustomController
{
    protected $view = 'backend.hotels';

    protected $route = 'admin.hotels';

    protected $storeRequestFile = StoreHotelRequest::class;

    protected $updateRequestFile = UpdateHotelRequest::class;

    public function __construct(HotelRepository $repository)
    {
        parent::__construct($repository);
    }

}


