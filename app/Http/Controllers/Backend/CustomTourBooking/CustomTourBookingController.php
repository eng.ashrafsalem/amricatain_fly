<?php

namespace App\Http\Controllers\Backend\CustomTourBooking;

use App\Http\Controllers\Backend\CustomController;
use App\Models\CustomTourBookings\Repositories\CustomTourBookingRepository;
use App\Models\CustomTourBookings\Requests\StoreCustomTourBookingRequest;
use App\Models\CustomTourBookings\Requests\UpdateCustomTourBookingRequest;


class CustomTourBookingController extends CustomController
{
    protected $view = 'backend.customtourbookings';

    protected $route = 'admin.customtourbookings';

    protected $storeRequestFile = StoreCustomTourBookingRequest::class;

    protected $updateRequestFile = UpdateCustomTourBookingRequest::class;

    public function __construct(CustomTourBookingRepository $repository)
    {
        parent::__construct($repository);
    }

}


