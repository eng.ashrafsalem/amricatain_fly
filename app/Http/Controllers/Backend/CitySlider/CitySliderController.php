<?php

namespace App\Http\Controllers\Backend\CitySlider;

use App\Http\Controllers\Backend\CustomController;
use App\Models\CitySliders\Repositories\CitySliderRepository;
use App\Models\CitySliders\Requests\StoreCitySliderRequest;
use App\Models\CitySliders\Requests\UpdateCitySliderRequest;


class CitySliderController extends CustomController
{
    protected $view = 'backend.citysliders';

    protected $route = 'admin.citysliders';

    protected $storeRequestFile = StoreCitySliderRequest::class;

    protected $updateRequestFile = UpdateCitySliderRequest::class;

    public function __construct(CitySliderRepository $repository)
    {
        parent::__construct($repository);
    }

}


