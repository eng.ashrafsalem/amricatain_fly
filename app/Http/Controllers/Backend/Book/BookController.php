<?php

namespace App\Http\Controllers\Backend\Book;

use App\Http\Controllers\Backend\CustomController;
use App\Models\Books\Repositories\BookRepository;
use App\Models\Books\Requests\StoreBookRequest;
use App\Models\Books\Requests\UpdateBookRequest;


class BookController extends CustomController
{
    protected $view = 'backend.books';

    protected $route = 'admin.books';

    protected $storeRequestFile = StoreBookRequest::class;

    protected $updateRequestFile = UpdateBookRequest::class;

    public function __construct(BookRepository $repository)
    {
        parent::__construct($repository);
    }

}


