<?php

namespace App\Http\Controllers\Backend\HotelBooking;

use App\Http\Controllers\Backend\CustomController;
use App\Models\HotelBookings\Repositories\HotelBookingRepository;
use App\Models\HotelBookings\Requests\StoreHotelBookingRequest;
use App\Models\HotelBookings\Requests\UpdateHotelBookingRequest;


class HotelBookingController extends CustomController
{
    protected $view = 'backend.hotelbookings';

    protected $route = 'admin.hotelbookings';

    protected $storeRequestFile = StoreHotelBookingRequest::class;

    protected $updateRequestFile = UpdateHotelBookingRequest::class;

    public function __construct(HotelBookingRepository $repository)
    {
        parent::__construct($repository);
    }

}


