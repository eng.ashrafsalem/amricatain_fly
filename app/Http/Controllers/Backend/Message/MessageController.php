<?php

namespace App\Http\Controllers\Backend\Message;

use App\Http\Controllers\Backend\CustomController;
use App\Models\Messages\Repositories\MessageRepository;
use App\Models\Messages\Requests\StoreMessageRequest;
use App\Models\Messages\Requests\UpdateMessageRequest;


class MessageController extends CustomController
{
    protected $view = 'backend.messages';

    protected $route = 'admin.messages';

    protected $storeRequestFile = StoreMessageRequest::class;

    protected $updateRequestFile = UpdateMessageRequest::class;

    public function __construct(MessageRepository $repository)
    {
        parent::__construct($repository);
    }

}


