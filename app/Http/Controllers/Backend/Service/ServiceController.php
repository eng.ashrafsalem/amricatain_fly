<?php

namespace App\Http\Controllers\Backend\Service;

use App\Http\Controllers\Backend\CustomController;
use App\Models\Services\Repositories\ServiceRepository;
use App\Models\Services\Requests\StoreServiceRequest;
use App\Models\Services\Requests\UpdateServiceRequest;


class ServiceController extends CustomController
{
    protected $view = 'backend.services';

    protected $route = 'admin.services';

    protected $storeRequestFile = StoreServiceRequest::class;

    protected $updateRequestFile = UpdateServiceRequest::class;

    public function __construct(ServiceRepository $repository)
    {
        parent::__construct($repository);
    }

}


