<?php

use App\Models\Image;

function images($id, $main, $model)
{
    return Image::where([
        'imageable_id' => $id,
        'imageable_type' => $model,
        'main_image' => $main
    ]);
}

// get the main image from images table
function mainImage($id, $model)
{
//    dd(images($id, 1, $model)->first()->image_url);
    return images($id, 1, $model)->first()->image_url??"";
}

function sliderImage($id, $model)
{
    return images($id, 0, $model)->get();
}

// store image in images table
function saveImage($id, $model, $url, $main)
{
    $image = new Image;
    $image->image_url = $url;
    $image->main_image = $main;
    $image->imageable_id = $id;
    $image->imageable_type = $model;

    $image ->save();
}

/**
 *
 * @delete image from storage folder
 **/
function deleteImageFromStorage($image_url)
{
    $path = public_path() . $image_url;
    unlink($path);
}

// delete all slider images
function deleteImageFromTable($id, $main, $model)
{
    foreach(images($id, $main, $model)->get() as $img)
        $img->delete();
}

// delete all related imags
function deleteAllFromBothTableAndStorage($id, $model)
{
    if(sliderImage($id, $model)->count() > 0)
    {
        foreach (sliderImage($id, $model) as $old_img)
        {
            deleteImageFromStorage(str_replace("public/", "", $old_img->image_url));
        }
        deleteImageFromTable($id, 0, $model);
    }

    if(mainImage($id, $model) != "")
    {
        deleteImageFromStorage(str_replace("public/", "", mainImage($id, $model)));
        deleteImageFromTable($id, 1, $model);
    }
}

// delete from main_image from both image table and storage
function deleteMainImageOnly($id, $model)
{
    // check if collection is empty
    if(mainImage($id, $model)!="")
    {
        deleteImageFromStorage(str_replace("public/", "", mainImage($id, $model)));
        deleteImageFromTable($id, 1, $model);
    }
}

// delete the slider_images from both image table and storage
function deleteSliderImagesOnly($id, $model)
{
    if(sliderImage($id, $model)->count() > 0)
    {
        foreach (sliderImage($id, $model) as $old_img)
        {
            deleteImageFromStorage(str_replace("public/", "", $old_img->image_url));
        }
        deleteImageFromTable($id, 0, $model);
    }
}
